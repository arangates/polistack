FROM python:3.6.4 as builder
COPY backend/requirements_dev.txt backend/requirements.txt
RUN pip install -r backend/requirements.txt

# to save image size, let's run on a small image, (tried alpine, wasted time)
# TODO: broken: test_in_container stage will try to build packages that are not wheels,
# TODO: will require gcc which cannot be found in slim
# FROM python:3-slim as production
FROM python:3.6.4 as production
COPY --from=builder /root/.cache /root/.cache
# postgresql-client for entrypoint
# gettext for compilemessages
RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list
RUN apt-get update \
    # https://github.com/dalibo/temboard/issues/211#issuecomment-342205157
    && mkdir -p /usr/share/man/man1 \
    && mkdir -p /usr/share/man/man7 \
    && apt-get install -y --no-install-recommends postgresql-client gettext vim \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean
ENV UWSGI_WSGI_FILE=rarebets/wsgi.py UWSGI_HTTP_SOCKET=:8000 UWSGI_MASTER=1 UWSGI_WORKERS=2 UWSGI_THREADS=8
ENV UWSGI_UID=1000 UWSGI_GID=2000
RUN mkdir -p /var/media /var/static
RUN chown -R 1000:2000 /var/media /var/static
COPY backend/backend.entrypoint backend/backend.entrypoint
RUN chmod +x backend/backend.entrypoint
ENTRYPOINT ["backend/backend.entrypoint"]
RUN mkdir -p /code/backend
WORKDIR /code/backend
COPY ./backend /code/backend
# Convenience for files created in volumes mounted by compose
# We don't care that they will be modifiable on contabo
RUN umask 0001
EXPOSE 8000
RUN pip install -r /code/backend/requirements.txt
CMD /usr/local/bin/uwsgi \
    --http-auto-chunked \
    --http-keepalive \
    --harakiri=120 \
    --static-map /static=/var/static \
    --static-map /media/pictures=/var/media/pictures \
    -b 16384

# kind of risky, right, what if someone forgets to add --target production in gitlab-ci.yml?
# let's find a way to protect ourselves from this
FROM python:3.6.4 as development
ENV DJANGO_SETTINGS_MODULE=rarebets.settings.development
COPY --from=production /code/backend /code/backend
RUN pip install -r /code/backend/requirements_dev.txt
WORKDIR /code/backend
CMD ["python", "./manage.py", "runserver", "0.0.0.0:8000"]


FROM keymetrics/pm2:10-alpine as frontend
COPY ./frontend /code/frontend
RUN cd /code/frontend && yarn install && yarn build
WORKDIR /code/frontend
CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]