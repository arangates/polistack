# Rarebets

If developing locally, not in containers (to come), once, on project start:

    apt install postgres redis-server
    # add sanity checks
    ./init
    # use whenever you want to check before commit, remember it works on git added files only
    ./run_hooks
    # create venv and install backend dependencies pip install -r requirements_dev.txt
    cd backend
    mkvirtualenv -p rb
    # database
    sudo su postgres -c "createuser -s $USER"
    createdb rarebets
    ./plough_and_populate_db.sh
    ./manage.py runserver
    # or
    cd backend
    pyenv virtualenv 3.6.0 rarebets
    pyenv local rarebets
    # frontend bootstrap
    cd frontend
    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    sudo apt-get install gcc g++ make
    sudo apt-get install -y nodejs
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
    sudo apt-get update && sudo apt-get install yarn
    yarn install
    yarn start

    # see source.me for convenience aliases
    source /home/bartek/workspace/sh/rarebets/backend/source.me

## docker

    cd certs
    openssl req -x509 -newkey rsa:4096 -keyout rb.local.key -out rb.local.pem -days 365 -nodes -subj '/CN=my new cert/C=SK'

# conventions:

## commit

    - Short commit message in present imperative, starting with capital letter,
      no longer than 50 chars. Correct spelling.
      yes: Add basic business test suite with Selenium.
      no: added factorie for bet managment
    - Second line empty
    - Third line and more:
      - sometimes optional when change is explained enough in the first line
      - what user can do now that they couldn't before
      - describe "why" and business "why" if applicable,
      - how to use,
      - surprises,
      - documentation used

sample

    RB-47 Add basic business test suite with Selenium.

    Let's have a suite of end to end tests replicating master tester behaviour.
    Some using Selenium that can be configured in headfull mode (see how test work in browser), by:

        py.test tests/e2e --headfull

    Running e2e tests requires chromedriver, instructions added in README.

    Apparently, Firefox driver does not work with Selenium now, so no choice of the driver offered.


# read:

deploy:
https://docs.docker.com/develop/develop-images/multistage-build/
https://docs.gitlab.com/ce/ci/variables/README.html
https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/ci/docker/using_docker_build.md
https://gitlab.com/gitlab-org/gitlab-runner/issues/1544
https://gitlab.com/snippets/185782
https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/
https://learnxinyminutes.com/docs/yaml/
https://tinyurl.com/y96mcp5k
https://www.colinodell.com/blog/201704/optimizing-dockerbased-ci-runners-shared-package-caches

# Notifications

The app aims to be loosely coupled with others and reusable. Therefore it uses
signals. It notifies frontend about changes in registered models via WebSockets.

This app is responsible for monitoring changes in a given models
and sending proper notifications to frontend via websockets.

General message structure:

    {
      "key": "receiver_will_decide_on_strategy_based_on_this",
      "data": [some_data]
    }

There are two directions with same data structure and similar meaning.

backend -> frontend

    {
        "key": "bet_changed",
        "data": [
            {
                "name": "Updated Bet instance.",
                "details": {
                    "id": 1,
                    "yes": 20,
                    "no": 5,
                    "name": "saaad",
                    "description": "Tv remember near. Experience commercial figure spend before here.asddsd",
                    "ends": "2018-03-26",
                    "status": "ACTIVE",
                    "created_by": 15,
                    "image": "/media/example.jpg"
                  },
                "created_at": "2018-04-03T10:34:54.299462Z"
          }
       ]
    }

Frontend, the receiver, decides what to do with data based on key `bet_changed`. You can expect to
have same data structure for a given key. Both teams must agree on the structure and available keys.

Here, a bet was created somehow in backend, frontend has a free way on how to display it.

frontend -> backend

    {
       "key": "see",
       "data":[1, 2, 3, 4]
    }

Frontend lets the backend know that these ids have been received and indeed seen by the user.
On backend side this maps to a method on `NotificationConsumer`.


# Manual interaction with API

Use [Postman](https://www.getpostman.com/) or [httpie](https://httpie.org/), I use httpie here.

    # get token
    http POST http://localhost:8000/auth/jwt/create/ email=reg0@example.com password=reg0pass
    token=eyJ0ewAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJ1c2VybmFtZSI6InJlZzBAZXhhbXBsZS5jb20iLCJleHAiOjE1MjQ0Nzc1NjksImVtYWlsIjoicmVnMEBleGFtcGxlLmNvbSIsIm9yaWdfaWF0IjoxNTI0NDc2NjY5fQ.S96KxibHldIk5hgzRy2Cf53Xnh0iATYanZrd-wEBJ1U
    http http://localhost:8000/auth/me/ Authorization:"JWT $token"
    http http://localhost:8000/bets/1/please_resolve/ Authorization:'JWT $token'

    # staging
    http POST https://api.s.rarebets.com/auth/jwt/create/ email=mateusz.j.k.borowski@gmail.com password=mateusz.j.k.borowski@gmail.com
    token=eyJ0eXAieiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJ1c2VybmFtZSI6InJlZzBAZXhhbXBsZS5jb20iLCJleHAiOjE1MjQ0Nzc1NjksImVtYWlsIjoicmVnMEBleGFtcGxlLmNvbSIsIm9yaWdfaWF0IjoxNTI0NDc2NjY5fQ.S96KxibHldIk5hgzRy2Cf53Xnh0iATYanZrd-wEBJ1U
    http https://api.s.rarebets.com/auth/me/ Authorization:"JWT $token"

# dev debug

    pytest -sk test_bets_with_bet_actions --log-level DEBUG
    # need to figure out how to show logs when test passes

# Business section

    Run elegant on staging:
    # Run the command, change --help to your own params
    ssh -t contabo "docker exec -it rarebets_staging_api_1 ./manage.py elegant_populate --help"
    # copy the files
    scp my_folder_with_media/*jpg contabo:/opt/rarebets/staging/srv/media/pictures/
    # fix their ownership to uwsgi
    ssh contabo 'chown 1000:2000 -R /opt/rarebets/staging/srv/media/pictures/'
    # verify it's there
    ssh contabo 'ls -l /opt/rarebets/staging/srv/media/pictures/'
    # check in admin or frontend

# Database Indexes
```
CREATE INDEX orderbooks_orderbookentry_order_id_index ON orderbooks_orderbookentry (order_id);
CREATE INDEX orderbooks_orderbookentry_order_book_id_index ON orderbooks_orderbookentry (order_book_id);
CREATE INDEX orderbooks_orderbookentry_order_created_at_index ON orderbooks_orderbookentry (created_at);
CREATE INDEX orderbooks_orderbookentry_order_status_index ON orderbooks_orderbookentry (status);

CREATE INDEX orderbooks_orderbook_bet_id_index ON orderbooks_orderbook (bet_id);

CREATE INDEX orderbooks_order_created_at_index ON orderbooks_order (created_at);
CREATE INDEX orderbooks_order_side_index ON orderbooks_order (side);
CREATE INDEX orderbooks_order_bet_id_index ON orderbooks_order (bet_id);
CREATE INDEX orderbooks_order_owner_id_index ON orderbooks_order (owner_id);

CREATE INDEX bets_bet_created_at_index ON bets_bet (created_at);
CREATE INDEX bets_bet_status_index ON bets_bet (status);
```
