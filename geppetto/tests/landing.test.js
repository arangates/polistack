const url = 'http://localhost:3000'
let page

beforeAll(async () => {
  page = await browser.newPage()
  await page.setViewport({width: 1920, height: 1080})
})

afterAll(async () => {
  await page.close()
})

describe('Landing page', () => {

  test('assert that <title> is correct', async () => {
    await page.goto(url)
    const title = await page.title()
    expect(title).toBe(
      'Rarebets'
    )
  })

  test('Login modal should pop up when create button is clicked while not logged in', async () => {

    await page.goto(url)
    await page.waitForSelector('[data-test-id="createBetButton"]')
    await page.click('[data-test-id="createBetButton"]')
    await page.waitForSelector('[data-test-id="modal"]')
    const element = await page.$('[data-test-id="modal"]')

    const modalText = await (await element.getProperty('innerHTML')).jsonValue()
    expect(modalText).toContain('Please log in to create a bet')
  })

})