/* eslint-disable no-undef */
const url = 'http://localhost:3000/login'
let page
import {svenLogsIn} from './helpers'

//przed kazdym testem odpalam browser z nowym pagem + ustawiamy viewport z wybranym rozmiaarem
beforeAll(async() => {
  page = await browser.newPage()
  await page.setViewport({ width: 1920, height: 1080 })
})

//po wszystkich zamknij page
afterAll(async() => {
  await page.close()
})

describe('Login page', () => {
  test('assert that <title> is correct', async () => {
    await page.goto(url)
    const title = await page.title()
    expect(title).toBe('Rarebets')
  })

  test('User cant login with improper data', async() => {
    await page.goto(url)
    await page.waitForSelector('form')

    await page.focus('input[name="email"]')
    await page.type('input[name="email"]', 'test@example.com')

    await page.focus('input[name="password"]')
    await page.type('input[name="password"]', 'testpass')

    await page.click('button[type=submit]')

    const userElement = await page.$('.user__name')

    expect(userElement).toBe(null)
  })

  test('User can login with proper data', async() => {
    await page.goto(url)
    await page.waitForSelector('form')

    await svenLogsIn(page)
    // wait when the new page loads and all the requests stop comming

    const userElement = await page.$('.user__name')
    const userElementText  = await (await userElement.getProperty('innerHTML')).jsonValue()
    
    expect(userElementText).toContain('Sven')
  })
})
