export const svenLogsIn = async page => {
  await page.goto('http://localhost:3000/login')

  const email = await page.$('input[name="email"]')
  const password = await page.$('input[name="password"]')

  await email.focus()
  await email.type('sven@example.com')
  await password.focus()
  await password.type('svenpass')

  const button = await page.$('button[type=submit]')
  await button.click()
  await page.waitForNavigation()
}
