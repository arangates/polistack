const url = 'http://localhost:3000/forgot'
let page

beforeAll(async () => {
  page = await browser.newPage()
})

afterAll(async () => {
  await page.close()
})

describe('Forgot password page', () => {

  test('assert that <title> is correct', async () => {
    await page.goto(url)
    const title = await page.title()
    expect(title).toBe(
      'Rarebets'
    )
  })

})