/* eslint-disable no-undef */
import { svenLogsIn } from './helpers'
import faker from 'faker'

let page

// przed kazdym testem odpalam browser z nowym pagem + ustawiamy viewport z wybranym rozmiaarem
beforeAll(async() => {
  page = await browser.newPage()
  await page.setViewport({ width: 1920, height: 1080 })
})

// po wszystkich zamknij page
afterAll(async() => {
  await page.close()
})

describe('Created bet', () => {
  test('Created bet modal should appear', async() => {
    await svenLogsIn(page)
    const button = await page.$(
      '[data-test-id="create-bet-button-authenticated"]'
    )
    await button.click()
    const modalText = await page.$eval(
      '[data-test-id="modal"]',
      e => e.innerHTML
    )
    expect(modalText).toContain('Create a bet')
  })

  test(
    'Created bet should be in pending right after creation',
    async() => {
      await svenLogsIn(page)

      const newBetButton = await page.$(
        '[data-test-id="create-bet-button-authenticated"]'
      )
      await newBetButton.click()

      await page.waitForSelector('[data-test-id="modal"]')
      const createBetModal = await page.$('[data-test-id="modal"]')

      const statement = await createBetModal.$('textarea[name="statement"]')
      await statement.focus()
      const betStatement =
        'Malta will leave the EU by 2020 ' + faker.lorem.word()
      await statement.type(betStatement)

      const odds = await page.$('[data-test-id="odds"]')

      /// /move the slider so that we have oddsYes = 1
      const oddsYes = await odds.$('.input-range__slider-container')
      const oddsYesCoordinates = await oddsYes.boundingBox()
      // click the oddsFor slider
      await page.mouse.move(oddsYesCoordinates.x, oddsYesCoordinates.y)
      await page.mouse.down()
      await page.mouse.move(oddsYesCoordinates.x + 300, oddsYesCoordinates.y)
      await page.mouse.up()
      /// /move the slider so that we have oddsAgainst = 1
      const oddsNo = await odds.$$('.input-range__slider-container')
      const oddsNoCoordinates = await oddsNo[1].boundingBox()
      await page.mouse.move(oddsNoCoordinates.x, oddsNoCoordinates.y)
      await page.mouse.down()
      await page.mouse.move(oddsNoCoordinates.x - 300, oddsNoCoordinates.y)
      await page.mouse.up()

      // type amount equal to 100
      const amount = await createBetModal.$('input[name="amount"]')
      await amount.focus()
      await amount.type('100')

      // click the Create bet button
      const createBetButton = await createBetModal.$(
        '[data-test-id="create-bet-button"]'
      )
      await createBetButton.click()

      // click Confirm on the bet summary modal
      const confirmCreateBetButton = await page.$(
        '[data-test-id="confirm-create-bet-button"]'
      )
      await confirmCreateBetButton.click()
      // click OK on the thank you modal
      await page.waitForSelector('[data-test-id="ok-button"]', {
        timeout: 3000
      })
      const okButton = await page.$('[data-test-id="ok-button"]')
      await okButton.click()
      // go to my bets
      await page.goto('http://localhost:3000/profile/bets')
      await page.waitForSelector('[data-test-id="pending"]', {
        timeout: 7000
      })
      const pendingBets = await page.$('[data-test-id="pending"]')
      await pendingBets.click()

      // //search for the created bet
      const searchMyBets = await page.$('[data-test-id="search-my-bets"]')
      await searchMyBets.type(betStatement)
      await searchMyBets.press('Enter')

      // check whether the created bet is in the list

      //test nie czeka na zakonczenie requesta do my-bet, wartosc zwracana jest natychmiast bo taka wartosc juz istnieje, test jest wiec uproszczony
      await page.waitForSelector('[data-test-id="my-bet"]', {
        timeout: 5000
      })

      const text = await page.$eval('[data-test-id="my-bet"]', e => e.innerHTML)
      expect(text).toContain(betStatement)
    },
    10000
  )
})
