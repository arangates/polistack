const url = 'http://localhost:3000/register'
let page

beforeAll(async () => {
  page = await browser.newPage()
})

afterAll(async () => {
  await page.close()
})

describe('Register Page', () => {

  test('assert that <title> is correct', async () => {
    await page.goto(url)
    const title = await page.title()
    expect(title).toBe(
      'Rarebets'
    )
  })


})