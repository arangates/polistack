let page
import {svenLogsIn} from './helpers'

beforeAll(async () => {
  page = await browser.newPage()
  await page.setViewport({ width: 1920, height: 1080 })
})

afterAll(async () => {
  await page.close()
})

describe('Edit profile page', () => {

  test('assert that user can edit his profile', async () => {
    
    const waitingTime = 1000
    await svenLogsIn(page)

    //go to the edit profile page
    await page.waitForSelector('.user__logged--name')
    const userElement = await page.$('.user__logged--name')
    await userElement.hover()
    await page.waitFor(waitingTime)
    await page.waitForSelector('[data-test-id="profile"]')
    const profileElement = await page.$('[data-test-id="profile"]')
    await profileElement.click()
    await page.waitFor(waitingTime)

    const fieldChange = 'aaaaaa'

    const firstName = await page.$('[data-test-id="first-name"]')
    await firstName.type(fieldChange)

    const lastName = await page.$('[data-test-id="last-name"]')
    await lastName.type(fieldChange)

    const email = await page.$('[data-test-id="user-email"]')
    await email.type(fieldChange)

    await page.waitFor(waitingTime)

    const saveAndConfrimButton = await page.$('[data-test-id="save-and-confirm"]')
    await saveAndConfrimButton.click() 
    await page.waitFor(waitingTime)

    expect(true).toBe(true)
  },50000)

})