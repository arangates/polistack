#!/usr/bin/env python3
from argparse import ArgumentParser
from collections import namedtuple
import functools
import logging.config
import os
import re
import subprocess
import sys
from typing import Any

# common part (copied verbatim to other hooks, no imports please)
JIRA_PROJECT_SYMBOL = 'RB'
DEBUG = False
LINE = '█' * 40
Result = namedtuple('Result', ['out', 'rc'])

colours = {'RED': 41, 'GREEN': 42, 'YELLOW': 43, 'blue': 94, 'magenta': 95, 'red': 31, 'green': 32, 'yellow': 33}


def set_logging():
    if DEBUG:
        # flake has nice logs, useful for getting setup.cfg right, flake8 doesn't have logs
        log = logging.getLogger('flake8.options.config')
        log.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter(f'\033[{colours["yellow"]}m%(name)s %(message)s\033[0m', datefmt='%H:%M:%S')
        ch.setFormatter(formatter)
        log.addHandler(ch)


def colour(colour_label: str, msg: str):
    sys.stdout.write(f'\033[{colours[colour_label]:d}m{msg}\033[0m\n')


def debug(*msgs: Any, c='yellow'):
    # takes strings or callables to save time if not DEBUG
    if DEBUG:
        colour(c, ' '.join(str(x()) if callable(x) else str(x) for x in msgs))


def git_available():
    return os.path.exists('.git')


@functools.lru_cache()
def run(cmd: str, fine_if_command_not_found=False, do_print=False, **kwargs) -> Result:
    try:
        result = subprocess.run(
            cmd if kwargs.get('shell', False) else cmd.split(),
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, **kwargs
        )
        out, rc = result.stdout.decode('utf-8'), result.returncode
    except Exception as e:
        out, rc = str(e), -100
        if fine_if_command_not_found and 'No such file or directory' in str(e):
            rc = 0
    debug('run', cmd, kwargs, out.strip(), rc, c='blue')
    if do_print:
        sys.stdout.write(out)
    return Result(out, rc)


def generic_debug_info():
    debug('in:', __file__, c='magenta')
    debug('repo exists:', lambda: git_available())
    debug('cwd:', lambda: os.getcwd())
    debug('last commit:', lambda: run('git rev-parse --short=8 HEAD~1').out)
    debug('branch_name:', lambda: repr(run('git rev-parse --abbrev-ref HEAD').out.rstrip()))

# hook specific


valid_commit_message = f'^({JIRA_PROJECT_SYMBOL}-(?P<task_no>\d+)|fixup!|WIP)'


def hook():
    generic_debug_info()
    commit_message = run('git log -1 --pretty=%B').out
    debug('commit_message', repr(commit_message))
    if not re.match(valid_commit_message, commit_message):
        colour(
            'RED',
            f"Start commit name with {JIRA_PROJECT_SYMBOL}-??, please. \n"
            "The commit is done, it's too late to stop you.\n"
            "So `git commit --amend`."
        )
        # we can't stop user, the commit is done, or... maybe we can auto git reset --soft HEAD~1 ;)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        '-a', '--all',
        help='Run on all files rather on git "Changes to be committed", the default.',
        action='store_false'
    )
    parser.add_argument(
        '-s', '--skip',
        help='ignored, kept so that interface of all hooks is the same',
    )
    parser.add_argument(
        '-d', '--debug',
        action='store_true'
    )
    args = parser.parse_args()
    if args.debug:
        DEBUG = True
    set_logging()
    sys.exit(hook())
