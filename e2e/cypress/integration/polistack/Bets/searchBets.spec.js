describe('Search Bets', () => {
  it('Invalid search term must not return result', () => {
    cy.visit('/')
    cy.get('input[name=phrase]').type('blablabla{enter}')

    cy.get('h2').should('not.exist')
  })

  it('Valid search term must return result', () => {
    cy.visit('/')

    const searchTerm = 'ronaldo'

    cy.server()
    cy
      .route('GET', `/bets/?page=1&statement__icontains=${searchTerm}`)
      .as('searchBets')

    cy.get('input[name=phrase]').type(`${searchTerm}{enter}`)

    cy
      .wait('@searchBets')
      .its('status')
      .should('be', '200')
    cy
      .get('h2')
      .contains('Ronaldo')
      .should('exist')
  })
})
