describe('Register test', () => {
  it('Invalid email should raise error', () => {
    cy.visit('/register')
    cy
      .get('input[name=email]')
      .type('blablabla')
      .blur()

    cy.contains('Invalid email').should('exist')
  })

  it('Should register with valid data', () => {
    cy.visit('/register')

    const max = 1000
    const min = 1
    const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min

    cy
      .get('input[name=email]')
      .type(`mateusz.j.k.borowski+user${randomNumber}@gmail.com`)

    cy.get('input[name=firstName]').type('Aranganathan')

    cy.get('input[name=lastName]').type('Rathinavelu')

    cy.get('input[name=password]').type('passwordkf')

    cy.get('input[name=passwordConfirm]').type('passwordkf')

    cy.server()
    cy.route('POST', '/auth/users/create/').as('registerUser')

    cy
      .get('button')
      .contains('Sign up')
      .click()

    cy
      .wait('@registerUser')
      .its('status')
      .should('be', '201')
    cy.get('[data-test-id="modal"]').should('exist')
  })
})
