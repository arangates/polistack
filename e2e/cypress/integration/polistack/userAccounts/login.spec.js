describe('User Login test', () => {
  it('Invalid email should fail', () => {
    cy.visit('/login')
    cy
      .get('input[name=email]')
      .type('blablabla')
      .blur()

    cy.contains('Invalid email').should('exist')
  })

  it('Should not log in with invalid credentials', () => {
    cy.visit('/login')
    cy.get('input[name=email]').type('blablabla@gmail.com')
    cy.get('input[name=password]').type('blablabla')

    cy.server()
    cy.route('POST', '/auth/jwt/create/').as('loginUser')

    cy.get('button[type=submit]').click()

    cy
      .wait('@loginUser')
      .its('status')
      .should('be', '400')
  })

  it('Should log in with valid credentials', () => {
    cy.visit('/login')
    cy.get('input[name=email]').type('wolfram@example.com')
    cy.get('input[name=password]').type('wolframpass')

    cy.server()
    cy.route('POST', '/auth/jwt/create/').as('loginUser')
    cy.route('GET', '/auth/me/').as('me')

    cy.get('button[type=submit]').click()

    cy
      .wait('@loginUser')
      .its('status')
      .should('be', '200')
    cy
      .wait('@me')
      .its('status')
      .should('be', '200')
    cy.get('.user__name').should('contain', 'Wolfram')
  })
})
