const matter = require('gray-matter')
const showdown = require('showdown')

const converter = new showdown.Converter()

module.exports = function loader(source) {
  // Parse front-matter using `matter`
  const parsedSource = matter(source)
  parsedSource.preview = converter.makeHtml(
    parsedSource.content
      .split(' ')
      .slice(0, 30)
      .join(' ')
  )
  // Convert markdown content to HTML
  parsedSource.content = converter.makeHtml(parsedSource.content)
  return `export default ${JSON.stringify(parsedSource)}`
}
