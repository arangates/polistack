module.exports = {
  apps: [
    {
      name: 'app',
      script: './build/server.js',
      instances: 4,
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        PORT: 80
      }
    }
  ]
}
