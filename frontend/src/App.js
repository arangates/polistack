/* eslint-disable react/prop-types */

import React, { Fragment } from 'react'
import { Helmet } from 'react-helmet'
import { Route, Switch, Redirect } from 'react-router-dom'
import { ConnectedRouter } from 'react-router-redux'
import Loadable from 'react-loadable'
import Loader from 'components/Loader'
import routes from 'routes'
import { logoJsonldDark } from 'images'

import './styles/styles.scss'

const ProtectedRoute = Loadable({
  loader: () => import('components/ProtectedRoute'),
  loading: Loader
})

const Modal = Loadable({
  loader: () => import('components/Modal'),
  loading: Loader
})

const Nav = Loadable({
  loader: () => import('components/Nav'),
  loading: Loader
})

const Footer = Loadable({
  loader: () => import('components/Footer'),
  loading: Loader
})

// https://developers.google.com/search/docs/data-types/sitelinks-searchbox
const generateSitelinksSearchboxJSONLD = () => {
  return {
    '@context': 'https://schema.org',
    '@type': 'WebSite',
    url: 'https://polistack.com/',
    potentialAction: [
      {
        '@type': 'SearchAction',
        target: 'https://polistack.com/?q={search_term_string}',
        'query-input': 'required name=search_term_string'
      }
    ]
  }
}

const generateOrganizationJSONLD = () => {
  return {
    '@context': 'https://schema.org',
    '@type': 'Organization',
    url: 'https://polistack.com/',
    logo: logoJsonldDark
  }
}
const App = ({ history }) => {
  return (
    <Fragment>
      <Helmet>
        <link rel="alternate" href="https://polistack.com/" hrefLang="en" />
      </Helmet>
      <Helmet>
        <script type="application/ld+json">
          {JSON.stringify(generateOrganizationJSONLD())}
        </script>
      </Helmet>
      <Helmet>
        <script type="application/ld+json">
          {JSON.stringify(generateSitelinksSearchboxJSONLD())}
        </script>
      </Helmet>
      <ConnectedRouter history={history}>
        <div>
          <Route path="/" component={Nav} />
          <Switch>
            {routes.map(route => {
              const routeProps = {
                key: route.path,
                exact: route.exact === true,
                path: route.path
              }
              if (route.hasOwnProperty('component')) {
                routeProps['component'] = route.component
              } else if (route.hasOwnProperty('render')) {
                routeProps['render'] = route.render
              }
              if (route.protected === true) {
                return <ProtectedRoute {...routeProps} />
              }
              return <Route {...routeProps} />
            })}
            <Redirect
              to={{
                pathname: '/',
                referrer: 404
              }}
            />
          </Switch>
          <Footer path="/" component={Footer} />
          <Modal />
        </div>
      </ConnectedRouter>
    </Fragment>
  )
}

export default App
