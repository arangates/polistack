import {
  FETCH_EVENT_SUCCESS,
  FETCH_EVENT_REQUEST,
  FETCH_EVENT_FAILURE
} from './types'
import EventService from 'services/event'

export const fetchEventDetails = eventId =>
  async function(dispatch) {
    dispatch({ type: FETCH_EVENT_REQUEST })
    try {
      const res = await EventService.getEventDetails(eventId)
      dispatch({ type: FETCH_EVENT_SUCCESS, payload: res })
    } catch (e) {
      dispatch({ type: FETCH_EVENT_FAILURE })
    }
  }
