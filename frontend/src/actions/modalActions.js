import { OPEN_MODAL, CLOSE_MODAL } from './types'
import {
  CHANGE_VOLUME,
  RESOLVE_BET,
  CREATE_BET_SUMMARY,
  PLACE_BET,
  INFO,
  PAYMENT
} from 'constants/modal.types'

export const openChangeVolumeModal = (
  betData,
  currentStatus
) => async dispatch => {
  dispatch({
    type: OPEN_MODAL,
    modal: CHANGE_VOLUME,
    data: { ...betData, currentStatus }
  })
}
export const openChoosePaymentModal = (
  paymentOption,
  amount
) => async dispatch => {
  dispatch({
    type: OPEN_MODAL,
    modal: PAYMENT,
    data: { paymentOption, amount }
  })
}

export const openInfoModal = (
  header,
  text,
  changeLocation = false,
  cancel
) => async dispatch => {
  dispatch({
    type: OPEN_MODAL,
    modal: INFO,
    data: { header, text, changeLocation, cancel }
  })
}

export const openResolveBetModal = betData => async dispatch => {
  dispatch({ type: OPEN_MODAL, modal: RESOLVE_BET, data: betData })
}

export const openCreateBetSummary = betData => async dispatch => {
  dispatch({
    type: OPEN_MODAL,
    modal: CREATE_BET_SUMMARY,
    data: betData
  })
}

export const openPlaceBetModal = (betData, choice) =>
  async function(dispatch, getState) {
    const balance = getState().user.profile.balance

    const betAmount = betData.betAmount > balance ? balance : betData.betAmount

    dispatch({
      type: OPEN_MODAL,
      modal: PLACE_BET,
      data: { ...betData, betAmount },
      choice: choice
    })
  }

export const closeModal = () => async dispatch => {
  dispatch({ type: CLOSE_MODAL })
}
