import AnalyticsService from 'services/analytics'
import {
  SEND_FORM_EVENT,
  SEND_FORM_EVENT_SUCCESS,
  SEND_FORM_EVENT_FAILURE
} from './types'

export const sendEventToGoogleAnalytics = userData => async dispatch => {
  dispatch({ type: SEND_FORM_EVENT })
  try {
    const res = await AnalyticsService.sendSignupFormEvent(userData)
    dispatch({ type: SEND_FORM_EVENT_SUCCESS, payload: res })
  } catch (error) {
    dispatch({ type: SEND_FORM_EVENT_FAILURE, payload: error })
  }
}
