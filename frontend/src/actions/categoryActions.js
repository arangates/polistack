import {
  FETCH_CATEGORIES_REQUEST,
  FETCH_CATEGORIES_SUCCESS
} from 'actions/types'
import categoryService from 'services/categories'

export const fetchCategories = () => async dispatch => {
  dispatch({ type: FETCH_CATEGORIES_REQUEST })
  const res = await categoryService.fetchCategories()
  dispatch({ type: FETCH_CATEGORIES_SUCCESS, payload: res })
}
