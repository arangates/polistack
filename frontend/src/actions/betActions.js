import {
  OPEN_MODAL,
  FETCH_BETS_SUCCESS,
  FETCH_BETS_REQUEST,
  FETCH_BETS_FAILURE
} from './types'
import { INFO } from 'constants/modal.types'
import betService from 'services/bets'

import { fetchUserBets, getProfile } from 'actions/userActions'

export const createBet = betData => async dispatch => {
  try {
    await betService.createBet(betData)
    dispatch({
      type: OPEN_MODAL,
      modal: INFO,
      data: {
        header: 'Thank you!',
        text:
          'Your bet request has been submitted. You can view the bet status in your user bets list.'
      }
    })
  } catch (error) {
    dispatch({
      type: OPEN_MODAL,
      modal: INFO,
      data: {
        header: 'Sorry',
        text:
          'There has been an internal server error. Sorry for that! We are working to fix it.'
      }
    })
  }
}

export const fetchBets = filters => async dispatch => {
  dispatch({ type: FETCH_BETS_REQUEST })
  try {
    const res = await betService.fetchBets(filters)
    dispatch({ type: FETCH_BETS_SUCCESS, payload: res })
  } catch (e) {
    dispatch({ type: FETCH_BETS_FAILURE })
  }
}

export const placeBet = betData =>
  async function(dispatch, getState) {
    try {
      const userBalance = getState().user.profile.balance
      const res = await betService.confirmBet(betData, userBalance, dispatch)
      if (res) {
        dispatch({
          type: OPEN_MODAL,
          modal: INFO,
          data: {
            header: 'Betted successfully',
            text:
              'Your bet has been successfully added. You can check it on your list the in user profile.'
          }
        })
      }

      dispatch(getProfile())
    } catch (error) {
      dispatch({
        type: OPEN_MODAL,
        modal: INFO,
        data: {
          header: 'No Balance',
          text: 'Please top up your account balance',
          changeLocation: {
            link: '/profile/account',
            text: 'Go to Account'
          },
          cancel: true
        }
      })
    }
  }

export const getBackOutAmount = bet => async dispatch => {
  return betService.getBackOutAmount(bet)
}

export const askToResolveBet = bet => async dispatch => {
  try {
    betService.askToResolveBet(bet)
    dispatch({
      type: OPEN_MODAL,
      modal: INFO,
      data: {
        header: 'Thank You',
        text: 'Administrators will be notified to check your bet.'
      }
    })
  } catch (error) {
    dispatch({
      type: OPEN_MODAL,
      modal: INFO,
      data: {
        header: 'Sorry',
        text:
          'There has been an internal server error. Sorry for that! We are working to fix it.'
      }
    })
  }
}

export const updateBetAmount = (updateData, transactionType) =>
  async function(dispatch, getState) {
    const formInstance = getState().form

    if (formInstance.changeVolume && formInstance.changeVolume.values) {
      const newValue = formInstance.changeVolume.values.amount

      try {
        await betService.updateBetAmount(
          { ...updateData, newValue },
          transactionType
        )

        dispatch({
          type: OPEN_MODAL,
          modal: INFO,
          data: {
            header: 'Thank you!',
            text:
              'Your bet request has been submitted. You can view the bet status in your user bets list.'
          }
        })
        dispatch(getProfile())
        dispatch(fetchUserBets(1, updateData.currentStatus))
      } catch (error) {
        dispatch({
          type: OPEN_MODAL,
          modal: INFO,
          data: {
            header: 'Sorry',
            text:
              'There has been an internal server error. Sorry for that! We are working to fix it.'
          }
        })
      }
    }
  }

export const updateBetStatus = (betId, status) => async dispatch => {
  try {
    await betService.updateBetStatus(betId, status)

    if (status === 'ACTIVE') {
      dispatch(fetchUserBets(1, 'PENDING'))
      dispatch({
        type: OPEN_MODAL,
        modal: INFO,
        data: {
          header: 'Thank you!',
          text: 'You have accepted the bet. It is now available for everyone.'
        }
      })
    }

    if (status === 'REJECTED') {
      dispatch(fetchUserBets(1, 'PENDING'))
      dispatch({
        type: OPEN_MODAL,
        modal: INFO,
        data: {
          header: 'Sorry for that!',
          text:
            'You have rejected the rephrased bet. You can always create another one.'
        }
      })
    }
  } catch (error) {
    dispatch({
      type: OPEN_MODAL,
      modal: INFO,
      data: {
        header: 'Sorry',
        text:
          'There has been an internal server error. Sorry for that! We are working to fix it.'
      }
    })
  }
}
