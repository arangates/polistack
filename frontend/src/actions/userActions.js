/* eslint-disable  */
import userService from 'services/user'
import { startSubmit, stopSubmit } from 'redux-form'
import { push } from 'react-router-redux'
import {
  FETCH_USER_BETS_REQUEST,
  FETCH_USER_BETS_SUCCESS,
  FETCH_USER_TRANSACTIONS,
  UPDATE_USER,
  LOGIN_FAILURE,
  OPEN_MODAL,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  LOGOUT,
  LOGIN_SUCCESS,
  FORGOT_PASSWORD
} from './types'

import { INFO } from 'constants/modal.types'

export const loginUser = loginData => async dispatch => {
  dispatch(startSubmit('login'))
  try {
    const user = await userService.login(loginData)
    dispatch({ type: LOGIN_SUCCESS, payload: user })

    dispatch(getProfile())
    dispatch(stopSubmit('login'))
    dispatch(push('/'))
  } catch (error) {
    const res = error.response.data
    dispatch({
      type: OPEN_MODAL,
      modal: INFO,
      data: {
        header: 'Sorry',
        text:
          'Your login or password is inccorect. Please try again or reset your password.'
      }
    })
    dispatch({ type: LOGIN_FAILURE, payload: res })
    dispatch(stopSubmit('login'))
  }
}

export const getProfile = () => async dispatch => {
  const user = await userService.getProfile()
  dispatch({ type: UPDATE_USER, payload: user })
}

export const registerUser = registerData => async dispatch => {
  dispatch(startSubmit('register'))

  try {
    const res = await userService.register(registerData)
    dispatch({ type: REGISTER_SUCCESS, payload: res.data })
    dispatch(stopSubmit('register'))
    dispatch({
      type: OPEN_MODAL,
      modal: 'INFO',
      data: {
        header: 'Thank you!',
        text:
          'We have received your registration request. Check your inbox to complete your registration.'
      }
    })
    return res
  } catch (error) {
    const res = error.response.data
    if (res.email) {
      dispatch({
        type: OPEN_MODAL,
        modal: INFO,
        data: {
          header: 'Sorry',
          text: 'Your account already exists.',
          changeLocation: {
            link: '/login',
            text: 'Go to login page'
          },
          cancel: true
        }
      })
    }
    dispatch({ type: REGISTER_FAILURE, payload: res })
    dispatch(stopSubmit('register'))
  }
}

export const logoutUser = () => async dispatch => {
  dispatch({ type: LOGOUT })
  dispatch(push('/'))
}

export const editProfile = userData => async dispatch => {
  dispatch(startSubmit('editProfile'))

  const reqObject = {
    first_name: userData.profile.firstName,
    last_name: userData.profile.lastName,
    email: userData.profile.email
  }

  const res = await userService.editProfile(reqObject)
  if (res.status === 200) {
      dispatch({
        type: OPEN_MODAL,
        modal: INFO,
        data: {
          header: 'Success',
          text: 'Your profile has been successfully edited.'
        }
      })
    const resObject = {
      first_name: res.data.first_name,
      last_name: res.data.last_name,
      email: res.data.email
    }
    dispatch({
      type: UPDATE_USER,
      payload: resObject
    })
    dispatch(stopSubmit('editProfile'))

  } else {
    dispatch(stopSubmit('editProfile'))
}

}

export const forgotPassword = email => async dispatch => {
  dispatch({ type: FORGOT_PASSWORD })
  try {
    return await userService.forgotPassword(email)
  } catch (error) {
    return error.response.status
  }
}

export const activateUser = activationData => async _ => {
  try {
    return await userService.activateUser(activationData)
  } catch (error) {
    return error.response.status
  }
}

export const fetchUserBets = (page, status, searchData) => async (
  dispatch,
  getState
) => {
  dispatch({ type: FETCH_USER_BETS_REQUEST })
  const res = await userService.fetchBets(page, status, searchData, getState)
  dispatch({ type: FETCH_USER_BETS_SUCCESS, payload: res })
}

export const fetchTransactions = page => async dispatch => {
  const res = await userService.fetchTransactions(page)
  dispatch(getProfile())
  dispatch({ type: FETCH_USER_TRANSACTIONS, payload: res })
}

export const initializePayment = (defaultData, paymentMethod) =>
  async function(dispatch, getState) {
    const formInstance = getState().form

    if (formInstance.paymentForm && formInstance.paymentForm.values) {
      const newPaymentOption =
        formInstance.paymentForm.values.paymentOption || paymentMethod
      const newAmountValue = formInstance.paymentForm.values.amount

      try {
        await userService.makePayment({ newPaymentOption, newAmountValue })

        dispatch({
          type: OPEN_MODAL,
          modal: INFO,
          data: {
            header: 'Thank you!',
            text: 'Your Deposit request has been submitted.'
          }
        })
      } catch (error) {
        dispatch({
          type: OPEN_MODAL,
          modal: INFO,
          data: {
            header: 'Sorry',
            text:
              'There has been an internal server error. Sorry for that! We are working to fix it.'
          }
        })
      }
    }
  }
