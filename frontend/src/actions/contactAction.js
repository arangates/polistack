import { INTIATE_CONTACT_REQUEST, OPEN_MODAL } from 'actions/types'
import ContactService from 'services/contact'
import { reset } from 'redux-form'
import { INFO } from 'constants/modal.types'

export const postContactForm = data => async dispatch => {
  dispatch({ type: INTIATE_CONTACT_REQUEST })
  try {
    let res = await ContactService.postContactForm(data)
    if (res.status === 204) {
      dispatch({
        type: OPEN_MODAL,
        modal: INFO,
        data: {
          header: 'Thank you',
          changeLocation: {
            text: 'Ok',
            link: '/'
          },
          text:
            'Your message has been sent and is under review. We will contact you shortly.'
        }
      })
      dispatch(reset('contact'))
    } else {
      dispatch({
        type: OPEN_MODAL,
        modal: INFO,
        data: {
          header: 'Sorry',
          text:
            'There has been an internal server error. Sorry for that! We are working to fix it.'
        }
      })
    }
  } catch (error) {
    return error.response.status
  }
}
