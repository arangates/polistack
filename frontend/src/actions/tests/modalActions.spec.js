/* eslint-disable no-undef */
import {
  openChangeVolumeModal,
  openResolveBetModal,
  openSuccessfulBetModal,
  closeModal
} from '../modalActions'

import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { OPEN_MODAL, CLOSE_MODAL } from '../types'
import {
  CHANGE_VOLUME,
  RESOLVE_BET,
  SUCCESSFUL_BET
} from '../../constants/modal.types'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Bet Actions', () => {
  it('Should open change volume modal', () => {
    const expectedActions = [{ type: OPEN_MODAL, modal: CHANGE_VOLUME }]

    const store = mockStore({ bet: {} })

    return store.dispatch(openChangeVolumeModal()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('Should open resolve bet modal', () => {
    const expectedActions = [{ type: OPEN_MODAL, modal: RESOLVE_BET }]

    const store = mockStore({ bet: {} })

    return store.dispatch(openResolveBetModal()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('Should open successful bet modal', () => {
    const expectedActions = [{ type: OPEN_MODAL, modal: SUCCESSFUL_BET }]

    const store = mockStore({ bet: {} })

    return store.dispatch(openSuccessfulBetModal()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('Should close any modal', () => {
    const expectedActions = [{ type: CLOSE_MODAL }]

    const store = mockStore({ bet: {} })

    return store.dispatch(closeModal()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
