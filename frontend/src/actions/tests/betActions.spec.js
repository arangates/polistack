/* eslint-disable no-undef */
import { createBet } from '../betActions'
import * as types from '../types'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Bet Actions', () => {
  it('Should not create new bet', () => {
    const expectedActions = [{"modal": "ERROR", "type": "OPEN_MODAL"}]

    const store = mockStore({bet: {}})

    return store.dispatch(createBet()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
