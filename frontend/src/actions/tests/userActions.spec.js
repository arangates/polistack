/* eslint-disable no-undef */
import { loginUser, logoutUser } from '../userActions'
import * as types from '../types'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { LOGIN_SUCCESS } from '../types'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('User Actions', () => {
  // it('Should login user', () => {
  //   const expectedActions = [{ type: types.LOGIN_SUCCESS }]
  //
  //   const store = mockStore({})
  //
  //
  //   return store.dispatch({type: LOGIN_SUCCESS}).then(() => {
  //     expect(store.getActions()).toEqual(expectedActions)
  //   })
  // })

  it('Should logout user', () => {
    const expectedActions = [{ type: types.LOGOUT }, {"payload": {"args": ["/"], "method": "push"}, "type": "@@router/CALL_HISTORY_METHOD"}]

    const store = mockStore({})

    return store.dispatch(logoutUser()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
