const initialState = {
  host: null,
  protocol: null,
  pathname: null
}

const serverReducer = (state = initialState) => state

export default serverReducer
