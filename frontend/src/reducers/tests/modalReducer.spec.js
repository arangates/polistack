import reducer from '../modalReducer'
import * as types from '../../actions/types'
import {
  CHANGE_VOLUME,
  RESOLVE_BET,
  SUCCESSFUL_BET
} from '../../constants/modal.types'

describe('modal reducer', () => {
  it('should return the initial state', () => {
    const initialState = {
      modal: null
    }

    expect(reducer(initialState, {})).toEqual({
      modal: null
    })
  })

  it('should handle change volume OPEN_MODAL', () => {
    const initialState = {
      modal: null
    }

    const action = {
      type: types.OPEN_MODAL,
      modal: CHANGE_VOLUME
    }

    expect(reducer(initialState, action)).toEqual({
      modal: CHANGE_VOLUME
    })
  })

  it('should handle resolve bet OPEN_MODAL', () => {
    const initialState = {
      modal: null
    }

    const action = {
      type: types.OPEN_MODAL,
      modal: RESOLVE_BET
    }

    expect(reducer(initialState, action)).toEqual({
      modal: RESOLVE_BET
    })
  })

  it('should handle successful bet OPEN_MODAL', () => {
    const initialState = {
      modal: null
    }

    const action = {
      type: types.OPEN_MODAL,
      modal: SUCCESSFUL_BET
    }

    expect(reducer(initialState, action)).toEqual({
      modal: SUCCESSFUL_BET
    })
  })
})
