import reducer from '../userReducer'
import * as types from '../../actions/types'

describe('user reducer', () => {
  it('should return the initial state', () => {
    const initialState = {
      authenticated: false,
      profile: {}
    }

    expect(reducer(initialState, {})).toEqual({
      authenticated: false,
      profile: {}
    })
  })

  it('should handle LOGIN_USER', () => {
    const initialState = {
      authenticated: false,
      profile: {}
    }

    const action = {
      type: types.LOGIN_SUCCESS,
      payload: {}
    }

    expect(reducer(initialState, action)).toEqual({
      authenticated: true,
      profile: {}
    })
  })

  it('should handle LOGOUT_USER', () => {
    const initialState = {
      authenticated: true,
      profile: {}
    }

    const action = {
      type: types.LOGOUT
    }

    expect(reducer(initialState, action)).toEqual({
      authenticated: false,
      profile: {}
    })
  })
})
