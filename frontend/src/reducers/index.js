import { combineReducers } from 'redux'
import userReducer from './userReducer'
import betReducer from './betReducer'
import modalReducer from './modalReducer'
import categoryReducer from './categoryReducer'
import siteReducer from './siteReducer'
import eventReducer from './eventReducer'
import { reducer as formReducer } from 'redux-form'
import { routerReducer } from 'react-router-redux'

export default combineReducers({
  user: userReducer,
  bet: betReducer,
  event: eventReducer,
  category: categoryReducer,
  modal: modalReducer,
  site: siteReducer,
  form: formReducer,
  router: routerReducer
})
