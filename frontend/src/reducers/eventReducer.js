import {
  FETCH_EVENT_REQUEST,
  FETCH_EVENT_SUCCESS,
  FETCH_EVENT_FAILURE
} from 'actions/types'

const initialState = {
  data: {},
  isLoading: false
}

const eventReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_EVENT_REQUEST:
      return {
        ...state,
        data: {},
        isLoading: true
      }
    case FETCH_EVENT_SUCCESS:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      }
    case FETCH_EVENT_FAILURE:
      return {
        ...state,
        data: {},
        isLoading: false
      }
    default:
      return state
  }
}

export default eventReducer
