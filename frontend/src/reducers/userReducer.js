import {
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  FETCH_USER_BETS_REQUEST,
  FETCH_USER_BETS_SUCCESS,
  FETCH_USER_TRANSACTIONS,
  UPDATE_USER
} from 'actions/types'

const initialState = {
  authenticated: false,
  profile: {},
  errors: {},
  bets: [],
  transactions: [],
  isLoading: false
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return { ...state, authenticated: true, profile: action.payload }
    case LOGIN_FAILURE:
      return { authenticated: false, profile: {}, errors: action.payload }
    case LOGOUT:
      return { ...state, authenticated: false, profile: {} }
    case UPDATE_USER:
      const profile = state.profile

      return {
        ...state,

        profile: {
          ...profile,
          firstName: action.payload.first_name,
          lastName: action.payload.last_name,
          email: action.payload.email,
          balance: action.payload.balance,
          id: action.payload.id
        }
      }
    case REGISTER_SUCCESS:
      return { ...state }
    case REGISTER_FAILURE:
      return { ...state }
    case FETCH_USER_BETS_REQUEST:
      return { ...state, isLoading: true }
    case FETCH_USER_BETS_SUCCESS:
      return { ...state, bets: action.payload, isLoading: false }
    case FETCH_USER_TRANSACTIONS:
      return { ...state, transactions: action.payload }
    default:
      return state
  }
}

export default userReducer
