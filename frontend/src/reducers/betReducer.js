import {
  FETCH_BETS_SUCCESS,
  FETCH_USER_BETS_REQUEST,
  FETCH_BETS_REQUEST,
  FETCH_BETS_FAILURE
} from 'actions/types'

const initialState = {
  current: {},
  isLoading: false
}

const betReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_BETS_REQUEST:
    case FETCH_BETS_REQUEST:
      return { ...state, isLoading: true }
    case FETCH_BETS_SUCCESS:
      return { ...state, current: action.payload, isLoading: false }
    case FETCH_BETS_FAILURE:
      return { ...state, isLoading: false }
    default:
      return state
  }
}

export default betReducer
