import {
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_REQUEST
} from 'actions/types'

const initialState = {
  categories: [],
  isLoading: false
}

const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORIES_REQUEST:
      return { ...state, isLoading: true }
    case FETCH_CATEGORIES_SUCCESS:
      return { ...state, categories: action.payload, isLoading: false }
    default:
      return state
  }
}

export default categoryReducer
