import React from 'react'
import Loadable from 'react-loadable'
import { Redirect } from 'react-router-dom'
import Loader from 'components/Loader'
import Landing from 'views/Landing'

const EventDetails = Loadable({
  loader: () => import('views/EventDetails'),
  loading: Loader
})

const Faq = Loadable({
  loader: () => import('views/Faq'),
  loading: Loader
})

const HowItWorks = Loadable({
  loader: () => import('views/HowItWorks'),
  loading: Loader
})

const UserProfile = Loadable({
  loader: () => import('views/UserProfile'),
  loading: Loader
})

const Login = Loadable({
  loader: () => import('views/Auth/Login'),
  loading: Loader
})

const Activate = Loadable({
  loader: () => import('views/Auth/Activate'),
  loading: Loader
})

const Contact = Loadable({
  loader: () => import('views/Contact'),
  loading: Loader
})

const Register = Loadable({
  loader: () => import('views/Auth/Register'),
  loading: Loader
})

const Terms = Loadable({
  loader: () => import('views/Terms'),
  loading: Loader
})

const Privacy = Loadable({
  loader: () => import('views/Privacy'),
  loading: Loader
})

const Sitemap = Loadable({
  loader: () => import('views/Sitemap'),
  loading: Loader
})

const ForgotPassword = Loadable({
  loader: () => import('views/Auth/ForgotPassword'),
  loading: Loader
})

const ResetPassword = Loadable({
  loader: () => import('views/Auth/ResetPassword'),
  loading: Loader
})

const News = Loadable({
  loader: () => import('views/Achievements'),
  loading: Loader
})

const routes = [
  {
    path: '/login',
    exact: true,
    component: Login
  },
  {
    path: '/register',
    exact: true,
    component: Register
  },
  {
    path: '/forgot',
    exact: true,
    component: ForgotPassword
  },
  {
    path: '/password/reset/confirm/:uid/:token',
    exact: true,
    component: ResetPassword
  },
  {
    path: '/',
    exact: true,
    component: Landing
  },
  {
    path: '/category',
    exact: true,
    render: () => <Redirect to="/" />
  },
  {
    path: '/event/details/:eventId/:eventName/',
    exact: true,
    component: EventDetails
  },
  {
    path: '/category/:categoryName',
    exact: true,
    component: Landing
  },
  {
    path: '/profile',
    component: UserProfile,
    protected: true
  },
  {
    path: '/faq',
    exact: true,
    component: Faq
  },
  {
    path: '/how-it-works',
    exact: true,
    component: HowItWorks
  },
  {
    path: '/privacy',
    exact: true,
    component: Privacy
  },
  {
    path: '/terms',
    exact: true,
    component: Terms
  },
  {
    path: '/sitemap',
    exact: true,
    component: Sitemap
  },
  {
    path: '/contact',
    exact: true,
    component: Contact
  },
  {
    path: '/news',
    exact: true,
    component: News
  },
  {
    path: '/news/:url',
    exact: true,
    component: News
  },
  {
    path: '/activate/:uid/:token',
    exact: true,
    component: Activate
  }
]

export default routes
