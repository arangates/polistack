export const IS_CLIENT = !!(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
)

export const IS_SERVER = !IS_CLIENT

export const DEFAULT_PAGE_TITLE =
  'Polistack | Stack Your Knowledge On Political Events | Predict Politics'

export const DEFAULT_PAGE_DESCRIPTION =
  'Polistack - check the wide range of political events to stack your knowledge on. The portal of politics predictions.'

export const GOOGLE_ANANLYTICS_ID = 'UA-126213961-4'
