import { injectGlobal, css } from 'styled-components'
import image from '../images/image.jpg'

injectGlobal`

*,
*::before,
*::after {
  box-sizing: border-box;
}

a {
  text-decoration: none;
  color: inherit;
}

h1, h2, h3, h4, h5, h6 {
  margin: 0;
}

h2 {
  font-size: 1.375rem;
}

h6 {
  font-size: .75rem;
}

ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

svg {
  vertical-align: middle;
}

html {
  font-size: 14px;

  @media screen and (min-width: 768px) {
    font-size: 15px;
  }

  @media screen and (min-width: 992px) {
    font-size: 16px;
  }
}

body {
  background: #fafcff;
  color: #102b3b;
  font-family: "Roboto", sans-serif;
  margin: 0;
  overflow-x: hidden;
}

.login__background {
  background-color: #fff;
  background-image: url(${image});
  background-size: 50% 100%;
  background-repeat: no-repeat;
  padding: 0 2em;
  height: 100vh;
}
`

const flex = (justify, align) => css`
  display: flex;
  justify-content: ${justify};
  align-items: ${align};
`

export { flex }
