import logo from '../images/rarebets_logo.svg'
import logoDark from '../images/rarebets_logo-dark.svg'
import loginScreenLogo from '../images/logo_login.svg'
import loginScreenLogoMobile from '../images/rarebets_logo-mobile.svg'
import logoFooter from '../images/rarebets_dark.svg'
import iconSearch from '../images/svg/icon_search.svg'
import iconAllBets from '../images/svg/icon_allbets.svg'
import iconNew from '../images/svg/icon_new.svg'
import iconSport from '../images/svg/icon_sport.svg'
import iconPeople from '../images/svg/icon_people.svg'
import iconCryptoCurrency from '../images/svg/icon_cryptocurrency.svg'
import iconWorldNews from '../images/svg/icon_world-news.svg'
import iconOther from '../images/svg/icon_other.svg'

import image from '../images/image.jpg'
import iconBullet from '../images/icon_bullet.png'
import iconBulletWhite from '../images/icon_bullet-white.png'

import iconNotification from '../images/svg/icon_notification.svg'
import iconEditProfile from '../images/svg/icon_edit-profile.svg'
import iconBets from '../images/svg/icon_bets.svg'
import iconLogout from '../images/svg/icon_logout.svg'

import iconEdit from '../images/svg/icon_edit.svg'
import iconResolve from '../images/svg/icon_resolve.svg'
import iconNormal from '../images/svg/icon_normal.svg'

import iconPagination from '../images/svg/icon_arrows.svg'
import iconDropdown from '../images/svg/arrow_down.svg'

import iconTransaction from '../images/svg/icon_transactions.svg'
import iconHistory from '../images/svg/icon_history.svg'
import iconBan from '../images/svg/icon_ban.svg'

import iconAccept from '../images/svg/icon_accept.svg'
import iconDelete from '../images/svg/icon_delete.svg'
import iconRe from '../images/svg/icon_re.svg'

import backgroundImageFirst from '../images/background_1.jpg'
import backgroundImageSecond from '../images/background_2.jpg'
import backgroundImageThird from '../images/background_3.jpg'
import logoJsonldDark from '../images/logo-dark.png'
import iconBonus from '../images/svg/icon_bonus.svg'

import iconCreateBet from '../images/svg/icon_create-bet.svg'
import iconMyBets from '../images/svg/icon_my-bets.svg'
import iconProfile from '../images/svg/icon_profile.svg'
import iconLogoutMobile from '../images/svg/icon_logout-mobile.svg'
import iconArrowRight from '../images/svg/icon_arrow_right.svg'
import iconArrowUp from '../images/svg/icon_arrow_up.svg'
import iconArrowDown from '../images/svg/icon_arrow_down.svg'
import iconCategories from '../images/svg/icon_categories.svg'
import iconWinner from '../images/svg/icon_winner.svg'
import iconRightNotify from '../images/svg/icon_right-notifications.svg'
import iconSuccess from '../images/svg/icon_success.svg'

import firstNews from '../images/achievements/news_1.jpg'
import secondNews from '../images/achievements/news_2.jpg'
import thirdNews from '../images/achievements/news_3.jpg'
import fourthNews from '../images/achievements/news_4.jpg'

export {
  iconSearch,
  logo,
  logoDark,
  loginScreenLogo,
  loginScreenLogoMobile,
  logoFooter,
  iconAllBets,
  iconNew,
  iconSport,
  iconPeople,
  iconCryptoCurrency,
  iconWorldNews,
  iconOther,
  image,
  iconBullet,
  iconBulletWhite,
  iconNotification,
  iconEditProfile,
  iconBets,
  iconLogout,
  iconEdit,
  iconResolve,
  iconNormal,
  iconPagination,
  iconDropdown,
  iconTransaction,
  iconHistory,
  iconBan,
  iconAccept,
  iconDelete,
  iconRe,
  iconBonus,
  backgroundImageFirst,
  backgroundImageSecond,
  backgroundImageThird,
  iconCreateBet,
  iconMyBets,
  iconProfile,
  iconLogoutMobile,
  iconArrowRight,
  iconCategories,
  iconWinner,
  iconRightNotify,
  iconSuccess,
  iconArrowUp,
  iconArrowDown,
  firstNews,
  secondNews,
  thirdNews,
  fourthNews,
  logoJsonldDark
}
