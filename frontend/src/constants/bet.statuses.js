export const ACTIVE = 'ACTIVE'
export const PENDING = 'PENDING'
export const ENDED = 'ENDED'

export const ORDER_PROCESSED = 'processed'
export const ORDER_PENDING = 'pending'

export const REPHRASED = 'REPHRASED'
