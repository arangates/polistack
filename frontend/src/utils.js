import isString from 'lodash.isstring'
import queryString from 'query-string'

const capitalize = word => {
  const firstLetter = word.charAt(0)
  let capitalizedWord = word
  if (firstLetter) {
    capitalizedWord = `${firstLetter.toUpperCase()}${word.slice(1)}`
  }
  return capitalizedWord
}

export const getSerializedCategoryName = categoryName => {
  if (!isString(categoryName)) {
    return null
  }
  return categoryName
    .trim()
    .toLowerCase()
    .split(' ')
    .join('-')
}

export const getDeserializedCategoryName = categoryName => {
  if (!isString(categoryName)) {
    return null
  }
  return categoryName
    .split('-')
    .map(capitalize)
    .join(' ')
}
export const getEventDetailsHref = event => {
  const slug = `/event/details`
  const eventId = event.id
  const eventName = encodeURIComponent(
    event.statement.replace(/\s+/g, '-').toLowerCase()
  )
  return `${slug}/${eventId}/${eventName}`
}
// TODO memoize calls
export const getSearchParams = (location, match) => {
  const queryParams = queryString.parse(location.search)
  const urlParams = match.params
  const routeParams = {
    order__bet__statement__icontains: queryParams,
    order_satus: urlParams
  }
  return routeParams
}
export const getSeachQueryFromParams = routeParams => {
  const categoryName = routeParams.categoryName
  const queryParamKeys = ['page', 'q', 'order__bet__statement__icontains']
  let nextQueryParams = {}
  queryParamKeys.forEach(key => {
    if (typeof routeParams[key] !== 'undefined' && routeParams[key] !== null) {
      nextQueryParams[key] = routeParams[key]
    }
  })
  if (nextQueryParams.page === 1) {
    delete nextQueryParams.page
  }
  const nextQueryString = queryString.stringify(nextQueryParams)
  return `/${categoryName ? `category/${categoryName}/` : ''}${
    nextQueryString.length > 0 ? `?${nextQueryString}` : ''
  }`
}
