import styled from 'styled-components'

const Bonus = styled.div`
  color: #102b3b;
  font-size: 0.75rem;
  flex: 1 0 100%;
  padding-right: 1em;

  display: flex;
  align-items: center;

  @media screen and (min-width: 576px) {
    flex: 1 0 50%;
  }

  p {
    font-size: 1.5rem;
    font-weight: 500;
    margin-left: 1em;

    > span {
      color: #f25c5e;
    }
  }
`

export default Bonus
