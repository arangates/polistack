import styled from 'styled-components'

const Wrapper = styled.div`
  background-color: #fff;
  border-radius: 4px;
  box-shadow: 0 2px 4px 0 rgba(234, 237, 242, 1);

  padding: 0.5625em 1.875em 1.5em;
  margin-bottom: 1.5em;

  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;

  @media screen and (min-width: 576px) {
    padding: 0.5625em 1.875em;
  }
`

export default Wrapper
