/* eslint react/prop-types: 0 */
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Wrapper from './Wrapper'
import Bonus from './Bonus'
import RegisterButton from './RegisterButton'
import { Svg } from 'components'
import { iconBonus } from 'images'

class BonusRegistration extends Component {
  render() {
    return (
      <Wrapper>
        <Bonus>
          <Svg icon={iconBonus} />
          <p>
            Receive a <span>₹ 1000</span> signing bonus
          </p>
        </Bonus>
        <RegisterButton to="/register">Register and get bonus</RegisterButton>
      </Wrapper>
    )
  }
}

export default withRouter(BonusRegistration)
