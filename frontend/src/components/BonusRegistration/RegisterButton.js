import styled from 'styled-components'
import { Link } from 'react-router-dom'

const RegisterButton = styled(Link)`
  color: #102b3b;
  font-size: 1.125rem;
  font-weight: 500;

  border-radius: 1.194em;
  box-shadow: 0 0.222em 0.667em 0 rgba(0, 0, 0, 0.1);
  cursor: pointer;

  height: 2.389em;
  max-width: 13.5em;
  width: 100%;

  display: flex;
  align-items: center;
  justify-content: center;

  margin: 0 0 0 auto;

  transition: 0.3s ease-in-out;

  &:hover {
    background: #e2e2e2;
  }
`

export default RegisterButton
