/* eslint-disable react/prop-types */
import styled from 'styled-components'
import { flex } from 'constants/global.styles'
import React from 'react'

const StyledBox = styled.div`
  ${flex('center', 'center')};
  position: relative;
  max-width: 7.75em;
  margin: 0 auto;
`

const BetCurrency = styled.div`
  ${flex('center', 'center')};
  width: 1.5em;
  height: 2.5em;
  position: absolute;
  right: 0;
  top: 0;

  > span {
    font-size: 0.875rem;
    font-weight: 700;
  }
`

const AmountBox = ({ children }) => {
  return (
    <StyledBox>
      {children}
      <BetCurrency>
        <span>$</span>
      </BetCurrency>
    </StyledBox>
  )
}

export default AmountBox
