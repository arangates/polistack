/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Pagination from '../index'

describe('Pagination', () => {
  it('should render correctly', () => {
    const output = shallow(<Pagination />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
