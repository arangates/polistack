import styled from 'styled-components'

const Wrapper = styled.ul`
  display: flex;
  justify-content: center;
  margin: 1.875em 0 5em;
`

export default Wrapper
