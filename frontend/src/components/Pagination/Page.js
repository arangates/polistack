import styled from 'styled-components'

const Page = styled.li`
  display: flex;
  justify-content: center;
  align-items: center;
  background: #fff;
  border: 0.167em solid #f1f1f2;
  border-radius: 1.458em;

  cursor: pointer;

  font-size: 0.75rem;
  font-weight: 700;

  padding: 0.75em 0;
  width: 6.667em;
  position: relative;
  transition: 0.3s ease-in-out;

  svg g {
    transition: 0.3s ease-in-out;
  }

  &:hover {
    background: #f25c5e;
    color: #fff;

    svg g {
      stroke: #fff;
    }
  }

  background: ${props => props.active && '#f25c5e'};
  color: ${props => props.active && '#fff'};

  &:not(:last-child) {
    margin-right: 0.625em;

    &::after {
      content: '';
      background: #f1f1f2;
      height: 1px;
      right: -0.795em;
      width: 0.625em;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
    }
  }
`

export default Page
