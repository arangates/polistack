import React, { Component } from 'react'
import { iconPagination } from 'images'
import PropTypes from 'prop-types'
import Wrapper from './Wrapper'
import Page from './Page'
import Svg from 'components/Svg'

class Pagination extends Component {
  iconRightStyle = {
    fill: 'none',
    stroke: '#828F97',
    transition: '.3s ease-in-out'
  }

  iconLeftStyle = {
    transform: 'rotate(180deg)',
    fill: 'none',
    stroke: '#828F97',
    transition: '.3s ease-in-out'
  }

  goToPreviousPage = () => {
    const { currentPage, changeCurrentPage } = this.props

    if (currentPage !== 1) {
      changeCurrentPage(currentPage - 1)
    }
  }

  goToNextPage = () => {
    const { resultCount, perPage, currentPage, changeCurrentPage } = this.props
    const lastPage = Math.ceil(resultCount / perPage)
    if (currentPage !== lastPage) {
      changeCurrentPage(currentPage + 1)
    }
  }

  renderPages = (length, currentPage, changeCurrentPage) => {
    const arr = [...new Array(length)]

    let elemArr = arr.map((item, index) => {
      return (
        <Page
          key={index + 1}
          active={currentPage === index + 1}
          onClick={() => changeCurrentPage(index + 1)}
        >
          {index + 1}
        </Page>
      )
    })
    if (length > 5 && currentPage > 3) {
      if (length - currentPage < 2) {
        elemArr = elemArr.slice(-5, length)
      } else {
        elemArr = elemArr.slice(currentPage - 3, currentPage + 2)
      }
    } else if (length > 5) {
      elemArr = elemArr.slice(0, 5)
    }

    return elemArr
  }

  render() {
    const { resultCount, perPage, currentPage, changeCurrentPage } = this.props
    const lastPage = Math.ceil(resultCount / perPage)
    // The default page size (configured in the backend) is 20
    return resultCount <= 20 || !lastPage ? (
      <Wrapper />
    ) : (
      <Wrapper>
        <Page onClick={this.goToPreviousPage}>
          <Svg icon={iconPagination} style={this.iconLeftStyle} />
        </Page>

        {this.renderPages(lastPage, currentPage, changeCurrentPage)}

        <Page onClick={this.goToNextPage}>
          <Svg icon={iconPagination} style={this.iconRightStyle} />
        </Page>
      </Wrapper>
    )
  }
}

Pagination.propTypes = {
  resultCount: PropTypes.number,
  currentPage: PropTypes.number,
  changeCurrentPage: PropTypes.func,
  perPage: PropTypes.number
}

export default Pagination
