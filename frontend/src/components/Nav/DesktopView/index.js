/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Authenticated from './Authenticated'
import NotAuthenticated from './NotAuthenticated'
import Searchbar from './Searchbar'

class DesktopView extends Component {
  render() {
    const { isAuthenticated, openInfoModal, logoutUser, menuOpen } = this.props
    return (
      <React.Fragment>
        <Searchbar onSubmit={this.props.searchBets} />
        {isAuthenticated ? (
          <Authenticated user={this.props.user} logoutUser={logoutUser} />
        ) : (
          <NotAuthenticated menuOpen={menuOpen} openInfoModal={openInfoModal} />
        )}
      </React.Fragment>
    )
  }
}

export default DesktopView
