/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import NotAuthenticated from '../index'

describe('NotAuthenticated', () => {
  it('should render correctly', () => {
    const output = shallow(<NotAuthenticated />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
