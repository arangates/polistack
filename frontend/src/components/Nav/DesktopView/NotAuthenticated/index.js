/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'components/index'

class NotAuthenticated extends Component {
  render() {
    const { menuOpened, openInfoModal } = this.props

    return (
      <div className={'nav__items' + (menuOpened ? ' open' : '')}>
        <div
          className="nav__item"
          onClick={() =>
            openInfoModal('Login needed', 'Please log in to create a bet', {
              link: '/login',
              text: 'Go to login page'
            })
          }
        />
        <Link to="/how-it-works" className="nav__item">
          <span>How it works</span>
        </Link>
        <Link to="/news" className="nav__item">
          <span>News</span>
        </Link>
        <Link to="/register" className="nav__item">
          <Button blue>Create account</Button>
        </Link>
        <Link to="/login" className="nav__item">
          <Button white>Log in</Button>
        </Link>
      </div>
    )
  }
}

export default NotAuthenticated
