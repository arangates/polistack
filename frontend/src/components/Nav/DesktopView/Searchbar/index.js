/* eslint-disable no-class-assign,react/prop-types */
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Svg from 'components/Svg/index'
import { iconSearch } from 'images'
import Search from './Search'
import SearchLabel from './SearchLabel'
import { Field, reduxForm } from 'redux-form'
import SearchForm from './SearchForm'
import { Input } from 'components/index'

class Searchbar extends Component {
  submit = e => {
    this.props.handleSubmit(e)
    ReactDOM.findDOMNode(this.searchInput).blur()
  }

  render() {
    return (
      <Search>
        <SearchForm onSubmit={this.submit}>
          <Field
            ref={input => {
              this.searchInput = input
            }}
            name="phrase"
            component={Input}
            type="search"
            className="white"
            placeholder="Search a bet..."
          />
          <SearchLabel onClick={this.submit}>
            <Svg icon={iconSearch} />
          </SearchLabel>
        </SearchForm>
      </Search>
    )
  }
}

Searchbar = reduxForm({
  form: 'search'
})(Searchbar)

export default Searchbar
