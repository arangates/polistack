import styled from 'styled-components'

const SearchLabel = styled.label`
  position: absolute;
  right: 0;
  height: 100%;
  padding: 0.5em 0.875em;
  cursor: pointer;
`

export default SearchLabel
