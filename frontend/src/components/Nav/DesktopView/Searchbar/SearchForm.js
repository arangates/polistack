import styled from 'styled-components'

const SearchForm = styled.form`
  max-width: 28.563em;
  width: 100%;
`

export default SearchForm
