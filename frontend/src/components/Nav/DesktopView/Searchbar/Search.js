import styled from 'styled-components'

const Search = styled.div`
  max-width: 18.75em;
  flex: 1 0 auto;
  margin: 0;
  position: relative;
`

export default Search
