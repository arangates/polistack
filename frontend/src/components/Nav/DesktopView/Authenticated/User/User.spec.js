/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import User from './index'

describe('User', () => {
  it('should render correctly', () => {
    const output = shallow(<User />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
