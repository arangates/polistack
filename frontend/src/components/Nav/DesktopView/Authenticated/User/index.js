/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Svg from 'components/Svg'
import { Link } from 'react-router-dom'

import { iconEditProfile, iconBets, iconLogout } from 'images'

import './user.scss'

class User extends Component {
  state = {
    userPanelOpened: false
  }

  openPanel = () => this.setState({ userPanelOpened: true })
  closePanel = () => this.setState({ userPanelOpened: false })

  render() {
    return (
      <div
        className={
          'nav__item user__logged' +
          (this.state.userPanelOpened ? ' user__logged--open' : '')
        }
        onMouseEnter={this.openPanel}
        onMouseLeave={this.closePanel}
      >
        <div className="user__logged--name">
          <div className="user__hello">
            <div className="user__greeting">
              Hello, <span className="user__name">{this.props.firstName}</span>
            </div>
          </div>
          <div className="user__panel--burger">
            <div className="burger__bar" />
          </div>
        </div>
        <ul className="user__logged--dropdown">
          <li data-test-id="my-bets">
            <Link to="/profile/trades">
              <div className="dropdown__image">
                <Svg icon={iconBets} />
              </div>
              My Trades
            </Link>
          </li>
          <li data-test-id="profile">
            <Link to="/profile/edit">
              <div className="dropdown__image">
                <Svg icon={iconEditProfile} />
              </div>
              Profile
            </Link>
          </li>
          <li onClick={this.props.logoutUser}>
            <Link to="#">
              <div className="dropdown__image">
                <Svg icon={iconLogout} />
              </div>
              Logout
            </Link>
          </li>
        </ul>
      </div>
    )
  }
}

export default User
