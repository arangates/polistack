/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import User from './User'

class Authenticated extends Component {
  render() {
    const { user: { profile }, logoutUser } = this.props
    return (
      <div className="nav__items user__logged">
        <Link to="/how-it-works" className="nav__item">
          <span>How it works</span>
        </Link>
        <Link to="/news" className="nav__item">
          <span>News</span>
        </Link>
        <User
          data-test-id="hello-user"
          logoutUser={logoutUser}
          firstName={profile.firstName}
        />
      </div>
    )
  }
}

export default Authenticated
