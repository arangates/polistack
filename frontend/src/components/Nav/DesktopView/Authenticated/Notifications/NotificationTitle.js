import styled from 'styled-components'

const NotificationTitle = styled.div`
  color: #828f97;
  font-size: 0.875rem;
  font-weight: 600;
  padding-left: 1em;
`

export default NotificationTitle
