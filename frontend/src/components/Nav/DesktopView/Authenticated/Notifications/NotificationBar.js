import styled from 'styled-components'

const NotificationBar = styled.div`
  display: flex;
  align-items: center;
`

export default NotificationBar
