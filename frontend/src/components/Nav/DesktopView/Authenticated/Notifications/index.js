/* eslint-disable */
import React, { Component } from 'react'
import { Svg } from 'components'
import { iconNotification } from 'images'
import Notification from './Notification'
import MediaQuery from 'react-responsive'
import NotificationValue from '../../../MobileView/NotificationValue'
import NotificationBar from './NotificationBar'
import NotificationTitle from './NotificationTitle'
import onClickOutside from 'react-onclickoutside'
import NotificationService from 'services/notifications'

class Notifications extends Component {
  state = {
    dropdownOpened: false,
    notifications: []
  }

  handleClickOutside = e => {
    this.setState({dropdownOpened: false})
  }

  toggleDropdown = async() => {

    const {notifications} = this.state

    if (notifications.length !== 0) {
      this.setState(prev => {
        return {dropdownOpened: !prev.dropdownOpened}
      })
    }

    await this.notificationService.markAsSeen(notifications)

    const seenAll = notifications.map(item => {
      return {...item, seen: new Date()}
    })

    this.setState({
      notifications: seenAll
    })

  }

  amountOfUnseenNotifications = notifications => {
    return notifications.reduce((prev, curr) => {
      return !curr.seen ? prev + 1 : prev
    }, 0)
  }

  renderNotifications = notifications =>
    notifications.map((item, index) => (
      <Notification
        key={index}
        item={item}
      />
    ))

  componentDidMount() {
    const {token} = this.props

    this.notificationService = new NotificationService(token)

    this.notificationService.onPendingEvent = event => {
      this.notificationService.sortByDate(event.data)
      this.setState({
        notifications: event.data
      })
    }

    this.notificationService.onBetEvent = event => {
      this.setState(prev => {
        return {notifications: [...event.data, ...prev.notifications]}
      })
    }
  }

  render() {
    const {notifications} = this.state

    const style = {
      maxHeight: '500px',
      overflowY: notifications.length > 5 ? 'scroll' : ''
    }

    return (
      <div className="nav__item nav__notifications">
        <div className="notifications" onClick={this.toggleDropdown}>
          <NotificationBar>
            <Svg icon={iconNotification}/>
            <MediaQuery query="(max-width: 767px)">
              <NotificationTitle>Notifications</NotificationTitle>
            </MediaQuery>
            {this.amountOfUnseenNotifications(this.state.notifications) !== 0 && (
              <NotificationValue>
                <span>{this.amountOfUnseenNotifications(this.state.notifications)}</span>
              </NotificationValue>
            )}
          </NotificationBar>
          <div
            className={`notification__dropdown ${
              this.state.dropdownOpened ? 'open' : ''
              }`}
          >
            <ul style={style}>
              {this.renderNotifications(this.state.notifications)}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default onClickOutside(Notifications)
