import styled from 'styled-components'

const Item = styled.li`
  &:not(:last-child) {
    border-bottom: 0.063em solid #eaedf2;
    padding-bottom: 0.625em;
    margin-bottom: 0.625em;
  }
`

export default Item
