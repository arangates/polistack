/* eslint-disable react/prop-types */
import React from 'react'
import moment from 'moment'

import Item from './Item'
import Title from './Title'
import Status from './Status'
import Date from './Date'
import './notifications.scss'

const Notification = ({ item }) => {
  const renderDate = createdAt => {
    const date = moment(createdAt)
    const now = moment()
    return date.from(now)
  }
  const { content_object: bet, name, created_at: createdAt } = item

  return (
    <Item>
      <Title>{bet.statement}</Title>
      <Status status={name}>
        <div>{name}</div>
      </Status>
      <Date>{renderDate(createdAt)}</Date>
    </Item>
  )
}

export default Notification
