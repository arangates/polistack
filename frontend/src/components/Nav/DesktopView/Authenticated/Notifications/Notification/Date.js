import styled from 'styled-components'

const Date = styled.div`
  color: #808f97;
  font-size: 0.75rem;
  font-weight: 400;
`

export default Date
