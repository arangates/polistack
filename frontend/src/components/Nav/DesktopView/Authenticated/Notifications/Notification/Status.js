import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const Status = styled.div`
  ${flex('space-between', 'center')};
  font-size: 0.75rem;
  font-weight: 900;
  padding: 0.8334em 0;
  color: ${props => props.status === 'won' && '#44aef3'};
  color: ${props => props.status === 'accepted' && '#102b3b'};
  color: ${props => props.status === 'lost' && '#f25c5e'};
`

export default Status
