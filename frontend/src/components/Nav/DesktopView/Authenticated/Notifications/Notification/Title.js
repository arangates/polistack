import styled from 'styled-components'

const Title = styled.div`
  color: #808f97;
  font-size: 0.75rem;
  font-weight: 500;
`

export default Title
