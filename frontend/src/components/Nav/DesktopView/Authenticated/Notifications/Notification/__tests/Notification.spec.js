/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Notification from '../index'

describe('Notification', () => {
  it('should render correctly', () => {
    const output = shallow(<Notification />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
