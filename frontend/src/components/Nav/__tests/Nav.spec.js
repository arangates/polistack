/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Nav from '../index'
import configureStore from 'redux-mock-store'

const mockStore = configureStore()

const store = mockStore({})

describe('Nav', () => {
  it('should render correctly', () => {
    const output = shallow(<Nav store={store} />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
