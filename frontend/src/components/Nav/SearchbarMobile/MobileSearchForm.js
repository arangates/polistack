import styled from 'styled-components'

const MobileSearchForm = styled.form`
  padding: 0 1em;
  width: 100%;
  z-index: 9;
  display: ${props => (props.open ? 'block' : 'none')};
  transition: 0.3s ease-in-out;
`

export default MobileSearchForm
