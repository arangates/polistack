/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import { Input } from 'components'
import { Field, reduxForm } from 'redux-form'
import MobileSearchForm from './MobileSearchForm'
import ReactDOM from 'react-dom'

class SearchbarMobile extends Component {
  submit = e => {
    this.props.handleSubmit(e)
    ReactDOM.findDOMNode(this.searchInput).blur()
  }

  render() {
    return (
      <MobileSearchForm onSubmit={this.submit} open={this.props.searchOpen}>
        <Field
          type="search"
          ref={input => {
            this.searchInput = input
          }}
          name="phrase"
          component={Input}
          className="white"
          placeholder="Search a bet..."
        />
      </MobileSearchForm>
    )
  }
}

SearchbarMobile = reduxForm({
  form: 'search'
})(SearchbarMobile)

export default SearchbarMobile
