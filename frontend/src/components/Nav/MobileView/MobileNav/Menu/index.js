/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Items from './Items'

class Menu extends Component {
  logout = () => {
    this.props.logoutUser()
    this.props.toggleMenu()
  }

  render() {
    const { toggleMenu } = this.props
    return (
      <React.Fragment>
        <Items>
          <li>
            <Link to="/profile/trades" onClick={toggleMenu}>
              <div className="mobile__item--title">My Trades</div>
            </Link>
          </li>
          <li>
            <Link to="/profile/edit" onClick={toggleMenu}>
              <div className="mobile__item--title">My profile</div>
            </Link>
          </li>
          <li>
            <Link to="/how-it-works" onClick={toggleMenu}>
              <div className="mobile__item--title">How it works</div>
            </Link>
          </li>
          <li>
            <Link to="/news" onClick={toggleMenu}>
              <div className="mobile__item--title">News</div>
            </Link>
          </li>
          <li>
            <Link to="#" onClick={this.logout}>
              <div className="mobile__item--title">Logout</div>
            </Link>
          </li>
        </Items>
      </React.Fragment>
    )
  }
}

export default Menu
