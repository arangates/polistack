import styled from 'styled-components'

const Items = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: center;
  > li {
    color: #102b3b;
    font-size: 1rem;
    font-weight: 500;
    margin-bottom: 20px;
  }
  &:not(:first-child) {
    margin-top: 1.25em;
  }
`

export default Items
