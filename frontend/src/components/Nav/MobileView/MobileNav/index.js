/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'components'
import SideNavigation from './SideNavigation'
import NotLogged from './NotLogged'
import Menu from './Menu'

class MobileNav extends Component {
  render() {
    const {
      userLogged,
      menuOpen,
      toggleMenu,
      openInfoModal,
      logoutUser
    } = this.props
    return (
      <SideNavigation open={menuOpen}>
        {userLogged ? (
          <React.Fragment>
            <Menu logoutUser={logoutUser} toggleMenu={toggleMenu} />
          </React.Fragment>
        ) : (
          <NotLogged onClick={toggleMenu}>
            <div
              style={{ marginBottom: '1.25em' }}
              onClick={() =>
                openInfoModal('Login needed', 'Please log in to create a bet', {
                  link: '/login',
                  text: 'Go to login page'
                })
              }
            />
            <Link to="/how-it-works">
              <span>How it works</span>
            </Link>
            <Link to="/news">
              <span>News</span>
            </Link>
            <Link to="/register" className="nav__item">
              <Button blue>Create account</Button>
            </Link>
            <Link to="/login" className="nav__item">
              <Button white>Log in</Button>
            </Link>
          </NotLogged>
        )}
      </SideNavigation>
    )
  }
}

export default MobileNav
