import styled from 'styled-components'

const NotLogged = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  span {
    color: #102b3b;
    font-family: 'Roboto', sans-serif;
    font-size: 0.85rem;
    font-weight: 600;
  }
  > a {
    height: 40px;
  }
  > .nav__item {
    margin-bottom: 10px;
  }
  &:not(:first-child) {
    margin-top: 1.25em;
  }
`

export default NotLogged
