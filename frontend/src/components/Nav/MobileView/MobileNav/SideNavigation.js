import styled from 'styled-components'

const SideNavigation = styled.div`
  background: #fff;
  position: fixed;
  right: -17.5em;
  top: calc(3.75em + 1px);
  width: 100%;
  padding: 1em 1.25em;
  height: calc(100% - 3.75em - 1px);
  z-index: 2;
  opacity: 0;
  visibility: hidden;
  transition: 0.3s ease-in-out;

  right: ${props => props.open && '0'};
  opacity: ${props => props.open && '1'};
  visibility: ${props => props.open && 'visible'};
`

export default SideNavigation
