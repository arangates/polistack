/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Burger from './Burger'
import NotificationValue from './NotificationValue'
import { iconSearch } from 'images'
import { Svg } from 'components'
import SearchbarMobile from '../SearchbarMobile'
import Logo from '../Logo'
import NotificationService from 'services/notifications'

import MobileNav from './MobileNav'

class MobileView extends Component {
  state = {
    notifications: []
  }

  componentDidMount() {
    const { token } = this.props.user.profile

    this.notificationService = new NotificationService(token)

    this.notificationService.onPendingEvent = event => {
      this.notificationService.sortByDate(event.data)
      this.setState({
        notifications: event.data
      })
    }

    this.notificationService.onBetEvent = event => {
      this.setState(prev => {
        return { notifications: [...event.data, ...prev.notifications] }
      })
    }
  }

  amountOfUnseenNotifications = notifications => {
    return notifications.reduce((prev, curr) => {
      return !curr.seen ? prev + 1 : prev
    }, 0)
  }

  render() {
    const {
      isAuthenticated,
      menuOpen,
      user,
      logoutUser,
      toggleSearch,
      toggleMenu,
      searchOpen,
      searchBets,
      openInfoModal
    } = this.props

    return (
      <React.Fragment>
        <div className="search__nav">
          <div className="search__nav--button" onClick={toggleSearch}>
            <Svg icon={iconSearch} />
          </div>
          <SearchbarMobile onSubmit={searchBets} searchOpen={searchOpen} />
        </div>
        <Logo search={searchOpen} />
        <Burger onClick={toggleMenu} open={menuOpen}>
          {isAuthenticated &&
            !menuOpen &&
            this.amountOfUnseenNotifications(this.state.notifications) !==
              0 && (
              <NotificationValue>
                <span>
                  {this.amountOfUnseenNotifications(this.state.notifications)}
                </span>
              </NotificationValue>
            )}
        </Burger>
        <MobileNav
          notifications={this.state.notifications}
          amountOfUnseenNotifications={this.amountOfUnseenNotifications}
          openInfoModal={openInfoModal}
          userLogged={isAuthenticated}
          toggleMenu={toggleMenu}
          user={user}
          logoutUser={logoutUser}
          menuOpen={menuOpen}
        />
      </React.Fragment>
    )
  }
}

export default MobileView
