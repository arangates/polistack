import styled from 'styled-components'

const NotificationValue = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: -0.25em;
  right: -0.25em;
  height: 0.938em;
  width: 0.938em;
  background-color: #f25c5e;
  border-radius: 0.938em;

  > span {
    color: #fff;
    font-size: 0.625rem;
    font-weight: 900;
  }
`

export default NotificationValue
