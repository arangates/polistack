/* eslint-disable prettier/prettier,react/prop-types */
import styled from 'styled-components'
import React from 'react'

const Toggle = styled.div`
  cursor: pointer;
  height: 1.428571429em;
  position: relative;
`

const Bar = styled.div`
  background: rgb(16, 43, 59);
  height: 2px;
  width: 2em;
  position: relative;
  transition: 0.3s ease-in-out;

  &:first-child {
    transform: ${props => props.open ? 'rotate(-45deg) translate(-.5em, .5em)' : ''};
    right: ${props => (props.open ? '0' : '')};
  }

  &:nth-child(2) {
    right: ${props => (props.open ? 'auto' : '0')};
    left: ${props => (props.open ? '0' : '')};
    width: ${props => (props.open ? '0' : '')};
    top: 0.5em;
  }

  &:nth-child(3) {
    top: 1em;
    left: ${props => (props.open ? '0' : '')};
    transform: ${props => props.open ? 'rotate(45deg) translate(-0.375em, -0.375em)' : ''};
  }
`

const Burger = ({open, onClick, children}) => {
  return (<Toggle onClick={onClick}>
    <Bar open={open} />
    <Bar open={open} />
    <Bar open={open} />
    {children}
  </Toggle>)
}

export default Burger
