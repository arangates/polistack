/* eslint-disable prettier/prettier */
import styled from 'styled-components'

const NavItems = styled.div`
  display: flex;
  align-items: center;

  @media screen and (max-width: 991px) {
    position: fixed;
    flex-direction: column;
    min-width: 20em;
    max-width: 20em;
    top: 3.75em;
    right: -21.125em;
    height: calc(100% - 3.75em);
    background: #fff;
    transition: 0.3s ease-in-out;
    z-index: 9;

    right: ${props => props.open && '0'};
    box-shadow: ${props =>
    props.open && ' -1px 0.75em 1.25em 0 rgba(165, 183, 195, 0.65)'};
  }
`

export default NavItems
