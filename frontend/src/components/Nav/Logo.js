import styled from 'styled-components'
import React from 'react'
import { Link } from 'react-router-dom'
import { Svg } from 'components'
import { logoDark } from 'images'

const StyledLogo = styled.div`
  @media screen and (max-width: 767px) {
    height: 100%;
    display: ${props => (props.search ? 'none' : 'flex')};
    align-items: center;
  }
`

const Logo = props => {
  return (
    <StyledLogo {...props}>
      <Link to="/">
        <Svg icon={logoDark} />
      </Link>
    </StyledLogo>
  )
}

export default Logo
