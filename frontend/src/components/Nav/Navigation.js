import styled from 'styled-components'

const Navigation = styled.div`
  @media screen and (max-width: 991px) {
    &::before {
      content: '';
      position: absolute;
      background: #fff;
      width: 100%;
      height: 100%;
      right: -100%;
    }

    justify-content: flex-end;

    .search__nav {
      margin-right: 1em;
      cursor: pointer;
    }
  }

  background: #fff;
  padding-left: 1.875em;
  flex: 1;
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
`

export default Navigation
