import styled from 'styled-components'

const NavItem = styled.div`
  &:not(:first-child) {
    margin-left: 1.25em;
  }

  @media screen and (max-width: 991px) {
    margin: 0.5em 0;

    &:not(:first-child) {
      margin-left: 0;
    }
  }
`

export default NavItem
