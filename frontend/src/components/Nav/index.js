/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import queryString from 'query-string'
import { withRouter } from 'react-router-dom'
import MediaQuery from 'react-responsive'
import Logo from './Logo'
import { connect } from 'react-redux'
import { logoutUser } from 'actions/userActions'
import { openInfoModal } from 'actions/modalActions'

import MobileView from './MobileView'
import DesktopView from './DesktopView'

import './nav.scss'

class Nav extends Component {
  isInAuthenticationRoute = () => {
    const { location: { pathname } } = this.props
    return (
      pathname === '/login' ||
      pathname === '/register' ||
      pathname.startsWith('/password') ||
      pathname === '/forgot'
    )
  }

  state = {
    menuOpen: false,
    searchOpen: false
  }

  toggleMenu = () =>
    this.setState(prev => {
      return { menuOpen: !prev.menuOpen }
    })

  toggleSearch = () =>
    this.setState(prev => {
      return { searchOpen: !prev.searchOpen }
    })

  getCategory = queryParams => {
    const category = queryParams.category || null
    return category
  }

  searchBets = searchPhrase => {
    const { location, history } = this.props
    const queryParams = queryString.parse(location.search)
    const category = this.getCategory(queryParams)
    // Discard the page number
    const nextQueryParams = {}
    if (searchPhrase) {
      nextQueryParams.q = searchPhrase.phrase
    }
    if (category) {
      nextQueryParams.category = category
    }
    const nextQueryString = queryString.stringify(nextQueryParams)
    history.push(`/${nextQueryString.length > 0 ? `?${nextQueryString}` : ''}`)
  }

  render() {
    const isAuthenticated = this.props.user.authenticated
    const { user, logoutUser, openInfoModal } = this.props

    return this.isInAuthenticationRoute() ? null : (
      <header>
        <div className="container heading">
          <MediaQuery query="(min-width: 768px)">
            <Logo />
          </MediaQuery>
          <div className="navigation">
            <MediaQuery query="(max-width: 767px)">
              <MobileView
                searchOpen={this.state.searchOpen}
                isAuthenticated={isAuthenticated}
                menuOpen={this.state.menuOpen}
                user={user}
                openInfoModal={openInfoModal}
                logoutUser={logoutUser}
                toggleSearch={this.toggleSearch}
                toggleMenu={this.toggleMenu}
              />
            </MediaQuery>

            <MediaQuery query="(min-width: 768px)">
              <DesktopView
                isAuthenticated={isAuthenticated}
                user={user}
                menuOpen={this.state.menuOpen}
                logoutUser={logoutUser}
                openInfoModal={openInfoModal}
                searchBets={this.searchBets}
              />
            </MediaQuery>
          </div>
        </div>
      </header>
    )
  }
}

function mapStateToProps({ user }) {
  return { user }
}

export default connect(mapStateToProps, {
  logoutUser,
  openInfoModal
})(withRouter(Nav))
