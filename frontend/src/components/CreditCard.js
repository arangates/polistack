import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const CreditCard = styled.div`
  background: ${props =>
    props.isEditable
      ? ''
      : 'linear-gradient(-240deg, #1d4862 0%, #102b3b 100%)'};
  border-radius: 0.375em;
  border: ${props => (props.isEditable ? '.063em solid #eaedf2' : '')};
  color: ${props => (props.isEditable ? '#828f97' : '#fff')};

  margin-top: ${props => (props.isEditable ? '1.875em' : '')};
  padding: 1.25em;
  flex: 0 1 100%;

  @media screen and (min-width: 768px) {
    flex: 0 1 48%;
    margin-top: ${props => (props.isEditable ? '0' : '')};
  }

  input {
    background: #fff !important;
  }
`

const CreditCardHeading = styled.div`
  ${flex('space-between', 'center')};
  margin-bottom: 2.25em;
`

const BankName = styled.div`
  color: #44aef3;
  font-size: 0.875rem;
  font-weight: 700;
`

const CreditCardOwner = styled.div`
  font-size: 1.25rem;
  font-weight: 700;
`

const CreditCardNumber = styled.div`
  margin: 1.375em 0 1.25em;
`

const CreditCardTitle = styled.h6`
  font-size: 0.75rem;
  font-weight: 900;
  color: #828f97;
  margin-bottom: 0.25em;
`

const CreditCardNumberValue = styled.div`
  font-size: 1.125rem;
  font-weight: 700;
  letter-spacing: 0.17em;
`

const CreditCardFooter = styled.div`
  display: flex;
  justify-content: ${props => (props.isEditable ? 'space-between' : '')};
  margin-top: ${props => (props.isEditable ? '1.675em' : '')};
`

const CreditCardValue = styled.div`
  font-size: 1rem;
  font-weight: 700;
`

const CreditCardSection = styled.div`
  flex: 1;
`

const CreditCardDropdown = styled.div`
  ${flex('flex-start', 'center')};
  border: 0.125em solid #eaedf2;
  padding: 0 2em 0 0.875em;
  position: relative;
  height: 2.5em;
  cursor: pointer;
`

const CreditCardDropdownValue = styled.div`
  color: #102b3b;
  font-size: 0.75rem;
  font-weight: 700;
`

const CreditCardDropdownArrow = styled.div`
  position: absolute;
  top: 0.375em;
  right: 0.625em;
`

const CreditCardOptions = styled.ul`
  position: absolute;
  left: 0;
  top: 2.25em;
  width: 100%;
  background: #fff;
  border: 0.125em solid #eaedf2;
  display: block;

  transform: ${props =>
    props.dropdownOpen ? 'translateY(0)' : 'translateY(-10px)'};
  visibility: ${props => (props.dropdownOpen ? 'visible' : 'hidden')};
  opacity: ${props => (props.dropdownOpen ? '1' : '0')};
  transition-duration: ${props =>
    props.dropdownOpen ? '100ms, 150ms, 25ms' : '70ms, 250ms, 250ms'};
  transition-delay: ${props =>
    props.dropdownOpen ? '35ms, 50ms, 25ms' : '25ms, 50ms, 0ms'};
  transition-property: opacity, transform, visibility;
  transition-timing-function: linear, cubic-bezier(0.23, 1, 0.32, 1);
`

const CreditCardOption = styled.li`
  color: #828f97;
  font-size: 0.625rem;
  font-weight: 400;
  padding: 0.5em 1em;
  transition: 0.3s ease-in-out;

  &:hover {
    background: #eaedf2;
  }
`

const CreditCardCCV = styled.div`
  max-width: 2.875em;
`

const CreditCardNumberInput = styled.div`
  max-width: 9em;
`

export {
  CreditCard,
  CreditCardHeading,
  BankName,
  CreditCardOwner,
  CreditCardTitle,
  CreditCardNumber,
  CreditCardNumberValue,
  CreditCardFooter,
  CreditCardValue,
  CreditCardSection,
  CreditCardDropdown,
  CreditCardDropdownValue,
  CreditCardDropdownArrow,
  CreditCardOptions,
  CreditCardOption,
  CreditCardCCV,
  CreditCardNumberInput
}
