/* eslint-disable no-unused-vars,react/prop-types */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import InputRange from 'react-input-range'

import './range-slider.scss'

class RangeSlider extends Component {
  render() {
    const { reverse, min, max, onChange, value } = this.props
    const rangeStyle = {
      activeTrack: reverse
        ? 'input-range__track input-range__track--active--reversed'
        : 'input-range__track input-range__track--active',
      disabledInputRange: 'input-range input-range--disabled',
      inputRange: `input-range`,
      labelContainer: 'input-range__label-container',
      maxLabel: 'input-range__label input-range__label--max',
      minLabel: 'input-range__label input-range__label--min',
      slider: 'input-range__slider',
      sliderContainer: 'input-range__slider-container',
      track: `input-range__track ${
        reverse ? '' : 'input-range__track--reversed'
      } input-range__track--background`,
      valueLabel: 'input-range__label input-range__label--value'
    }

    return (
      <InputRange
        classNames={rangeStyle}
        maxValue={max}
        minValue={min}
        value={value}
        onChange={onChange}
      />
    )
  }
}

RangeSlider.propTypes = {
  value: PropTypes.number,
  onChange: PropTypes.func,
  reverse: PropTypes.bool
}

export default RangeSlider
