import styled from 'styled-components'

const Container = styled.div`
  min-height: ${props => props.fullHeight && 'calc(100vh - 140px)'};
  padding: 0 1em;
  width: 100%;

  @media screen and (max-width: 991px) {
    background: ${props => props.heading && '#fff'};
  }

  background: ${props => props.heading && '#fff'};
  height: ${props => props.heading && '3.75em'};
  display: ${props => (props.heading || props.loaderPage) && 'flex'};
  align-items: ${props => (props.heading || props.loaderPage) && 'center'};

  @media (min-width: 1200px) {
    margin: 0 auto;
    width: 1140px;
    max-width: 100%;
  }
`

export const Heading = styled.h1`
  display: block;
  padding-top: 1em;
`

export default Container
