/* eslint-disable react/prop-types */
import styled from 'styled-components'
import React from 'react'

const ErrorText = styled.span`
  font-size: 0.75rem;
  font-weight: 500;
  color: #f25c5e;
`
const StyledInput = styled.input`
  [type='tel'],
  [type='email'],
  [type='password'],
  [type='search'],
  [type='text'] {
    ::placeholder {
      font-family: 'Roboto', sans-serif;
      font-size: 0.75rem;
      font-weight: 500;
      color: #808f97;
    }
  }

  box-shadow: inset 0 1px 3px 0 #f0f1f6;

  color: #828e96;

  font-family: 'Roboto', sans-serif;
  font-size: 0.75rem;
  font-weight: 500;

  padding: 0.875em;
  height: 3.334em;
  width: 100%;

  border: ${props =>
    props.isInvalid ? '0.063em solid #f25c5e' : '0.063em solid #eaedf2'};
  background: ${props => (props.white ? '#fff' : '#f4f5f9')};
  background: ${props => props.isInvalid && '#FCF3F4'};

  ::placeholder {
    text-align: ${props => props.placeholderCenter};
  }
`

const Input = ({
  input,
  type,
  label,
  meta: { touched, error, warning },
  ...custom
}) => {
  return (
    <React.Fragment>
      <StyledInput
        type={type}
        isInvalid={touched && error}
        placeholder={label}
        {...input}
        {...custom}
      />
      {touched && error && <ErrorText>{error}</ErrorText>}
    </React.Fragment>
  )
}

export default Input
