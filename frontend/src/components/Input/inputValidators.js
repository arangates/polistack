/* eslint-disable  */
import { isDecimal, isFloat } from 'validator'

const validate = (curr, prev) => {
  const regex = /(\..*){2,}/

  if (
    (!regex.test(curr) && curr.slice(-1) === '.' && curr.length > 1) ||
    curr.length === 0
  ) {
    return curr
  }

  if (
    isDecimal(curr, { decimal_digits: '1,2' }) &&
    isFloat(curr, { min: 1.0, max: 100000.0 })
  ) {
    return curr
  } else {
    return prev
  }
}

const intValidate = (curr, prev) => {
  const regex = /(\..*){2,}/

  if (
    (!regex.test(curr) && curr.slice(-1) === '.' && curr.length > 1) ||
    curr.length === 0
  ) {
    return curr
  }

  if (
    isDecimal(curr, { decimal_digits: '1,2' }) &&
    isFloat(curr, { min: 1.0, max: 100.0 }) &&
    isFloat(curr, { gt: 0, lt: 100 })
  ) {
    return curr
  } else {
    return prev
  }
}


export { validate as default, intValidate }
