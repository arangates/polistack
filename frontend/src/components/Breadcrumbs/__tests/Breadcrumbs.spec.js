/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Breadcrumbs from '../index'

describe('Breadcrumbs', () => {
  it('should render correctly', () => {
    const output = shallow(<Breadcrumbs />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
