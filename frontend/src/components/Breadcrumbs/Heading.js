import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const Heading = styled.div`
  ${flex('space-between', 'flex-start')};
`

export default Heading
