/* eslint react/prop-types: 0 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import { IS_CLIENT } from 'constants/globals'
import { getDeserializedCategoryName } from 'utils'
import Heading from './Heading'
import Breadcrumb from './Breadcrumb'
import { Helmet } from 'react-helmet'

const ReCategory = /^\/category\//
const ReNews = /^\/news\//
const ReEvent = /^\/event\//

class Breadcrumbs extends Component {
  pathNames = [
    { pathname: '/', title: 'Homepage' },
    { pathname: '/profile/trades', title: 'MyTrades' },
    { pathname: '/profile', title: 'UserProfile' },
    { pathname: '/profile/edit', title: 'Edit' },
    { pathname: '/profile/account', title: 'Account' },
    { pathname: '/admin', title: 'Admin' },
    { pathname: '/admin/bets', title: 'Bets' },
    { pathname: '/admin/users', title: 'Users' },
    { pathname: '/admin/transactions', title: 'Transactions' },
    { pathname: '/news', title: 'News' },
    {
      pathname: '/category',
      title: 'Categories'
    }
  ]

  splitPathname = pathname =>
    pathname
      .split('/')
      .map(item => `/${item}`)
      .reduce((prev, curr) => {
        prev.length === 0 || prev.length === 1
          ? prev.push(curr)
          : prev.push(prev[prev.length - 1] + curr)
        return prev
      }, [])
      .filter((item, index, arr) => arr.indexOf(item) === index)
      .filter(
        path =>
          path.length === 1 ||
          (path.length > 1 &&
            path.charAt(path.length - 1) !== '/' &&
            path.charAt(0) !== '?' &&
            path.slice(0, 2) !== '/?')
      )
      .map(item => {
        return { pathname: item, title: 'a' }
      })

  isCategory = path => ReCategory.test(path)
  isEvent = path => ReEvent.test(path)
  isNews = path => ReNews.test(path)

  getCategoryName = path => {
    const components = path.split('/')
    return components[components.length - 1]
  }
  getNameSeperatedByDash = path => {
    if (path.includes('-')) {
      const components = path.split('/').reverse()[0]
      return components
    } else {
      return undefined
    }
  }

  // https://developers.google.com/search/docs/data-types/breadcrumb
  generateBreadcrumbJSONLD = breadcrumbs => {
    const listItem = breadcrumbs.filter(item => item).map((sitem, sindex) => {
      return {
        '@type': 'ListItem',
        position: sindex + 1,
        item: {
          '@id': `https://polistack.com${sitem.pathname}`,
          name: sitem.title
        }
      }
    })

    return {
      '@context': 'http://schema.org',
      '@type': 'BreadcrumbList',
      itemListElement: listItem
    }
  }

  matchPathnames = arr => {
    return arr.map(item => {
      if (this.isCategory(item.pathname)) {
        return {
          pathname: item.pathname,
          title: getDeserializedCategoryName(
            this.getCategoryName(item.pathname)
          )
        }
      }
      if (this.isEvent(item.pathname) && item.pathname.includes('-')) {
        return {
          pathname: item.pathname,
          title: getDeserializedCategoryName(
            this.getNameSeperatedByDash(item.pathname)
          )
        }
      }
      if (this.isNews(item.pathname)) {
        return {
          pathname: item.pathname,
          title: getDeserializedCategoryName(
            this.getNameSeperatedByDash(item.pathname)
          )
        }
      }
      return this.pathNames.find(element => element.pathname === item.pathname)
    })
  }

  render() {
    const paths = this.splitPathname(
      IS_CLIENT ? this.props.location.pathname : this.props.pathname
    )
    const breadcrumbs = this.matchPathnames(paths)
    return (
      <>
        <Helmet>
          <script type="application/ld+json">
            {JSON.stringify(this.generateBreadcrumbJSONLD(breadcrumbs))}
          </script>
        </Helmet>
        <Heading>
          <Breadcrumb isEvent={this.isEvent(this.props.location.pathname)}>
            {breadcrumbs.map(
              (item, index) =>
                item && (
                  <li key={index}>
                    <Link to={item.pathname}>{item.title}</Link>
                  </li>
                )
            )}
          </Breadcrumb>
        </Heading>
      </>
    )
  }
}

const mapStateToProps = ({ site: { pathname } }) => ({ pathname })

export default withRouter(connect(mapStateToProps)(Breadcrumbs))
