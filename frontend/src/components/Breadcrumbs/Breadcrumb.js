import styled from 'styled-components'

const Breadcrumb = styled.ul`
  color: #828f97;
  font-size: 0.75rem;

  display: flex;
  align-items: center;
  margin-top: 1em;
  li {
    @media screen and (max-width: 576px) {
      max-width: ${props => props.isEvent && '40%'};
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
    }

    &:last-child {
      color: #102b3b;
      font-weight: 500;
    }
    &:not(:last-child)::after {
      content: '//';
      margin: 0 0.715em;
    }
  }
`

export default Breadcrumb
