import styled from 'styled-components'

const Main = styled.div`
  padding-top: 2em;
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  justify-content: space-between;
`

export default Main
