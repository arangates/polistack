import styled from 'styled-components'

const Row = styled.div`
  margin: 0 -1em;
`

export default Row
