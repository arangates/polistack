import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const StatusTabs = styled.div`
  display: flex;
  justify-content: center;
`

const StatusTab = styled.div`
  ${flex('center', 'center')};
  background: #fff;
  color: ${props => (props.active ? '#44aef3' : '#828f97')};
  font-size: 0.75rem;
  font-weight: 900;
  height: 3.333em;
  max-width: 8.5em;
  width: 100%;
  border: ${props => (props.active ? 'none' : '.1667em solid #eaedf2')};
  box-shadow: ${props =>
    props.active ? '0 .1667em .5em 0 rgba(0, 0, 0, .15);' : ''};
  cursor: pointer;
  text-align: center;
  transition: 0.3s ease-in-out;

  &:first-child {
    border-radius: 1.667em 0 0 1.667em;
  }

  &:last-child {
    border-radius: 0 1.667em 1.667em 0;
  }

  &:hover {
    border: none;
    box-shadow: 0 0.1667em 0.5em 0 rgba(0, 0, 0, 0.15);
    color: #44aef3;
  }
`

export { StatusTabs, StatusTab }
