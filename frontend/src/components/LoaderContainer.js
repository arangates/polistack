import styled from 'styled-components'

const LoaderContainer = styled.div`
  min-height: ${props => !props.customHeight && 'calc(100vh - 140px)'};
  min-height: ${props => props.customHeight && '50vh'};
  padding: 0 1em;
  width: 100%;
  display: flex;
  align-items: center;
`

export default LoaderContainer
