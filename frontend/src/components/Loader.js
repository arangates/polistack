/* eslint-disable react/prop-types */
import styled, { keyframes } from 'styled-components'
import PropTypes from 'prop-types'
import React from 'react'
import { LoaderContainer } from 'components'

const rotate360 = keyframes`
  from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
`

const StyledLoader = styled.div`
  font-size: 10px;
  margin: ${props => (props.centered ? '0 auto 10em' : '0 auto')};
  text-indent: -9999em;
  width: ${props => `${props.size}em`};
  height: ${props => `${props.size}em`};
  border-radius: 50%;
  background: ${props => (props.white ? '#fff' : '#44AEF3')};
  background: linear-gradient(
    to right,
    ${props => (props.white ? '#fff' : '#44AEF3')} 10%,
    rgba(255, 255, 255, 0) 42%
  );
  position: relative;
  animation: ${rotate360} 1.4s infinite linear;

  transform: translateZ(0);

  :before {
    width: 50%;
    height: 50%;
    background: ${props => (props.white ? '#fff' : '#44AEF3')};
    border-radius: 100% 0 0 0;
    position: absolute;
    top: 0;
    left: 0;
    content: '';
  }

  :after {
    background: ${props => (props.white ? props.bgColor : '#F9FBFF')};
    width: 75%;
    height: 75%;
    border-radius: 50%;
    content: '';
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }
`

const Loader = props => {
  return (
    <LoaderContainer
      fullHeight={props.fullHeight}
      loaderPage={props.loaderPage}
      customHeight={props.customHeight}
    >
      <StyledLoader {...props} />
    </LoaderContainer>
  )
}

Loader.propTypes = {
  size: PropTypes.number,
  bgColor: PropTypes.string,
  white: PropTypes.bool
}

Loader.defaultProps = {
  size: 4
}

export default Loader
