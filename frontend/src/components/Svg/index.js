/* eslint-disable no-undef,react/prop-types,no-return-assign */
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import SVGInline from 'react-svg-inline'
import { DOMParser, XMLSerializer } from 'xmldom'

class Svg extends Component {
  parseIcon = icon => {
    const parser = new DOMParser()
    return parser.parseFromString(icon)
  }

  serializeIcon = icon => {
    const serializer = new XMLSerializer()
    return serializer.serializeToString(icon)
  }

  injectStyles = (icon, styles = {}) => {
    icon.documentElement.setAttribute(
      'style',
      Object.keys(styles)
        .map(item => `${item}: ${styles[item]}`)
        .join(';')
    )
    return icon
  }

  render() {
    const { style, icon } = this.props

    return (
      <SVGInline
        svg={this.serializeIcon(this.injectStyles(this.parseIcon(icon), style))}
        onClick={this.props.onClick}
      />
    )
  }
}

Svg.propTypes = {
  style: PropTypes.object,
  icon: PropTypes.string
}

export default Svg
