import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const TableItem = styled.div`
  display: flex;
  align-items: center;
  color: ${props => (props.light ? '#828f97' : '#102b3b')};
  font-size: 0.75rem;
  font-weight: 600;
  height: ${props => (props.light ? '2em' : '5em')};
  margin-left: ${props => (props.light ? '1em' : '')};
  position: relative;

  &::before {
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    width: 2px;
    height: 100%;
    background: #44aef3;
    content: ${props => (props.rephrased ? '""' : '')};
  }

  &:nth-child(even) {
    background: #fafcff;
  }

  &:not(:last-child) {
    border-bottom: ${props =>
      props.light ? '' : '1px solid rgba(244, 245, 249, 1)'};
  }
`

const TableCell = styled.div`
  flex: 1;
`

const TableCellImage = styled.div`
  ${flex('center', 'center')};
  flex: 0 1 4.25em;
  height: 100%;
  position: relative;

  &::after {
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    border-right: 10px solid transparent;
    border-top: ${props => (props.statusNew ? '10px solid #22d039' : '')};
    border-top: ${props => (props.statusResolved ? '10px solid #44aef3' : '')};
  }
`

const UserProfileImage = styled.div`
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  height: 2.188em;
  width: 2.188em;
  border-radius: 2.188em;
`

const TableCellType = TableCell.extend`
  font-size: ${props =>
    props.inflow || props.outflow || props.fee ? '.875rem' : ''};
  flex: 1 1 40%;
  max-width: 40%;
  font-weight: 500;
  padding-right: 1.65em;
  padding-left: 1em;
  line-height: 1.5em;

  display: ${props => (props.rephrased || props.win ? 'flex' : '')};
  flex-direction: ${props => (props.rephrased || props.win ? 'column' : '')};

  > div {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  > span {
    color: ${props => props.rephreased && '#44aef3'};
    color: ${props => props.win && '#f9c20a'};
    color: ${props => props.lost && '#102b3b'};
    font-size: 0.625rem;
    font-weight: 900;
  }
`

const BetAuthor = styled.div`
  color: #44aef3;
  font-size: 0.625rem;
  font-weight: 900;
`

const UserEmail = styled.div`
  color: #828f97;
  font-weight: 400;
`

const TableCellDate = TableCell.extend`
  font-weight: 400;
`

const TableCellOdds = TableCell.extend`
  color: #102b3b;
`

const TableCellOptions = TableCell.extend`
  flex: 0 0 7em;
  display: flex;
  justify-content: flex-end;
  padding-right: 0.625em;
`

const TableCellOption = styled.div`
  font-size: 1rem;
  background-color: #fff;
  border: 0.125em solid #eaedf2;
  border-radius: 0.938em;
  cursor: pointer;
  height: 1.875em;
  width: 1.875em;

  ${flex('center', 'center')} &:not(:last-child) {
    margin-right: 0.625em;
  }
`

const TableCellCategory = styled.div`
  flex: 0 1 4em;
  padding-left: 1em;
  cursor: pointer;
  position: relative;
  height: 100%;
  display: flex;
  align-items: center;
`

const CategoryDropdown = styled.div`
  position: absolute;
  z-index: 9;
  background: #fff;
  box-shadow: 0 2px 8px 0 rgba(187, 195, 207, 0.3);
  min-width: 15em;
  top: 5em;
  left: 0;

  transform: ${props => (props.isOpen ? 'translateY(0)' : 'translateY(-1em)')};
  visibility: ${props => (props.isOpen ? 'visible' : 'hidden')};
  opacity: ${props => (props.isOpen ? '1' : '0')};
  transition-duration: ${props =>
    props.isOpen ? '100ms, 150ms, 25ms' : '70ms, 250ms, 250ms'};
  transition-delay: ${props =>
    props.isOpen ? '35ms, 50ms, 25ms' : '25ms, 50ms, 0ms'};
  transition-property: opacity, transform, visibility;
  transition-timing-function: linear, cubic-bezier(0.23, 1, 0.32, 1);
`

const CategoryItem = styled.div`
  display: flex;
  align-items: center;
  padding: 1.25em 1em;
  color: ${props => (props.isActive ? '#102b3b' : '#828f97')};
  transition: 0.3s ease-in-out;

  &:hover {
    color: #102b3b;

    svg {
      fill: #102b3b;
    }
  }

  .SVGInline {
    width: 25px;
    text-align: center;
  }

  svg {
    fill: ${props => (props.isActive ? '#102b3b' : '#828f97')};

    transition: 0.3s ease-in-out;
  }
`

const Title = styled.span`
  font-size: 0.875rem;
  font-weight: 500;
  padding-left: 1.5em;
`

export {
  TableItem,
  TableCell,
  TableCellImage,
  UserProfileImage,
  TableCellType,
  BetAuthor,
  UserEmail,
  TableCellDate,
  TableCellOdds,
  TableCellOptions,
  TableCellOption,
  TableCellCategory,
  CategoryDropdown,
  CategoryItem,
  Title
}
