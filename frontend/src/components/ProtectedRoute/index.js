/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

class ProtectedRoute extends Component {
  render() {
    const { authenticated, component: Component, rest } = this.props

    return (
      <Route
        {...rest}
        render={props => {
          return authenticated === true ? (
            <React.Fragment>
              <Component {...props} />
            </React.Fragment>
          ) : (
            <Redirect to="/login" />
          )
        }}
      />
    )
  }
}

function mapStateToProps({ user }) {
  return { authenticated: user.authenticated }
}

export default connect(mapStateToProps, {})(ProtectedRoute)
