/* eslint-disable react/prop-types */
import styled from 'styled-components'
import React from 'react'

const StyledTextArea = styled.textarea`
  ::placeholder {
    font-family: 'Roboto', sans-serif;
    font-size: 0.75rem;
    font-weight: 500;
    color: #808f97;
  }

  box-shadow: inset 0 1px 3px 0 #f0f1f6;
  color: #828e96;
  font-family: 'Roboto', sans-serif;
  font-size: 0.75rem;
  font-weight: 500;
  padding: 0.875em;
  width: 100%;
  max-width: 100%;

  margin-top: ${props => (props.topMargin ? '1em' : '')};

  border: ${props =>
    props.isInvalid ? '0.063em solid #f25c5e' : '0.063em solid #eaedf2'};
  background: ${props => (props.white ? '#fff' : '#f4f5f9')};
  background: ${props => props.isInvalid && '#FCF3F4'};
  height: auto;
`

const TextArea = ({
  input,
  label,
  meta: { touched, error, warning },
  ...custom
}) => {
  return (
    <StyledTextArea
      placeholder={label}
      {...input}
      {...custom}
      isInvalid={touched && error}
    />
  )
}

export default TextArea
