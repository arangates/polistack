/*eslint-disable*/
import styled from 'styled-components'
import { flex } from 'constants/global.styles'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Loader from './Loader'

const BOX_SHADOW = {
  white: '0em 0.125em 0.375em 0em rgba(0, 0, 0, .15)',
  popupWhite: '0 .25em .75em 0 rgba(0, 0, 0, .1);',
  popupBlue: '0 .625em .625em 0 rgba(68, 174, 243, .4)'
}

const COLORS = {
  lightRed: '#f25c5e',
  red: '#d54547',
  lightBlue: '#44aef3',
  blue: '#3494d3',
  white: '#fff',
  gray: '#828f97'
}

const StyledButton = styled.div`
  ${flex('center', 'center')};
  border: none;
  border-radius: ${props => (props.popup ? '.25em' : '1.458em')};
  color: ${props => (props.white ? COLORS.gray : COLORS.white)};
  height: ${props => (props.popup ? '3.75em' : '2.917em')};
  width: ${props => (props.extended ? '48%' : '10em')};
  width: ${props => props.fullWidth && '100%'};
  opacity: 0.5;
  height: ${props => (props.popup ? '3.75em' : '2.917em')};

  width: ${props => props.big && '12.084em'};
  height: ${props => props.big && '3.750em'};
  border-radius: ${props => props.big && '.25em'};
  text-transform: ${props => props.big && 'uppercase'};

  height: ${props => (props.popup ? '3.75em' : '3em')};
  width: ${props => (props.extended ? '48%' : '')};
  width: ${props => (props.confirm ? '100%' : '')};
  width: ${props => (!props.confirm || !props.extended ? '10em' : '')};
  transition: 0.3s ease-in-out;
  outline: none;
  font-family: 'Roboto', sans-serif;
  font-size: 0.75rem;
  font-weight: 900;

  margin: ${props => props.centered && '1.25em auto 0'};

  background: ${({red}) => red && COLORS.lightRed};
  background: ${({blue}) => blue && COLORS.lightBlue};
  background: ${({white}) => white && COLORS.white};

  box-shadow: ${({popup, white}) => !popup && white && BOX_SHADOW.white};
  box-shadow: ${({popup, blue}) => popup && blue && BOX_SHADOW.popupBlue};
  box-shadow: ${({popup, white}) => popup && white && BOX_SHADOW.popupWhite};
  text-transform: ${props => props.popup && 'uppercase'};
  cursor: not-allowed;
`

class Button extends Component {
  render() {
    let bgColor = ''

    if (this.props.red) {
      bgColor = COLORS.lightRed
    } else if (this.props.blue) {
      bgColor = COLORS.lightBlue
    } else {
      bgColor = COLORS.white
    }

    return (
      <StyledButton {...this.props}>
        <Loader white bgColor={bgColor} size={2.5}/>
      </StyledButton>
    )
  }
}

Button.propTypes = {
  fullWidth: PropTypes.bool,
  big: PropTypes.bool,
  popup: PropTypes.bool,
  extended: PropTypes.bool,
  red: (props, propName, componentName) => {
    if (!props.red && !props.blue && !props.white) {
      return new Error(
        `One of props 'red' or 'blue' or 'white' was not specified in '${componentName}'.`
      )
    }
  },
  blue: (props, propName, componentName) => {
    if (!props.red && !props.blue && !props.white) {
      return new Error(
        `One of props 'red' or 'blue' or 'white' was not specified in '${componentName}'.`
      )
    }
  },
  white: (props, propName, componentName) => {
    if (!props.red && !props.blue && !props.white) {
      return new Error(
        `One of props 'red' or 'blue' or 'white' was not specified in '${componentName}'.`
      )
    }
  }
}

export default Button
