import styled from 'styled-components'

const Content = styled.div`
  flex: 1;
  max-width: 100%;
`

export default Content
