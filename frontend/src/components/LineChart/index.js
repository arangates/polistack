/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { ResponsiveLine } from '@nivo/line'
import MediaQuery from 'react-responsive'
import Wrapper from './Wrapper'
const commonProperties = {
  lineWidth: 3,
  fontSize: 8,
  colors: '#44aef3',
  enableGridY: false,
  axisBottom: {
    orient: 'bottom',
    tickSize: 0,
    tickRotation: 0
  },
  axisLeft: {
    orient: 'left',
    tickSize: 0,
    tickRotation: 0
  },
  margin: { top: 20, right: 5, bottom: 60, left: 25 }
}

class LineChart extends Component {
  render() {
    const { graphData, axisInterval, precision } = this.props
    const preparedData = [
      {
        id: 'Trades',
        color: '#44aef3',
        data: graphData.points
      }
    ]

    return (
      <Wrapper>
        <MediaQuery query="(min-width: 768px)">
          <ResponsiveLine
            {...commonProperties}
            data={preparedData}
            xScale={{
              type: 'time',
              format: '%Y-%m-%dT%H:%M:%S.%f%Z',
              precision: precision
            }}
            yScale={{
              type: 'linear'
            }}
            axisBottom={{
              format: precision === 'day' ? '%b %d' : '%H',
              tickValues: `every ${axisInterval} ${precision}s`
            }}
            enableDotLabel
            dotSize={16}
            dotBorderWidth={1}
            dotBorderColor="inherit:darker(0.3)"
          />
        </MediaQuery>
        <MediaQuery query="(max-width: 768px)">
          <ResponsiveLine
            {...commonProperties}
            data={preparedData}
            xScale={{
              type: 'time',
              format: '%Y-%m-%dT%H:%M:%S.%f%Z',
              precision: precision
            }}
            yScale={{
              type: 'linear'
            }}
            axisBottom={{
              format: precision === 'day' ? '%d' : '%H',
              tickValues: `every ${axisInterval} ${precision}s`
            }}
            enableDotLabel
            dotSize={16}
            dotBorderWidth={1}
            dotBorderColor="inherit:darker(0.3)"
          />
        </MediaQuery>
      </Wrapper>
    )
  }
}

export default LineChart
