import styled from 'styled-components'

const InputGroupSpread = styled.div`
  display: flex;
  justify-content: space-between;

  > div {
    flex: 0 1 48%;
  }
`

const InputGroup = styled.div`
  margin: 0.625em 0;

  h6 {
    color: #828f97;
    font-weight: 900;
  }

  input {
    margin-top: 0.625em;
  }
`

export { InputGroup, InputGroupSpread }
