import React from 'react'
import PropTypes from 'prop-types'
import Select from 'cdax-react-select'

const customStyles = {
  container: (base, state) => ({
    ...base,
    fontSize: 12,
    minWidth: '200px'
  }),
  control: (base, state) => {
    return {
      ...base,
      background: !state.isDisabled && '#fff',
      border: '0.063em solid #eaedf2'
    }
  },
  loadingIndicator: base => ({
    ...base,
    display: 'none'
  })
}

const PureSelectDropdown = props => {
  return <Select styles={customStyles} {...props} />
}
PureSelectDropdown.propTypes = {
  options: PropTypes.array,
  name: PropTypes.string,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  multi: PropTypes.bool,
  clearable: PropTypes.bool,
  input: PropTypes.object,
  value: PropTypes.object,
  defaultValue: PropTypes.object,
  searchable: PropTypes.bool,
  onChange: PropTypes.func
}

PureSelectDropdown.defaultProps = {
  isClearable: false,
  isSearchable: false
}

export default PureSelectDropdown
