import styled from 'styled-components'

const Table = styled.div`
  margin-top: 1.25em;
`

const TableHeading = styled.div`
  background: #fff;
  color: ${props => (props.dark ? 'initial' : '#828f97')};
  font-size: 0.75rem;
  font-weight: 700;
  display: flex;
  align-items: center;
  padding: 0.75em 0;
  border-bottom: 1px solid rgba(244, 245, 249, 1);
`

const TableHeadingImage = styled.div`
  flex: 0 1 4.25em;
  height: 100%;
`

const TableHeadingType = styled.div`
  flex: 1 1 40%;
  max-width: 40%;
  padding-left: 1em;
`

const TableRowGroup = styled.div`
  border: 1px solid rgba(244, 245, 249, 1);
  border-top: none;
`

const TableCategory = styled.div`
  flex: 0 1 4em;
`

export {
  Table,
  TableHeading,
  TableHeadingImage,
  TableHeadingType,
  TableRowGroup,
  TableCategory
}
