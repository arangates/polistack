import styled from 'styled-components'

const Divider = styled.div`
  height: 0.063em;
  max-width: ${props => (props.wide ? '45em' : '11.375em')};
  width: 100%;
  margin: 1.25em auto;
  background-color: #f1f1f2;
`

export default Divider
