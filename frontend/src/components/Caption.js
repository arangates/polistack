import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Styled from 'styled-components'
import { Divider } from 'components'

const ShowMoreButton = Styled.button`
  background: none;
  outline: none;
  color: #808f97;
  border: none; 
  font: inherit;
  cursor: pointer;
  font-size: 1em;
`
const RawCaption = Styled.div`
  font-size: 0.8em;
  padding: ${props => (props.hasMargins ? '1em' : '0')};
  border-radius: 4px;
  text-align: center;
  margin-top: ${props => (props.hasMargins ? '1em' : '0')};
  margin-bottom: ${props => (props.hasMargins ? '1em' : '0')};
  margin-left: ${props => (props.hasMargins ? '1em' : '0')};
  margin-right: ${props => (props.hasMargins ? '1em' : '0')};
`

const MessageContainer = Styled.div`
   white-space: pre-wrap;
  color: #767676;
  text-align: justify;
  margin-top: ${props => (props.isHidden ? '0' : '1em')};
  overflow: ${props => (props.isHidden ? 'hidden' : 'visible')};
  height: ${props => (props.isHidden ? '56px' : 'auto')};
`
const Text = Styled.div`
  font-size: 1em;
  color: #5a5a5a;
  margin-bottom: 1em;

  > p {
    font-size: 1em;
    font-family: 'Open Sans', sans-serif;
  }
`
class Caption extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isHidden: true
    }
  }

  toggleVisibility = () => this.setState({ isHidden: !this.state.isHidden })

  render() {
    const { children, hasMargins } = this.props
    const { isHidden } = this.state
    return (
      <RawCaption hasMargins={hasMargins}>
        <MessageContainer isHidden={isHidden}>
          <Text dangerouslySetInnerHTML={{ __html: children }} />
        </MessageContainer>
        <Divider wide />
        <ShowMoreButton onClick={this.toggleVisibility}>
          {isHidden ? 'Read the Full Rules' : 'Show less Rules'}
        </ShowMoreButton>
      </RawCaption>
    )
  }
}

Caption.propTypes = {
  children: PropTypes.node.isRequired,
  hasMargins: PropTypes.bool
}

Caption.defaultProps = {
  hasMargins: false
}

export default Caption
