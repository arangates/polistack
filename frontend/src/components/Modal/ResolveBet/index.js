/* eslint-disable react/prop-types */
import React from 'react'
import { Button } from 'components'
import { Footer, ModalHeader, Title, Content } from '../Modal'
import { connect } from 'react-redux'
import { askToResolveBet } from 'actions/betActions'

const ResolveBet = ({ data, closeModal, askToResolveBet }) => {
  return (
    <React.Fragment>
      <ModalHeader>
        <Title>Ask to resolve bet</Title>
      </ModalHeader>
      <Content>
        <p>{data.statement}</p>

        <div className="bet__informations">
          <div className="bet__information--item">
            <h5>Odds</h5>
            <h6>
              {Math.round(data.yes * 100) / 100}:{Math.round(data.no * 100) /
                100}
            </h6>
          </div>
          <div className="bet__information--item">
            <h5>Volume</h5>
            <h6>
              {data.user_bet_actions.reduce((prev, curr) => {
                return prev + Number(curr.amount)
              }, 0)}$
            </h6>
          </div>
        </div>
      </Content>
      <Footer>
        <Button white extended popup onClick={closeModal}>
          Cancel
        </Button>
        <Button blue extended popup onClick={() => askToResolveBet(data)}>
          Resolve
        </Button>
      </Footer>
    </React.Fragment>
  )
}

export default connect(null, { askToResolveBet })(ResolveBet)
