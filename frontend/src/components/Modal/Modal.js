import styled from 'styled-components'

const ModalHeader = styled.div`
  text-align: center;
  padding-bottom: 1.25em;
`

const Content = styled.div`
  padding-bottom: 1.25em;
  text-align: center;
  .bet__informations {
    border-top: 1px solid #eaedf2;
    padding-top: 0.625em;
    display: flex;
    .bet__information--item {
      &:not(:last-child) {
        border-right: 1px solid #eaedf2;
        padding-right: 1.2em;
      }
      &:last-child {
        padding-left: 1.25em;
      }
      flex: 1;
      h5 {
        color: #828f97;
        font-size: 0.75rem;
        font-weight: 700;
      }
      h6 {
        color: #102b3b;
        font-size: 0.875rem;
        font-weight: 900;
        margin-top: 0.7143em;
      }
    }
  }
  p {
    color: #828f97;
    font-size: 0.875rem;
    line-height: 1.429em;
  }
  .maximum__amount {
    text-align: center;
    margin-top: 1.25em;
    span {
      font-size: 0.75rem;
      font-weight: 900;
      margin-top: 0.75em;

      &.max__amount {
        color: #22d039;
      }

      &.min__amount {
        color: #f25c5e;
      }
    }
  }
  .user__volume {
    text-align: center;
    h5 {
      color: #102b3b;
      font-size: 0.875rem;
      font-weight: 900;
    }
    input {
      max-width: 10.313em;
      text-align: center;
      margin-top: 0;
    }
  }
  textarea {
    margin-top: 1.25em;
  }
  .user-to-ban {
    border: 0.063em solid #eaedf2;
    max-width: 16.375em;
    width: 100%;
    margin: 0 auto;
    .profile__image {
      background-size: cover;
      background-position: center;
      border-radius: 5em;
      box-shadow: 0 0.25em 0.5em 0 rgba(68, 174, 243, 0.3);
      margin: 1.25em auto 0.625em;
      height: 5em;
      width: 5em;
    }
    .profile__name {
      text-align: center;
      margin: 0.625em 0 1.25em;
      h4 {
        color: #293244;
        font-size: 1rem;
        font-weight: 700;
      }
      .user__email {
        color: #828f97;
        font-size: 0.75rem;
      }
    }
  }
`

const Title = styled.h2``

const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  text-transform: uppercase;
  border-top: 1px solid #eaedf2;
  padding-top: 1.25em;

  .btn__confirm {
    width: 48%;

    h5 {
      color: #828f97;
      font-size: 0.625rem;
      font-weight: 400;
      text-align: center;
      text-transform: none;
      margin-bottom: 1.5em;
    }
  }
`

export { ModalHeader, Title, Footer, Content }
