/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import ModalOverlay from '../index'

describe('ModalOverlay', () => {
  it('should render correctly', () => {
    const output = shallow(<ModalOverlay />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
