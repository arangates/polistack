import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const Modal = styled.div`
  ${flex('center', 'center')};
  background: rgba(16, 43, 59, 0.9);
  position: fixed;
  z-index: 2;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  padding: 0 1em;
`

export default Modal
