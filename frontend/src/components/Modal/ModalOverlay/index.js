/* eslint-disable react/prop-types */
import React from 'react'
import ModalContainer from '../ModalContainer'
import Modal from './Modal'

const ModalOverlay = ({ children, data }) => {
  return (
    <Modal>
      <ModalContainer children={children} data={data} />
    </Modal>
  )
}

export default ModalOverlay
