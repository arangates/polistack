/* eslint-disable react/prop-types */
import React from 'react'
import { connect } from 'react-redux'
import { Footer, ModalHeader, Title, Content } from '../Modal'

const CreateBetSummary = ({ data }) => {
  return (
    <React.Fragment>
      <ModalHeader>
        <Title>Confirm bet</Title>
      </ModalHeader>
      <Content>
        <p>{data.statement}</p>

        <div className="bet__informations">
          <div className="bet__information--item">
            <h5>Odds</h5>
            <h6>
              {data.yes}:{data.no}
            </h6>
          </div>
          <div className="bet__information--item">
            <h5>Volume</h5>
            <h6>{data.amount}$</h6>
          </div>
        </div>
      </Content>
      <Footer />
    </React.Fragment>
  )
}

export default connect(null, {})(CreateBetSummary)
