/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import ModalContainer from '../index'

import configureStore from 'redux-mock-store'

const mockStore = configureStore()

const store = mockStore({})

describe('ModalContainer', () => {
  it('should render correctly', () => {
    const output = shallow(<ModalContainer store={store} />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
