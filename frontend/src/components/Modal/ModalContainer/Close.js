import styled from 'styled-components'

const Close = styled.div`
  position: absolute;
  padding: 1em;
  top: 0;
  right: 0;

  img {
    cursor: pointer;
  }
`

export default Close
