import styled from 'styled-components'

const ModalContainer = styled.div`
  background: #fff;
  padding: 1.25em;
  position: relative;
  max-width: ${props => (props.wide ? '35em' : '22em')};
  width: 100%;
`

export default ModalContainer
