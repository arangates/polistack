/* eslint-disable react/prop-types,no-unused-vars */
import React, { Component } from 'react'
import onClickOutside from 'react-onclickoutside'
import { connect } from 'react-redux'
import { iconDelete } from 'images'
import { closeModal } from 'actions/modalActions'
import { Svg } from 'components'
import Container from './Container'
import Close from './Close'

class ModalContainer extends Component {
  handleClickOutside = () => this.props.closeModal()

  render() {
    const { children, closeModal, modal, data } = this.props
    return (
      <Container data-test-id="modal">
        <Close>
          <div style={{ cursor: 'pointer' }}>
            <Svg icon={iconDelete} alt="Close modal" onClick={closeModal} />
          </div>
        </Close>
        {React.cloneElement(children, { closeModal, data })}
      </Container>
    )
  }
}

function mapStateToProps({ modal }) {
  return { modal: modal.modal }
}

export default connect(mapStateToProps, { closeModal })(
  onClickOutside(ModalContainer)
)
