/* eslint-disable react/prop-types */
import React from 'react'
import { Button } from 'components'
import { Footer, ModalHeader, Title, Content } from '../Modal'
import { withRouter } from 'react-router-dom'

const Info = ({
  data: { header, text, changeLocation, cancel = false },
  closeModal,
  history
}) => {
  const handleButton = () => {
    closeModal()
    if (changeLocation) {
      history.push(changeLocation.link)
    }
  }

  return (
    <React.Fragment>
      <ModalHeader>
        <Title>{header}</Title>
      </ModalHeader>
      <Content>
        <p>{text}</p>
      </Content>
      <Footer>
        {cancel && (
          <Button white extended popup onClick={closeModal}>
            Cancel
          </Button>
        )}
        {cancel ? (
          <div className="btn__confirm">
            <Button
              data-test-id="ok-button"
              blue
              popup
              extended
              confirm
              onClick={handleButton}
            >
              {changeLocation ? changeLocation.text : 'Ok'}
            </Button>
          </div>
        ) : (
          <Button
            data-test-id="ok-button"
            blue
            popup
            extended
            confirm
            onClick={handleButton}
          >
            {changeLocation ? changeLocation.text : 'Ok'}
          </Button>
        )}
      </Footer>
    </React.Fragment>
  )
}

export default withRouter(Info)
