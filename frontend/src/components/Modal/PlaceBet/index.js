/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import { Button } from 'components'
import { placeBet } from 'actions/betActions'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { Footer, ModalHeader, Title, Content } from '../Modal'
import PriceBox from './PriceBox'
import BetInput from './BetInput'
import validate, { betValidate, numberValidate } from './validators'

class PlaceBet extends Component {
  state = {
    amount: this.props.data.betAmount,
    price: this.getInitialPrice()
  }

  getInitialPrice() {
    if (this.props.side === 'no') {
      if (this.props.data.highestBid === 0) {
        return 50
      } else {
        return 100 - this.props.highestBid
      }
    } else {
      if (this.props.data.lowestAsk === 100) {
        return 50
      } else {
        return this.props.lowestAsk
      }
    }
  }

  componentDidMount() {
    if (this.props.data.side === 'no') {
      const noPrice =
        this.props.data.highestBid === 0 ? 50 : 100 - this.props.data.highestBid
      this.props.initialize({
        betAmount: this.props.data.betAmount,
        betPrice: noPrice
      })
      this.setState({
        price: noPrice
      })
    } else {
      const yesPrice =
        this.props.data.lowestAsk === 100 ? 50 : this.props.data.lowestAsk
      this.props.initialize({
        betAmount: this.props.data.betAmount,
        betPrice: yesPrice
      })
      this.setState({
        price: yesPrice
      })
    }
  }

  handlePriceChange = e => {
    const price = e.target.value
    if (betValidate(price)) {
      this.setState({
        price: price
      })
    }
  }
  handleAmountChange = e => {
    const amount = e.target.value
    if (amount) {
      this.setState({
        amount: amount
      })
    }
  }

  render() {
    const {
      data: { statement, betId, side },
      closeModal,
      placeBet
    } = this.props
    return (
      <React.Fragment>
        <ModalHeader>
          <Title>Place trade</Title>
        </ModalHeader>
        <Content>
          <p>{statement}</p>
          <form>
            <div className="bet__informations">
              <div className="bet__information--item">
                <h5>Price</h5>
                <h6>
                  <PriceBox>
                    <Field
                      name="betPrice"
                      component={BetInput}
                      type="text"
                      normalize={betValidate}
                      white
                      placeholder="100"
                      onChange={this.handlePriceChange}
                      style={{ textAlign: 'center' }}
                    />
                  </PriceBox>
                </h6>
              </div>
              <div className="bet__information--item">
                <h5>Volume</h5>
                <h6>
                  <PriceBox>
                    <Field
                      name="betAmount"
                      component={BetInput}
                      type="text"
                      normalize={numberValidate}
                      white
                      placeholder="100"
                      onChange={this.handleAmountChange}
                      style={{ textAlign: 'center' }}
                    />
                  </PriceBox>
                </h6>
              </div>
            </div>
          </form>
        </Content>
        <Footer>
          <Button white extended popup onClick={closeModal}>
            Cancel
          </Button>

          <div className="btn__confirm">
            <h5>You are betting {side === 'yes' ? 'for' : 'against'}</h5>
            <Button
              blue
              extended
              popup
              confirm
              onClick={() => {
                placeBet({
                  betId: betId,
                  price: this.state.price,
                  shares: this.state.amount,
                  side: side
                })
              }}
            >
              Confirm
            </Button>
          </div>
        </Footer>
      </React.Fragment>
    )
  }
}

PlaceBet = reduxForm({
  form: 'betForm',
  validate,
  enableReinitialize: true
})(PlaceBet)
export default connect(null, { placeBet })(PlaceBet)
