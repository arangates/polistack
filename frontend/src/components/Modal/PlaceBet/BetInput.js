/* eslint-disable react/prop-types */
import styled from 'styled-components'
import React from 'react'

const ErrorText = styled.span`
  font-size: 0.75rem;
  font-weight: 500;
  color: #f25c5e;
`
const StyledInput = styled.input`
  ::placeholder {
    font-family: 'Roboto', sans-serif;
    font-size: 0.75rem;
    font-weight: 500;
    color: #808f97;
    text-align: center;
  }

  font-family: 'Roboto', sans-serif;
  font-size: 0.75rem;
  font-weight: 500;

  height: 3.75em;
  width: 100%;

  border: ${props =>
    props.isInvalid ? '0.063em solid #f25c5e' : '0.063em solid #eaedf2'};
  background: #f9fafc;
`

const Input = ({
  input,
  type,
  label,
  meta: { touched, error, warning },
  ...custom
}) => {
  return (
    <React.Fragment>
      <StyledInput
        type={type}
        isInvalid={touched && error}
        placeholder={label}
        {...input}
        {...custom}
      />
      {touched && error && <ErrorText>{error}</ErrorText>}
    </React.Fragment>
  )
}

export default Input
