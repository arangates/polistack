import { isDecimal, isFloat } from 'validator'

const numberValidate = (curr, prev) => {
  const regex = /(\..*){2,}/

  if (
    (!regex.test(curr) && curr.slice(-1) === '.' && curr.length > 1) ||
    curr.length === 0
  ) {
    return curr
  }

  if (
    isDecimal(curr, { decimal_digits: '1,2' }) &&
    isFloat(curr, { min: 1.0, max: 100000.0 })
  ) {
    return curr
  } else {
    return prev
  }
}
const betValidate = (curr, prev) => {
  const regex = /(\..*){2,}/

  if (
    (!regex.test(curr) && curr.slice(-1) === '.' && curr.length > 1) ||
    curr.length === 0
  ) {
    return curr
  }

  if (
    isDecimal(curr, { decimal_digits: '1,2' }) &&
    isFloat(curr, { min: 1.0, max: 100.0 }) &&
    isFloat(curr, { gt: 0, lt: 100 })
  ) {
    return curr
  } else {
    return prev
  }
}

const validate = ({ betPrice, betAmount }) => {
  const errors = {}
  if (!betAmount) {
    errors.betAmount = 'Volume Required'
  }

  if (!betPrice) {
    errors.betPrice = 'Price Required'
  } else if (isNaN(Number(betPrice))) {
    errors.betPrice = 'Must be a number'
  } else if (Number(betPrice) < 1 || Number(betPrice) > 99) {
    errors.betPrice = 'Please enter number between 1 to 99'
  }
  return errors
}

export { validate as default, betValidate, numberValidate }
