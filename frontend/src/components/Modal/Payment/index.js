/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import { Button, Input } from 'components'
import { reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux'
import { CREDIT_CARD } from 'constants/payment.methods'
import { initializePayment } from 'actions/userActions'
import maxTwoDecimals from 'components/Input/inputValidators'
import { Footer, ModalHeader, Title, Content } from '../Modal'

class Payment extends Component {
  state = {
    paymentMethod: CREDIT_CARD,
    paymentAmount: ''
  }

  componentDidMount() {
    this.props.initialize({
      amount: 100
    })
  }

  changePaymentMethod = method => this.setState({ paymentMethod: method })
  confirmPayment = async _ => {
    await this.props.initializePayment(
      this.props.data,
      this.state.paymentMethod
    )
  }

  render() {
    const { closeModal } = this.props

    return (
      <React.Fragment>
        <ModalHeader>
          <Title>Payment</Title>
        </ModalHeader>
        <Content>
          <p>Choose payment Method.</p>

          <div className="user__volume">
            <h5>Amount</h5>
            <div className="input__group">
              <div className="amount__box">
                <div className="bet__currency" />
                <Field
                  name="amount"
                  component={Input}
                  normalize={maxTwoDecimals}
                  type="text"
                  white
                  placeholder="100"
                />
              </div>
            </div>
          </div>
        </Content>
        <Footer>
          <Button white extended popup onClick={closeModal}>
            Cancel
          </Button>
          <Button blue extended popup onClick={this.confirmPayment}>
            Confirm
          </Button>
        </Footer>
      </React.Fragment>
    )
  }
}

Payment = reduxForm({
  form: 'paymentForm',
  enableReinitialize: true
})(Payment)

function mapStateToProps({ user }) {
  return { balance: user.profile.balance }
}

export default connect(mapStateToProps, {
  initializePayment
})(Payment)
