/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import {
  CHANGE_VOLUME,
  RESOLVE_BET,
  CREATE_BET_SUMMARY,
  PLACE_BET,
  INFO,
  PAYMENT
} from 'constants/modal.types'

import ModalOverlay from './ModalOverlay'
import CreateBetSummary from './CreateBetSummary'
import ChangeVolume from './ChangeVolume'
import Payment from './Payment'
import ResolveBet from './ResolveBet'
import ConfirmBet from './PlaceBet'
import Info from './Info'

class Modal extends Component {
  renderInBody = component => {
    return ReactDOM.createPortal(
      <ModalOverlay data={this.props.modal.data}>{component}</ModalOverlay>,
      document.body
    )
  }

  render() {
    const { modal: { modal } } = this.props
    switch (modal) {
      case CREATE_BET_SUMMARY:
        return this.renderInBody(<CreateBetSummary />)
      case CHANGE_VOLUME:
        return this.renderInBody(<ChangeVolume />)
      case PAYMENT:
        return this.renderInBody(<Payment />)
      case RESOLVE_BET:
        return this.renderInBody(<ResolveBet />)
      case PLACE_BET:
        return this.renderInBody(<ConfirmBet />)
      case INFO:
        return this.renderInBody(<Info />)
      default:
        return null
    }
  }
}

function mapStateToProps({ modal }) {
  return { modal }
}

export default connect(mapStateToProps, {})(Modal)
