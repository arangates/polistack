/* eslint-disable react/prop-types */
import React from 'react'
import { INCREASE, REDUCE } from 'constants/volume.changes'
import { StatusTabs, StatusTab } from 'components/StatusTabs'

const VolumeChangeType = ({ transactionType, changeTransactionType }) => {
  return (
    <StatusTabs>
      <StatusTab
        active={transactionType === INCREASE}
        onClick={() => changeTransactionType(INCREASE)}
      >
        Increase
      </StatusTab>
      <StatusTab
        active={transactionType === REDUCE}
        onClick={() => changeTransactionType(REDUCE)}
      >
        Reduce
      </StatusTab>
    </StatusTabs>
  )
}

export default VolumeChangeType
