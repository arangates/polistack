/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import { Button, Input } from 'components'
import { INCREASE } from 'constants/volume.changes'
import { reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux'
import { updateBetAmount } from 'actions/betActions'
import maxTwoDecimals from 'components/Input/inputValidators'
import { Footer, ModalHeader, Title, Content } from '../Modal'

class ChangeVolume extends Component {
  state = {
    transactionType: INCREASE,
    maxBackAmount: ''
  }

  componentDidMount() {
    this.props.initialize({
      amount: 100
    })
  }

  changeTransactionType = type => this.setState({ transactionType: type })
  updateBetAmount = async _ => {
    await this.props.updateBetAmount(
      this.props.data,
      this.state.transactionType
    )
  }

  render() {
    const { closeModal } = this.props

    return (
      <React.Fragment>
        <ModalHeader>
          <Title>Change volume</Title>
        </ModalHeader>
        <Content>
          <p>Change the volume of your bet.</p>

          <div className="user__volume">
            <h5>New volume</h5>
            <div className="input__group">
              <div className="amount__box">
                <div className="bet__currency" />
                <Field
                  name="amount"
                  component={Input}
                  normalize={maxTwoDecimals}
                  type="text"
                  white
                  placeholder="100"
                />
              </div>
            </div>
          </div>
        </Content>
        <Footer>
          <Button white extended popup onClick={closeModal}>
            Cancel
          </Button>
          <Button blue extended popup onClick={this.updateBetAmount}>
            Change
          </Button>
        </Footer>
      </React.Fragment>
    )
  }
}

ChangeVolume = reduxForm({
  form: 'changeVolume',
  enableReinitialize: true
})(ChangeVolume)

function mapStateToProps({ user }) {
  return { balance: user.profile.balance }
}

export default connect(mapStateToProps, {
  updateBetAmount
})(ChangeVolume)
