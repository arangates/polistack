/* eslint-disable react/prop-types */
import React from 'react'
import { INCREASE } from 'constants/volume.changes'

const MaximumAmount = ({ transactionType, balance, maxBackAmount }) => {
  return (
    <div className="maximum__amount">
      <h6>
        Maximum amount of volume to be{' '}
        {transactionType === INCREASE ? 'increased' : 'reduced'}:
      </h6>
      <span
        className={transactionType === INCREASE ? 'max__amount' : 'min__amount'}
      >
        {transactionType === INCREASE ? `${balance}$` : `${maxBackAmount}$`}
      </span>
    </div>
  )
}

export default MaximumAmount
