/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import ChangeVolume from '../index'
import configureStore from 'redux-mock-store'

const mockStore = configureStore()

const store = mockStore({  user: {
    authenticated: true,
    profile: {
      token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImZpc2gwQGV4YW1wbGUuY29tIiwiZXhwIjoxNTIzOTY2MjA5LCJlbWFpbCI6ImZpc2gwQGV4YW1wbGUuY29tIiwib3JpZ19pYXQiOjE1MjM5NjUzMDl9.9CdvxKBAUALHqYO_zTEomQOMPF47cU-XXJDWX7zT2Fs',
      firstName: 'Fish0',
      lastName: 'Fish0',
      email: 'fish0@example.com',
      balance: 49800,
      id: 1
    },
    errors: {
      non_field_errors: [
        'Unable to log in with provided credentials.'
      ]
    },
    isLoading: true,
    bets: {
      count: 6,
      next: null,
      previous: null,
      results: [
        {
          id: 28,
          yes: '5.00',
          no: '5.00',
          statement: '1',
          ends: null,
          status: 'ENDED',
          created_by: 1,
          created_at: '2018-04-18T07:59:35.840597Z',
          image: '/media/alex-jodoin-246078.jpg',
          user_bet_actions: [
            {
              id: 30,
              side: 'yes',
              bet: 28,
              amount: '12.00',
              created_at: '2018-04-18T07:59:35.847263Z',
              paired_amount: '0.00'
            }
          ],
          listId: '28'
        },
        {
          id: 31,
          yes: '5.00',
          no: '5.00',
          statement: 'asdadasdsad',
          ends: null,
          status: 'ENDED',
          created_by: 1,
          created_at: '2018-04-18T08:15:34.877919Z',
          image: '/media/alex-jodoin-246078_ZyZTwbY.jpg',
          user_bet_actions: [
            {
              id: 33,
              side: 'yes',
              bet: 31,
              amount: '12.00',
              created_at: '2018-04-18T08:15:34.890070Z',
              paired_amount: '0.00'
            }
          ],
          listId: '31'
        },
        {
          id: 32,
          yes: '5.00',
          no: '5.00',
          statement: 'asdasdsadsa',
          ends: null,
          status: 'ENDED',
          created_by: 1,
          created_at: '2018-04-18T08:16:10.558087Z',
          image: '/media/alex-jodoin-246078_Lu40eRW.jpg',
          user_bet_actions: [
            {
              id: 34,
              side: 'yes',
              bet: 32,
              amount: '12.00',
              created_at: '2018-04-18T08:16:10.564907Z',
              paired_amount: '0.00'
            }
          ],
          listId: '32'
        },
        {
          id: 35,
          yes: '5.00',
          no: '5.00',
          statement: 'asdadasdasdasdasdasdasdasdasdasdasdas',
          ends: null,
          status: 'ENDED',
          created_by: 1,
          created_at: '2018-04-18T08:19:23.428952Z',
          image: '/media/alex-jodoin-246078_yNhoNxu.jpg',
          user_bet_actions: [
            {
              id: 37,
              side: 'yes',
              bet: 35,
              amount: '12.00',
              created_at: '2018-04-18T08:19:23.434310Z',
              paired_amount: '0.00'
            }
          ],
          listId: '35'
        },
        {
          id: 36,
          yes: '5.00',
          no: '5.00',
          statement: 'asdzxc',
          ends: null,
          status: 'PENDING',
          created_by: 1,
          created_at: '2018-04-18T09:22:44.275622Z',
          image: null,
          user_bet_actions: [
            {
              id: 38,
              side: 'yes',
              bet: 36,
              amount: '12.00',
              created_at: '2018-04-18T09:22:44.283733Z',
              paired_amount: '0.00'
            }
          ],
          listId: '36'
        },
        {
          id: 1,
          yes: '3.00',
          no: '2.00',
          statement: 'Trump will be reelected in 2020',
          ends: '2010-08-05',
          status: 'ACTIVE',
          created_by: 3,
          created_at: '2018-04-18T10:06:48.247685Z',
          image: '/media/trump.jpg',
          user_bet_actions: [
            {
              id: 28,
              side: 'yes',
              bet: 1,
              amount: '150.00',
              created_at: '2018-04-18T07:23:08.904465Z',
              paired_amount: '150.00'
            },
            {
              id: 29,
              side: 'yes',
              bet: 1,
              amount: '0.00',
              created_at: '2018-04-18T07:24:31.515385Z',
              paired_amount: '0.00'
            },
            {
              id: 39,
              side: 'yes',
              bet: 1,
              amount: '38.00',
              created_at: '2018-04-18T10:17:58.478477Z',
              paired_amount: '0.00'
            },
            {
              id: 40,
              side: 'yes',
              bet: 1,
              amount: '50000.00',
              created_at: '2018-04-18T10:18:49.209149Z',
              paired_amount: '0.00'
            }
          ],
          listId: '1'
        }
      ]
    },
    transactions: {
      count: 5,
      next: null,
      previous: null,
      results: [
        {
          id: 1,
          amount: '100000.00',
          bet: null,
          user: 1,
          created_at: '2018-04-18T07:06:47.886779Z',
          fee: '2.13'
        },
        {
          id: 4,
          amount: '0.00',
          bet: {
            id: 35,
            yes: '5.00',
            no: '5.00',
            statement: 'asdadasdasdasdasdasdasdasdasdasdasdas',
            ends: null,
            created_by: 1,
            image: 'http://localhost:8000/media/alex-jodoin-246078_yNhoNxu.jpg',
            status: 'ENDED'
          },
          user: 1,
          created_at: '2018-04-18T09:04:25.806317Z',
          fee: '0.00'
        },
        {
          id: 5,
          amount: '0.00',
          bet: {
            id: 28,
            yes: '5.00',
            no: '5.00',
            statement: '1',
            ends: null,
            created_by: 1,
            image: 'http://localhost:8000/media/alex-jodoin-246078.jpg',
            status: 'ENDED'
          },
          user: 1,
          created_at: '2018-04-18T09:04:41.192476Z',
          fee: '0.00'
        },
        {
          id: 6,
          amount: '0.00',
          bet: {
            id: 31,
            yes: '5.00',
            no: '5.00',
            statement: 'asdadasdsad',
            ends: null,
            created_by: 1,
            image: 'http://localhost:8000/media/alex-jodoin-246078_ZyZTwbY.jpg',
            status: 'ENDED'
          },
          user: 1,
          created_at: '2018-04-18T09:05:55.814730Z',
          fee: '0.00'
        },
        {
          id: 7,
          amount: '0.00',
          bet: {
            id: 32,
            yes: '5.00',
            no: '5.00',
            statement: 'asdasdsadsa',
            ends: null,
            created_by: 1,
            image: 'http://localhost:8000/media/alex-jodoin-246078_Lu40eRW.jpg',
            status: 'ENDED'
          },
          user: 1,
          created_at: '2018-04-18T09:05:56.395744Z',
          fee: '0.00'
        }
      ]
    }
  },})

describe('ChangeVolume', () => {
  it('should render correctly', () => {
    const output = shallow(<ChangeVolume store={store}/>)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
