import styled from 'styled-components'

const Copyright = styled.div`
  color: #828f97;
  font-size: 0.75rem;
  text-align: center;
  padding: 1.667em 0;
`

export default Copyright
