import styled from 'styled-components'

const Nav = styled.ul`
  display: flex;
  @media screen and (max-width: 768px) {
    flex-direction: column;
    text-align: center;
  }

  li {
    color: #828f97;
    font-size: 0.75rem;
    font-weight: 700;
    @media screen and (max-width: 768px) {
      width: 100%;
      margin-bottom: 1em;
    }

    &:not(:last-child) {
      margin-right: 2.5em;
    }
  }
`

export default Nav
