import styled from 'styled-components'

const Top = styled.div`
  @media screen and (max-width: 768px) {
    flex-direction: column-reverse;
  }
  border-bottom: 1px solid #dfe9ed;
  display: flex;

  justify-content: space-around;
  align-items: center;
  padding: 1.875em 0;
`

export default Top
