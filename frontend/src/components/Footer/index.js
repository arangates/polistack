/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { logoFooter } from 'images'
import { Link, withRouter } from 'react-router-dom'
import { Container } from 'components'
import Top from './Top'
import Copyright from './Copyright'
import Nav from './Nav'
import Wrapper from './Wrapper'
import Svg from '../Svg'

class Footer extends Component {
  isInAuthenticationRoute = () => {
    return (
      this.props.location.pathname === '/login' ||
      this.props.location.pathname === '/register' ||
      this.props.location.pathname === '/forgot' ||
      this.props.location.pathname.startsWith('/password')
    )
  }

  isLandingPageRoute = () => {
    return this.props.location.pathname === '/'
  }

  render() {
    return this.isInAuthenticationRoute() ? null : (
      <Wrapper>
        <Container>
          <Top>
            <Link to="/">
              <Svg icon={logoFooter} />
            </Link>
            <Nav>
              <li>
                <Link to="/faq">FAQ</Link>
              </li>
              <li>
                <Link to="/how-it-works">How it works</Link>
              </li>
              <li>
                <Link to="/news">News</Link>
              </li>
              <li>
                <Link to="/contact">Contact</Link>
              </li>
              <li>
                <Link to="/privacy">Privacy</Link>
              </li>
              <li>
                <Link to="/sitemap">Sitemap</Link>
              </li>
              <li>
                <Link to="/terms">Terms of service</Link>
              </li>
            </Nav>
          </Top>
          {this.isLandingPageRoute() && (
            <Copyright>Copyright © 2019 polistack.com</Copyright>
          )}
        </Container>
      </Wrapper>
    )
  }
}

export default withRouter(Footer)
