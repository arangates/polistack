import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Container } from 'components'
import Breadcrumbs from '../../components/Breadcrumbs'

const Heading = styled.h2`
  font-weight: bold;
  margin-top: 1em;
  margin-bottom: 1em;
  font-size: 2rem;
  &::after {
    background: none;
  }
`
const Title = styled.h1`
  margin-top: 1em;
`

const Image = styled.img`
  display: block;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 2em;
  width: 100%;
`

const Text = styled.div`
  font-size: 1em;
  color: #5a5a5a;
  margin-bottom: 1em;
  a {
    color: blue;
  }
  > p {
    font-size: 1em;
    margin-bottom: 2em;
    font-family: 'Open Sans', sans-serif;
  }
`

const Achievement = ({ image, title, markup }) => (
  <Container>
    <Title>News</Title>
    <Breadcrumbs />
    <Heading>{title}</Heading>
    <Image src={image} alt={title} />
    <Text dangerouslySetInnerHTML={{ __html: markup }} />
  </Container>
)

Achievement.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  markup: PropTypes.string.isRequired
}

export default Achievement
