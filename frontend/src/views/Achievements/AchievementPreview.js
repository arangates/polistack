import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'
import { Link } from 'react-router-dom'
import MediaQuery from 'react-responsive'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: flex-start;
  padding: 1em;
  border: 1px solid rgba(0, 0, 0, 0.1);
  margin-top: 1em;
  box-shadow: 3px 3px 0px 0px #ededed;

  &:last-child {
    margin-bottom: 1em;
  }
`
const MobileContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: flex-start;
  padding: 1em;
  border: 1px solid rgba(0, 0, 0, 0.1);
  margin-top: 1em;
  box-shadow: 3px 3px 0px 0px #ededed;

  &:last-child {
    margin-bottom: 1em;
  }
`

const Image = styled.img`
  order: 0;
  align-self: center;
  height: 10em;
  border-radius: 4px;
`
const MobileImage = styled.img`
  order: 0;
  align-self: center;
  height: 13em;
  border-radius: 4px;
`

const Text = styled.div`
  order: 1;
  margin-left: 1em;

  & > h2 {
    font-weight: bold;

    &::after {
      background: none;
    }
  }

  p {
    font-size: 1em;
    color: #5a5a5a;
  }
`
const MobileText = styled.div`
  order: 1;
  margin-top: 1em;

  & > h2 {
    font-weight: bold;

    &::after {
      background: none;
    }
  }

  p {
    font-size: 1em;
    color: #5a5a5a;
  }
`

const DateText = styled.span`
  font-size: 0.8em;
  color: #cdcdcd;
`

const AchievementPreview = ({ url, title, image, date, markup }) => {
  // moment.locale(intl.locale)
  const fullUrl = `${'/news'}/${url}`
  return (
    <div>
      <MediaQuery query="(max-width: 991px)">
        <Link to={fullUrl}>
          <MobileContainer id={`event-${url}`}>
            <MobileImage src={image} alt={title} />
            <MobileText>
              <h2>{title}</h2>
              <div
                dangerouslySetInnerHTML={{ __html: markup.replace('**', '') }}
              />
              <DateText>{moment(date).format('MMMM YYYY')}</DateText>
            </MobileText>
          </MobileContainer>
        </Link>
      </MediaQuery>
      <MediaQuery query="(min-width: 991px)">
        <Link to={fullUrl}>
          <Container id={`event-${url}`}>
            <Image src={image} alt={title} />
            <Text>
              <h2>{title}</h2>
              <div
                dangerouslySetInnerHTML={{ __html: markup.replace('**', '') }}
              />
              <DateText>{moment(date).format('MMMM YYYY')}</DateText>
            </Text>
          </Container>
        </Link>
      </MediaQuery>
    </div>
  )
}

AchievementPreview.propTypes = {
  url: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  markup: PropTypes.string.isRequired
}

export default AchievementPreview
