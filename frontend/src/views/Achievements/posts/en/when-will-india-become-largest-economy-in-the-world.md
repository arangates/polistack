---
title: When will India become largest economy in the world?
date: 2019-11-17T03:24:00
image: ../static/media/news_4.jpg
---
India is currently the sixth largest economy in the world worth USD 2.7 trillion. This is double of the amount from mid 2015. As a comparison, in the first 55 years of existence, the country increased its GDP by USD 1 trillion. Given the extraordinary pace of GDP growth, forecasted to be 7.4% for 2019, the Indian economy should reach USD 5 trillion by 2025.  It has recently surpassed China as the fastest growing world economy and accounts for 2% of global trade.

The country overtook France as the sixth largest economy in 2018 and is predicted to overtake UK by 2020 to become the fifth largest economy with a GDP of USD 2.9 tillion. India is also among the top Foreign Direct Investments (FDI) destinations in the world having received almost 300 billion FDI in the last five years. Check **Polistack** for a wide range of political economy betting, including betting on how politics impacts the growth of the country. **Polistack** is a political prediction market, where you can stack your expertise with others.

There were some headwinds in the recent years however. Slow industry growth, a constantly high current account and the decreasing value of the rupee could all be observed after the global financial crisis in 2008. 

The availability of credit is also on the decline. Borrowing to businesses is expected to decrease by 3% in 2019 according to Fitch Ratings. The cost of credit might also be a concern, even though the Royal Bank of India had the policy rates cut by 135 basis points in 2019. The transmission mechanism of policy rates to the real interest rates in the economy is a big question – the latter remain rather high. You can check the the betting odds of policy change in India on **Polistack**.

Despite these interim problems India remains viewed as a safe and trustworthy debt issuer – the country has never defaulted on debt in its history.

Prime Minister Narendra Modi has made the ease of doing business and increasing the standard of living as main objectives of his policies. At the same time bureaucracy, taxes and corruption are on a rapid decline. This business-friendly environment allows FDI to rise significantly year to year. PM Modi stated, that his government is commited to further improving the tax regime, which he described as one of the most people-friendly in the world. The period of the last 5 years is commonly viewed as a cleanup of malinvestments: writing off bad balance sheets and reducing overleveraging in the system. 

You can follow how the government of PM Modi continues to improve India’s economy on **Polistack**. Check the betting odds on political events.