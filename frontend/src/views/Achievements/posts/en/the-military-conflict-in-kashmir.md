---
title: The military conflict in Kashmir
date: 2019-11-15T03:24:00
image: ../static/media/news_1.jpg
---
The beginnings of the conflict between India and Pakistan are complex, however they were always centered around the state of Jammu and Kashmir. The Partition of India in 1947 was followed by multiple wars, dated for: 1947, 1965, 1971, 1999. The last war was preceded by both countries having developed nuclear arsenal, leading to an international diplomatic effort to end the military conflict. Despite the war having ended multiple militant groups have been formed up to this day. You can find a wide range of political betting on **Polistack**, including election betting and international relations events betting. **Polistack** is a political prediction market, where you can stack your political expertise with others.

The 2019 part of the military conflict between India and Pakistan in Kashmir, a region currently controlled in parts by each of the countries, started with a suicide attack on 14 February. Jaish-e-Mohammad, a jihadist group from Pakistan, claimed responsibility for the suicide car bombing killing 40 Indian security officers. The government of Pakistan denied any wrongdoing.

Airstrikes launched by both countries followed. The airstrike conducted by India on February 26 in Khyber Pakhtunkhwa, officially targeted at a terrorist camp, did not result in any causalties. The retaliatory airstrike conducted by Pakistan on February 27, resulted in an Indian pilot being briefly imprisoned. 

The same day Pakistan closed their airspace, canceling all commercial flights. It fully reopened on July 15, 2019, causing large financial losses to the country. The Samjhauta Express train, which connects India with Pakistan twice a week was suspended by the latter country for 5 days.

**Imran Khan**, Pakistan’s Prime Minister, has accused the Indian Government of the desire to retaliate against Pakistan to increase their General Election odds. India’s **Bharatiya Janata Party** ruling party won the May 2019 elections. You can check the election betting odds on **Polistack**.

In August consecutive events of the conflict took place. The Indian Government removed the special status of the state of Jammu and Kashmir. Both sides exchanged fire on August 15, resulting in five casualties. Six days later an exchange of gunshots lead to the deaths of a police officer and a terrorist from the Laskar-e-Taiba organization.

You can follow further events and odds in the conflict between India and Pakistan on **Polistack**. 