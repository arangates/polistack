---
title: Current demographics of India
date: 2019-12-15T03:24:00
image: ../static/media/news_2.jpg
---
India is currently the second most populated country in the world, however it is projected that it will outrun China by the year 2024. 

India has a young population since more than half is under the age of 25, while 65% is under the age of 35. The country is male dominant, since the ration of female to male in 2016 was 944:1000. It is also a highly diversified nation in terms of ethnic groups and religions. 

Based on income and gross national product per capita India is one of the poorest countries on Earth, which is also caused by its huge population. According to data from 2010 almost 30% of the population was living below the poverty line.

It has been reported that in 2014, 11 states in India implemented a two-children policy for the purpose of creating a more economically stable country. Currently there are speculations that the policy will be implemented on a national scale. Recently **Rajya Sabha**, also known as the Council of States, proposed the “Population Regulation Bill 2019”. According to the bill, members of families with more than two children will be disqualified from being an elected representative and will receive a reduction in benefits under the Public Distribution System (PDS). Furthermore, government employees would have to commit not to procreate more than two children. 

**Polistack** allows you to bet on political events concerning India, which take place on a national as well as global scale.

It is possible that families who would not follow the new regulations could face further consequences as denial of health care, fees and even jail. In addition, according to the Economic Survey of 2018 there is a desire to have male children over female, which combined with the proposed bill can cause selective and unethical practices.  

You can find a wide range of political betting on **Polistack**, including election betting and international relations events betting. **Polistack** is a political prediction market, where you can set your odds on a political betting. 





