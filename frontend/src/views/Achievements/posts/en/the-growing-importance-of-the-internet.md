---
title: The growing importance of the Internet
date: 2019-11-12T03:24:00
image: ../static/media/news_3.jpg
---
China is the only country with a larger number of monthly active users (MAUs) of Internet than India. According to the Internet and Mobile Association of India there is currently 451 million MAUs in India, which comprises 36% of the population. Approximately half of this number is represented by people living in urban areas. Given the lack of internet access in many rural areas, there is great growth opportunity for Internet there. India has the largest number of people unconnected to the Internet in the world, which makes it the biggest market for technology today.

What is worth mentioning is that 67% of the MAUs are daily active users – this equates to around 300 million users. There is twice as many male Internet users than female users. Two thirds of MAUs in India are between 12 and 29 years of age. The fact that such a large proportion of users is young signifies the huge growth potential in Internet usage in India in the near future. You can find a wide range of political betting at **Polistack**, including betting on legislature regarding technological advancement and the statistics of Internet usage. **Polistack** is a political prediction market, where you can stack your political expertise with others.

A third of users in urban areas accessed Internet daily for more than one hour. In rural part of the country, a third of users admitted to accessing Internet for 15-30 minutes a day. This is most likely impacted by the early stages of Internet in these part of India, including issues like connection quality and affordable cost.

Internet penetration for regions of the country with the highest figures is the following: Delhi-NCR (69%), Kerala (54%),  J&K, Haryana, Himachal Pradesh and Punjab (49%), Tamil Nadu (47%), Maharashtra (43%). Check out **Polistack** to bet on the statistics of Indian politics and changes in society.

Both the Indian Goverment and American tech giants play large parts in spreading out Internet connectivity and technology in India. The Government is planning to install 250 thousand hotspots in rural India. Facebook is planning to install 20 thousand hotspots with Internet avaiable for a symbolic price. Google co-financed free WiFi at 400 train stations throughout the country.

The Indian Government is introducing yet more technology-friendly regulations, allowing Internet to be accessible to everyone. You can check the the betting odds of how politics helps technology adaptation on **Polistack**.

A significant element of India’s online advancement has been **Reliance Jio**, the USD 20 billion investment of Mukesh Ambani, the country’s wealthiest man. The innovative mobile network offered half a year of free 4G Internet for all new users. Almost 350 million users have subscribed to the network since its inception 3 years ago. 

You can follow further events and odds on how politics and technology evolve in India on **Polistack**.


