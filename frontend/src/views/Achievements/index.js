import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { Container, Breadcrumbs } from 'components'
import { compose } from 'redux'
import { connect } from 'react-redux'
import AchievementPreview from './AchievementPreview'
import Achievement from './Achievement'
import { DEFAULT_PAGE_TITLE } from 'constants/globals'

import isString from 'lodash.isstring'
import { Helmet } from 'react-helmet'
import Posts from 'babel-loader!dir-loader-es6!./posts.config.js' // eslint-disable-line

const Achievements = ({ match, hostname, intl }) => {
  let pageTitle = `Polistack | Predict politics | News`
  let postUrl = null
  if (isString(match.params.url) && match.params.url.trim().length > 0) {
    postUrl = match.params.url
  }
  let newsPosts = null
  let post = null

  if (postUrl === null) {
    newsPosts = Object.keys(Posts['en'] || {})
      .filter(key => /\.md$/.test(key))
      .map(key => {
        const source = Posts['en'][key].src.default
        return {
          url: `${key.replace(/\.md$/, '')}`,
          id: source.data.id,
          title: source.data.title,
          image: source.data.image,
          date: source.data.date,
          markup: source.preview
        }
      })
  } else {
    post = Object.keys(Posts['en'] || {})
      .filter(key => /\.md$/.test(key))
      .map(key => {
        const source = Posts['en'][key].src.default
        return {
          url: `${key.replace(/\.md$/, '')}`,
          title: source.data.title,
          image: source.data.image,
          date: source.data.date,
          markup: source.content
        }
      })
      .find(post => post.url === postUrl)
  }

  if (newsPosts !== null && newsPosts.length > 0) {
    return (
      <Fragment>
        <Helmet>
          {isString(pageTitle) ? (
            <title>{pageTitle}</title>
          ) : (
            DEFAULT_PAGE_TITLE
          )}
          <meta
            name="description"
            content="Read the latest predictions about political events, what's going to happen in the world. It’s all about political predictions."
          />
        </Helmet>
        <Container>
          <h1 style={{ marginTop: '1em' }}>News</h1>
          <Breadcrumbs />
          {newsPosts
            .sort((a, b) => new Date(b.date) - new Date(a.date))
            .map(post => <AchievementPreview key={post.url} {...post} />)}
        </Container>
      </Fragment>
    )
  } else if (post !== null && post !== undefined) {
    return (
      <Fragment>
        <Helmet>
          <title>{post.title} </title>
        </Helmet>
        <Achievement {...post} />
      </Fragment>
    )
  } else {
    return <Redirect to="/news" />
  }
}

Achievements.propTypes = {
  match: PropTypes.object,
  hostname: PropTypes.string.isRequired
}

const mapStateToProps = () => ({ hostname: 'polistack' })

export default compose(connect(mapStateToProps))(Achievements)
