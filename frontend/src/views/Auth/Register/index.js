/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Container } from 'components'
import { Helmet } from 'react-helmet'
import Header from '../Common/Header'
import RegisterForm from './RegisterForm'
import LoginContent from '../Common/LoginContent'
import { connect } from 'react-redux'
import { registerUser } from 'actions/userActions'
import { withRouter } from 'react-router-dom'
import RandomBackground from '../AuthBackground'
import { sendEventToGoogleAnalytics } from 'actions/analyticsActions'
import { IS_CLIENT } from '../../../constants/globals'

class Register extends Component {
  register = async userData => {
    const res = await this.props.registerUser(userData)
    if (res && res.status === 201) {
      if (process.env.NODE_ENV === 'production' && IS_CLIENT) {
        try {
          this.props.sendEventToGoogleAnalytics(userData)
        } catch (err) {
          console.log(err)
        }
      }
    }
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>Polistack - Predict politics - Register</title>
        </Helmet>
        <RandomBackground />
        <Container>
          <Header type="login" />
          <LoginContent>
            <RegisterForm onSubmit={this.register} />
          </LoginContent>
        </Container>
      </div>
    )
  }
}

export default connect(null, { registerUser, sendEventToGoogleAnalytics })(
  withRouter(Register)
)
