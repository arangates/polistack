/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Register from '../index'
import configureStore from 'redux-mock-store'

const mockStore = configureStore()

const store = mockStore({})

describe('Register', () => {
  it('should render correctly', () => {
    const output = shallow(<Register store={store} />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
