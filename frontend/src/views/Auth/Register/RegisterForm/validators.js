import { isEmail, equals, isLength } from 'validator'

const validate = ({
  email,
  password,
  passwordConfirm,
  firstName,
  lastName
}) => {
  const errors = {}

  if (email && !isEmail(email)) {
    errors.email = 'Invalid email'
  }

  if (!email) {
    errors.email = 'Required'
  }

  if (!firstName) {
    errors.firstName = 'Required'
  }

  if (!lastName) {
    errors.lastName = 'Required'
  }

  if (!password) {
    errors.password = 'Required'
  }

  if (password && !isLength(password, { min: 8, max: 128 })) {
    errors.password = 'Password not strong enough (at least 8 characters)'
  }

  if (!passwordConfirm) {
    errors.passwordConfirm = 'Required'
  }

  if (password && passwordConfirm && !equals(password, passwordConfirm)) {
    errors.passwordConfirm = 'Passwords do not match'
  }

  return errors
}

export default validate
