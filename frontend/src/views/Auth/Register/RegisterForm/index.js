/* eslint-disable no-class-assign,react/prop-types */
import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import { Input, InputGroup, Button } from 'components'
import Form from '../../Common/Form'
import FormButtons from '../../Common/FormButtons'
import Heading from '../../Common/Heading'
import FieldLabel from '../../Common/FieldLabel'
import Details from '../../Common/Details'
import PasswordGroup from '../../Common/PasswordGroup'
import validate from './validators'

class RegisterForm extends Component {
  render() {
    const { handleSubmit, dirty, submitting, invalid } = this.props

    return (
      <Form onSubmit={handleSubmit}>
        <Heading>Sign up</Heading>
        <Details>Enter your details below</Details>

        <InputGroup>
          <FieldLabel>Email</FieldLabel>
          <Field
            name="email"
            component={Input}
            type="text"
            white
            placeholder="Email"
          />
        </InputGroup>

        <InputGroup>
          <FieldLabel>First name</FieldLabel>
          <Field
            name="firstName"
            component={Input}
            type="text"
            white
            placeholder="First name"
          />
        </InputGroup>

        <InputGroup>
          <FieldLabel>Last name</FieldLabel>
          <Field
            name="lastName"
            component={Input}
            type="text"
            white
            placeholder="Last name"
          />
        </InputGroup>

        <InputGroup>
          <PasswordGroup>
            <FieldLabel>Password</FieldLabel>
          </PasswordGroup>
          <Field
            name="password"
            component={Input}
            type="password"
            white
            placeholder="********"
          />
        </InputGroup>

        <InputGroup>
          <PasswordGroup>
            <FieldLabel>Confirm your password</FieldLabel>
          </PasswordGroup>
          <Field
            name="passwordConfirm"
            component={Input}
            type="password"
            white
            placeholder="********"
          />
        </InputGroup>

        <FormButtons>
          <Button
            blue
            disabled={!dirty || submitting || invalid}
            isLoading={submitting}
          >
            Sign up
          </Button>
        </FormButtons>
      </Form>
    )
  }
}

RegisterForm = reduxForm({
  form: 'register',
  validate
})(RegisterForm)

export default RegisterForm
