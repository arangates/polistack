import React, { Component } from 'react'
import styled from 'styled-components'

import {
  backgroundImageFirst,
  backgroundImageSecond,
  backgroundImageThird
} from 'constants/images'

const AuthBackground = styled.div`
  height: 100vh;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: absolute;
  left: 0;
  transition: 0.3s ease-in-out;

  @media screen and (min-width: 768px) {
    width: 40%;
  }

  @media screen and (min-width: 992px) {
    width: 50%;
  }
`

class RandomBackground extends Component {
  render() {
    const backgroundImage = [
      backgroundImageFirst,
      backgroundImageSecond,
      backgroundImageThird
    ]

    const randomImage = Math.floor(Math.random() * backgroundImage.length)
    return (
      <AuthBackground
        style={{ backgroundImage: `url(${backgroundImage[randomImage]})` }}
      />
    )
  }
}
export default RandomBackground
