/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Row, Container } from 'components'
import ResetPasswordForm from './ResetPasswordForm'
import LoginContent from '../Common/LoginContent'
import userService from 'services/user'
import Header from '../Common/Header'
import RandomBackground from '../AuthBackground'
import { openInfoModal } from 'actions/modalActions'
import { connect } from 'react-redux'

class ResetPassword extends Component {
  resetPassword = async passwords => {
    const { uid, token } = this.props.match.params
    const passwordData = {
      uid: uid,
      token: token,
      ...passwords
    }
    const res = await userService.updatePassword(passwordData)
    if (res === 204) {
      this.props.openInfoModal(
        'Thank you',
        'Your password has been reset successfully!',
        {
          link: '/login',
          text: 'Go to login page'
        }
      )
    } else {
    }
  }

  render() {
    return (
      <Row>
        <RandomBackground />
        <Container>
          <Header type="register" />
          <LoginContent>
            <ResetPasswordForm onSubmit={this.resetPassword} />
          </LoginContent>
        </Container>
      </Row>
    )
  }
}

export default connect(null, { openInfoModal })(ResetPassword)
