/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Input, InputGroup, Button } from 'components'
import { reduxForm, Field } from 'redux-form'
import Form from '../../Common/Form'
import FormButtons from '../../Common/FormButtons'
import Heading from '../../Common/Heading'
import FieldLabel from '../../Common/FieldLabel'
import validate from './validators'
import Details from '../../Common/Details'

class ResetPasswordForm extends Component {
  render() {
    const { handleSubmit } = this.props

    return (
      <Form onSubmit={handleSubmit}>
        <Heading>Reset password</Heading>
        <Details>Enter password of minimum 8 charaters long</Details>
        <InputGroup>
          <FieldLabel>New Password</FieldLabel>
          <Field
            name="new_password"
            component={Input}
            type="password"
            white
            placeholder="********"
          />
        </InputGroup>

        <InputGroup>
          <FieldLabel>Confirm New Password</FieldLabel>
          <Field
            name="re_new_password"
            component={Input}
            type="password"
            white
            placeholder="********"
          />
        </InputGroup>

        <FormButtons>
          <Button blue>Reset Password</Button>
        </FormButtons>
      </Form>
    )
  }
}

ResetPasswordForm = reduxForm({
  form: 'resetPassword',
  validate
})(ResetPasswordForm)

export default connect(null)(ResetPasswordForm)
