import { isLength, equals } from 'validator'
const validate = ({
  new_password: password,
  re_new_password: passwordConfirm
}) => {
  const errors = {}

  if (!password) {
    errors.new_password = 'Required'
  }

  if (password && !isLength(password, { min: 8, max: 128 })) {
    errors.new_password = 'Password not strong enough (at least 8 characters)'
  }

  if (!passwordConfirm) {
    errors.re_new_password = 'Required'
  }

  if (password && passwordConfirm && !equals(password, passwordConfirm)) {
    errors.re_new_password = 'Passwords do not match'
  }

  return errors
}

export default validate
