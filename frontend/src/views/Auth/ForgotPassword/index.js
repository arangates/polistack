/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Row, Container } from 'components'
import Header from '../Common/Header'
import ForgotPasswordForm from './ForgotPasswordForm'
import LoginContent from '../Common/LoginContent'
import userService from 'services/user'
import RandomBackground from '../AuthBackground'

class ForgotPassword extends Component {
  sendResetLink = email => {
    userService.forgotPassword(email)
  }

  render() {
    return (
      <Row>
        <RandomBackground />

        <Container>
          <Header type="register" />
          <LoginContent>
            <ForgotPasswordForm onSubmit={this.sendResetLink} />
          </LoginContent>
        </Container>
      </Row>
    )
  }
}

export default ForgotPassword
