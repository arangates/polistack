/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import { openInfoModal } from 'actions/modalActions'
import { connect } from 'react-redux'
import { Input, InputGroup, Button } from 'components'
import { reduxForm, Field } from 'redux-form'
import Form from '../../Common/Form'
import FormButtons from '../../Common/FormButtons'
import Heading from '../../Common/Heading'
import FieldLabel from '../../Common/FieldLabel'
import Details from '../../Common/Details'
import validate from './validators'

class ForgotPasswordForm extends Component {
  handleClick = () => {
    this.props.openInfoModal(
      'Thank you',
      'An email with a password reset link has been sent. Please check your inbox.'
    )
  }
  render() {
    const { handleSubmit } = this.props

    return (
      <Form onSubmit={handleSubmit}>
        <Heading>Forgot password?</Heading>
        <Details>
          Enter your login (email address) below. We will send you an email with
          a password reset link.
        </Details>
        <InputGroup>
          <FieldLabel>Login (email)</FieldLabel>
          <Field
            name="email"
            component={Input}
            type="text"
            white
            placeholder="login (email)"
          />
        </InputGroup>

        <FormButtons>
          <Button blue onClick={this.handleClick}>
            Send
          </Button>
        </FormButtons>
      </Form>
    )
  }
}

ForgotPasswordForm = reduxForm({
  form: 'forgotPassword',
  validate
})(ForgotPasswordForm)

export default connect(null, { openInfoModal })(ForgotPasswordForm)
