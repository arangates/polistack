import { isEmail } from 'validator'

const validate = ({ email }) => {
  const errors = {}

  if (email && !isEmail(email)) {
    errors.email = 'Invalid email'
  }

  if (!email) {
    errors.email = 'Required'
  }

  return errors
}

export default validate
