import styled from 'styled-components'

const FieldLabel = styled.h6`
  color: #102b3b;
  font-size: 0.75rem;
  font-weight: 500;
  margin-bottom: 0.8334em;
  text-transform: uppercase;
`

export default FieldLabel
