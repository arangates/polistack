import styled from 'styled-components'

const Details = styled.h4`
  font-size: 1rem;
  font-weight: 300;
  margin-bottom: 2.5em;
`

export default Details
