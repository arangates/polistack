import styled from 'styled-components'

const Heading = styled.h2`
  color: #102b3b;
  font-size: 1.375rem;
  font-weight: 700;
  margin-bottom: 0.4545em;
`

export default Heading
