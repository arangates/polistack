import styled from 'styled-components'

const Account = styled.div`
  color: #828f97;
  display: flex;
  align-items: center;

  > span {
    font-size: 0.5rem;
    margin-right: 1.3333em;
  }
`

export default Account
