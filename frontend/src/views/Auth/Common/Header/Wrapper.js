import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const Wrapper = styled.div`
  ${flex('space-between', 'center')};
  padding: 1.25em 1.875em;
  position: relative;
`

export default Wrapper
