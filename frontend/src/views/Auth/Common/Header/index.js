import React from 'react'
import { loginScreenLogo, loginScreenLogoMobile } from 'images'
import { Link } from 'react-router-dom'
import Wrapper from './Wrapper'
import Logo from './Logo'
import Account from './Account'
import MediaQuery from 'react-responsive'
import { Svg, Button } from 'components'

const Header = ({ type }) => {
  const renderRegisterHeader = () => (
    <Wrapper>
      <Logo>
        <Link to="/">
          <MediaQuery query="(max-width: 767px)">
            <Svg icon={loginScreenLogoMobile} />
          </MediaQuery>
          <MediaQuery query="(min-width: 768px)">
            <Svg icon={loginScreenLogo} />
          </MediaQuery>
        </Link>
      </Logo>
      <Account>
        <span>Need an account?</span>
        <Link to="/register">
          <Button white>Create account</Button>
        </Link>
      </Account>
    </Wrapper>
  )

  const renderLoginHeader = () => (
    <Wrapper>
      <Logo>
        <Link to="/">
          <MediaQuery query="(max-width: 767px)">
            <Svg icon={loginScreenLogoMobile} />
          </MediaQuery>
          <MediaQuery query="(min-width: 768px)">
            <Svg icon={loginScreenLogo} />
          </MediaQuery>
        </Link>
      </Logo>

      <Account>
        <span>Already have an account?</span>
        <Link to="/login">
          <Button white>Log in</Button>
        </Link>
      </Account>
    </Wrapper>
  )

  return type === 'register' ? renderRegisterHeader() : renderLoginHeader()
}

export default Header
