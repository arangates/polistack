import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const PasswordGroup = styled.div`
  ${flex('space-between', 'center')} a {
    color: #828f97;
    font-size: 0.75rem;
    display: block;
    text-decoration: underline;
    margin-bottom: 1.25em;
  }
`

export default PasswordGroup
