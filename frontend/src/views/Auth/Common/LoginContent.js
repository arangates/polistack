import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const LoginContent = styled.div`
  ${flex('center', 'center')};
  padding: 2em 1.875em;

  @media screen and (min-width: 768px) {
    ${flex('flex-end', 'center')};
    min-height: 20em;
    padding: 0 6em 0 0;
  }
`

export default LoginContent
