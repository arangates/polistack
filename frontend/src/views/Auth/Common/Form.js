import styled from 'styled-components'

const Form = styled.form`
  color: #828f97;
  max-width: 22.5em;
  width: 100%;
`

export default Form
