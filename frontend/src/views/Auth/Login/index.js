/* eslint-disable react/prop-types,no-unused-expressions */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import { loginUser } from 'actions/userActions'
import { Container } from 'components'
import Header from '../Common/Header'
import LoginContent from '../Common/LoginContent'
import RandomBackground from '../AuthBackground'

import LoginForm from './LoginForm'

class Login extends Component {
  login = userData => this.props.loginUser(userData)

  componentDidMount() {
    this.checkIfUserLoggedIn()
  }
  checkIfUserLoggedIn = () => !!this.props.user.profile.token

  render() {
    return this.props.user.profile.token ? (
      <Redirect to="/" />
    ) : (
      <>
        <Helmet>
          <title>Polistack - Predict politics - Log in</title>
        </Helmet>
        <RandomBackground />
        <Container>
          <Header type="register" />
          <LoginContent>
            <LoginForm onSubmit={this.login} />
          </LoginContent>
        </Container>
      </>
    )
  }
}

function mapStateToProps({ user }) {
  return { user }
}

export default connect(mapStateToProps, { loginUser })(withRouter(Login))
