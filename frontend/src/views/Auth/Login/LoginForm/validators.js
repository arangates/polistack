import { isEmail, isEmpty } from 'validator'

const validate = ({ email, password }) => {
  const errors = {}

  if (email && !isEmail(email)) {
    errors.email = 'Invalid email'
  }

  if (!email || isEmpty(email)) {
    errors.email = 'Required'
  }

  if (!password || isEmpty(password)) {
    errors.password = 'Required'
  }

  return errors
}

export default validate
