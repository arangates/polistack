/* eslint-disable no-class-assign,react/prop-types,no-unused-vars */
import React, { Component } from 'react'
import Button from 'components/Button'
import { Link } from 'react-router-dom'
import { Field, reduxForm } from 'redux-form'
import { Input, InputGroup } from 'components'
import Form from '../../Common/Form'
import FormButtons from '../../Common/FormButtons'
import Heading from '../../Common/Heading'
import FieldLabel from '../../Common/FieldLabel'
import Details from '../../Common/Details'
import PasswordGroup from '../../Common/PasswordGroup'
import validate from './validators'

class LoginForm extends Component {
  render() {
    const { handleSubmit, dirty, submitting, invalid } = this.props

    return (
      <Form onSubmit={handleSubmit}>
        <Heading>Sign in</Heading>
        <Details>Enter your details below</Details>

        <InputGroup>
          <FieldLabel>Email</FieldLabel>
          <Field
            name="email"
            component={Input}
            type="text"
            white
            placeholder="Email"
          />
        </InputGroup>

        <InputGroup>
          <FieldLabel>Password</FieldLabel>
          <Field
            name="password"
            component={Input}
            type="password"
            white
            placeholder="********"
          />
        </InputGroup>

        <InputGroup style={{ marginBottom: 0 }}>
          <PasswordGroup>
            <Link to="/forgot">Forgot password?</Link>
          </PasswordGroup>
        </InputGroup>

        <FormButtons>
          <Button
            blue
            type="submit"
            disabled={!dirty || submitting || invalid}
            isLoading={submitting}
          >
            Log in
          </Button>
        </FormButtons>
      </Form>
    )
  }
}

LoginForm = reduxForm({
  form: 'login',
  validate
})(LoginForm)

export default LoginForm
