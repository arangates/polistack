/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { activateUser } from 'actions/userActions'
import { Loader, Container, Button } from 'components'
import { Redirect, Link } from 'react-router-dom'
import Wrapper from './Wrapper'
import Heading from './Heading'

class Activate extends Component {
  state = {
    uid: null,
    token: null,
    isLoading: true,
    activated: false
  }

  async componentDidMount() {
    const { uid, token } = this.props.match.params
    const res = await this.props.activateUser({ uid, token })
    if (res === 204) {
      this.setState({
        isLoading: false,
        activated: true
      })
    } else {
      this.setState({
        isLoading: false,
        activated: false
      })
    }
  }

  render() {
    return (
      <Wrapper>
        <Container>
          {this.state.isLoading ? (
            <Loader />
          ) : this.state.activated ? (
            <Heading>
              <h1>Your account is activated</h1>
              <Link to="/login" className="nav__item">
                <Button white>Log in</Button>
              </Link>
            </Heading>
          ) : (
            <Redirect to="/404" />
          )}
        </Container>
      </Wrapper>
    )
  }
}

export default connect(null, { activateUser })(Activate)
