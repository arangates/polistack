import styled from 'styled-components'

const Heading = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: #293244;
  margin-top: 1em;
  padding: 2em;
  > a {
    margin-top: inherit;
  }
  @media screen and (max-width: 576px) {
    > h1 {
      font-size: larger;
    }
  }
`

export default Heading
