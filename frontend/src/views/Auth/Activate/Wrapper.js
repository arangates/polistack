import styled from 'styled-components'

const Wrapper = styled.div`
  padding: 3em 0;
  height: 81.5vh;
  display: flex;
  align-items: center;
  justify-content: center;
`

export default Wrapper
