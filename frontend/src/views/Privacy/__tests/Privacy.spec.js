/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Privacy from '../index'

describe('Privacy', () => {
  it('should render correctly', () => {
    const output = shallow(<Privacy />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
