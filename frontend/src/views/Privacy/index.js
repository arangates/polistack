import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'

import { Container } from 'components'
import Wrapper from './Wrapper'
import Heading from './Heading'
import { Section, Title } from './Section'

class Privacy extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  render() {
    const { host, protocol } = this.props
    const canonicalUrl = `${protocol}://${host}/privacy`
    return (
      <Wrapper>
        <Helmet>
          <title>Polistack - Predict politics - Privacy Policy</title>
          <link rel="canonical" href={canonicalUrl} />
        </Helmet>
        <Container>
          <Heading>
            <h1>PRIVACY POLICY</h1>
            <h3>Last updated on August, 31, 2019</h3>

            <p>
              Reverence Technologies ("Reverence," the"Company," “Polistack,”
              “the Operator,” "we," "us," and "our,") respect your privacy and
              are committed to protecting it through our compliance with this
              privacy policy. This policy describes the types of information
              that we may collect from you when you access or use our Website
              www.polistack.com (“The Site”) and our practices for collecting,
              using, maintaining, protecting and disclosing that information.
              <br />
              <br />
              In the course of registering for, using and participating on the
              Site you may be required to give your name, residence address,
              workplace address, email address, date of birth, educational
              qualifications and similar Personal Information("Personal
              Information").<br />
              <br />
              Please read this policy carefully to understand our policies and
              practices regarding your information and how we will treat it.If
              you do not agree with our policies and practices, your choice is
              not to use the Site. By accessing or using the Site, you agree to
              this privacy policy. This policy may change from time to time,
              your continued use of the Site after we make changes is deemed to
              be acceptance of those changes, so please check the policy
              periodically for updates.<br />
              <br />
            </p>
          </Heading>
          <Section>
            <Title>INFORMATION COLLECTED BY US</Title>
            <p>
              We collect Personal Information directly from you, when you
              provide it to us and/or automatically when you browse throughthe
              Site. The information provided directly by you includes your name,
              addresses, email address, date of birth, educational
              qualifications, phone numbers, and other similar information.{' '}
              <br />
              <br />
              The information automatically collected by us when you browse
              through the Site include usage details, computer device
              information, location information, IP addresses and other
              information collected through cookies, web beacons and other
              tracking technologies.
            </p>
          </Section>
          <Section>
            <Title>USE OF THE INFORMATION COLLECTED BY US</Title>
            <p>
              The information that you provide to us is used to enhance your
              usage of the Site as well as to personalise your experience while
              browsing the Site. The information is also used to display
              relevant advertisements, provide support and communicate with you
              and to comply with our legal obligations. We may use the
              information we obtain about you to:<br />
              <br />
              a. Register you for membership at the Site, and manageand maintain
              your account on the Site;<br />
              b. Enable you to communicate with us;<br />
              c. Provide the Service;<br />
              d. Respond to your questions and comments and provide User
              support;<br />
              e. Communicate with you about our services, offers, events and
              promotions;<br />
              f. Publish your testimonials about the Site, including on our
              websites and blogs, and on social networks (if we choose to
              publish your testimonial, we will include only your first name,
              last initial, city and state);<br />
              g. Manage your participation in our events and otherpromotions;<br />
              h. Tailor our services to suit your personal interests and the
              manner in which visitors use our sites, applications and social
              media assets;<br />
              i. Operate, evaluate and improve the Service offered by us and
              your Participation experience;<br />
              j. Analyze and enhance our marketing communications andstrategies
              (including by identifying when emails sent to you have been
              received and read);<br />
              k. Analyze trends and statistics regarding visitors’ use of our
              sites, mobile applications and social media assets, and the
              purchases visitors make on our sites;<br />
              l. Protect against and prevent fraud, unauthorized transactions,
              claims and other liabilities, and manage risk exposure, including
              by identifying potential hackers and other unauthorizedusers;<br />
              m. Enforce our Site Terms of Use;<br />
              n. Comply with applicable legal requirements and industry
              standards and our policies.<br />
              <br />
              We may combine the information we collect with publicly available
              information and information we receive from business partners and
              other third parties. We may use that combined information to
              enhance and personalize your experience with us, to communicate
              with you about services and events that may be of interest to you,
              for other promotional purposes, and for other purposes described
              in this section. <br />
              <br />
              You may choose to opt-out of promotional communication that you
              receive from us by following the unsubscribe instructions within
              each communication. You may not choose to opt-out of
              non-promotional communications. If you wish not to receive any
              non-promotional communication from us, you may choose toterminate
              your account with us.<br />
              <br />
              We also may use the information we obtain about you in other ways
              for which we provide specific notice at the time of collection.
            </p>
          </Section>
          <Section>
            <Title>INFORMATION WE SHARE</Title>
            <p>
              We do not sell, rent or otherwise disclose Personal Information
              about you, except as described in this Privacy Policy.<br />
              <br />
              We may share the personal information we collect with our
              affiliate companies, business partners, franchisees, marketing
              agents, ad network vendors and their participants, and otherthird
              parties for the purposes described in this Privacy Policy,
              including to communicate with you about services, offers, events
              and promotions that we believe may be of interest to you. <br />
              <br />
              We also may disclose information about you (i) if we are required
              to do so by law or legal process (such as a court order), (ii) in
              response to a request by law enforcement authorities for the
              purposes of verification of identity, prevention, detection,
              investigation including cyber incidents, prosecution and
              punishment of offences or (iii) when we believe disclosure is
              necessary or appropriate to prevent physical harm or financial
              loss or in connection with an investigation of suspected oractual
              illegal activity. <br />
              <br />
              We also reserve the right to transfer personal information we have
              about you in the event we sell, merge or transfer all or a portion
              of our business or assets. We would not be obliged to provide you
              with a prior notice of such sale, merger or transfer and you
              hereby consent to the consequent transfer of your personal
              information to the transferee entity. Following such a sale,
              merger or transfer, you may contact the entity to which we
              transferred your personal information with any inquiries
              concerning the processing of that information. In the event ofour
              bankruptcy, insolvency, administration or enforcement of any of
              our creditors' rights generally which lead to any such similar
              action, we would not have control over the manner in which your
              personal information is used. In such an event your personal
              information may be treated as any other asset of ours and maybe
              transferred to or shared with any third party and may be used in a
              manner not contemplated in this Privacy Policy without obtaining
              your consent or having provided a notice to you.
            </p>
          </Section>
          <Section>
            <Title>
              YOUR CHOICES ABOUT HOW WE USE AND SHARE YOUR INFORMATION
            </Title>
            <p>
              We strive to provide you with choices regarding the personal
              information you provide to us. We offer you certain choicesabout
              what information we collect from you, how we use and disclose the
              information, and how we communicate with you.<br />
              <br />
              You may withdraw any consent you previously provided to us or
              object at any time on legitimate grounds to the processing ofyour
              personal information. We will apply your preferences going
              forward. In some circumstances, withdrawing your consent to the
              Site’s use or disclosure of your personal information will mean
              that you cannot take advantage of certain offerings or site
              features.<br />
              <br />
              The following choices with respect to your information areoffered
              by us: <br />
              <br />
              Sharing Information with Business Partners: You may direct us to
              not share your Personal Information with our business partnersfor
              those partners’ own purposes. To do so, please email us at
              support@polistack.com. <br />
              <br />
              Marketing Emails: You may choose not to receive marketing email
              communications from us by clicking on the unsubscribe link in the
              marketing emails or by adjusting your email preferences using the
              online account you may establish on our sites.<br />
              <br />
              Cookies: Most browsers will tell you how to stop accepting new
              cookies, how to be notified when you receive a new cookie, andhow
              to disable existing cookies. Please note, however, that without
              cookies you may not be able to take full advantage of all of our
              sites’ features. In addition, disabling cookies may cancel
              opt-outs that rely on cookies, such as web analytics or targeted
              advertising opt-outs.
            </p>
          </Section>
          <Section>
            <Title>ACCESS AND CORRECTION</Title>
            <p>
              You may obtain a copy of certain Personal Information we maintain
              about you or update or correct inaccuracies in that information
              using the online account you may establish on the Site.In
              addition, if you believe other personal information we maintain
              about you is inaccurate, or if you would like to review, changeor
              delete any Personal Information, you may request that we correct
              or amend the information by contacting us by using the “Contact
              Us” link provided at the bottom of every page. If we deny an
              access request, we will notify you of the reasons for the denial.
            </p>
          </Section>
          <Section>
            <Title>HOW WE PROTECT PERSONAL INFORMATION</Title>
            <p>
              WWe maintain administrative, technical and physical safeguards
              designed to assist us in protecting the personal information we
              collect against accidental, unlawful or unauthorized destruction,
              loss, alteration, access, disclosure or use. <br />
              <br />
              Please note that no electronic transmission of information can be
              entirely secure. We cannot guarantee that the security measures we
              have in place to safeguard personal information will never be
              defeated or fail, or that those measures will always besufficient
              or effective. <br />
              <br />
              To further protect yourself, you should safeguard your account
              username and password and not share that information with anyone.
              You should also sign off your account and close your browser
              window when you have finished your visit to our site. Please note
              that we will never ask for your password via email.
            </p>
          </Section>
          <Section>
            <Title>CHILDREN POLICY</Title>
            <p>
              We do not direct our Site to children under the age of eighteen.
              We require registered users of the site to be at least eighteen
              years old. If we learn that a user is under eighteen years of age,
              we will promptly delete any personal information that the
              individual has provided to us, apart from taking any other
              recourse as stipulated under the terms and conditions.
            </p>
          </Section>
          <Section>
            <Title>UPDATES TO THIS PRIVACY POLICY</Title>
            <p>
              This Privacy Policy may be updated periodically and without prior
              notice to you to reflect changes in our personal information
              practices. We will post a prominent notice on The Site to notify
              you of any significant changes to our Privacy Policy and indicate
              at the bottom of the policy when it was most recently updated.
            </p>
          </Section>
          <Section>
            <Title>THIRD PARTY LINKS</Title>
            <p>
              The Site may contain links to third-party websites. Your use of
              these features may result in the collection, processing orsharing
              of information about you, depending on the feature. Please be
              aware that we are not responsible for the content or privacy
              practices of other websites or services which may be linked on our
              services. We do not endorse or make any representations about
              third-party websites or services. Our Privacy Policy does not
              cover the information you choose to provide to or that is
              collected by these third parties. We strongly encourage you to
              read such third parties’ privacy policies.
            </p>
          </Section>
          <Section>
            <Title>DATA RETENTION AND ACCOUNT TERMINATION</Title>
            <p>
              You can close your account by visiting your profile settings page
              on The Site. We will remove your public posts from view and/or
              dissociate them from your account profile, but we may retain
              information about you for the purposes authorized under this
              Privacy Policy unless prohibited by law. Thereafter, we will
              either delete your Personal Information or de-identify it so that
              it is anonymous and not attributed to your identity.
            </p>
          </Section>
          <Section>
            <Title>DATA TRANSFERS</Title>
            <p>
              The information we obtain from or about you may be processed and
              stored on our various servers located across the globe, which may
              provide for different data protection rules than the country in
              which you reside. Each location may provide for different data
              protection rules than the country in which you reside. We will
              protect your information as described in this Privacy Policy. By
              using the Site, you consent to the collection, transfer, use,
              storage and disclosure of your information as described in this
              Privacy Policy, including to the transfer of your information
              outside of your country of residence.
            </p>
          </Section>
          <Section>
            <Title>CONTACT US</Title>
            <p>
              If you have any questions or comments about this Privacy Policy,
              or if you would like us to update information we have about you or
              your preferences, please contact us by email at
              support@polistack.com.<br />
              <br />
              If you notice any discrepancies in the information provided to us
              or have any grievance against us, you may contact us at
              support@polistack.com.
            </p>
          </Section>
        </Container>
      </Wrapper>
    )
  }
}

Privacy.propTypes = {
  host: PropTypes.string,
  protocol: PropTypes.string
}

const mapStateToProps = ({ site: { host, protocol } }) => ({ host, protocol })

export default connect(mapStateToProps)(Privacy)
