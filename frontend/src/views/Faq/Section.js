import styled from 'styled-components'

const Section = styled.div`
  margin-top: 2em;
`

const Title = styled.h3`
  font-size: 1.25rem;
  font-weight: 500;
  margin-bottom: 0.5em;
`

export { Section, Title }
