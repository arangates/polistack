import styled from 'styled-components'

const Heading = styled.div`
  border-top: 1px solid #293244;
  color: #293244;
  margin-top: 1em;
  padding-top: 2em;

  h3 {
    font-weight: 400;

    &::after {
      content: '';
      background: #e65a5e;
      display: block;
      height: 0.071em;
      width: 5em;
      margin: 0.5em 0;
    }
  }
`

export default Heading
