/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Faq from '../index'

describe('Faq', () => {
  it('should render correctly', () => {
    const output = shallow(<Faq />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
