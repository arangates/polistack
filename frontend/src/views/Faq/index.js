import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Container } from 'components'
import Wrapper from './Wrapper'
import Heading from './Heading'
import { Section, Title } from './Section'

class Faq extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  // https://developers.google.com/search/docs/data-types/faqpage
  generateJSONLDFaq = () => {
    return {
      '@context': 'https://schema.org',
      '@type': 'FAQPage',
      mainEntity: [
        {
          '@type': 'Question',
          name: 'What is Polistack?',
          acceptedAnswer: {
            '@type': 'Answer',
            text:
              '<p> Polistack is an on-line prediction platform that offers prediction markets on political events in India. It is a research-based game of skill platform offered only to the residents of India with expert knowledge of the local politics.<br /> <br /> Data on the political predictions from the platform is made available to universities involved in the research of politics in India. This market data can be used in scientific research, providing real-world insights into the political sentiment. For more information on how to become a Polistack research partner, please contact us at support@polistack.com. </p> '
          }
        },
        {
          '@type': 'Question',
          name: 'How can I make payments / deposit money to my account?',
          acceptedAnswer: {
            '@type': 'Answer',
            text:
              ' We only offer political events for Indian residents with expert knowledge in politics.'
          }
        },
        {
          '@type': 'Question',
          name:
            'Is there a minimum / maximum price of an event that I can trade?',
          acceptedAnswer: {
            '@type': 'Answer',
            text:
              'Yes, you can trade at a price of minimum 1 INR and maximum 99 INR'
          }
        },
        {
          '@type': 'Question',
          name: 'What does the price of a given event depend on?',
          acceptedAnswer: {
            '@type': 'Answer',
            text:
              'The market price of a given event fluctuates based on the likelihood a given event turns out to be true and on supply/demand dynamics of the trades of participants (that is the quantity and price level of orders placed by participants). However once the event is resolved, the market price is always either 0 INR or 100 INR. '
          }
        }
      ]
    }
  }

  render() {
    const { host, protocol } = this.props
    const canonicalUrl = `${protocol}://${host}/faq`
    return (
      <Wrapper>
        <Helmet>
          <title>
            Polistack - Predict politics - Frequently Asked Questions
          </title>
          <link rel="canonical" href={canonicalUrl} />
        </Helmet>
        <Helmet>
          <script type="application/ld+json">
            {JSON.stringify(this.generateJSONLDFaq())}
          </script>
        </Helmet>
        <Container>
          <Heading>
            <h1>FAQ</h1>
            <h3>Last updated on August 20, 2019</h3>
          </Heading>
          <Section>
            <Title>What is Polistack?</Title>
            <p>
              Polistack is an on-line prediction platform that offers prediction
              markets on political events in India. It is a research-based game
              of skill platform offered only to the residents of India with
              expert knowledge of the local politics.<br />
              <br />
              Data on the political predictions from the platform is made
              available to universities involved in the research of politics in
              India. This market data can be used in scientific research,
              providing real-world insights into the political sentiment. For
              more information on how to become a Polistack research partner,
              please contact us at support@polistack.com.
            </p>
          </Section>
          <Section>
            <Title>How does Polistack work?</Title>
            <p>
              Participants of the platform trade on political events . Only
              events with two possible outcomes: true and false are present in
              the platform. <br />
              <br />
              For each participant who predicts that a given event will take
              place (true), there must be another person taking the position
              that the given event will not take place (false). <br />
              <br />
              When a statement is resolved, it will either be true (we then
              assign it a value of 100 INR) or false (we then assign it a value
              of 0 INR). <br />
              <br />
              <br />
              Example: Participant A, after doing extensive research, is certain
              that Prime Minister Narendra Modi will be reelected in 2024. He
              buys the event "Modi will be reelected in 2024" at the price of 60
              INR. <br />
              <br />
              If the statement turns out to be true: (Modi is going to be
              reelected in 2024) then participant A makes a gross profit of 40
              INR, as the price of the event increased to 100 INR. <br />
              <br />
              If the statement turns out to be false: participant A makes a loss
              of 60 INR, as the price of the event decreased to 0 INR.
            </p>
          </Section>
          <Section>
            <Title>What kind of events are present on Polistack?</Title>
            <p>
              We only offer political events for Indian residents with expert
              knowledge in politics.
            </p>
          </Section>
          <Section>
            <Title>
              How can I make payments / deposit money to my account?
            </Title>
            <p>
              You can make payments to your account with debit/credit cards and
              e-wallets. We will be adding more payment methods soon.
            </p>
          </Section>
          <Section>
            <Title>How can I place a trade?</Title>
            <p>
              1. Find the event you are interested in at https://polistack.com/.<br />
              <br />
              2. Click "Yes" or "No" (depending on whether you want to trade for
              or against the statement of the event). <br />
              <br />
              3. Type in the price and the volume you wish to trade in the popup
              that appears. The price can range from 1 to 99 INR. The maximum
              volume you can input depends on your account balance.<br />
              <br />
              4. Once you click "Confirm" the trade order will be placed.<br />
              <br />
              <br />
              Note: The higher price you input, the more likely your trade takes
              place. The trade will only take place if there is another
              participant willing to take the opposite side of the trade. Hence
              if you are interested in trading "for" the event, there must be
              another participant willing to trade "against" the event for an
              appropriate price.
            </p>
          </Section>
          <Section>
            <Title>
              Is there a minimum / maximum price of an event that I can trade?
            </Title>
            <p>
              Yes, you can trade at a price of minimum 1 INR and maximum 99 INR.
            </p>
          </Section>
          <Section>
            <Title>What does the price of a given event depend on?</Title>
            <p>
              The market price of a given event fluctuates based on the
              likelihood a given event turns out to be true and on supply/demand
              dynamics of the trades of participants (that is the quantity and
              price level of orders placed by participants). However once the
              event is resolved, the market price is always either 0 INR or 100
              INR.
            </p>
          </Section>
        </Container>
      </Wrapper>
    )
  }
}

Faq.propTypes = {
  host: PropTypes.string,
  protocol: PropTypes.string
}

const mapStateToProps = ({ site: { host, protocol } }) => ({ host, protocol })

export default connect(mapStateToProps)(Faq)
