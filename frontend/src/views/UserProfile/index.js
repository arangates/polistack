/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, Main } from 'components'
import Content from './Content'
import { withRouter } from 'react-router-dom'
import {
  openChangeVolumeModal,
  openResolveBetModal
} from 'actions/modalActions'

class UserProfile extends Component {
  componentWillMount() {
    if (this.props.location.pathname === '/profile') {
      this.props.history.push('/profile/trades')
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.pathname === '/profile') {
      this.props.history.push('/profile/trades')
    }
  }

  render() {
    const { user, openChangeVolumeModal, openResolveBetModal } = this.props

    return (
      <Container fullHeight>
        <Main>
          <Content
            user={user}
            openChangeVolumeModal={openChangeVolumeModal}
            openResolveBetModal={openResolveBetModal}
          />
        </Main>
      </Container>
    )
  }
}

function mapStateToProps({ user }) {
  return { user }
}

export default connect(mapStateToProps, {
  openChangeVolumeModal,
  openResolveBetModal
})(withRouter(UserProfile))
