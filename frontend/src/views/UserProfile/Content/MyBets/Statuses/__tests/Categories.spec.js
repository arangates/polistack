/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Categories from '../index'

describe('Categories', () => {
  it('should render correctly', () => {
    const output = shallow(<Categories />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
