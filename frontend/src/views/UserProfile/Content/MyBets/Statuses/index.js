/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { ACTIVE, PENDING, ENDED } from 'constants/bet.statuses'
import { StatusTabs, StatusTab } from 'components/StatusTabs'

class Statuses extends Component {
  render() {
    const { changeCurrentStatus, currentStatus } = this.props

    return (
      <StatusTabs>
        <StatusTab
          active={currentStatus === ACTIVE}
          onClick={() => changeCurrentStatus(ACTIVE)}
        >
          Current
        </StatusTab>
        <StatusTab
          data-test-id="pending"
          active={currentStatus === PENDING}
          onClick={() => changeCurrentStatus(PENDING)}
        >
          Pending
        </StatusTab>

        <StatusTab
          active={currentStatus === ENDED}
          onClick={() => changeCurrentStatus(ENDED)}
        >
          Ended
        </StatusTab>
      </StatusTabs>
    )
  }
}

export default Statuses
