/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import MediaQuery from 'react-responsive'
import Heading from './Heading'
import ResultItem from './ResultItem'

import { Table, TableRowGroup } from 'components/TableHeading'
import NoResults from './NoResults'

class Results extends Component {
  renderBets = bets => {
    const {
      currentStatus,
      openChangeVolumeModal,
      openResolveBetModal
    } = this.props
    return bets.map(item => {
      return (
        <ResultItem
          currentStatus={currentStatus}
          item={item}
          key={item.id}
          openChangeVolumeModal={openChangeVolumeModal}
          openResolveBetModal={openResolveBetModal}
        />
      )
    })
  }

  render() {
    const { bets, currentStatus } = this.props
    return (
      <div data-test-id="my-bet">
        <MediaQuery query="(max-width: 575px)">
          {this.renderBets(bets)}
          {bets.length === 0 && <NoResults>No results found</NoResults>}
        </MediaQuery>
        <MediaQuery query="(min-width: 576px)">
          <Table>
            <Heading currentStatus={currentStatus} />
            {bets.length === 0 && <NoResults>No results found</NoResults>}
            <TableRowGroup>{this.renderBets(bets)}</TableRowGroup>
          </Table>
        </MediaQuery>
      </div>
    )
  }
}

export default Results
