/* eslint-disable react/prop-types */

import React, { Component } from 'react'
import { TableHeading, TableHeadingType } from 'components/TableHeading'
import { TableCell, TableCellOptions } from 'components/TableItem'
import { ENDED } from 'constants/bet.statuses'

class Heading extends Component {
  render() {
    const { currentStatus } = this.props

    return (
      <TableHeading>
        <TableHeadingType>Name</TableHeadingType>
        <TableCell>Side</TableCell>
        <TableCell>Price</TableCell>
        <TableCell>Amount</TableCell>
        {currentStatus === ENDED && <TableCell>Winning Side</TableCell>}
        <TableCellOptions />
      </TableHeading>
    )
  }
}

export default Heading
