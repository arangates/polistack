/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Results from '../index'

const bets = [
  {
    id: 28,
    yes: '5.00',
    no: '5.00',
    statement: '1',
    ends: null,
    status: 'ENDED',
    created_by: 1,
    created_at: '2018-04-18T07:59:35.840597Z',
    image: '/media/alex-jodoin-246078.jpg',
    user_bet_actions: [
      {
        id: 30,
        side: 'yes',
        bet: 28,
        amount: '12.00',
        created_at: '2018-04-18T07:59:35.847263Z',
        paired_amount: '0.00'
      }
    ],
    listId: '28'
  },
  {
    id: 31,
    yes: '5.00',
    no: '5.00',
    statement: 'asdadasdsad',
    ends: null,
    status: 'ENDED',
    created_by: 1,
    created_at: '2018-04-18T08:15:34.877919Z',
    image: '/media/alex-jodoin-246078_ZyZTwbY.jpg',
    user_bet_actions: [
      {
        id: 33,
        side: 'yes',
        bet: 31,
        amount: '12.00',
        created_at: '2018-04-18T08:15:34.890070Z',
        paired_amount: '0.00'
      }
    ],
    listId: '31'
  },
  {
    id: 32,
    yes: '5.00',
    no: '5.00',
    statement: 'asdasdsadsa',
    ends: null,
    status: 'ENDED',
    created_by: 1,
    created_at: '2018-04-18T08:16:10.558087Z',
    image: '/media/alex-jodoin-246078_Lu40eRW.jpg',
    user_bet_actions: [
      {
        id: 34,
        side: 'yes',
        bet: 32,
        amount: '12.00',
        created_at: '2018-04-18T08:16:10.564907Z',
        paired_amount: '0.00'
      }
    ],
    listId: '32'
  },
  {
    id: 35,
    yes: '5.00',
    no: '5.00',
    statement: 'asdadasdasdasdasdasdasdasdasdasdasdas',
    ends: null,
    status: 'ENDED',
    created_by: 1,
    created_at: '2018-04-18T08:19:23.428952Z',
    image: '/media/alex-jodoin-246078_yNhoNxu.jpg',
    user_bet_actions: [
      {
        id: 37,
        side: 'yes',
        bet: 35,
        amount: '12.00',
        created_at: '2018-04-18T08:19:23.434310Z',
        paired_amount: '0.00'
      }
    ],
    listId: '35'
  },
  {
    id: 36,
    yes: '5.00',
    no: '5.00',
    statement: 'asdzxc',
    ends: null,
    status: 'PENDING',
    created_by: 1,
    created_at: '2018-04-18T09:22:44.275622Z',
    image: null,
    user_bet_actions: [
      {
        id: 38,
        side: 'yes',
        bet: 36,
        amount: '12.00',
        created_at: '2018-04-18T09:22:44.283733Z',
        paired_amount: '0.00'
      }
    ],
    listId: '36'
  },
  {
    id: 1,
    yes: '3.00',
    no: '2.00',
    statement: 'Trump will be reelected in 2020',
    ends: '2010-08-05',
    status: 'ACTIVE',
    created_by: 3,
    created_at: '2018-04-18T10:06:48.247685Z',
    image: '/media/trump.jpg',
    user_bet_actions: [
      {
        id: 28,
        side: 'yes',
        bet: 1,
        amount: '150.00',
        created_at: '2018-04-18T07:23:08.904465Z',
        paired_amount: '150.00'
      },
      {
        id: 29,
        side: 'yes',
        bet: 1,
        amount: '0.00',
        created_at: '2018-04-18T07:24:31.515385Z',
        paired_amount: '0.00'
      },
      {
        id: 39,
        side: 'yes',
        bet: 1,
        amount: '38.00',
        created_at: '2018-04-18T10:17:58.478477Z',
        paired_amount: '0.00'
      },
      {
        id: 40,
        side: 'yes',
        bet: 1,
        amount: '50000.00',
        created_at: '2018-04-18T10:18:49.209149Z',
        paired_amount: '0.00'
      }
    ],
    listId: '1'
  }
]

describe('Results', () => {
  it('should render correctly', () => {
    const output = shallow(<Results bets={bets}/>)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
