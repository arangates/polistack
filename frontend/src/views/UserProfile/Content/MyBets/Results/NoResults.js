import styled from 'styled-components'

const NoResults = styled.div`
  font-size: 0.875rem;
  text-align: center;
  padding: 1.5em 0;
`

export default NoResults
