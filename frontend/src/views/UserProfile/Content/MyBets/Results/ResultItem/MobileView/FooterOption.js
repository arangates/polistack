import styled from 'styled-components'

const FooterOption = styled.div`
  font-size: 1rem;
  background-color: #fff;
  border: 0.125em solid #eaedf2;
  border-radius: 2em;
  cursor: pointer;
  height: 2em;
  width: 2em;

  display: flex;
  align-items: center;
  justify-content: center;

  &:not(:last-child) {
    margin-right: 0.625em;
  }
`

export default FooterOption
