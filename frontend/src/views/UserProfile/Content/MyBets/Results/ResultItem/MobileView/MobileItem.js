/* eslint-disable prettier/prettier */
import styled from 'styled-components'

const MobileItem = styled.div`
  border: 0.071em solid #eaedf2;
  padding: 1.25em;
  margin-top: 1.25em;
  position: relative;

  &:nth-child(odd) {
    background: #fafcff;
  }

  &::after {
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    border-right: 10px solid transparent;
    border-top: ${props => props.statusNew && ' 10px solid #22d039'};
    border-top: ${props => props.statusResolved && ' 10px solid #44aef3'};
  }

`

export default MobileItem
