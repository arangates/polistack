import styled from 'styled-components'

const Footer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  margin-top: 1.25em;
`

export default Footer
