/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import ResultItem from '../index'
import configureStore from 'redux-mock-store'

const mockStore = configureStore()

const store = mockStore({})

describe('ResultItem', () => {
  it('should render correctly', () => {
    const output = shallow(<ResultItem store={store} />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
