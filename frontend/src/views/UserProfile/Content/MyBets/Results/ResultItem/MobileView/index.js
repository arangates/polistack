/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Svg } from 'components'
import { REPHRASED, ENDED, ORDER_PENDING } from 'constants/bet.statuses'
import { iconEdit } from 'images'
import MobileItem from './MobileItem'
import Footer from './Footer'
import FooterOption from './FooterOption'
import Details from './Details'
import { DetailGroup, DetailHeader, DetailParagraph } from './DetailGroup'
import StatusIcon from '../StatusIcon'

class MobileView extends Component {
  render() {
    const {
      item,
      roundNumber,
      openChangeVolumeModal,
      currentStatus
    } = this.props
    return (
      <MobileItem
        statusNew={item.status !== REPHRASED}
        statusResolved={item.status === 'ENDED'}
      >
        <StatusIcon item={item} />
        <Details>
          <DetailGroup>
            <DetailHeader>{item.bet_statement}</DetailHeader>
          </DetailGroup>

          <DetailGroup>
            <DetailHeader>Side</DetailHeader>
            <DetailParagraph light>{item.side}</DetailParagraph>
          </DetailGroup>

          <DetailGroup>
            <DetailHeader>Price</DetailHeader>
            <DetailParagraph>₹ {roundNumber(item.price)}</DetailParagraph>
          </DetailGroup>

          <DetailGroup>
            <DetailHeader>Amount</DetailHeader>
            <DetailParagraph>{roundNumber(item.shares)}</DetailParagraph>
          </DetailGroup>
          {currentStatus === ENDED && (
            <DetailGroup>
              <DetailHeader>Winning Side</DetailHeader>
              <DetailParagraph>{item.winning_side}</DetailParagraph>
            </DetailGroup>
          )}
        </Details>

        <Footer>
          {item.status === ORDER_PENDING && (
            <FooterOption
              onClick={() => openChangeVolumeModal(item, currentStatus)}
            >
              <Svg icon={iconEdit} style={{ fill: '#828F97' }} />
            </FooterOption>
          )}
        </Footer>
      </MobileItem>
    )
  }
}

export default MobileView
