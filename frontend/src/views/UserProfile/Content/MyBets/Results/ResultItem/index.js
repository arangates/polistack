/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import MediaQuery from 'react-responsive'
import { connect } from 'react-redux'
import { updateBetStatus } from 'actions/betActions'
import DesktopView from './DesktopView'
import MobileView from './MobileView'

class ResultItem extends Component {
  roundNumber = number => Math.round(number * 100) / 100

  renderAmount = (arr, prop) =>
    this.roundNumber(arr.reduce((prev, curr) => prev + Number(curr[prop]), 0))

  render() {
    const {
      type,
      item,
      currentStatus,
      openChangeVolumeModal,
      openResolveBetModal,
      updateBetStatus
    } = this.props
    return (
      <React.Fragment>
        <MediaQuery maxWidth={575}>
          <MobileView
            type={type}
            item={item}
            openChangeVolumeModal={openChangeVolumeModal}
            currentStatus={currentStatus}
            openResolveBetModal={openResolveBetModal}
            renderAmount={this.renderAmount}
            roundNumber={this.roundNumber}
            updateBetStatus={updateBetStatus}
          />
        </MediaQuery>

        <MediaQuery minWidth={576}>
          <DesktopView
            currentStatus={currentStatus}
            item={item}
            openChangeVolumeModal={openChangeVolumeModal}
            openResolveBetModal={openResolveBetModal}
            renderAmount={this.renderAmount}
            roundNumber={this.roundNumber}
            updateBetStatus={updateBetStatus}
          />
        </MediaQuery>
      </React.Fragment>
    )
  }
}

export default connect(null, { updateBetStatus })(ResultItem)
