/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { ORDER_PENDING, ENDED, REPHRASED } from 'constants/bet.statuses'
import Svg from 'components/Svg'

import {
  TableItem,
  TableCell,
  TableCellType,
  TableCellOptions,
  TableCellOption
} from 'components/TableItem'

import { iconEdit } from 'images'

class DesktopView extends Component {
  render() {
    const {
      item,
      currentStatus,
      roundNumber,
      openChangeVolumeModal
    } = this.props
    return (
      <TableItem rephrased={item.status === REPHRASED}>
        <TableCellType rephrased={item.status === REPHRASED}>
          <div>{item.bet_statement}</div>
        </TableCellType>
        <TableCell>
          <span>{item.side}</span>
        </TableCell>
        <TableCell>
          <span>₹ {roundNumber(item.price)}</span>
        </TableCell>
        <TableCell>
          <span>{roundNumber(item.shares)}</span>
        </TableCell>
        {currentStatus === ENDED && (
          <TableCell>
            <span>{item.winning_side}</span>
          </TableCell>
        )}
        <TableCellOptions>
          {item.status === ORDER_PENDING && (
            <TableCellOption
              onClick={() => openChangeVolumeModal(item, currentStatus)}
            >
              <Svg icon={iconEdit} style={{ fill: '#828F97' }} />
            </TableCellOption>
          )}
        </TableCellOptions>
      </TableItem>
    )
  }
}

export default DesktopView
