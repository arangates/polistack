import styled from 'styled-components'

const StatusContainer = styled.div`
  display: flex;
  align-items: center;
`

export default StatusContainer
