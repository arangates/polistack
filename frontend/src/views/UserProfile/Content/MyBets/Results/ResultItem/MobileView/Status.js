import styled from 'styled-components'

const Status = styled.div`
  & > div {
    justify-content: flex-start;

    &::after {
      display: none;
    }
  }
`

export default Status
