import styled from 'styled-components'

const DetailGroup = styled.div`
  flex: 1 0 50%;
  padding: 0 1em;
  margin-top: 1em;

  &:first-child {
    flex: 1 0 100%;
  }
`

const DetailHeader = styled.h5`
  color: #828f97;
  font-size: 0.75rem;
`

const DetailParagraph = styled.span`
  color: #102b3b;
  font-size: 0.75rem;
  font-weight: ${props => (props.light ? 400 : 700)};
`

export { DetailGroup, DetailHeader, DetailParagraph }
