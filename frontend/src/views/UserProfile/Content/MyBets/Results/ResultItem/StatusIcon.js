/* eslint-disable */
import React, { Component } from 'react'
import MediaQuery from 'react-responsive'
import { TableCellImage } from 'components/TableItem'
import Svg from 'components/Svg'
import { iconNew, iconNormal, iconWinner } from 'images'
import moment from 'moment/moment'
import { PENDING, ENDED, REPHRASED, ACTIVE } from 'constants/bet.statuses'
import Status from './MobileView/Status'
import MobileStatus from './MobileView/MobileStatus'
import StatusContainer from './MobileView/StatusContainer'

class StatusIcon extends Component {
  render() {
    const { item } = this.props
    const betIsNew = moment().diff(moment(item.created_at), 'days') < 3 && item.status === PENDING

    const betIsWon = ((item.win === 'yes' &&
    item.user_bet_actions[0].side === 'yes') || (item.win === 'no' &&
    item.user_bet_actions[0].side === 'no')) && item.status === ENDED

    const betIsLost = !((item.win === 'yes' &&
    item.user_bet_actions[0].side === 'yes') || (item.win === 'no' &&
    item.user_bet_actions[0].side === 'no')) && item.status === ENDED

    return (
      <React.Fragment>
        {betIsNew && (
            <React.Fragment>
              <MediaQuery maxWidth={575}>
                <Status>
                  <Svg icon={iconNew} style={{ fill: '#22d039' }} />
                </Status>
              </MediaQuery>
              <MediaQuery minWidth={576}>
                <TableCellImage statusNew>
                  <Svg icon={iconNew} style={{ fill: '#22d039' }} />
                </TableCellImage>
              </MediaQuery>
            </React.Fragment>
          )}

        { betIsWon && (
            <React.Fragment>
              <MediaQuery maxWidth={575}>
                <StatusContainer>
                  <Status>
                    <Svg icon={iconWinner} />
                  </Status>
                  <MobileStatus win={betIsWon}>Won</MobileStatus>
                </StatusContainer>
              </MediaQuery>
              <MediaQuery minWidth={576}>
                <TableCellImage statusWin>
                  <Svg icon={iconWinner} />
                </TableCellImage>
              </MediaQuery>
            </React.Fragment>
          )}

        { item.status === REPHRASED && (
          <React.Fragment>
            <MediaQuery maxWidth={575}>
              <StatusContainer>
                <MobileStatus lost={item.win === 'no'}>Rephrased</MobileStatus>
              </StatusContainer>
            </MediaQuery>
            <MediaQuery minWidth={576}>
              <TableCellImage />
            </MediaQuery>
          </React.Fragment>
        )}

        { betIsLost && (
          <React.Fragment>
            <MediaQuery maxWidth={575}>
              <StatusContainer>
                <MobileStatus lost={!betIsWon}>Lost</MobileStatus>
              </StatusContainer>
            </MediaQuery>
            <MediaQuery minWidth={576}>
              <TableCellImage />
            </MediaQuery>
          </React.Fragment>
        )}

        {item.status === ACTIVE && (
          <React.Fragment>
            <MediaQuery maxWidth={575}>
              <StatusContainer>
              </StatusContainer>
            </MediaQuery>
            <MediaQuery minWidth={576}>
              <TableCellImage />
            </MediaQuery>
          </React.Fragment>
        )}
      </React.Fragment>
    )
  }
}

export default StatusIcon
