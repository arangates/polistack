import styled from 'styled-components'

const MobileStatus = styled.div`
  color: ${props => props.win && '#f9c20a'};
  color: ${props => props.lost && '#102b3b'};
  font-weight: 700;

  margin-left: ${props => props.win && '1em'};
`

export default MobileStatus
