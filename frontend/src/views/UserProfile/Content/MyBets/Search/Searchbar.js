import styled from 'styled-components'

const Searchbar = styled.form`
  position: relative;
  margin: 1.25em 0 0;

  > input {
    padding-right: 4em;
  }

  > label {
    position: absolute;
    right: 0;
    top: 0;
    height: 100%;
    padding: 0.5em 0.875em;
    cursor: pointer;
  }
`

export default Searchbar
