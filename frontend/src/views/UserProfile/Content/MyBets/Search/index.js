/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import { Input } from 'components'
import ReactDOM from 'react-dom'
import Searchbar from './Searchbar'

class Search extends Component {
  submit = e => {
    this.props.handleSubmit(e)
    ReactDOM.findDOMNode(this.searchInput).blur()
  }
  render() {
    return (
      <Searchbar onSubmit={this.submit}>
        <Field
          data-test-id="search-my-bets"
          type="search"
          ref={input => {
            this.searchInput = input
          }}
          name="phrase"
          component={Input}
          className="white"
          placeholder="Search a bet..."
        />
      </Searchbar>
    )
  }
}

Search = reduxForm({
  form: 'searchMyBets'
})(Search)

export default Search
