/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Search from '../index'

describe('Search', () => {
  it('should render correctly', () => {
    const output = shallow(<Search />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
