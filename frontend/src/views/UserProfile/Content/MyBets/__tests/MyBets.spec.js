/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import MyBets from '../index'
import configureStore from 'redux-mock-store'

const mockStore = configureStore()

const store = mockStore({})

describe('MyBets', () => {
  it('should render correctly', () => {
    const output = shallow(<MyBets store={store}/>)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
