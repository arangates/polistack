import styled from 'styled-components'

const BetsContent = styled.div`
  background: #fff;
  padding: 1.875em;
  box-shadow: 0 0.125em 1.25em 0 rgba(165, 183, 195, 0.15);

  h2 {
    margin-bottom: 1em;
  }

  .transaction__row--group {
    border: 1px solid #f4f5f9;
    border-top: none;
  }
`

export default BetsContent
