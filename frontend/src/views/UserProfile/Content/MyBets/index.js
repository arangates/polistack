/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Statuses from './Statuses'
import Search from './Search'
import Results from './Results'
import Pagination from 'components/Pagination'
import { connect } from 'react-redux'
import { fetchUserBets } from 'actions/userActions'
import Loader from 'components/Loader'
import BetsContent from './BetsContent'
import { ACTIVE } from 'constants/bet.statuses'

class MyBets extends Component {
  state = {
    currentStatus: ACTIVE,
    currentPage: 1
  }

  changeCurrentStatus = currentStatus => {
    if (this.state.currentStatus !== currentStatus) {
      this.setState({ currentStatus, currentPage: 1 })
      this.props.fetchUserBets(1, currentStatus)
    }
  }

  changeCurrentPage = currentPage => {
    this.setState({ currentPage })
    this.props.fetchUserBets(currentPage, this.state.currentStatus)
  }

  componentWillMount() {
    this.props.fetchUserBets()
  }

  componentWillReceiveProps(nextProps) {
    this.props.user.bets = nextProps.user.bets
  }

  searchUserBets = searchData => {
    this.props.fetchUserBets(
      this.props.currentPage,
      this.state.currentStatus,
      searchData
    )
  }

  render() {
    const { user: { bets, isLoading } } = this.props
    return (
      <BetsContent>
        <Statuses
          currentStatus={this.state.currentStatus}
          changeCurrentStatus={this.changeCurrentStatus}
        />
        <Search onSubmit={this.searchUserBets} />
        {!bets || bets.length === 0 || isLoading ? (
          <Loader customHeight />
        ) : (
          <React.Fragment>
            <Results
              currentStatus={this.state.currentStatus}
              bets={bets.results}
              openChangeVolumeModal={this.props.openChangeVolumeModal}
              openResolveBetModal={this.props.openResolveBetModal}
            />
            <Pagination
              perPage={20}
              resultCount={bets.count}
              currentPage={this.state.currentPage}
              changeCurrentPage={this.changeCurrentPage}
            />
          </React.Fragment>
        )}
      </BetsContent>
    )
  }
}

function mapStateToProps({ user }) {
  return { user }
}

export default connect(mapStateToProps, { fetchUserBets })(MyBets)
