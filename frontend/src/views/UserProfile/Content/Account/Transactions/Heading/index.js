import React, { Component } from 'react'
import { TableCell } from 'components/TableItem'
import { TableHeading } from 'components/TableHeading'
class Heading extends Component {
  render() {
    return (
      <TableHeading>
        <TableCell>Type</TableCell>
        <TableCell>Name</TableCell>
        <TableCell />
        <TableCell>Amount</TableCell>
      </TableHeading>
    )
  }
}

export default Heading
