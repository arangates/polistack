/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import MediaQuery from 'react-responsive'
import Heading from './Heading'
import Header from './Header'

import Pagination from 'components/Pagination'
import { Table, TableRowGroup } from 'components/TableHeading'

import Transaction from './Transaction'

class Transactions extends Component {
  renderTransactions = transactions =>
    transactions.map(item => <Transaction transaction={item} key={item.id} />)

  render() {
    const { resultCount, currentPage, changeCurrentPage } = this.props
    return (
      <React.Fragment>
        <Header>Transaction history</Header>
        <MediaQuery query="(max-width: 575px)">
          {this.renderTransactions(this.props.transactions)}
        </MediaQuery>

        <MediaQuery query="(min-width: 576px)">
          <Table>
            <Heading />

            <TableRowGroup>
              {this.renderTransactions(this.props.transactions)}
            </TableRowGroup>
          </Table>
        </MediaQuery>

        <Pagination
          perPage={20}
          resultCount={resultCount}
          currentPage={currentPage}
          changeCurrentPage={changeCurrentPage}
        />
      </React.Fragment>
    )
  }
}

export default Transactions
