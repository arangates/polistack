import styled from 'styled-components'

const Header = styled.h5`
  color: #44aef3;
  font-size: 0.875rem;
  font-weight: 700;
`

export default Header
