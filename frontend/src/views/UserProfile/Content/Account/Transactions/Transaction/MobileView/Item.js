import styled from 'styled-components'

const Item = styled.div`
  border: 0.071em solid #eaedf2;
  padding: 1.25em;
  margin-top: 1.25em;

  &:nth-child(odd) {
    background: #fafcff;
  }

  h5 {
    color: #828f97;
    font-size: 0.75rem;
  }

  h6 {
    color: #102b3b;
    font-size: 0.75rem;
  }

  span {
    font-size: 0.875rem;
    font-weight: 700;

    &.light {
      font-weight: 400;
    }
  }
`

export default Item
