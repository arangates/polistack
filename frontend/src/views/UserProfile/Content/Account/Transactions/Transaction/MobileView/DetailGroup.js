import styled from 'styled-components'

const DetailGroup = styled.div`
  flex: 1 0 50%;
  padding: 0 1em;
  margin-top: 1em;
`

export default DetailGroup
