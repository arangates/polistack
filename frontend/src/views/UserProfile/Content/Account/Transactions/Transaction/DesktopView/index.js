/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { TableCell, TableItem } from 'components/TableItem'

class DesktopView extends Component {
  render() {
    const { type, amount } = this.props.transaction

    return (
      <TableItem>
        <TableCell>{type}</TableCell>
        <TableCell>{this.props.transaction.bet_statement || 'N/A'}</TableCell>
        <TableCell />
        <TableCell>₹ {amount}</TableCell>
      </TableItem>
    )
  }
}

export default DesktopView
