import styled from 'styled-components'

const ListItem = styled.div`
  margin-top: 1.25em;

  &:nth-child(even) {
    background: #fafcff;
  }
`

export default ListItem
