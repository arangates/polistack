import styled from 'styled-components'

const Details = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -1em;
`

export default Details
