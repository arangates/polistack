/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Details from './Details'
import DetailGroup from './DetailGroup'
import Item from './Item'
class MobileView extends Component {
  render() {
    const { type, amount } = this.props.transaction
    return (
      <Item>
        <div>
          <h5>Name</h5>
          <span>{this.props.transaction.bet_statement || 'N/A'}</span>
        </div>
        <Details>
          <DetailGroup>
            <h5>Type</h5>
            <span>{type}</span>
          </DetailGroup>

          <DetailGroup>
            <h5>Amount</h5>
            <span>₹ {amount}</span>
          </DetailGroup>
        </Details>
      </Item>
    )
  }
}

export default MobileView
