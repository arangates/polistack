/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import MediaQuery from 'react-responsive'
import MobileView from './MobileView'
import DesktopView from './DesktopView'

class Transaction extends Component {
  render() {
    const { transaction } = this.props
    return (
      <React.Fragment>
        <MediaQuery query="(max-width: 575px)">
          <MobileView transaction={transaction} />
        </MediaQuery>
        <MediaQuery query="(min-width: 576px)">
          <DesktopView transaction={transaction} />
        </MediaQuery>
      </React.Fragment>
    )
  }
}

export default Transaction
