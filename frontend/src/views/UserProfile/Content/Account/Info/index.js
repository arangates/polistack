/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import {
  CreditCard,
  CreditCardHeading,
  BankName,
  CreditCardOwner,
  CreditCardTitle,
  CreditCardNumber,
  CreditCardNumberValue,
  CreditCardFooter,
  CreditCardValue,
  CreditCardSection
} from 'components/CreditCard'

import AccountStatus from './AccountStatus'

import Balance from '../Balance'

class Info extends Component {
  render() {
    const { balance } = this.props
    return (
      <AccountStatus>
        <CreditCard>
          <CreditCardHeading>
            <BankName>Connected payments</BankName>
            <div>VISA</div>
          </CreditCardHeading>

          <CreditCardOwner>Your name</CreditCardOwner>

          <CreditCardNumber>
            <CreditCardTitle>Card number</CreditCardTitle>
            <CreditCardNumberValue>**** **** **** 2928</CreditCardNumberValue>
          </CreditCardNumber>

          <CreditCardFooter>
            <CreditCardSection>
              <CreditCardTitle>Expiration</CreditCardTitle>
              <CreditCardValue>July/2019</CreditCardValue>
            </CreditCardSection>

            <CreditCardSection>
              <CreditCardTitle>CCV</CreditCardTitle>
              <CreditCardValue>***</CreditCardValue>
            </CreditCardSection>
          </CreditCardFooter>
        </CreditCard>

        <Balance balance={balance} />
      </AccountStatus>
    )
  }
}

export default Info
