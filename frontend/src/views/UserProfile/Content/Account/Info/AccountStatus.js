import styled from 'styled-components'

const AccountStatus = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding-bottom: 2.5em;
`

export default AccountStatus
