import styled from 'styled-components'

const AccountContent = styled.div`
  background: #fff;
  padding: 1.875em;
  box-shadow: 0 0.125em 1.25em 0 rgba(165, 183, 195, 0.15);

  h2 {
    margin-bottom: 1em;
  }
`

export default AccountContent
