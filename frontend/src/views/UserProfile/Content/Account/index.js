/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Transactions from './Transactions'
import AccountContent from './AccountContent'
import Info from './Info'
import { connect } from 'react-redux'
import { fetchTransactions } from 'actions/userActions'
import { Loader } from 'components'
import { openInfoModal } from 'actions/modalActions'

class Account extends Component {
  componentDidMount() {
    this.props.fetchTransactions()
  }

  state = {
    currentPage: 1
  }

  changeCurrentPage = currentPage => {
    this.setState({ currentPage })
    this.props.fetchTransactions(currentPage)
  }

  render() {
    const { transactions } = this.props

    return (
      <AccountContent>
        <Info
          balance={this.props.profile.balance}
          openInfoModal={openInfoModal}
        />
        {!transactions || !transactions.data ? (
          <Loader />
        ) : (
          <Transactions
            transactions={transactions.data.results}
            currentPage={this.state.currentPage}
            resultCount={transactions.data.count}
            changeCurrentPage={this.changeCurrentPage}
          />
        )}
      </AccountContent>
    )
  }
}

function mapStateToProps({ user }) {
  return {
    transactions: user.transactions,
    profile: user.profile
  }
}

export default connect(mapStateToProps, { fetchTransactions, openInfoModal })(
  Account
)
