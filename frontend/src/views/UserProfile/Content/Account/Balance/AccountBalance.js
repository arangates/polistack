import styled from 'styled-components'

const AccountBalance = styled.div`
  border: 0.063em solid #eaedf2;
  border-radius: 0.375em;

  flex: 0 1 100%;
  margin-top: 1.875em;

  @media screen and (min-width: 768px) {
    flex: 0 1 48%;
    margin-top: 0;
  }

  padding: 1.875em;

  text-align: center;

  h6 {
    color: #828f97;
    font-size: 0.75rem;
    font-weight: 900;
  }
`
const AccountBalanceValue = styled.div`
  color: #102b3b;
  font-size: 2.25rem;
  font-weight: 900;
  margin-top: 0.1389em;
`
const AccountBalanceOptions = styled.div`
  display: flex;
  justify-content: space-between;
  border-top: 1px solid #f1f1f2;
  margin: 2em 1em 0;
  padding-top: 2em;
`

export { AccountBalance, AccountBalanceValue, AccountBalanceOptions }
