/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import { openChoosePaymentModal, openInfoModal } from 'actions/modalActions'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import {
  AccountBalance,
  AccountBalanceOptions,
  AccountBalanceValue
} from './AccountBalance'

import Button from 'components/Button'

class Balance extends Component {
  withdraw = () => {
    this.props.openInfoModal(
      'Withdraw',
      'Please contact us at support@polistack.com to submit a withdrawal request.'
    )
  }
  deposit = () => {
    this.props.openChoosePaymentModal('credit card', 15)
  }
  render() {
    const { balance } = this.props
    return (
      <AccountBalance>
        <h6>Your available funds</h6>
        <AccountBalanceValue>₹ {balance}</AccountBalanceValue>

        <AccountBalanceOptions>
          <Button red onClick={this.withdraw}>
            Withdraw
          </Button>

          <Button blue onClick={this.deposit}>
            Deposit
          </Button>
        </AccountBalanceOptions>
      </AccountBalance>
    )
  }
}

Balance = reduxForm({
  form: 'withdraw'
})(Balance)

export default connect(null, { openInfoModal, openChoosePaymentModal })(Balance)
