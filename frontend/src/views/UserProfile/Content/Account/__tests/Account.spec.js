/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Account from '../index'
import configureStore from 'redux-mock-store'

const mockStore = configureStore()

const store = mockStore({
  user: {
    transactions: {
      "count": 5,
      "next": null,
      "previous": null,
      "results": [
        {
          "id": 1,
          "amount": "100000.00",
          "bet": null,
          "user": 1,
          "created_at": "2018-04-18T07:06:47.886779Z",
          "fee": "2.13"
        },
        {
          "id": 4,
          "amount": "0.00",
          "bet": {
            "id": 35,
            "yes": "5.00",
            "no": "5.00",
            "statement": "asdadasdasdasdasdasdasdasdasdasdasdas",
            "ends": null,
            "created_by": 1,
            "image": "http://localhost:8000/media/alex-jodoin-246078_yNhoNxu.jpg",
            "status": "ENDED",
            "volume": 12
          },
          "user": 1,
          "created_at": "2018-04-18T09:04:25.806317Z",
          "fee": "0.00"
        },
        {
          "id": 5,
          "amount": "0.00",
          "bet": {
            "id": 28,
            "yes": "5.00",
            "no": "5.00",
            "statement": "1",
            "ends": null,
            "created_by": 1,
            "image": "http://localhost:8000/media/alex-jodoin-246078.jpg",
            "status": "ENDED",
            "volume": 12
          },
          "user": 1,
          "created_at": "2018-04-18T09:04:41.192476Z",
          "fee": "0.00"
        },
        {
          "id": 6,
          "amount": "0.00",
          "bet": {
            "id": 31,
            "yes": "5.00",
            "no": "5.00",
            "statement": "asdadasdsad",
            "ends": null,
            "created_by": 1,
            "image": "http://localhost:8000/media/alex-jodoin-246078_ZyZTwbY.jpg",
            "status": "ENDED",
            "volume": 12
          },
          "user": 1,
          "created_at": "2018-04-18T09:05:55.814730Z",
          "fee": "0.00"
        },
        {
          "id": 7,
          "amount": "0.00",
          "bet": {
            "id": 32,
            "yes": "5.00",
            "no": "5.00",
            "statement": "asdasdsadsa",
            "ends": null,
            "created_by": 1,
            "image": "http://localhost:8000/media/alex-jodoin-246078_Lu40eRW.jpg",
            "status": "ENDED",
            "volume": 12
          },
          "user": 1,
          "created_at": "2018-04-18T09:05:56.395744Z",
          "fee": "0.00"
        }
      ]
    }
  }
})
describe('Account', () => {
  it('should render correctly', () => {
    const output = shallow(<Account store={store}/>)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
