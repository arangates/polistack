/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import EditProfileForm from './EditProfileForm'
import { connect } from 'react-redux'
import { editProfile } from 'actions/userActions'
import PropTypes from 'prop-types'
import AccountContent from './AccountContent'
import ProfileAccount from './ProfileAccount'

class EditProfile extends Component {
  editProfile = userData => {
    this.props.editProfile(userData)
  }

  render() {
    return (
      <ProfileAccount>
        <AccountContent>
          <EditProfileForm user={this.props.user} onSubmit={this.editProfile} />
        </AccountContent>
      </ProfileAccount>
    )
  }
}

export default connect(null, { editProfile })(EditProfile)

EditProfile.propTypes = {
  user: PropTypes.object,
  editProfile: PropTypes.func
}
