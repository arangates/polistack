import { isEmail, equals, isLength } from 'validator'

const validate = userData => {
  if (!userData.profile || !userData.password) return

  const {
    profile: { email, firstName, lastName },
    password: { password, passwordConfirm }
  } = userData

  const errors = {
    profile: {},
    password: {}
  }

  if (!isEmail(email)) {
    errors.profile.email = 'Invalid email'
  }

  if (!email || email === '') {
    errors.profile.email = 'Required'
  }

  if (!firstName || firstName === '') {
    errors.profile.firstName = 'Required'
  }

  if (!lastName || lastName === '') {
    errors.profile.lastName = 'Required'
  }

  if (password && !isLength(password, { min: 8, max: 128 })) {
    errors.password.password =
      'Password not strong enough (at least 8 characters)'
  }

  if (password && !passwordConfirm) {
    errors.password.passwordConfirm = 'Passwords do not match'
  }

  if (password && passwordConfirm && !equals(password, passwordConfirm)) {
    errors.password.passwordConfirm = 'Passwords do not match'
  }

  return errors
}

export default validate
