/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Wrapper from './Wrapper'
import { Button } from 'components'

class Buttons extends Component {
  render() {
    const { history, isLoading, disabled, handleSubmit } = this.props
    return (
      <Wrapper>
        <Button
          white
          popup
          big
          onClick={e => {
            e.preventDefault()
            history.goBack()
          }}
        >
          Cancel
        </Button>

        <Button
          data-test-id="save-and-confirm"
          blue
          popup
          big
          isLoading={isLoading}
          disabled={disabled}
          onClick={handleSubmit}
        >
          Save & confirm
        </Button>
      </Wrapper>
    )
  }
}

export default withRouter(Buttons)
