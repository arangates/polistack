/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Buttons from '../index'

describe('Buttons', () => {
  it('should render correctly', () => {
    const output = shallow(<Buttons />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
