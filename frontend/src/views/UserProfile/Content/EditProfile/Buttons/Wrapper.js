import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 1em;
  
    button {
    height: 3.75em;
    width: 12.084em;

    &:not(:last-child) {
      margin-right: 2.5em;
    }
  
`

export default Wrapper
