import styled from 'styled-components'

const Wrapper = styled.div`
  margin: 2.25em 0 5em;
`

export default Wrapper
