/* eslint-disable react/prop-types */
import React, { Component, Fragment } from 'react'
import { Input, InputGroup, InputGroupSpread } from 'components'
import { Field } from 'redux-form'

class ProfileData extends Component {
  render() {
    return (
      <Fragment>
        <InputGroupSpread>
          <InputGroup>
            <h6>First name</h6>
            <Field
              data-test-id="first-name"
              name="firstName"
              component={Input}
              type="text"
              white
              placeholder="First name"
            />
          </InputGroup>
          <InputGroup>
            <h6>Last name</h6>
            <Field
              data-test-id="last-name"
              name="lastName"
              component={Input}
              type="text"
              white
              placeholder="Last name"
            />
          </InputGroup>
        </InputGroupSpread>
      </Fragment>
    )
  }
}

export default ProfileData
