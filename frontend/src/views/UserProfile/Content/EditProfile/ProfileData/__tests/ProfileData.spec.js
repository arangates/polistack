/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import ProfileData from '../index'

describe('ProfileData', () => {
  it('should render correctly', () => {
    const output = shallow(<ProfileData />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
