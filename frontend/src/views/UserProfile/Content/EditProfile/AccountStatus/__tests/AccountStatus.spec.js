/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import AccountStatus from '../index'

describe('AccountStatus', () => {
  it('should render correctly', () => {
    const output = shallow(<AccountStatus />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
