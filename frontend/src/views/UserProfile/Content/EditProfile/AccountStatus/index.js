import React, { Component } from 'react'

import CreditCardInfo from './CreditCardInfo'
import CreditCardEditable from './CreditCardEditable'
import Wrapper from './Wrapper'

class AccountStatus extends Component {
  render() {
    return (
      <Wrapper>
        <CreditCardInfo />
        <CreditCardEditable />
      </Wrapper>
    )
  }
}

export default AccountStatus
