import React, { Component } from 'react'
import {
  CreditCard,
  CreditCardHeading,
  BankName,
  CreditCardOwner,
  CreditCardTitle,
  CreditCardNumber,
  CreditCardNumberValue,
  CreditCardFooter,
  CreditCardValue,
  CreditCardSection
} from 'components/CreditCard'

class CreditCardInfo extends Component {
  render() {
    return (
      <CreditCard>
        <CreditCardHeading>
          <BankName>Connected payments</BankName>
          <div>VISA</div>
        </CreditCardHeading>

        <CreditCardOwner>Your name</CreditCardOwner>

        <CreditCardNumber>
          <CreditCardTitle>Card number</CreditCardTitle>
          <CreditCardNumberValue>**** **** **** 2928</CreditCardNumberValue>
        </CreditCardNumber>

        <CreditCardFooter>
          <CreditCardSection>
            <CreditCardTitle>Expiration</CreditCardTitle>
            <CreditCardValue>Month/Year</CreditCardValue>
          </CreditCardSection>

          <CreditCardSection>
            <CreditCardTitle>CCV</CreditCardTitle>
            <CreditCardValue>***</CreditCardValue>
          </CreditCardSection>
        </CreditCardFooter>
      </CreditCard>
    )
  }
}

export default CreditCardInfo
