import React, { Component } from 'react'

import { Input, Svg } from 'components'
import { Field } from 'redux-form'

import { iconDropdown } from 'images'

import {
  CreditCard,
  CreditCardHeading,
  BankName,
  CreditCardTitle,
  CreditCardFooter,
  CreditCardDropdown,
  CreditCardDropdownValue,
  CreditCardDropdownArrow,
  CreditCardOptions,
  CreditCardOption,
  CreditCardCCV,
  CreditCardNumberInput
} from 'components/CreditCard'

class CreditCardEditable extends Component {
  state = {
    dropdownOpened: false
  }

  handleDropdownOpen = () => {
    this.setState(prev => {
      return { dropdownOpened: !prev.dropdownOpened }
    })
  }

  render() {
    return (
      <CreditCard isEditable>
        <CreditCardHeading>
          <BankName>Connected payments</BankName>
          <div>VISA</div>
        </CreditCardHeading>

        <div>
          <CreditCardTitle>Card member</CreditCardTitle>
          <Field
            name="username"
            component={Input}
            type="text"
            white
            placeholder="Verna Aguilar"
          />
        </div>

        <CreditCardFooter isEditable>
          <CreditCardNumberInput>
            <CreditCardTitle>Card number</CreditCardTitle>
            <Field
              name="cardNumber"
              component={Input}
              placeholder="1234-5678-9012-3456"
              type="text"
              white
            />
          </CreditCardNumberInput>
          <div>
            <CreditCardTitle>Expiration</CreditCardTitle>
            <CreditCardDropdown onClick={this.handleDropdownOpen}>
              <CreditCardDropdownArrow>
                <Svg icon={iconDropdown} className="icon__dropdown" />
              </CreditCardDropdownArrow>
              <CreditCardDropdownValue>July 2019</CreditCardDropdownValue>
              <CreditCardOptions dropdownOpen={this.state.dropdownOpened}>
                <CreditCardOption>July 2019</CreditCardOption>
                <CreditCardOption>August 2019</CreditCardOption>
                <CreditCardOption>September 2019</CreditCardOption>
              </CreditCardOptions>
            </CreditCardDropdown>
          </div>

          <CreditCardCCV>
            <CreditCardTitle>CCV</CreditCardTitle>
            <Field
              name="ccv"
              component={Input}
              placeholder="442"
              type="text"
              white
            />
          </CreditCardCCV>
        </CreditCardFooter>
      </CreditCard>
    )
  }
}

export default CreditCardEditable
