/* eslint-disable no-class-assign,react/prop-types */
import React, { Component } from 'react'
import AccountStatus from '../AccountStatus'
import ProfileData from '../ProfileData'
import Buttons from '../Buttons'
import { reduxForm, FormSection } from 'redux-form'
import validate from '../validators'

class EditProfileForm extends Component {
  componentDidMount() {
    const {
      user: { profile: { firstName, lastName, email } },
      initialize
    } = this.props

    initialize({
      profile: {
        firstName,
        lastName,
        email
      }
    })
  }

  render() {
    const { dirty, submitting, invalid } = this.props
    const disabled = !dirty || submitting || invalid

    return (
      <form>
        <h2>Edit your profile</h2>
        <AccountStatus />
        <FormSection name="profile">
          <ProfileData />
        </FormSection>
        <Buttons disabled={disabled} handleSubmit={this.props.handleSubmit} />
      </form>
    )
  }
}

EditProfileForm = reduxForm({
  form: 'editProfile',
  validate
})(EditProfileForm)

export default EditProfileForm
