import styled from 'styled-components'

const Profile = styled.div`
  margin: 2.25em 0 5em;
`

export default Profile
