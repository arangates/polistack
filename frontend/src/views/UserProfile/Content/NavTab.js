import styled from 'styled-components'
import { NavLink } from 'react-router-dom'

const NavTab = styled(NavLink)`
  flex: 1;
  border-bottom: 0.125em solid rgba(234, 237, 242, 1);
  cursor: pointer;
  text-align: center;
  padding: 1em;
  transition: 0.3s ease-in-out;

  &:hover {
    border-bottom: 0.125em solid rgba(242, 92, 94, 1);

    > div {
      color: #293244;
    }
  }

  &.${'active'} {
    border-bottom: 0.125em solid rgba(242, 92, 94, 1);

    > div {
      color: #293244;
    }
  }
`

const TabTitle = styled.div`
  color: #828f97;
  font-size: 0.75rem;
  font-weight: 900;
  transition: 0.3s ease-in-out;
`

export { NavTab, TabTitle }
