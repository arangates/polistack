/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Route, withRouter } from 'react-router-dom'

import { Content as Wrapper, Breadcrumbs } from 'components'

import Profile from './Profile'

import Account from './Account'
import EditProfile from './EditProfile'
import MyBets from './MyBets'
import Nav from './Nav'
import { NavTab, TabTitle } from './NavTab'

class Content extends Component {
  render() {
    const { location } = this.props
    const headerStyle = location.pathname === '/profile/edit' ? 'none' : ''
    return (
      <Wrapper>
        <Breadcrumbs />
        <Profile>
          <Nav style={{ display: headerStyle }}>
            <NavTab exact to={`/profile/trades`} activeClassName="active">
              <TabTitle>My Trades</TabTitle>
            </NavTab>
            <NavTab exact to={`/profile/account`} activeClassName="active">
              <TabTitle>Account</TabTitle>
            </NavTab>
          </Nav>

          <Route path={'/profile/account'} component={Account} />
          <Route
            path={'/profile/edit'}
            render={props => <EditProfile {...props} user={this.props.user} />}
          />
          <Route
            path={'/profile/trades'}
            render={props => (
              <MyBets
                {...props}
                openChangeVolumeModal={this.props.openChangeVolumeModal}
                openResolveBetModal={this.props.openResolveBetModal}
              />
            )}
          />
        </Profile>
      </Wrapper>
    )
  }
}

export default withRouter(Content)
