/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Terms from '../index'

describe('Terms', () => {
  it('should render correctly', () => {
    const output = shallow(<Terms />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
