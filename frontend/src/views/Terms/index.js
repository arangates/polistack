import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'

import { Container } from 'components'
import Wrapper from './Wrapper'
import Heading from './Heading'
import { Section, Title } from './Section'

class Terms extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  render() {
    const { host, protocol } = this.props
    const canonicalUrl = `${protocol}://${host}/terms`
    return (
      <Wrapper>
        <Helmet>
          <title>Polistack - Predict politics - Terms of Service</title>
          <link rel="canonical" href={canonicalUrl} />
        </Helmet>
        <Container>
          <Heading>
            <h1>Terms of Use</h1>
            <h3>Last updated on August, 31, 2019</h3>

            <Section>
              <Title>1. General</Title>
              <p>
                The website Polistack is operated by Reverence Technologies, sp.
                z o.o., (“Reverence”) having its office at Piekna 44/21 street,
                00-672, Warsaw, Poland. <br />
                <br />
                By accessing and/or using the website, you (“the User”) are
                hereby agreeing to these Terms of Use which constitute a legally
                binding contract between Reverence and you. It is clarified that
                these Terms of Use are applicable to and binding on all existing
                and future users of the website.<br />
                <br />
                As long as you comply with these Terms of Use, Reverence grants
                you a personal, non-exclusive, non-transferable, limited
                privilege to enter and use the Site. Accessing, browsing or
                otherwise using the Site indicates your agreement to all the
                terms and conditions in these Terms of Use, so please read this
                agreement carefully before proceeding. You may not use the Site
                if you do not accept the Terms or are unable to be bound by the
                Terms. Your use of the Site is at your own risk.
              </p>
            </Section>
          </Heading>
          <Section>
            <Title>About the platform and service</Title>
            <p>
              In these Terms & Conditions:<br />
              <br />
              “Polistack” means the brand and all products offered;<br />
              <br />
              ‘Online’ (accessed via a computer or laptop) via www.polistack.com
              and ‘Mobile’ (accessed via a mobile phone or tablet) and other
              similar URLs used from time to time. <br />
              <br />
              “Customer/s” mean(s) a Registered User/Customer<br />
              <br />
              “Intellectual Property” means trademarks and trade names, whether
              registered or not, including trade mark applications and
              registered trademarks, with the goodwill which attaches in such
              trade names and trademarks, domain names, getup, trade dress and
              trading style, including without limitation as presented in
              websites; domain name registrations and any variations thereof now
              and in the future; any copyright in the getup, trade dress or
              trading style, any right or license under copyright to use such
              getup, trade dress or trading style, any software code,
              architecture of software, look and feel of software, or any other
              intellectual property, owned by or licensed to us, in each case in
              any part of the world.<br />
              <br />
              “Operator” means the operator of the Website.<br />
              <br />
              “Participate” means any of the conduct described in 4.1 below,
              including expressing a yes/no opinion on the outcome of a
              political event posted on the Website.<br />
              <br />
              “Political event” or “Event” means a statement posted on the
              website by the Operator with two possible outcomes (i.e. true or
              false) on which a participant, by using his skill and research is
              required to express a yes/no opinion.<br />
              <br />
              “Registered User/Customer” means a person who has successfully
              registered an account with us in the manner described in clause
              5.1.<br />
              <br />
              “Service” means the availability and provision of the Website that
              enables you to Participate.<br />
              <br />
              “we/us/our” means the Operator together with (where context
              permits) its holding company/ies and associated company/ies.<br />
              <br />
              “Website” means www.polistack.com and other similar URLs used from
              time to time.<br />
              <br />
              “You/Your”, means the “Customer”.
            </p>
          </Section>
          <Section>
            <Title>TERMS OF PARTICIPATION AT POLISTACK</Title>
            <p>
              3.1. Restrictions<br />
              <br />
              You may only Participate at Polistack if you are over 18 years of
              age. It is illegal to Participate at Polistack if you are under 18
              years of age. You are solely responsible for ascertaining whether
              it is legal for you, in the jurisdiction in which you are located,
              to participate at Polistack and acceptance of these terms and
              usage of the website by you shall be construed as your affirmation
              that participation at Polistack is legal for you. We do not
              warrant the legality of your Participation at Polistack in terms
              of the laws of jurisdiction in which you are located. <br />
              <br />
              Any person who is knowingly in breach of this section, including
              any attempt to circumvent this restriction, for example, by using
              a VPN, proxy or similar service that masks or manipulates the
              identification of your real location, or by otherwise providing
              false or misleading information regarding your location or place
              of residence, or by using the Website through a third party or on
              behalf of a third party located in an excluded territory is in
              breach of these Terms & Conditions. Usage of this website in
              breach of this section shall be at your own risk and you may be
              subject to criminal prosecution as per applicable laws.<br />
              <br />
              <br />
              3.2. Acceptance<br />
              <br />
              By accepting these Terms & Conditions you are fully aware that
              there is a risk of losing money and you are fully responsible for
              any such loss. You agree that your Participation at Polistack is
              at your sole option, discretion and risk. In relation to your
              losses you shall have no claims whatsoever against Polistack or
              any partner, or respective directors, officers or employees.<br />
              <br />
              <br />
              3.3. Identification Documentation<br />
              <br />
              To participate at Polistack, you are required to enter your
              personal details during the account registration process. Personal
              details include, but are not limited to: first name, surname,
              email address etc. The ‘Know Your Client’ procedure will be
              carried out when a Customer makes a deposit into their Customer
              account. <br />
              <br />
              Upon making a withdrawal request you may also be required to send
              in valid identification documents proving your age and address.
              Upon such request, the withdrawal will not be processed for
              payment until Polistack has received all requested identification
              documents. Acceptable identification documentation includes, but
              is not limited to: 1. copy of a valid photographic identification
              document, such as a Passport or Driver’s License; and 2. copy of a
              recent credit / debit / bank account statement (note: the account
              statement must relate to a financial method used and must not be
              older than 3 months).<br />
              <br />
              <br />
              3.4 Age Verification Policy and Identification<br />
              <br />
              The payment of a withdrawal request will only be made to a
              Registered Customer. We may withhold any funds in your Polistack
              Customer account until your age is successfully verified. If, on
              completion of the age verification process, you are found to be
              underage, Polistack will return to you any deposits made on the
              account, having voided all winnings.<br />
              <br />
              <br />
              3.5 Risk<br />
              <br />
              You accept that your Participation at Polistack is based upon your
              skills and knowledge qua the subject matter of the event as may be
              posted by the Operator and is at your sole option, discretion and
              risk. Upon expressing a yes/no opinion on a particular political
              event, it shall be assumed that you have conducted the necessary
              research for arriving at the said opinion. It is clarified that
              Polistack does not promote opinions/Participation based on mere
              speculation. Polistack reserves its right, at its sole discretion,
              to call upon you to supply/furnish the relevant data which may
              have formed the basis of your opinion. Further, you agree that
              your Participation at Polistack is for your own personal
              entertainment and non-professional use and that you are acting on
              your own behalf.<br />
              <br />
              <br />
              3.6 Acceptable Use<br />
              <br />
              You represent, warrant and agree that you will comply with all
              applicable laws, statutes and regulations in relation to your use
              of the Website and the Service. We are not responsible for any
              illegal or unauthorised use of the Website or the Service by you.
            </p>
          </Section>
          <Section>
            <Title>4. OVERVIEW AND PARTICIPATION</Title>
            <p>
              4.1 Introduction<br />
              <br />
              Polistack is an on-line skill-based platform that offers users
              with expert knowledge of local politics to express a yes/no
              opinion on political events in India based solely on their skill.
              It is a research-based game of skill, wherein on the basis of
              research and skill, a registered user shall be entitled to express
              a yes/no opinion on the outcome of a political event posted on the
              Website (“Participation.”) The object of the platform is to use
              the data collected from the predictions for scientific research
              thereby providing real-world insights into the political
              sentiment. <br />
              <br />
              The opinions expressed on the Website by participants shall be a
              culmination of research and skill as well as their political
              acumen and knowledge and Polistack is not an online gambling or
              investment vehicle. The platform is offered only to the residents
              of India and only those with expert knowledge of the local
              politics shall be eligible to participate on the Website.<br />
              <br />
              Your participation in the form of you having expressed an opinion
              shall accordingly be construed and deemed to AFFIRM affirmation
              from your end qua the factum of your opinion being a culmination
              of your research, skill, political acumen and local political
              knowledge having been so expressed by a resident of India. <br />
              <br />
              The data collected from the political predictions made by
              Registered Users on the Website shall be made available by the
              Operator to Universities which are involved in research of
              Politics in India, in furtherance of the research objective of
              Polistack. The data collected from the predictions can be used in
              scientific research, providing real-world insights into the
              political sentiment.<br />
              <br />
              <br />
              4.2 Participation<br />
              <br />
              If a registered user has expert knowledge about a political event
              posted on the Website, he may Participate by expressing a yes/no
              opinion on the outcome of the said event. The rules of
              Participation are detailed under the “Frequently Asked Questions”
              ("FAQ") Section, which shall form an integral part of these terms
              and conditions. <br />
              <br />
              <br />
              4.3 Winnings<br />
              <br />
              The object of the platform is to use the data collected from the
              predictions for scientific research thereby providing real-world
              insights into the political sentiment and the winnings of the
              participants are merely an incentive to encourage the users to
              take part in the data collection process. The profit or loss of a
              participant shall be calculated at the end of each political
              event. The earnings of a participant shall be distributed after
              the market is resolved, subject to a deduction of fees by the
              Operator as stipulated in 4.4. <br />
              <br />
              <br />
              4.4 Fees<br />
              <br />
              The winnings of a participant shall be subject to the following
              fees, which shall be deducted by the Operator for covering the
              operating expenses and for providing the Service on the Platform:
              i. A fee of 10% shall be deducted by the Operator on the winnings
              of a participant, before the amount is distributed to the account
              of the participant. ii. In case a user wishes to withdraw any
              funds from his account, the Operator shall charge a withdrawal fee
              of 1%, as detailed in 6.4. <br />
              <br />
              <br />
              4.5 Research<br />
              <br />
              The main object of the platform is to use the data collected from
              the predictions for scientific research thereby providing
              real-world insights into the political sentiment. In furtherance
              to this objective, the data on the political predictions from the
              platform shall be made available by the Operator to universities
              involved in the research of politics in India. By participating on
              the Website, you hereby give your consent to usage of the data
              collected by the Operator, in any manner whatsoever in furtherance
              of the said objective, including making the said data available to
              universities and/or other organizations involved in the research
              of politics in India.
            </p>
          </Section>
          <Section>
            <Title>5. YOUR ACCOUNT</Title>
            <p>
              5.1. Single account<br />
              <br />
              Each registered user may only register and operate a single
              account with Polistack. If a registered user holds more than one
              account with Polistack, we reserve the right to close all accounts
              and confiscate all funds. <br />
              <br />
              <br />
              5.2. Accuracy<br />
              <br />
              You are required to keep your registration details up to date at
              all times. If you change your address, e-mail, phone number or any
              other contact or personal information, please contact
              support@polistack.com in order to update your account information.
              The name that you provide Polistack at registration must be
              identical to that listed on your government issued identification.{' '}
              <br />
              <br />
              <br />
              5.3. Password<br />
              <br />
              The Customer account registration process requires you to choose
              your own username and password combination. You must keep this
              information secret. Any actions carried out through your account
              will stand if your username and password have been entered
              correctly. Polistack can bear no responsibility for unauthorized
              use or misuse of personal details. <br />
              <br />
              <br />
              5.4. No liability<br />
              <br />
              Polistack shall accept no liability for any damages or losses
              which are deemed or alleged to have arisen out of or in connection
              with your Participation; including and without limitation, delays
              or interruptions in operation or transmission, loss or corruption
              of data, communication or lines failure, any person’s misuse of
              the Website, its content or any errors or omissions in the
              Website’s content. <br />
              <br />
              <br />
              5.5. Dormant / Inactive Accounts<br />
              <br />
              Polistack will consider an account to be dormant/inactive after a
              period of 6 months since the last account login. Polistack shall
              endeavour to contact the Customer prior to designating an account
              as Dormant. In case no response is received from the Customer
              within 7 days of the date on which Polistack has attempted to
              contact the Customer, the account will be designated as Dormant.
              Once your Customer account has been designated as a Dormant
              account, Polistack shall be entitled to charge you a monthly
              administration fee of INR 1,000/- (the “Administrative Fee”). The
              first Administrative Fee will be charged at the end of the 6th
              month after the last account login was recorded. Polistack will
              cease the deduction of any Administrative Fee should the Customer
              login into his account during the Dormancy period. <br />
              <br />
              <br />
              5.6. Interest<br />
              <br />
              Any funds held in your Customer account shall not attract
              interest. <br />
              <br />
              <br />
              5.7. Account Suspension<br />
              <br />
              Polistack reserves the right to suspend, close or terminate your
              Customer account at its sole discretion, should you be suspected
              of having violated these Terms & Conditions. You will be notified
              of Polistack’s decision via e-mail. During any suspension period,
              it will not be possible for you to unlock the account. <br />
              <br />
              <br />
              5.8. Account Closure<br />
              <br />
              If you wish to close your Customer account, please contact
              support@polistack.com for assistance. <br />
              <br />
              <br />
              5.9. Change<br />
              <br />
              Polistack reserves the right to suspend, modify or remove or add
              content to the Website or Services at its sole discretion with
              immediate effect and without notice. We shall not be liable to you
              for any loss suffered as a result of any changes made or for any
              modification or suspension of or discontinuance of the Website or
              Services and you shall have no claims against Polistack in such
              regard.
            </p>
          </Section>
          <Section>
            <Title>6. DEPOSITS AND WITHDRAWALS</Title>
            <p>
              6.1. Identification Checks<br />
              <br />
              Polistack may successfully receive and verify your identification
              documents, either via internal means or a third party, before you
              can make a Withdrawal from your account. <br />
              <br />
              <br />
              6.2. Credit Checks<br />
              <br />
              Polistack reserves the right to run external verification checks
              on all cardholders with third party credit agencies on the basis
              of the information provided on registration. <br />
              <br />
              <br />
              6.3. Records<br />
              <br />
              It is the cardholder’s responsibility to retain copies of
              transaction records and these Terms & Conditions as updated from
              time-to-time. <br />
              <br />
              <br />
              6.4 Deposits<br />
              <br />
              Each Registered User shall be entitled to transfer money into his
              account via the payment method of his choice, viz credit cards,
              debit cards and/or e-wallets. Once the funds are credited, we will
              notify the user regarding the same, via email and thereafter the
              funds shall be reflected in his account. <br />
              <br />
              The time that the deposit will take to process shall be dependent
              on the payment method selected. Once the funds are reflected in
              the account of the user, the user shall be entitled to express
              opinion on the website. <br />
              <br />
              In accordance with our anti-money laundering obligations, we
              reserve the right to raise queries or demand documentation related
              to the source of the deposited funds. If the provided information
              and/or documentation is not viewed as satisfactory, Polistack may
              suspend/terminate the Customer’s account and pass on any necessary
              information to the relevant authorities. <br />
              <br />
              It is clarified that any funds deposited by you and/or held in
              your Customer account shall not attract any interest whatsoever.{' '}
              <br />
              <br />
              <br />
              6.5. Withdrawals<br />
              <br />
              You shall be entitled to withdraw the funds (or a part thereof)
              lying in your account, at any time, by sending a request to the
              Operator, by cicking “Withdrawal” button in the user’s account.{' '}
              <br />
              <br />
              After reviewing your request for withdrawal, we shall transfer the
              funds sought to be withdrawn to the same payment method used to
              make a deposit to the account, subject to a deduction of 1% on the
              amount sought to be withdrawn (“Withdrawal Fee.”) <br />
              <br />
              Please note that withdrawals may experience a slight delay due to
              our identity verification process and certain deposit methods will
              require additional verification at time of Withdrawal. In the case
              of a withdrawal being made for the first time, a large withdrawal
              or changes being made to payment options, we may take additional
              security measures to ensure that you are the rightful recipient of
              the funds. <br />
              <br />
              <br />
              6.6 Updating Payment Details<br />
              <br />
              Updating or adding additional payment details for the sole purpose
              of making a withdrawal may only be done by contacting
              support@polistack.com. <br />
              <br />
              <br />
              6.7 Error<br />
              <br />
              Should funds be credited to a Customer's account or credited to a
              financial account and/or credit/debit card in error, it is the
              Customer's responsibility to notify Polistack of the error without
              delay. Any winnings subsequent to the error shall be deemed
              invalid and returned to Polistack. We reserve the right to
              withhold all or part of your balance and/or recover from your
              account deposits, pay outs and any winnings that are attributable
              to said error.
            </p>
          </Section>
          <Section>
            <Title>7. PRIVACY POLICY</Title>
            <p>
              Polistack is committed to protecting and respecting your privacy.
              Further, the Operator complies with all applicable data protection
              and privacy laws. If you do not understand how we handle or use
              the personal information you provide us, we recommend that you
              review our Privacy Policy.<br />
              <br />
              Our Privacy Policy is inseparably linked to these Terms &
              Conditions and its acceptance is a prerequisite to account
              registration.<br />
              <br />
              You hereby consent to receive marketing communications from the
              Operator in respect of its offerings by way of email, post, SMS
              and telephone notifications (including auto-calling), any of which
              you may unsubscribe from at any time by contacting
              support@polistack.com.
            </p>
          </Section>
          <Section>
            <Title>8. INTELLECTUAL PROPERTY</Title>
            <p>
              You acknowledge and agree that all right, title and interest in
              the Intellectual Property is our absolute property or duly
              licensed to us. Any use of the Intellectual Property without our
              prior written consent is not permitted. You agree not to (and
              agree not to assist or facilitate any third party to) copy,
              reproduce, transmit, publish, display, distribute, commercially
              exploit, or tamper with the Intellectual Property in any manner
              whatsoever.<br />
              <br />
              You acknowledge and agree that the material and content contained
              within the Website is made available for your personal,
              non-commercial use only. Any other use of such material and
              content is strictly prohibited.
            </p>
          </Section>
          <Section>
            <Title>9. INTERRUPTIONS IN SERVICES</Title>
            <p>
              9.1. No warranties<br />
              <br />
              The Service and the Website are provided on an “as is” basis and
              to the fullest extent permitted by law, we make no warranty or
              representation, whether express or implied, in relation to the
              satisfactory quality, fitness for purpose, completeness or
              accuracy of the Service or the Website.<br />
              <br />
              <br />
              9.2. Malfunctions<br />
              <br />
              We shall not be liable for any computer malfunctions, failure of
              telecommunications services or internet connections, nor attempts
              by you to Participate at Polistack by methods, means or ways not
              intended by us. In case of any error in the software in any
              manner, Polistack reserves the right to alter Customer balances
              and account details to correct such mistakes.<br />
              <br />
              <br />
              9.3. Viruses<br />
              <br />
              Although we shall take all reasonable measures to ensure that the
              Website is free from computer viruses we cannot and do not
              guarantee that the Website and is free of such problems. It is
              your responsibility to protect your systems and have in place the
              ability to reinstall any data or programs lost due to a virus.<br />
              <br />
              <br />
              9.4. Service Suspension<br />
              <br />
              We may temporarily suspend the whole or any part of the Service
              for any reason at our sole discretion. We may, but shall not be
              obliged to, give you as much notice as is reasonably practicable
              of such suspension. We will restore the Service, as soon as is
              reasonably practicable, after such temporary suspension.
            </p>
          </Section>
          <Section>
            <Title>10. CUSTOMER SERVICE AND CUSTOMER COMPLAINTS</Title>
            <p>
              10.1. Customer Service<br />
              <br />
              You may contact Customer Service at any time by the e-mail:
              support@polistack.com<br />
              <br />
              <br />
              10.2. Submission of Complaint/Claims<br />
              <br />
              Customer complaints/claims of any nature must be submitted within
              30 days of the issue occurring.<br />
              <br />
              In order to ensure that your complaint/claim is directed to and
              investigated by the correct department, written communication must
              be submitted to Polistack via the following means: E-mail:
              support@polistack.com<br />
              <br />
              <br />
              10.3. Information which Must be Included in any Written
              Communication with Polistack<br />
              <br />
              To protect your privacy, all email communications between you and
              Polistack should be carried out using the email address that you
              have registered against your Customer account held with Polistack.
              Failure to do so may result in our response being delayed.<br />
              <br />
              The following information must be included in any written
              communication with Polistack (including a Complaint): 1. your
              username; 2. your first name and surname, as registered on your
              Customer account; 3. a detailed explanation of the
              complaint/claim; and 4. any specific dates and times associated
              with the complaint/claim (if applicable).<br />
              <br />
              Please note that any failure to submit written communication with
              the information outlined above may result in a delay in our
              ability to identify and respond to your complaint/claim in a
              timely manner. Upon receipt, we will endeavour to reply to your
              communication within 72 hours. Further, best efforts will be made
              to resolve any reported matter promptly and, at a maximum, within
              one month.
            </p>
          </Section>
          <Section>
            <Title>11. INDEMNITY AND LIMITATION OF LIABILITY</Title>
            <p>
              11.1 Indemnity<br />
              <br />
              You hereby agree to indemnify and hold harmless us, our directors,
              officers, employees, shareholders, agents and affiliates, our
              ultimate parent and parent companies and any of our subsidiaries
              against any and all costs, expenses, liabilities and damages
              (whether direct, indirect, special, consequential, exemplary or
              punitive or other) arising from any Participation by you,
              including without limitation: 1. Visiting, use or re-use of the
              Website; 2. Use or re-use of the Website by means of
              telecommunication services; 3. Use or re-use of any materials at,
              or obtained from, the Website or any other source whatsoever; 4.
              Entry to, or use or re-use of the Website server; 5. Facilitating
              or making a deposit into your account at Polistack; 6.
              Participating on Polistack through any delivery mechanism offered;
              and 7. Acceptance and use of any winnings at or from Polistack.<br />
              <br />
              <br />
              11.2. Limitation of Liability<br />
              <br />
              The total liability of our directors, officers, employees,
              shareholders, agents and affiliates, our ultimate parent and
              parent companies and any of our subsidiaries to you in contract,
              tort, negligence or otherwise, for any loss or damage howsoever
              arising from any cause, whether direct or indirect, or for any
              amounts (even where we have been notified by you of the
              possibility of such loss or damage) shall not exceed the value of
              the money lying in your trading/customer account. <br />
              <br />
              <br />
              11.3. Links<br />
              <br />
              Polistack shall not be liable in contract, tort, negligence, or
              otherwise, for any loss or damage arising from or in any way
              connected with your use of any link contained on the Website. We
              are not responsible for the content contained on any internet site
              linked to/from the Websites or via the Services.
            </p>
          </Section>
          <Section>
            <Title>12. POLISTACK NOT A FINANCIAL INSTITUTION</Title>
            <p>
              12.1 No legal or tax advice<br />
              <br />
              Polistack does not provide advice regarding tax and/or legal
              matters. Customers who wish to obtain advice regarding tax and
              legal matters are advised to contact appropriate advisors.<br />
              <br />
              <br />
              12.2 Anti-Money Laundering<br />
              <br />
              Customers are strictly prohibited from using Polistack and its
              systems to facilitate any type of illegal money transfer. You must
              not use the Website for any unlawful or fraudulent activity or
              prohibited transaction (including money laundering proceeds of
              crime) under the laws of any jurisdiction that applies to you. If
              Polistack suspects that you may be engaging in, or have engaged in
              fraudulent, unlawful or improper activity, including money
              laundering activities or any conduct which violates these Terms &
              Conditions, your access to Polistack will be terminated
              immediately and your account may be blocked. If your account is
              terminated or blocked under such circumstances, Polistack is under
              no obligation to refund to you any money that may be in your
              account. In addition, Polistack shall be entitled to inform the
              relevant authorities, other online service providers, banks,
              credit card companies, electronic payment providers or other
              financial institution of your identity and of any suspected
              unlawful, fraudulent or improper activity. You will cooperate
              fully with any Polistack investigation into such activity. It is
              reiterated that your usage of the website and acceptance of the
              terms and conditions shall be construed as your affirmation that
              Participation at Polistack is legal for you and that you will not
              use the Website for any unlawful or fraudulent activity or
              prohibited transaction.
            </p>
          </Section>
          <Section>
            <Title>13. TERMINATION/SUSPENSION OF ACCOUNT</Title>
            <p>
              13.1 Polistack hereby reserves the right to cancel your account
              for any reason whatsoever at any time without notifying you. Any
              balance in your account at the time of such a cancellation will be
              credited to your credit/debit card or financial account.<br />
              <br />
              <br />
              13.2. Without limiting section 12.1, we hereby reserve the right,
              at our sole discretion, to cancel or suspend your account
              (notwithstanding any other provision contained in these Terms &
              Conditions) where we have reason to believe that you have engaged
              or are likely to engage in any of the following activities: 1. If
              you have more than one active account at Polistack; 2. You become
              bankrupt; 3. If you provide incorrect or misleading information
              while registering a Polistack account; 4. If you attempt to use
              your Customer account through a VPN, proxy or similar service that
              masks or manipulates the identification of your real location, or
              by otherwise providing false or misleading information regarding
              your citizenship, location or place of residence; 5. If you are
              not over 18 years old; 6. If you have allowed or permitted
              (whether intentionally or unintentionally) someone else to
              Participate using your Polistack account; 7. If Polistack has
              received a “charge back” and/or a "return" notification via a
              deposit mechanism used on your account; 8. If you have failed our
              Enhanced Due Diligence, if any, or are found to be colluding,
              cheating, money laundering or undertaking any kind of fraudulent
              activity.<br />
              <br />
              <br />
              13.3. If Polistack closes or suspends your Customer account for
              any of the reasons referred to in 12.2 above, you shall be liable
              for any and all claims, losses, liabilities, damages, costs and
              expenses incurred or suffered by Polistack (together “Claims”)
              arising therefrom and shall indemnify and hold Polistack harmless
              on demand for such Claims.<br />
              <br />
              <br />
              13.4. If we have reasonable grounds to believe that you have
              participated in any of the activities set out in clause 12.2 above
              then we reserve the right to withhold all or part of the balance
              and/or recover from your account deposits, payouts, bonuses, any
              winnings that are attributable to any of the practices
              contemplated in clause 12.2. In such circumstances, your details
              will be passed on to any applicable regulatory authority,
              regulatory body or any other relevant external third parties. The
              rights set out here are without prejudice to any other rights that
              we may have against you under these Terms & Conditions or
              otherwise.<br />
              <br />
              <br />
            </p>
          </Section>
          <Section>
            <Title>14. MISCELLANEOUS</Title>
            <p>
              14.1. Entire Agreement<br />
              <br />
              These Terms & Conditions represent the complete, final and
              exclusive agreement between you and Polistack and supersede and
              merge all prior agreements, representations and understandings
              between you and Polistack in regard to your Participation at
              Polistack.<br />
              <br />
              <br />
              14.2. Amendments to Term & Conditions<br />
              <br />
              Polistack hereby reserves the right to amend these Terms &
              Conditions, or to implement or amend any procedures, at any time.
              A notification message advising that changes have been made to our
              Terms & Conditions will appear upon a Customer’s next login to the
              Polistack Website and the Customer is required to accept the
              changes to the Terms & Conditions before being able to continue to
              Participate at Polistack.<br />
              <br />
              <br />
              14.3. Tax<br />
              <br />
              You are solely responsible for any applicable taxes on any
              winnings that you collect from Polistack.<br />
              <br />
              <br />
              14.4. Affirmation of Compliance<br />
              <br />
              Wherever in these Terms and conditions any criteria has been
              prescribed for any purpose whatsoever or wherever these Terms and
              Conditions require the assessment/compliance of the applicable
              laws on Your end, then the factum of registration and/or
              subsequent participation shall be deemed to be and construed as an
              affirmation on Your end of having satisfied the said Criteria or
              having complied with or being in Compliance with the applicable
              laws concerned. <br />
              <br />
              <br />
              14.5. Force Majeure<br />
              <br />
              Polistack shall not be liable or responsible for any failure to
              perform, or delay in performance of, any of our obligations qua
              the Services that is caused by events outside of our reasonable
              control.<br />
              <br />
              <br />
              14.6. No agency<br />
              <br />
              Nothing in these Terms & Conditions shall be construed as creating
              any agency, partnership, trust arrangement, fiduciary relationship
              or any other form of joint enterprise between you and us.<br />
              <br />
              <br />
              14.7. Severability<br />
              <br />
              If any of the Terms & Conditions are determined by any competent
              authority to be invalid, unlawful or unenforceable to any extent,
              such term, condition or provision will to that extent be severed
              from the remaining terms, conditions and provisions which will
              continue to be valid to the fullest extent permitted by law. In
              such cases, the part deemed invalid or unenforceable shall be
              amended in a manner consistent with the applicable law to reflect,
              as closely as possible, Polistack’s original intent. The Terms &
              Conditions prevail over any communication via email, chat or
              phone. Please note that all correspondence and telephone calls may
              be recorded.<br />
              <br />
              <br />
              14.8. Assignment<br />
              <br />
              These Terms & Conditions are personal to you, and are not
              assignable, transferable or sub-licensable by you except with our
              prior written consent. We reserve the right to assign, transfer or
              delegate any of our rights and obligations hereunder to any third
              party without notice to you.<br />
              <br />
              <br />
              14.9. Business Transfers<br />
              <br />
              In the event of a change of control, merger, acquisition, or sale
              of assets of the company, your Customer account and associated
              data may be part of the assets transferred to the purchaser or
              acquiring party. In such an event, we will provide you with notice
              via e-mail or notice on our Website explaining your options with
              regard to the transfer of your account.<br />
              <br />
              <br />
              14.10 Dispute Resolution and Arbitration<br />
              <br />
              All claims, disputes, differences or questions of any nature
              arising between you and Polistack, in relation to the
              construction, meaning or interpretation of any term used or clause
              of these Terms and Conditions or as to the rights, duties,
              liabilities of the parties in relation to or in connection with
              your Participation on Polistack and/or your use of the Website,
              shall be referred to a Sole Arbitrator who shall be appointed by
              the Operator. The seat and venue of the arbitration proceedings
              shall be Malta. The arbitration shall be conducted as per the laws
              of Malta, for the time being in force. <br />
              <br />
              <br />
              14.11 Jurisdiction<br />
              <br />
              The Courts in Malta shall have exclusive jurisdiction to determine
              any issues arising out of or in relation to the arbitration
              proceedings. <br />
              <br />
              <br />
              14.12 Waiver<br />
              <br />
              The failure or delay on part of the Operator to enforce or to
              exercise, at any time or for any period of time, any term of or
              any right, power or privilege arising pursuant to these Terms does
              not constitute and shall not be construed as a waiver or
              acquiescence of such term or right and shall in no way affect the
              Operator’s right later to enforce or exercise it nor shall any
              single or partial exercise of any remedy, right, power or
              privilege preclude any further exercise of the same or the
              exercise of any other remedy, right, power or privilege.
            </p>
          </Section>
        </Container>
      </Wrapper>
    )
  }
}

Terms.propTypes = {
  host: PropTypes.string,
  protocol: PropTypes.string
}

const mapStateToProps = ({ site: { host, protocol } }) => ({ host, protocol })

export default connect(mapStateToProps)(Terms)
