import styled from 'styled-components'

const Wrapper = styled.div`
  position: fixed;
  padding: 1em;
  background: #fafcff;
  max-width: 12em;
  transition: 0.3s ease-in-out;

  left: ${props => (props.open ? 0 : '-11.25em')};
  box-shadow: ${props =>
    props.open && '0 0.125em 1.25em 0 rgba(165, 183, 195, 0.65)'};

  @media screen and (min-width: 992px) {
    position: relative;
    left: auto;
    background: transparent;
    padding: 0;
    padding-right: 1.25em;
  }

  h4 {
    margin: 1.25em 0;
  }
`

export default Wrapper
