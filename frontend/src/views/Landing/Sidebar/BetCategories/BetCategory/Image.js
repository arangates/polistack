import styled from 'styled-components'

const Image = styled.div`
  text-align: center;
  width: 1.375em;
`

export default Image
