import styled from 'styled-components'

const Title = styled.div`
  font-size: 0.875rem;
  padding-left: 1em;
  font-weight: 500;
`

export default Title
