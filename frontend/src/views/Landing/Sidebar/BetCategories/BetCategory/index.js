/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Svg from 'components/Svg'
import { getSerializedCategoryName } from 'utils'
import PropTypes from 'prop-types'
import Item from './Item'
import Image from './Image'
import Title from './Title'
import {
  getRouteParams,
  getUrlFromRouteParams,
  setCategoryName,
  unsetPageNumber
} from '../../../utilities'

class BetCategory extends Component {
  render() {
    const { isActive, icon, category, location, match } = this.props
    const routeParams = getRouteParams(location, match)
    let nextRouteParams = unsetPageNumber(routeParams)
    nextRouteParams = setCategoryName(
      nextRouteParams,
      getSerializedCategoryName(category)
    )
    const categoryUrl = getUrlFromRouteParams(nextRouteParams)
    return (
      <li>
        <Item isactive={isActive ? 1 : 0} to={categoryUrl}>
          <Image>
            <Svg icon={icon} style={{ maxWidth: '22px' }} />
          </Image>
          <Title>{category === null ? 'All' : category}</Title>
        </Item>
      </li>
    )
  }
}

BetCategory.propTypes = {
  isActive: PropTypes.bool,
  icon: PropTypes.string,
  category: PropTypes.string
}

export default withRouter(BetCategory)
