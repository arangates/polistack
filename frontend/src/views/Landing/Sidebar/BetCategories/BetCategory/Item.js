/* eslint-disable react/prop-types */
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { PropTypes } from 'prop-types'

const Item = styled(Link)`
  color: #828f97;
  cursor: pointer;
  display: flex;
  align-items: center;
  padding: 1em 0 1em 1.25em;
  transition: 0.3s ease-in-out;
  position: relative;
  &::before {
    content: '';
    position: absolute;
    left: 0;
    height: 0;

    transition: 0.3s ease-in-out;
  }

  &:hover {
    color: #102b3b;
    &::before {
      height: 100%;
      width: 4px;
      background: #f25c5e;
      top: 0;
    }
    svg {
      fill: #102b3b;
    }
  }

  &::before {
    height: ${props => (props.isactive ? '100%' : '')};
    width: ${props => (props.isactive ? '4px' : '')};
    background: ${props => (props.isactive ? '#f25c5e' : '')};
  }
  svg {
    fill: ${props => (props.isactive ? '#102b3b' : '#828f97')};
  }

  color: ${props => (props.isactive ? '#102b3b' : '')};
`

Item.propTypes = {
  isactive: PropTypes.number
}

export default Item
