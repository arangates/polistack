/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import BetCategory from './BetCategory'
import PropTypes from 'prop-types'
import { iconAllBets, iconOther } from 'images'
import { getSerializedCategoryName } from 'utils'

class BetCategories extends Component {
  render() {
    const { currentCategory, categories } = this.props
    return (
      <div>
        <ul className="category__list">
          <BetCategory
            icon={iconAllBets}
            category={null}
            isActive={currentCategory === null}
          />
          {categories.map(item => (
            <BetCategory
              key={item.name}
              icon={item.icon}
              category={item.name}
              isActive={
                currentCategory === getSerializedCategoryName(item.name)
              }
            />
          ))}
          <BetCategory
            icon={iconOther}
            category="Other"
            isActive={currentCategory === 'Other'}
          />
        </ul>
      </div>
    )
  }
}

BetCategories.propTypes = {
  categories: PropTypes.array
}

export default BetCategories
