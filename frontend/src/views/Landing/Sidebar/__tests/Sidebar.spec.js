/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Sidebar from '../index'

describe('Sidebar', () => {
  it('should render correctly', () => {
    const output = shallow(<Sidebar />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
