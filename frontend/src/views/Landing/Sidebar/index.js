/* eslint-disable react/prop-types */
import React, { PureComponent } from 'react'
import BetCategories from './BetCategories'
import PropTypes from 'prop-types'
import Wrapper from './Wrapper'

class Sidebar extends PureComponent {
  state = {
    open: false
  }

  handleSidebarOpen = () => {
    this.setState(prev => {
      return { open: !prev.open }
    })
  }

  render() {
    const { categories, currentCategory } = this.props

    return (
      <Wrapper open={this.state.open}>
        <h4>Select category</h4>
        <BetCategories
          categories={categories}
          currentCategory={currentCategory}
        />
      </Wrapper>
    )
  }
}

Sidebar.propTypes = {
  categories: PropTypes.array
}

export default Sidebar
