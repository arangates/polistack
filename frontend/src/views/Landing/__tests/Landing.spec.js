/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Landing from '../index'
import configureStore from 'redux-mock-store'

const mockStore = configureStore()

const store = mockStore({bet: {
    current: {
      count: 27,
      next: 'http://localhost:8000/bets/?page=2',
      previous: null,
      results: [
        {
          id: 1,
          yes: 3,
          no: 2,
          statement: 'Trump will be reelected in 2020',
          ends: '2010-08-05',
          created_by: 3,
          image: 'http://localhost:8000/media/trump.jpg',
          status: 'ACTIVE',
          volume: 50288
        },
        {
          id: 2,
          yes: 1,
          no: 6,
          statement: 'Twitter account of Trump will be banned at least once by the end of 2018',
          ends: '1990-03-23',
          created_by: 3,
          image: 'http://localhost:8000/media/trumptwitterban.png',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 3,
          yes: 1,
          no: 2,
          statement: 'Trump will tweet the words Crooked Hillary until end of 2018',
          ends: '1993-04-02',
          created_by: 3,
          image: 'http://localhost:8000/media/trumpclinton.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 4,
          yes: 1,
          no: 2,
          statement: 'Melania Trump will give birth to a child by 2020',
          ends: '2013-01-12',
          created_by: 3,
          image: 'http://localhost:8000/media/melaniatrump.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 5,
          yes: 1,
          no: 10,
          statement: 'UK governement will ban Trump from visiting before the end of his first term',
          ends: '2006-07-24',
          created_by: 3,
          image: 'http://localhost:8000/media/ukgovtbantrump.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 6,
          yes: 1,
          no: 50,
          statement: 'Eminem will publicly endorse Trump in the 2020 elections',
          ends: '1998-11-08',
          created_by: 3,
          image: 'http://localhost:8000/media/eminemtrump.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 7,
          yes: 1,
          no: 2,
          statement: 'LeBron James will have more points per game than James Harden at season end',
          ends: '1994-06-15',
          created_by: 3,
          image: 'http://localhost:8000/media/lebronharden.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 8,
          yes: 1,
          no: 50,
          statement: 'Bill Gates will become the sole owner of a NBA team in 2018',
          ends: '1982-05-20',
          created_by: 3,
          image: 'http://localhost:8000/media/gatesnba.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 9,
          yes: 1,
          no: 5,
          statement: 'Cristiano Ronaldo will score 20 goals or more in the Champions League in 2018',
          ends: '1999-11-23',
          created_by: 3,
          image: 'http://localhost:8000/media/cristianoronaldo.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 10,
          yes: 1,
          no: 8,
          statement: 'Abramovich will sell all his shares in Chelsea by end of 2018',
          ends: '1972-07-23',
          created_by: 3,
          image: 'http://localhost:8000/media/abramovichchelsea.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 11,
          yes: 1,
          no: 8,
          statement: 'North Korea will drop an atomic bomb on US soil by end of 2018',
          ends: '2015-01-22',
          created_by: 3,
          image: 'http://localhost:8000/media/northkoreabomb.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 12,
          yes: 1,
          no: 6,
          statement: 'Zuckerberg will get arrested by end of 2019',
          ends: '1979-02-01',
          created_by: 3,
          image: 'http://localhost:8000/media/zuckerbergarrested.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 13,
          yes: 1,
          no: 5,
          statement: 'Uber will be banned in the State of New York by 2020',
          ends: '1979-02-02',
          created_by: 3,
          image: 'http://localhost:8000/media/uberbanned.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 14,
          yes: 1,
          no: 10,
          statement: 'Xi Jinping will step down by 2020',
          ends: '1974-03-22',
          created_by: 3,
          image: 'http://localhost:8000/media/xistepdown.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 15,
          yes: 1,
          no: 15,
          statement: 'China will expel at least one Russian diplomat in 2018',
          ends: '2012-06-17',
          created_by: 3,
          image: 'http://localhost:8000/media/chinaexpelsrussia.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 16,
          yes: 1,
          no: 5,
          statement: 'Duterte will insult Trump publicly in 2018',
          ends: '2010-10-23',
          created_by: 3,
          image: 'http://localhost:8000/media/dutertetrump.jpg',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 17,
          yes: 1,
          no: 8,
          statement: 'Bitcoin will rise above 15000 by end of April',
          ends: '1997-02-02',
          created_by: 3,
          image: 'http://localhost:8000/media/bitcoinrise.png',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 18,
          yes: 1,
          no: 4,
          statement: 'Ethereum will fall below 300 by end of May',
          ends: '1973-12-30',
          created_by: 3,
          image: 'http://localhost:8000/media/ethereumfall.png',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 19,
          yes: 1,
          no: 10,
          statement: 'Binance will file for bankrupcy protection by the end of 2018',
          ends: '2007-03-30',
          created_by: 3,
          image: 'http://localhost:8000/media/binancebankrupt.png',
          status: 'ACTIVE',
          volume: 100
        },
        {
          id: 20,
          yes: 1,
          no: 20,
          statement: 'Michelle and Barack Obama will get a divorce by 2020',
          ends: '2016-10-22',
          created_by: 3,
          image: 'http://localhost:8000/media/obamasdivorce.jpg',
          status: 'ACTIVE',
          volume: 100
        }
      ]
    },
    isLoading: false
  },
  category: {
    categories: [
      {
        name: 'Cryptocurrencies',
        icon: '\n<svg width="22" height="14" xmlns="http://www.w3.org/2000/svg">\n    <path d="M15 6a3 3 0 1 0 0-6 3 3 0 0 0 0 6M7 6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m0 2c-2.33 0-7 1.17-7 3.5V14h14v-2.5C14 9.17 9.33 8 7 8m8 0c-.29 0-.62 0-.97.05C15.19 8.891 16 10 16 11.5V14h6v-2.5C22 9.17 17.33 8 15 8"\n          fill-rule="evenodd"/>\n</svg>\n'
      },
      {
        name: 'Financial Markets',
        icon: '\n<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">\n    <path d="M10.19 9.115L3.5 2.455c2.17-1.9 4.72-2.7 7.63-2.39-.22.66-.33 1.38-.33 2.16 0 .72.16 1.53.49 2.44.33.9.71 1.62 1.21 2.15l-2.31 2.3zm9.47-1.73c-.6.21-1.22.32-1.88.32-1.09 0-2.14-.32-3.14-.98l3.091-3.05a10.79 10.79 0 0 1 1.929 3.71zm-6.1-1.65c-1.34-1.65-1.65-3.45-.93-5.39.62.16 1.33.46 2.13.92.79.45 1.44.9 1.94 1.33l-3.14 3.14zm-6.75 6.77a6.695 6.695 0 0 0-3.23-1.59c-1.22-.23-2.39-.16-3.52.22-.03-.22-.06-.6-.06-1.13 0-1.03.25-2.18.72-3.45.47-1.26 1.05-2.28 1.73-3.05l6.66 6.69-2.3 2.31zm3.38-1.28l6.31 6.329c-2.17 1.901-4.72 2.701-7.62 2.391.21-.66.32-1.38.32-2.16 0-.62-.14-1.35-.42-2.18-.28-.83-.61-1.511-.98-2.04l2.39-2.34zm-3.42 3.42c1.06 1.53 1.28 3.199.65 5.02-1.42-.41-2.69-1.05-3.75-1.93l3.1-3.09zm6.79-6.84c1.97 1.47 4.1 1.83 6.38 1.08.03.21.06.59.06 1.12 0 1.03-.25 2.18-.72 3.45-.47 1.26-1.049 2.28-1.73 3.05l-6.33-6.31 2.34-2.39zM.34 12.635c.6-.22 1.22-.331 1.88-.331 1.34 0 2.51.421 3.51 1.261l-3.14 3.14a10.61 10.61 0 0 1-2.25-4.07z"\n          fill-rule="evenodd"/>\n</svg>\n'
      },
      {
        name: 'People',
        icon: '\n<svg xmlns="http://www.w3.org/2000/svg" width="40" height="26" viewBox="0 0 40 26">\n    <path fill="#102B3B" fill-rule="evenodd" d="M27.2727273,11.0344828 C30.2854545,11.0344828 32.7272727,8.5645977 32.7272727,5.51724138 C32.7272727,2.46988506 30.2854545,0 27.2727273,0 C24.26,0 21.8181818,2.46988506 21.8181818,5.51724138 C21.8181818,8.5645977 24.26,11.0344828 27.2727273,11.0344828 M12.7272727,11.0344828 C15.74,11.0344828 18.1818182,8.5645977 18.1818182,5.51724138 C18.1818182,2.46988506 15.74,0 12.7272727,0 C9.71454545,0 7.27272727,2.46988506 7.27272727,5.51724138 C7.27272727,8.5645977 9.71454545,11.0344828 12.7272727,11.0344828 M12.7272727,14.7126437 C8.49090909,14.7126437 0,16.8643678 0,21.1494253 L0,25.7471264 L25.4545455,25.7471264 L25.4545455,21.1494253 C25.4545455,16.8643678 16.9636364,14.7126437 12.7272727,14.7126437 M27.2727273,14.7126437 C26.7454545,14.7126437 26.1454545,14.7126437 25.5090909,14.8045977 C27.6181818,16.3512644 29.0909091,18.3908046 29.0909091,21.1494253 L29.0909091,25.7471264 L40,25.7471264 L40,21.1494253 C40,16.8643678 31.5090909,14.7126437 27.2727273,14.7126437"/>\n</svg>\n'
      },
      {
        name: 'Sport',
        icon: '\n<svg width="22" height="14" xmlns="http://www.w3.org/2000/svg">\n    <path d="M15 6a3 3 0 1 0 0-6 3 3 0 0 0 0 6M7 6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m0 2c-2.33 0-7 1.17-7 3.5V14h14v-2.5C14 9.17 9.33 8 7 8m8 0c-.29 0-.62 0-.97.05C15.19 8.891 16 10 16 11.5V14h6v-2.5C22 9.17 17.33 8 15 8"\n          fill-rule="evenodd"/>\n</svg>\n'
      },
      {
        name: 'Trump',
        icon: '\n<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">\n    <path d="M10.19 9.115L3.5 2.455c2.17-1.9 4.72-2.7 7.63-2.39-.22.66-.33 1.38-.33 2.16 0 .72.16 1.53.49 2.44.33.9.71 1.62 1.21 2.15l-2.31 2.3zm9.47-1.73c-.6.21-1.22.32-1.88.32-1.09 0-2.14-.32-3.14-.98l3.091-3.05a10.79 10.79 0 0 1 1.929 3.71zm-6.1-1.65c-1.34-1.65-1.65-3.45-.93-5.39.62.16 1.33.46 2.13.92.79.45 1.44.9 1.94 1.33l-3.14 3.14zm-6.75 6.77a6.695 6.695 0 0 0-3.23-1.59c-1.22-.23-2.39-.16-3.52.22-.03-.22-.06-.6-.06-1.13 0-1.03.25-2.18.72-3.45.47-1.26 1.05-2.28 1.73-3.05l6.66 6.69-2.3 2.31zm3.38-1.28l6.31 6.329c-2.17 1.901-4.72 2.701-7.62 2.391.21-.66.32-1.38.32-2.16 0-.62-.14-1.35-.42-2.18-.28-.83-.61-1.511-.98-2.04l2.39-2.34zm-3.42 3.42c1.06 1.53 1.28 3.199.65 5.02-1.42-.41-2.69-1.05-3.75-1.93l3.1-3.09zm6.79-6.84c1.97 1.47 4.1 1.83 6.38 1.08.03.21.06.59.06 1.12 0 1.03-.25 2.18-.72 3.45-.47 1.26-1.049 2.28-1.73 3.05l-6.33-6.31 2.34-2.39zM.34 12.635c.6-.22 1.22-.331 1.88-.331 1.34 0 2.51.421 3.51 1.261l-3.14 3.14a10.61 10.61 0 0 1-2.25-4.07z"\n          fill-rule="evenodd"/>\n</svg>\n'
      },
      {
        name: 'World news',
        icon: '\n<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">\n    <path d="M10.19 9.115L3.5 2.455c2.17-1.9 4.72-2.7 7.63-2.39-.22.66-.33 1.38-.33 2.16 0 .72.16 1.53.49 2.44.33.9.71 1.62 1.21 2.15l-2.31 2.3zm9.47-1.73c-.6.21-1.22.32-1.88.32-1.09 0-2.14-.32-3.14-.98l3.091-3.05a10.79 10.79 0 0 1 1.929 3.71zm-6.1-1.65c-1.34-1.65-1.65-3.45-.93-5.39.62.16 1.33.46 2.13.92.79.45 1.44.9 1.94 1.33l-3.14 3.14zm-6.75 6.77a6.695 6.695 0 0 0-3.23-1.59c-1.22-.23-2.39-.16-3.52.22-.03-.22-.06-.6-.06-1.13 0-1.03.25-2.18.72-3.45.47-1.26 1.05-2.28 1.73-3.05l6.66 6.69-2.3 2.31zm3.38-1.28l6.31 6.329c-2.17 1.901-4.72 2.701-7.62 2.391.21-.66.32-1.38.32-2.16 0-.62-.14-1.35-.42-2.18-.28-.83-.61-1.511-.98-2.04l2.39-2.34zm-3.42 3.42c1.06 1.53 1.28 3.199.65 5.02-1.42-.41-2.69-1.05-3.75-1.93l3.1-3.09zm6.79-6.84c1.97 1.47 4.1 1.83 6.38 1.08.03.21.06.59.06 1.12 0 1.03-.25 2.18-.72 3.45-.47 1.26-1.049 2.28-1.73 3.05l-6.33-6.31 2.34-2.39zM.34 12.635c.6-.22 1.22-.331 1.88-.331 1.34 0 2.51.421 3.51 1.261l-3.14 3.14a10.61 10.61 0 0 1-2.25-4.07z"\n          fill-rule="evenodd"/>\n</svg>\n'
      }
    ],
    currentCategory: null,
    isLoading: false
  },})

describe('Landing', () => {
  it('should render correctly', () => {
    const output = shallow(<Landing store={store} />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
