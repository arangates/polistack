import queryString from 'query-string'

const areSetsEqual = (a, b) => {
  if (a.size !== b.size) {
    return false
  }
  for (const key of a)
    if (!b.has(key)) {
      return false
    }
  return true
}

// Shallow equality check at the moment, can be customized when needed
export const areRouteParamsEqual = (current, previous) => {
  const currentKeys = new Set(Object.keys(current))
  const previousKeys = new Set(Object.keys(previous))
  return (
    areSetsEqual(currentKeys, previousKeys) &&
    Object.keys(current).reduce(
      (acc, key) => acc && current[key] === previous[key],
      true
    )
  )
}

// Get the sanitized current page number from the URL
export const getPageNumber = routeParams => {
  const rawPageNumber = routeParams.page
  let pageNumber = 1
  try {
    pageNumber = Number.parseInt(rawPageNumber)
  } catch (e) {
    pageNumber = 1
  }
  if (isNaN(pageNumber) || pageNumber <= 0) {
    // sanitize
    pageNumber = 1
  }
  return pageNumber
}

export const unsetPageNumber = routeParams => {
  let nextRouteParams = { ...routeParams }
  delete nextRouteParams.page
  return nextRouteParams
}

export const setPageNumber = (routeParams, nextPageNumber) => {
  let nextRouteParams = { ...routeParams }
  nextRouteParams.page = nextPageNumber
  return nextRouteParams
}

export const isPageNumberEqual = (routeParams, pageNumber) =>
  routeParams.page === pageNumber

// Get the category name from the URL
export const getCategoryName = routeParams =>
  (routeParams && routeParams.categoryName) || null

// Get the sort order from the URL
export const getSortOrder = routeParams =>
  (routeParams && routeParams.sortOrder) || null

export const isCategoryNameEqual = (routeParams, categoryName) =>
  routeParams.categoryName === categoryName

export const setCategoryName = (routeParams, categoryName) => ({
  ...routeParams,
  categoryName
})

// Get the search phrase from the URL
export const getSearchPhrase = routeParams => routeParams.q || null

// Get a dictionary of all route parameters from the URL
export const getRouteParams = (location, match) => {
  const queryParams = queryString.parse(location.search)
  const urlParams = match.params
  const routeParams = {
    page: getPageNumber(queryParams),
    q: getSearchPhrase(queryParams),
    sortOrder: getSortOrder(queryParams),
    categoryName: getCategoryName(urlParams)
  }
  return routeParams
}

export const getCurrentCategoryPageTitle = (categoryName, currentTitle) => {
  return categoryName
    ? currentTitle.split(' - ').join(` - ${categoryName} - `)
    : currentTitle
}

export const getReferrer = location => {
  if (location.referrer) {
    return location.referrer
  }
}

export const getUrlFromRouteParams = routeParams => {
  const categoryName = routeParams.categoryName
  // Remove any route params that are not query params
  const queryParamKeys = ['page', 'q', 'sortOrder']
  let nextQueryParams = {}
  queryParamKeys.forEach(key => {
    if (typeof routeParams[key] !== 'undefined' && routeParams[key] !== null) {
      nextQueryParams[key] = routeParams[key]
    }
  })
  // If the page number is 1, remove it from the URL altogether
  if (nextQueryParams.page === 1) {
    delete nextQueryParams.page
  }
  const nextQueryString = queryString.stringify(nextQueryParams)
  return `/${categoryName ? `category/${categoryName}/` : ''}${
    nextQueryString.length > 0 ? `?${nextQueryString}` : ''
  }`
}
