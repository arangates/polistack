import styled from 'styled-components'

const Wrapper = styled.div`
  flex: 1;
  max-width: 100%;

  @media screen and (min-width: 992px) {
    padding-left: ${props => props.landing && '1.875em'};
  }
`

export default Wrapper
