/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import Svg from 'components/Svg'

import Category from './Category'
import Toggler from './Toggler'
import Wrapper from './Wrapper'
import Title from './Title'

import { iconCategories, iconAllBets, iconOther, iconDelete } from 'images'

class Filters extends Component {
  state = {
    filtersOpened: false
  }

  toggleFilters = () => {
    this.setState(prev => {
      return { filtersOpened: !prev.filtersOpened }
    })
  }

  render() {
    const { currentCategory, categories } = this.props
    return (
      <React.Fragment>
        <Toggler onClick={this.toggleFilters}>
          {this.state.filtersOpened ? (
            <Svg icon={iconDelete} />
          ) : (
            <Svg icon={iconCategories} />
          )}
        </Toggler>
        <Wrapper open={this.state.filtersOpened}>
          <Title>Categories</Title>
          <ul>
            <Category
              icon={iconAllBets}
              category={null}
              isActive={currentCategory === null}
            />
            {categories.map(item => (
              <Category
                key={item.name}
                toggleFilters={this.toggleFilters}
                icon={item.icon}
                category={item.name}
                isActive={currentCategory === item.name}
              />
            ))}
            <Category
              icon={iconOther}
              category="Other"
              toggleFilters={this.toggleFilters}
              isActive={currentCategory === 'Other'}
            />
          </ul>
        </Wrapper>
      </React.Fragment>
    )
  }
}

export default Filters
