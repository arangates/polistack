import styled from 'styled-components'

const Toggler = styled.div`
  background: #fff;
  border-radius: 1.844em;
  box-shadow: 0em 0.125em 0.25em 0em #828f97;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 2.5em;
  width: 6.25em;
  position: fixed;
  bottom: 1.25em;
  left: 50%;
  transform: translateX(-50%);
  z-index: 2;
`

export default Toggler
