import styled from 'styled-components'
import { PropTypes } from 'prop-types'

const Item = styled.span`
  color: #828f97;
  cursor: pointer;
  display: flex;
  align-items: center;
  transition: 0.3s ease-in-out;
  position: relative;

  padding: 1.25em;
  border: 0.063em solid #eef3f6;
  border-radius: 0.25em;

  &:not(:last-child) {
    margin-bottom: 1.25em;
  }

  &:hover {
    color: #102b3b;
    svg {
      fill: #102b3b;
    }
  }
  svg {
    fill: ${props => (props.active ? '#102b3b' : '#828f97')};
  }

  color: ${props => (props.active ? '#102b3b' : '')};
`
Item.propTypes = {
  active: PropTypes.number
}

export default Item
