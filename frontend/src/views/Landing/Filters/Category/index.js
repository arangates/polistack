/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import Svg from 'components/Svg'
import PropTypes from 'prop-types'
import { getSerializedCategoryName } from 'utils'
import {
  getRouteParams,
  unsetPageNumber,
  setCategoryName,
  getUrlFromRouteParams
} from '../../utilities'
import Item from './Item'
import Image from './Image'
import Title from './Title'

class Category extends Component {
  render() {
    const {
      isActive,
      icon,
      category,
      location,
      match,
      toggleFilters
    } = this.props
    const routeParams = getRouteParams(location, match)
    let nextRouteParams = unsetPageNumber(routeParams)
    nextRouteParams = setCategoryName(
      nextRouteParams,
      getSerializedCategoryName(category)
    )
    const categoryUrl = getUrlFromRouteParams(nextRouteParams)
    return (
      <li onClick={toggleFilters}>
        <Link to={categoryUrl}>
          <Item active={isActive ? 1 : 0}>
            <Image>
              <Svg icon={icon} />
            </Image>
            <Title>{category === null ? 'All' : category}</Title>
          </Item>
        </Link>
      </li>
    )
  }
}

Category.propTypes = {
  isActive: PropTypes.bool,
  icon: PropTypes.string,
  category: PropTypes.string,
  location: PropTypes.object,
  match: PropTypes.object,
  toggleFilters: PropTypes.func
}

export default withRouter(Category)
