import styled from 'styled-components'

const Image = styled.div`
  text-align: center;
  width: 3em;
`

export default Image
