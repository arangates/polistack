import styled from 'styled-components'

const Wrapper = styled.div`
  position: fixed;
  top: ${props => (props.open ? 'calc(3.75em + 1px)' : '100%')};
  background: #fff;
  left: 0;
  width: 100%;
  height: calc(100% - 3.75em - 1px);
  padding: 1.375em;
  overflow-y: scroll;

  visibility: ${props => (props.open ? 'visible' : 'hidden')};
  opacity: ${props => (props.open ? '1' : '0')};

  transition: 0.3s ease-in-out;
`

export default Wrapper
