import styled from 'styled-components'

const Title = styled.h2`
  font-size: 1rem;
  text-transform: uppercase;
  padding: 0.5em 0 1.5em 0;
`

export default Title
