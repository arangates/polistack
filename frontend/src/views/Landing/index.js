/* eslint-disable react/prop-types,no-class-assign */
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Helmet } from 'react-helmet'
import { withRouter } from 'react-router-dom'
import MediaQuery from 'react-responsive'
import { bindActionCreators } from 'redux'
import { fetchBets } from 'actions/betActions'
import { fetchCategories } from 'actions/categoryActions'
import { Container, Heading, Main, Loader } from 'components'
import { DEFAULT_PAGE_TITLE, DEFAULT_PAGE_DESCRIPTION } from 'constants/globals'
import Sidebar from './Sidebar'
import Content from './Content'
import Filters from './Filters'
import Wrapper from './Wrapper'
import {
  areRouteParamsEqual,
  getCategoryName,
  getSortOrder,
  getPageNumber,
  getRouteParams,
  getUrlFromRouteParams,
  getCurrentCategoryPageTitle,
  getReferrer,
  isPageNumberEqual,
  setPageNumber,
  unsetPageNumber
} from './utilities'
import { getDeserializedCategoryName } from 'utils'
import queryString from 'query-string'
import { logoJsonldDark } from 'images'

class Landing extends PureComponent {
  // API calls are done inside a static method so it can be invoked on the server side as well,
  // (We don't have access to component instances on the server-side)
  // TODO find out whether it's possible to gain access to component instances as they're rendered on the server
  static async loadData(
    location,
    match,
    dispatch,
    shouldLoadCategories = true
  ) {
    const routeParams = getRouteParams(location, match)
    if (shouldLoadCategories) {
      await dispatch(fetchCategories())
    }
    await dispatch(fetchBets(routeParams))
  }

  // Convert a dictionary of route parameters to a URL and then navigate to that URL
  setRouteParams = nextRouteParams => {
    const nextUrl = getUrlFromRouteParams(nextRouteParams)
    this.props.history.push(nextUrl)
  }

  // Update the page number in the URL
  setPageNumber = pageNumber => {
    const { location, match } = this.props
    // TODO memoize this
    const routeParams = getRouteParams(location, match)
    if (!isPageNumberEqual(routeParams, pageNumber)) {
      let nextRouteParams = unsetPageNumber(routeParams)
      if (pageNumber) {
        nextRouteParams = setPageNumber(nextRouteParams, pageNumber)
      }
      this.setRouteParams(nextRouteParams)
    }
  }

  sortBets = params => {
    const { history } = this.props
    // Discard the page number
    const nextQueryParams = {}
    if (params.value) {
      nextQueryParams.sortOrder = params.value
    }
    const nextQueryString = queryString.stringify(nextQueryParams)
    history.push(`/${nextQueryString.length > 0 ? `?${nextQueryString}` : ''}`)
  }
  // https://developers.google.com/search/docs/data-types/carousel
  generateBetsJSONLDCarousel = bets => {
    if (bets && bets.results) {
      const itemList = bets.results.map((bet, betIndex) => {
        return {
          '@type': 'ListItem',
          position: (betIndex + 1).toString,
          item: {
            '@type': 'NewsArticle',
            mainEntityOfPage: {
              '@type': 'WebPage',
              '@id': 'https://polistack.com/'
            },
            headline: bet.statement,
            image: [bet.image],
            datePublished: '2019-12-05T08:00:00+08:00',
            dateModified: '2019-12-05T09:20:00+08:00',
            author: {
              '@type': 'Organization',
              name: 'Polistack'
            },
            publisher: {
              '@type': 'Organization',
              name: 'Polistack',
              logo: {
                '@type': 'ImageObject',
                url: logoJsonldDark
              }
            },
            description: bet.statement
          }
        }
      })
      return {
        '@context': 'https://schema.org',
        '@type': 'ItemList',
        itemListElement: [...itemList]
      }
    }
  }
  async componentDidMount() {
    const { location, match, dispatch } = this.props
    await Landing.loadData(location, match, dispatch)
    window.scrollTo(0, 0)
  }

  async componentDidUpdate(prevProps) {
    const { location, match, dispatch } = this.props
    const { location: prevLocation, match: prevMatch } = prevProps
    // TODO memoize this computation
    const routeParams = getRouteParams(location, match)
    const prevRouteParams = getRouteParams(prevLocation, prevMatch)
    if (!areRouteParamsEqual(routeParams, prevRouteParams)) {
      await Landing.loadData(location, match, dispatch, false)
      window.scrollTo(0, 0)
    }
  }

  render() {
    const {
      host,
      protocol,
      bets,
      categories,
      isCategoryLoading,
      isLoading,
      location,
      match
    } = this.props
    const canonicalUrl = `${protocol}://${host}`

    // TODO memoize this
    const routeParams = getRouteParams(location, match)
    const pageNumber = getPageNumber(routeParams)
    const categoryName = getCategoryName(routeParams)
    const sortOrder = getSortOrder(routeParams)
    const referrer = getReferrer(location)
    return (
      <Container fullHeight>
        <Helmet>
          <title>
            {getCurrentCategoryPageTitle(
              getDeserializedCategoryName(categoryName),
              DEFAULT_PAGE_TITLE
            )}
          </title>
          <link rel="canonical" href={canonicalUrl} />
          <meta
            name="google-site-verification"
            content="W5O6GcfjbQyB3NQwCUThu5Ji0QsGoRGcpbrzAlyp6II"
          />
          <meta name="description" content={DEFAULT_PAGE_DESCRIPTION} />
        </Helmet>
        <Heading>Polistack - Predict politics</Heading>
        <Helmet>
          <script type="application/ld+json">
            {JSON.stringify(this.generateBetsJSONLDCarousel(bets))}
          </script>
        </Helmet>

        <Main>
          <Sidebar
            isLoading={isCategoryLoading}
            categories={categories}
            currentCategory={categoryName}
          />
          <Wrapper>
            {!isLoading ? (
              <Content
                changeSort={this.sortBets}
                sortOrder={sortOrder}
                isCategoryLoading={isCategoryLoading}
                currentCategory={categoryName}
                bets={bets}
                currentPage={pageNumber}
                changeCurrentPage={this.setPageNumber}
                referrer={referrer}
              />
            ) : (
              <Loader customHeight />
            )}
            <MediaQuery query="(max-width: 991px)">
              <Filters categories={categories} currentCategory={categoryName} />
            </MediaQuery>
          </Wrapper>
        </Main>
      </Container>
    )
  }
}

function mapStateToProps({
  site: { host, protocol },
  bet: { current, isLoading },
  category: { categories, isLoading: isCategoryLoading }
}) {
  return {
    host,
    protocol,
    isLoading,
    isCategoryLoading,
    bets: current,
    categories
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch, // `dispatch` is needed for SSR (See the static `loadData` method on the component)
    ...bindActionCreators({ fetchBets, fetchCategories }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Landing))
