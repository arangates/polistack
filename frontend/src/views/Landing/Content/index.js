/* eslint-disable react/prop-types,no-unused-vars */
import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import Heading from './Heading'
import Results from './Results'
import Pagination from 'components/Pagination'
import { Content as Wrapper } from 'components'
import NoMatch from './NoMatch'

class Content extends Component {
  render() {
    const {
      bets,
      currentPage,
      currentCategory,
      isCategoryLoading,
      referrer,
      changeSort,
      sortOrder
    } = this.props

    return (
      <Wrapper>
        <Helmet>
          {currentPage > 1 && (
            <link rel="prev" href={`/?page=${currentPage - 1}`} />
          )}
          {currentPage < Math.ceil(bets.count / 20) && (
            <link rel="next" href={`/?page=${currentPage + 1}`} />
          )}
        </Helmet>
        <Heading
          changeSort={changeSort}
          sortOrder={sortOrder}
          currentCategory={currentCategory}
          betsCount={bets.count}
          isCategoryLoading={isCategoryLoading}
        />
        {referrer === 404 && (
          <NoMatch>
            <h2>Sorry! The page you wanted to visit does not exist!</h2>
          </NoMatch>
        )}
        <Results results={bets.results} />
        <Pagination
          perPage={20}
          resultCount={bets.count}
          currentPage={currentPage}
          changeCurrentPage={this.props.changeCurrentPage}
        />
      </Wrapper>
    )
  }
}

export default Content
