import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const NoMatch = styled.div`
  ${flex('center', 'center')};
  > h2 {
    text-align: center;
    margin: 5em;
  }
`

export default NoMatch
