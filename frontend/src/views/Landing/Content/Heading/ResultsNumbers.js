import styled from 'styled-components'

const ResultsNumbers = styled.h4`
  font-weight: 500;
  margin: 0;
  > span {
    font-weight: 700;
  }
`

export default ResultsNumbers
