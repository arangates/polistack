/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Breadcrumbs from 'components/Breadcrumbs'
import Wrapper from './Wrapper'
import ResultsNumbers from './ResultsNumbers'
import BonusRegistration from 'components/BonusRegistration'
import { PureSelectDropdown } from 'components'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { getDeserializedCategoryName } from 'utils'

class Heading extends Component {
  state = {
    dropdownOpen: false,
    sortOptions: [
      {
        label: 'Descending Volume',
        value: '-volume'
      },
      {
        label: 'Descending Name',
        value: '-statement'
      },
      {
        label: 'Ascending Volume',
        value: 'volume'
      },
      {
        label: 'Ascending Name',
        value: 'statement'
      }
    ]
  }
  getSortBy = sortBy =>
    this.state.sortOptions.filter(item => item.value === sortBy)[0]

  render() {
    const { currentCategory, betsCount, changeSort, sortOrder } = this.props
    const isAuthenticated = this.props.user.authenticated
    return (
      <React.Fragment>
        {!isAuthenticated && <BonusRegistration />}
        <Wrapper>
          <Breadcrumbs />
          <PureSelectDropdown
            value={this.getSortBy(sortOrder)}
            onChange={changeSort}
            options={this.state.sortOptions}
            placeholder="Sort By"
            searchable={false}
          />
        </Wrapper>
        <ResultsNumbers>
          <span>{betsCount}</span>
          <span> bets in </span>
          <span style={{ color: '#44aef3' }}>
            {currentCategory === null
              ? 'All'
              : getDeserializedCategoryName(currentCategory)}
          </span>
          <span> category</span>
        </ResultsNumbers>
      </React.Fragment>
    )
  }
}

function mapStateToProps({ user }) {
  return { user }
}

Heading.propTypes = {
  currentCategory: PropTypes.string,
  betsCount: PropTypes.number
}

export default connect(mapStateToProps, {})(withRouter(Heading))
