import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const Wrapper = styled.div`
  ${flex('space-between', 'flex-start')};
`

export default Wrapper
