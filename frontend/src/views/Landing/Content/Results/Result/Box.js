import styled from 'styled-components'

const Box = styled.div`
  flex: 0 1 100%;

  @media screen and (min-width: 768px) {
    flex: 0 1 48%;
  }

  background: #fff;
  box-shadow: 0 0.125em 1.25em 0 rgba(165, 183, 195, 0.15);

  padding: 1.25em;
  margin: 1.375em 0 0;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export default Box
