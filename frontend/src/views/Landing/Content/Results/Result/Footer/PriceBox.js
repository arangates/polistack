import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const PriceBox = styled.div`
  &:focus {
    outline: 5px auto -webkit-focus-ring-color;
  }

  input:focus {
    outline: none;
  }

  ${flex('center', 'center')};
  background: #f9fafc;
  border: 0.063em solid #eaedf2;
  height: 2.813em;
  margin-top: 0.25em;
  position: relative;

  input {
    border-right: 0;
  }
`

export default PriceBox
