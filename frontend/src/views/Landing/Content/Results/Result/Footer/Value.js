import styled from 'styled-components'

const Value = styled.div`
  font-size: 1.125rem;
  font-weight: 900;
`

export default Value
