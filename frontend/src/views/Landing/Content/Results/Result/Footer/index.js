/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import BetInput from './BetInput'
import validate from 'components/Input/inputValidators'
import BetItem from './BetItem'
import Wrapper from './Wrapper'
import Title from './Title'
import Value from './Value'
import BetBox from './BetBox'
import PriceBox from './PriceBox'
import { connect } from 'react-redux'
import { openPlaceBetModal, openInfoModal } from 'actions/modalActions'

class Footer extends Component {
  state = {
    amount: 100
  }

  handleAmountChange = e => {
    const amount = e.target.value

    if (validate(amount)) {
      this.setState({
        amount: amount
      })
    }
  }

  placeBetSummary = side => {
    const { isAuthenticated, openConfirmBetModal, openInfoModal } = this.props

    const bet = {
      side,
      betAmount: this.state.amount,
      odds: this.props.odds,
      highestBid: this.props.highestBid,
      lowestAsk: this.props.lowestAsk,
      statement: this.props.statement,
      betId: this.props.id
    }
    if (isAuthenticated) {
      openConfirmBetModal(bet)
    } else {
      openInfoModal('Login needed', 'Please log in to place a bet', {
        link: '/login',
        text: 'Go to login page'
      })
    }
  }

  render() {
    const { odds, highestBid, lowestAsk } = this.props

    return (
      <form>
        <Wrapper>
          <BetItem>
            <h4>Yes</h4>
            <BetBox white onClick={() => this.placeBetSummary('yes')}>
              <Title>Best offer</Title>
              <Value>
                {'₹'} {lowestAsk === 100 ? '-' : lowestAsk}
              </Value>
            </BetBox>
          </BetItem>

          <BetItem>
            <h3>₹ {`${odds === 0 ? '-' : odds}`}</h3>
            <PriceBox>
              <Field
                name="betInput"
                component={BetInput}
                type="text"
                normalize={validate}
                white
                placeholder="100"
                onChange={this.handleAmountChange}
                style={{ textAlign: 'center' }}
              />
            </PriceBox>
          </BetItem>

          <BetItem>
            <h4>No</h4>
            <BetBox blue onClick={() => this.placeBetSummary('no')}>
              <Title>Best offer</Title>
              <Value>
                {'₹'} {highestBid === 0 ? '-' : 100 - highestBid}
              </Value>
            </BetBox>
          </BetItem>
        </Wrapper>
      </form>
    )
  }
}

Footer = reduxForm({})(Footer)

function mapStateToToProps({ user }) {
  return { isAuthenticated: user.authenticated }
}

export default connect(mapStateToToProps, {
  openConfirmBetModal: openPlaceBetModal,
  openInfoModal
})(Footer)
