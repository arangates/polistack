import styled from 'styled-components'

const BetItem = styled.div`
  margin: 0 0.625em;
  flex: 1;

  h3 {
    font-size: 1.5em;
    font-weight: 900;
    text-align: center;
  }

  h4 {
    font-size: 0.875rem;
    text-align: center;
    text-transform: uppercase;
  }
`

export default BetItem
