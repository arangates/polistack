/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import TruncateMarkup from 'react-truncate-markup'
import Box from './Box'

class Content extends Component {
  render() {
    return (
      <Box>
        <h6>
          Total volume:{' '}
          <span> {Math.round(100 * this.props.volume) / 100}</span>
        </h6>

        <TruncateMarkup lines={2}>
          <h2>{this.props.statement}</h2>
        </TruncateMarkup>
      </Box>
    )
  }
}

export default Content
