/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Content from '../index'

describe('Content', () => {
  it('should render correctly', () => {
    const output = shallow(<Content />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
