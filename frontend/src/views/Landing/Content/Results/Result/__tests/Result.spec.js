/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Result from '../index'

const data = {
  image: 'test',
  id: 'test',
  statement: 'test',
  yes: 'test',
  no: 'test',
  volume: 'test'
}

describe('Result', () => {
  it('should render correctly', () => {
    const output = shallow(<Result data={data}/>)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
