import styled from 'styled-components'
import { LazyLoadImage } from 'react-lazy-load-image-component'

const Holder = styled(LazyLoadImage)`
  height: 15em;
  width: 100%;
  background-size: cover;
  background-position: center;
`

export default Holder
