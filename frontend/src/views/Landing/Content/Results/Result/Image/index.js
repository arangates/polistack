/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Holder from './Holder'

class Image extends Component {
  getImgAlt = () => `${this.props.statement} - predict this political market`
  render() {
    return <Holder alt={this.getImgAlt()} src={this.props.image} />
  }
}

export default Image
