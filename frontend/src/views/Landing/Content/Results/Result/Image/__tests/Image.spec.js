/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Image from '../index'

describe('Image', () => {
  it('should render correctly', () => {
    const output = shallow(<Image />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
