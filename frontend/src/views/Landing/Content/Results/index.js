/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Result from './Result'
import Wrapper from './Wrapper'

class Results extends Component {
  render() {
    const results = this.props.results || []

    return (
      <Wrapper>
        {results.map(item => <Result key={item.id} data={item} />)}
      </Wrapper>
    )
  }
}

export default Results
