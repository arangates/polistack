/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Content from '../index'

const bets = [
  {
    id: 1,
    yes: 3,
    no: 2,
    statement: 'Trump will be reelected in 2020',
    ends: '2010-08-05',
    created_by: 3,
    image: 'http://localhost:8000/media/trump.jpg',
    status: 'ACTIVE',
    volume: 50288
  },
  {
    id: 2,
    yes: 1,
    no: 6,
    statement: 'Twitter account of Trump will be banned at least once by the end of 2018',
    ends: '1990-03-23',
    created_by: 3,
    image: 'http://localhost:8000/media/trumptwitterban.png',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 3,
    yes: 1,
    no: 2,
    statement: 'Trump will tweet the words Crooked Hillary until end of 2018',
    ends: '1993-04-02',
    created_by: 3,
    image: 'http://localhost:8000/media/trumpclinton.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 4,
    yes: 1,
    no: 2,
    statement: 'Melania Trump will give birth to a child by 2020',
    ends: '2013-01-12',
    created_by: 3,
    image: 'http://localhost:8000/media/melaniatrump.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 5,
    yes: 1,
    no: 10,
    statement: 'UK governement will ban Trump from visiting before the end of his first term',
    ends: '2006-07-24',
    created_by: 3,
    image: 'http://localhost:8000/media/ukgovtbantrump.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 6,
    yes: 1,
    no: 50,
    statement: 'Eminem will publicly endorse Trump in the 2020 elections',
    ends: '1998-11-08',
    created_by: 3,
    image: 'http://localhost:8000/media/eminemtrump.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 7,
    yes: 1,
    no: 2,
    statement: 'LeBron James will have more points per game than James Harden at season end',
    ends: '1994-06-15',
    created_by: 3,
    image: 'http://localhost:8000/media/lebronharden.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 8,
    yes: 1,
    no: 50,
    statement: 'Bill Gates will become the sole owner of a NBA team in 2018',
    ends: '1982-05-20',
    created_by: 3,
    image: 'http://localhost:8000/media/gatesnba.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 9,
    yes: 1,
    no: 5,
    statement: 'Cristiano Ronaldo will score 20 goals or more in the Champions League in 2018',
    ends: '1999-11-23',
    created_by: 3,
    image: 'http://localhost:8000/media/cristianoronaldo.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 10,
    yes: 1,
    no: 8,
    statement: 'Abramovich will sell all his shares in Chelsea by end of 2018',
    ends: '1972-07-23',
    created_by: 3,
    image: 'http://localhost:8000/media/abramovichchelsea.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 11,
    yes: 1,
    no: 8,
    statement: 'North Korea will drop an atomic bomb on US soil by end of 2018',
    ends: '2015-01-22',
    created_by: 3,
    image: 'http://localhost:8000/media/northkoreabomb.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 12,
    yes: 1,
    no: 6,
    statement: 'Zuckerberg will get arrested by end of 2019',
    ends: '1979-02-01',
    created_by: 3,
    image: 'http://localhost:8000/media/zuckerbergarrested.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 13,
    yes: 1,
    no: 5,
    statement: 'Uber will be banned in the State of New York by 2020',
    ends: '1979-02-02',
    created_by: 3,
    image: 'http://localhost:8000/media/uberbanned.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 14,
    yes: 1,
    no: 10,
    statement: 'Xi Jinping will step down by 2020',
    ends: '1974-03-22',
    created_by: 3,
    image: 'http://localhost:8000/media/xistepdown.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 15,
    yes: 1,
    no: 15,
    statement: 'China will expel at least one Russian diplomat in 2018',
    ends: '2012-06-17',
    created_by: 3,
    image: 'http://localhost:8000/media/chinaexpelsrussia.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 16,
    yes: 1,
    no: 5,
    statement: 'Duterte will insult Trump publicly in 2018',
    ends: '2010-10-23',
    created_by: 3,
    image: 'http://localhost:8000/media/dutertetrump.jpg',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 17,
    yes: 1,
    no: 8,
    statement: 'Bitcoin will rise above 15000 by end of April',
    ends: '1997-02-02',
    created_by: 3,
    image: 'http://localhost:8000/media/bitcoinrise.png',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 18,
    yes: 1,
    no: 4,
    statement: 'Ethereum will fall below 300 by end of May',
    ends: '1973-12-30',
    created_by: 3,
    image: 'http://localhost:8000/media/ethereumfall.png',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 19,
    yes: 1,
    no: 10,
    statement: 'Binance will file for bankrupcy protection by the end of 2018',
    ends: '2007-03-30',
    created_by: 3,
    image: 'http://localhost:8000/media/binancebankrupt.png',
    status: 'ACTIVE',
    volume: 100
  },
  {
    id: 20,
    yes: 1,
    no: 20,
    statement: 'Michelle and Barack Obama will get a divorce by 2020',
    ends: '2016-10-22',
    created_by: 3,
    image: 'http://localhost:8000/media/obamasdivorce.jpg',
    status: 'ACTIVE',
    volume: 100
  }
]

describe('Content', () => {
  it('should render correctly', () => {
    const output = shallow(<Content bets={bets}/>)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
