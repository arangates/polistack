/* eslint-disable react/prop-types,no-unused-vars,space-before-function-paren */
import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import isString from 'lodash.isstring'
import { Container, Loader, Breadcrumbs } from 'components'
import { fetchEventDetails } from 'actions/eventActions'
import { DEFAULT_PAGE_TITLE, DEFAULT_PAGE_DESCRIPTION } from 'constants/globals'
import { getEventDetailsHref } from 'utils'
import Event from './Event'
import Main from './Main'
import Description from './Description'
import RelatedEvents from './RelatedEvents'
import OrderBook from './OrderBook'
import TimeSeries from './TimeSeries'
import Comments from './Comments'
import Nav from './TimeSeries/Nav'
import { NavTab, TabTitle } from './TimeSeries/NavTab'
import {
  MarketDetails,
  MarketRelated,
  CommentSection,
  BlueScroller
} from './Market'
class EventDetails extends Component {
  state = {
    currentTab: 1
  }
  static async loadData(location, match, dispatch) {
    const { eventId } = match.params
    await dispatch(fetchEventDetails(eventId))
  }

  async componentDidMount() {
    const { location, match, dispatch } = this.props
    await EventDetails.loadData(location, match, dispatch)
    window.scrollTo(0, 0)
  }

  async componentDidUpdate(prevProps) {
    const { location, match, dispatch } = this.props
    const { match: prevMatch } = prevProps
    if (match.params.eventId !== prevMatch.params.eventId) {
      await EventDetails.loadData(location, match, dispatch)
      window.scrollTo(0, 0)
    }
  }

  render() {
    const { host, protocol, eventDetails } = this.props
    const switchTab = async Tab => {
      this.setState({
        currentTab: Tab.id
      })
    }
    const renderTab = Tab => {
      return (
        <NavTab
          key={Tab.id}
          onClick={() => switchTab(Tab)}
          className={this.state.currentTab === Tab.id ? 'active' : ''}
        >
          <TabTitle>{Tab.name}</TabTitle>
        </NavTab>
      )
    }
    const Tabs = [{ id: 1, name: 'Recommended' }, { id: 2, name: 'Orderbook' }]
    let canonicalHref = null
    let pageTitle = null
    let pageDescription = null
    if (isString(eventDetails.statement)) {
      canonicalHref = `${protocol}://${host}${getEventDetailsHref(
        eventDetails
      )}`
      pageTitle = `Predict politics - ${eventDetails.statement} | Polistack`
      pageDescription = `${
        eventDetails.statement
      } - Predict politics - check the conditions of trading on this political event`
    }

    const event = {
      url: canonicalHref,
      id: eventDetails.id,
      title: pageTitle
    }
    return (
      <Container fullHeight>
        <Helmet>
          {isString(pageTitle) ? (
            <title>{pageTitle}</title>
          ) : (
            DEFAULT_PAGE_TITLE
          )}
          {isString(pageDescription) ? (
            <meta name="description" content={pageDescription} />
          ) : (
            DEFAULT_PAGE_DESCRIPTION
          )}
          {isString(canonicalHref) && (
            <link rel="canonical" href={canonicalHref} />
          )}
        </Helmet>
        <Breadcrumbs />
        {eventDetails.orderBookId ? (
          <Main>
            <MarketDetails>
              <Event data={eventDetails} />
              <Description description={eventDetails.description} />
              <TimeSeries orderBookId={eventDetails.orderBookId} />
            </MarketDetails>
            <MarketRelated>
              <Nav>{Tabs ? Tabs.map(Tab => renderTab(Tab)) : ''}</Nav>
              {this.state.currentTab === 1 ? (
                <RelatedEvents eventId={eventDetails.id} />
              ) : (
                <BlueScroller>
                  <OrderBook orderBookId={eventDetails.orderBookId} />
                </BlueScroller>
              )}
            </MarketRelated>
            <CommentSection>
              <Comments event={event} />
            </CommentSection>
          </Main>
        ) : (
          <Loader />
        )}
      </Container>
    )
  }
}

function mapStateToProps({
  site: { host, protocol },
  event: { data, isLoading }
}) {
  return {
    host,
    protocol,
    eventDetails: data,
    isLoading
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    ...bindActionCreators({ fetchEventDetails }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withRouter(EventDetails)
)
