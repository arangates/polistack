import styled from 'styled-components'

const NavTab = styled.span`
  flex: 1;
  cursor: pointer;
  text-align: center;
  padding: 1em;
  transition: 0.3s ease-in-out;

  &:hover {
    > div {
      color: #293244;
    }
  }

  &.${'active'} {
    background-color: #fff;
    box-shadow: 0px -8px 1em 0em rgba(0, 0, 0, 0.07);
    > div {
      color: #828f97;
    }
  }
`

const TabTitle = styled.div`
  color: #44aef3;
  font-size: 0.75rem;
  font-weight: 900;
  transition: 0.3s ease-in-out;
`

export { NavTab, TabTitle }
