/* eslint-disable react/prop-types,no-unused-vars,space-before-function-paren */
import React, { Component } from 'react'
import { LineChart } from 'components'
import Nav from './Nav'
import { NavTab, TabTitle } from './NavTab'
import EventService from 'services/event'
import MediaQuery from 'react-responsive'

class TimeSeries extends Component {
  state = {
    orderbookId: null,
    graphData: null,
    interval: 7,
    axisInterval: 1,
    precision: 'day'
  }
  getGraphDetails = async (orderBookId, interval) => {
    const res = await EventService.getGraphData(orderBookId, interval)
    if (res.status === 200) {
      this.setState({ orderbookId: orderBookId, graphData: res.data })
    }
  }
  async componentDidMount() {
    const { orderBookId } = this.props
    if (orderBookId) {
      await this.getGraphDetails(orderBookId, 7)
    }
  }
  render() {
    const {
      orderbookId,
      graphData,
      interval,
      axisInterval,
      precision
    } = this.state
    const Tabs = [
      { id: 1, name: '24hr', interval: 1, axisInterval: 1, precision: 'hour' },
      { id: 2, name: '7 Day', interval: 7, axisInterval: 1, precision: 'day' },
      {
        id: 3,
        name: '30 Day',
        interval: 30,
        axisInterval: 2,
        precision: 'day'
      },
      { id: 4, name: '90 Day', interval: 90, axisInterval: 8, precision: 'day' }
    ]
    const mobileTabs = [
      { id: 1, name: '24hr', interval: 1, axisInterval: 1, precision: '4hour' },
      { id: 2, name: '7 Day', interval: 7, axisInterval: 1, precision: 'day' },
      {
        id: 3,
        name: '30 Day',
        interval: 30,
        axisInterval: 2,
        precision: 'day'
      },
      { id: 4, name: '90 Day', interval: 90, axisInterval: 4, precision: 'day' }
    ]

    const switchTab = async Tab => {
      await this.getGraphDetails(orderbookId, Tab.interval)
      this.setState({
        interval: Tab.interval,
        axisInterval: Tab.axisInterval,
        precision: Tab.precision
      })
    }
    const renderTab = Tab => {
      return (
        <NavTab
          key={Tab.id}
          onClick={() => switchTab(Tab)}
          className={interval === Tab.interval ? 'active' : ''}
        >
          <TabTitle>{Tab.name}</TabTitle>
        </NavTab>
      )
    }

    return (
      <>
        {graphData ? (
          <div style={{ margin: '2em 0em' }}>
            <Nav>{Tabs ? Tabs.map(Tab => renderTab(Tab)) : ''}</Nav>
            <MediaQuery query="(min-width: 768px)">
              <LineChart
                graphData={graphData}
                interval={interval}
                axisInterval={axisInterval}
                precision={precision}
              />
            </MediaQuery>
            <MediaQuery query="(max-width: 768px)">
              <LineChart
                graphData={graphData}
                interval={interval}
                axisInterval={axisInterval}
                precision={precision}
              />
            </MediaQuery>
          </div>
        ) : (
          ''
        )}
      </>
    )
  }
}

export default TimeSeries
