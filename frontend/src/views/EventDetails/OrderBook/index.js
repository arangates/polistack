/* eslint-disable react/prop-types,no-unused-vars */
import React, { Component } from 'react'
import { TableCell, TableItem } from 'components/TableItem'
import MiniTable from './MiniTable'
import Heading from './Heading'
import EventService from 'services/event'

class OrderBook extends Component {
  state = {
    orderbook: null,
    orderbookId: null
  }
  getOrderBookDetails = async orderBookId => {
    const res = await EventService.getOrderBook(orderBookId)
    if (res.status === 200) {
      this.setState({ orderbook: res.data })
    }
  }
  async componentDidMount() {
    const { orderBookId } = this.props
    if (orderBookId) {
      await this.getOrderBookDetails(orderBookId)
    }
  }
  renderItem = item => {
    return (
      <TableItem key={item.price} light>
        <TableCell>{item.yes_volume ? item.yes_volume : ''}</TableCell>
        <TableCell />
        <TableCell>{item.price}</TableCell>
        <TableCell />
        <TableCell>{item.no_volume ? item.no_volume : ''}</TableCell>
      </TableItem>
    )
  }

  renderOrderBook = entries =>
    entries ? entries.map(item => this.renderItem(item)) : ''

  render() {
    const { orderbook } = this.state

    return (
      <>
        {orderbook ? (
          <MiniTable>
            <Heading />
            {this.renderOrderBook(orderbook.entries)}
          </MiniTable>
        ) : (
          ''
        )}
      </>
    )
  }
}

export default OrderBook
