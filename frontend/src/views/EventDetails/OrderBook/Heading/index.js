import React, { Component } from 'react'
import { TableCell } from 'components/TableItem'
import { TableHeading } from 'components/TableHeading'
class Heading extends Component {
  render() {
    return (
      <div style={{ order: 1 }}>
        <TableHeading dark>
          <TableCell>Buy side</TableCell>
          <TableCell />
          <TableCell>Price</TableCell>
          <TableCell />
          <TableCell>Sell side</TableCell>
        </TableHeading>
      </div>
    )
  }
}

export default Heading
