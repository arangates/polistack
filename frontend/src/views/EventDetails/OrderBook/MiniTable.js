import styled from 'styled-components'

const MiniTable = styled.div`
  flex: 0 1 100%;

  @media screen and (min-width: 768px) {
    flex: 0 1 48%;
  }
  Heading {
    order: 1;
  }

  background: #fff;
  box-shadow: 0 0.125em 1.25em 0 rgba(165, 183, 195, 0.15);

  margin: 0.5em 0 0;
  padding: 1.25em;

  display: flex;
  flex-direction: column-reverse;
`

export default MiniTable
