import styled from 'styled-components'

const BetItem = styled.div`
  margin: 0 0.625em;
  flex: 1;
  &:not(:last-child) {
    border-right: 0.5px solid #eaedf2;
    padding-right: 1.2em;
  }
  h3 {
    font-size: 1.5em;
    font-weight: 900;
    text-align: center;
  }

  h4 {
    font-size: 0.875rem;
    text-align: center;
    font-weight: normal;
    text-transform: uppercase;
  }
`

export default BetItem
