import styled from 'styled-components'

const Value = styled.div`
  display: ${props => props.flex && 'flex'};
  justify-content: ${props => props.flex && 'space-evenly'};
  padding: ${props => props.flex && '0em 3em'};
  font-size: ${props => (props.flex ? '0.8rem' : '1.125rem')};
  font-weight: ${props => !props.flex && '900'};
  color: ${props => props.green && '#51c981'};
  color: ${props => props.red && '#F25C5E'};
`

export default Value
