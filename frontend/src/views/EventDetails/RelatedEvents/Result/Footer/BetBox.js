import styled from 'styled-components'

const BetBox = styled.div`
  border-radius: 0.25em;
  text-align: center;
  height: 1.813em;
  display: flex;
  flex-direction: column;
  justify-content: center;
  cursor: pointer;
  transition: 0.3s ease-in-out;

  //WHITE
  color: ${props => props.white && '#828f97'};
  background: ${props => props.white && '#fff'};
  box-shadow: ${props => props.white && '0 0.25em 0.75em 0 rgba(0, 0, 0, 0.1)'};

  &:hover {
    background: ${props => props.white && '#828e96'};
    color: ${props => props.white && '#fff'};
  }

  //BLUE
  color: ${props => props.blue && '#fff'};
  background: ${props => props.blue && 'rgba(68, 174, 243, 1)'};
  box-shadow: ${props =>
    props.blue && '0 0.625em 0.625em 0 rgba(68, 174, 243, 0.4)'};

  &:hover {
    background: ${props => props.blue && '#3494d3'};
  }
`

export default BetBox
