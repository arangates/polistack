import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  align-items: flex-end;
  margin: 0 -0.625em;
`

export default Wrapper
