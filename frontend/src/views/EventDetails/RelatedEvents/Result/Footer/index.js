/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import BetItem from './BetItem'
import Wrapper from './Wrapper'
import Value from './Value'
import BetBox from './BetBox'
import { iconArrowUp, iconArrowDown } from 'images'
import Svg from 'components/Svg/index'
class Footer extends Component {
  render() {
    const { highestBid, lowestAsk, deltaYes, deltaNo } = this.props

    return (
      <form>
        <Wrapper>
          <BetItem>
            <h4>Yes</h4>
            <BetBox>
              <Value>
                {'₹'} {lowestAsk === 100 ? '-' : lowestAsk}
              </Value>
            </BetBox>
            <BetBox>
              {Math.sign(deltaYes) > 0 ? (
                <Value green flex>
                  <Svg icon={iconArrowUp} />
                  {deltaYes}
                </Value>
              ) : (
                <Value red flex>
                  {deltaYes === 0 ? '' : <Svg icon={iconArrowDown} />}
                  {deltaYes === 0 ? '' : deltaYes}
                </Value>
              )}
            </BetBox>
          </BetItem>

          <BetItem>
            <h4>No</h4>
            <BetBox>
              <Value>
                {'₹'} {highestBid === 0 ? '-' : 100 - highestBid}
              </Value>
            </BetBox>
            <BetBox>
              {Math.sign(deltaNo) > 0 ? (
                <Value green flex>
                  <Svg icon={iconArrowUp} />
                  {deltaNo}
                </Value>
              ) : (
                <Value red flex>
                  {deltaNo === 0 ? '' : <Svg icon={iconArrowDown} />}
                  {deltaNo === 0 ? '' : deltaNo}
                </Value>
              )}
            </BetBox>
          </BetItem>
        </Wrapper>
      </form>
    )
  }
}

export default Footer
