import styled from 'styled-components'
import { flex } from 'constants/global.styles'

const Currency = styled.div`
  ${flex('center', 'center')};
  height: 2.813em;
  width: 1.5em;
  position: absolute;
  right: 0;
  top: 0;

  > span {
    font-size: 0.75rem;
    font-weight: 700;
  }
`

export default Currency
