/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Image from './Image'
import Content from './Content'
import Footer from './Footer'
import Box from './Box'
import { Link } from 'react-router-dom'

class Result extends Component {
  render() {
    const { data } = this.props
    const getDetailUrl = () => {
      const slug = `/event/details`
      const eventId = data.id
      const eventName = encodeURIComponent(
        data.statement.replace(/\s+/g, '-').toLowerCase()
      )
      return `${slug}/${eventId}/${eventName}`
    }

    return data ? (
      <Box>
        <Link to={getDetailUrl()}>
          <div>
            <Image statement={data.statement} image={data.image} />
            <Content statement={data.statement} volume={data.volume} />
          </div>
        </Link>
        <div>
          <Footer
            highestBid={data.highest_bid}
            lowestAsk={data.lowest_ask}
            deltaYes={data.delta_yes}
            deltaNo={data.delta_no}
          />
        </div>
      </Box>
    ) : (
      ''
    )
  }
}

export default Result
