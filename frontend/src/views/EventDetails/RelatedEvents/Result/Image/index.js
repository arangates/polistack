/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Holder from './Holder'
import Container from './Container'

class Image extends Component {
  getImgAlt = () => `${this.props.statement} - predict this political market`
  render() {
    return (
      <Container>
        <Holder alt={this.getImgAlt()} src={this.props.image} />
      </Container>
    )
  }
}

export default Image
