import styled from 'styled-components'

const Holder = styled.img`
  height: 15em;
  width: 100%;
  background-size: cover;
  background-position: center;
`

export default Holder
