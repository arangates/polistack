import styled from 'styled-components'

const Container = styled.div`
  max-height: 6em;
  overflow: hidden;
`

export default Container
