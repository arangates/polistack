/* eslint-disable react/prop-types,no-unused-vars */
import React, { Component } from 'react'
import Result from './Result'
import EventService from 'services/event'

class RelatedEvents extends Component {
  state = {
    relatedEvents: null,
    eventId: null
  }
  getRelatedEvents = async eventId => {
    const res = await EventService.getRelatedEvents(eventId)
    if (res.status === 200) {
      this.setState({ relatedEvents: res.data })
    }
  }
  async componentDidMount() {
    const { eventId } = this.props
    if (eventId) {
      await this.getRelatedEvents(eventId)
    }
  }
  renderItem = item => {
    return <Result key={item.id} data={item} />
  }

  renderRelatedEvents = entries =>
    entries ? entries.map(item => this.renderItem(item)) : ''

  render() {
    const { relatedEvents } = this.state

    return (
      <>
        {relatedEvents ? (
          <div>{this.renderRelatedEvents(relatedEvents)}</div>
        ) : (
          ''
        )}
      </>
    )
  }
}

export default RelatedEvents
