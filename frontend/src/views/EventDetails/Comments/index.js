import React from 'react'
import { DiscussionEmbed } from 'disqus-react'
import PropTypes from 'prop-types'

class Comments extends React.Component {
  render() {
    const { event } = this.props

    const disqusShortname = 'polistack'
    const disqusConfig = {
      url: event.url,
      identifier: event.id,
      title: event.title
    }

    return (
      <div className="event">
        <h2>Comments</h2>
        <DiscussionEmbed shortname={disqusShortname} config={disqusConfig} />
      </div>
    )
  }
}

Comments.propTypes = {
  event: PropTypes.object
}

export default Comments
