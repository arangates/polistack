import styled from 'styled-components'

const Details = styled.div`
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  justify-content: space-between;
`
const CommentSection = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  > .event {
    width: inherit;
  }
`
const MarketDetails = styled.div`
  flex: 2;
`
const MarketRelated = styled.div`
  flex: 1;
  margin-left: 1em;
  @media screen and (max-width: 768px) {
    margin-top: 2em;
    margin-left: 0em;
  }
`

const BlueScroller = styled.div`
  height: 80vh;
  overflow: auto;
  ::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
    background-color: #f5f5f5;
    border-radius: 10px;
  }

  ::-webkit-scrollbar {
    width: 8px;
    background-color: #f5f5f5;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 8px;
    background-color: #44aef3d4;
  }
`

export { MarketDetails, Details, MarketRelated, CommentSection, BlueScroller }
