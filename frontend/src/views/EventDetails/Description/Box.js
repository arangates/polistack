import styled from 'styled-components'

const Box = styled.div`
  flex: 0 1 100%;

  @media screen and (min-width: 768px) {
    flex: 0 1 100%;
  }
  > h1 {
    margin-bottom: 0.25em;
  }
  background: #fff;
  box-shadow: 0 0.125em 1.25em 0 rgba(165, 183, 195, 0.15);

  padding: 1em;
  margin: 1.375em 0 0;
`

export default Box
