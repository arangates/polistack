/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Box from './Box'

export default class Description extends Component {
  render() {
    const { description } = this.props
    return <Box>{description}</Box>
  }
}
