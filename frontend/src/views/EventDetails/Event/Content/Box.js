import styled from 'styled-components'

const Box = styled.div`
  margin-top: 0.9375em;
  padding: 1.25em;

  h1 {
    font-size: 1.375rem;
    line-height: 1.25em;
    margin-top: 0.25em;
  }

  h6 {
    color: #828f97;
    font-weight: 500;
    > span {
      color: #44aef3;
      font-weight: 900;
    }
  }
`

export default Box
