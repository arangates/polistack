import styled from 'styled-components'

const BetItem = styled.div`
  margin: 0 0.625em;
  margin-bottom: ${props => (props.alignAuto ? 'auto' : '0 0.625em')};
  margin-top: ${props => (props.top ? '1.5em' : '')};
  flex: 1;
  align-self: flex-start;
  color: ${props => props.light && '#102b3b78'};
  align-self: ${props => props.light && 'center'};
  h3 {
    font-size: 1.5em;
    font-weight: 900;
    text-align: center;
  }

  h4 {
    font-size: 1rem;
    text-align: center;
    text-transform: uppercase;
  }
`

export default BetItem
