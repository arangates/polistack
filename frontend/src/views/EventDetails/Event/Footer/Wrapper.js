import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 0.9375em;
`

export default Wrapper
