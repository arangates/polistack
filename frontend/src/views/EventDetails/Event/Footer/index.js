/* eslint-disable react/prop-types,no-class-assign */
import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import BetInput from './BetInput'
import validate from 'components/Input/inputValidators'
import BetItem from './BetItem'
import Wrapper from './Wrapper'
import Title from './Title'
import Value from './Value'
import BetBox from './BetBox'
import PriceBox from './PriceBox'
import { connect } from 'react-redux'
import { openPlaceBetModal, openInfoModal } from 'actions/modalActions'
import MediaQuery from 'react-responsive'
import { iconArrowUp, iconArrowDown } from 'images'
import Svg from 'components/Svg/index'
class Footer extends Component {
  state = {
    amount: 100
  }

  handleAmountChange = e => {
    const amount = e.target.value

    if (validate(amount)) {
      this.setState({
        amount: amount
      })
    }
  }

  placeBetSummary = side => {
    const { isAuthenticated, openConfirmBetModal, openInfoModal } = this.props

    const bet = {
      side,
      betAmount: this.state.amount,
      odds: this.props.odds,
      highestBid: this.props.highestBid,
      lowestAsk: this.props.lowestAsk,
      statement: this.props.statement,
      betId: this.props.id
    }
    if (isAuthenticated) {
      openConfirmBetModal(bet)
    } else {
      openInfoModal('Login needed', 'Please log in to place a bet', {
        link: '/login',
        text: 'Go to login page'
      })
    }
  }

  render() {
    const {
      odds,
      highestBid,
      lowestAsk,
      latestYes,
      latestNo,
      deltaYes,
      deltaNo
    } = this.props

    return (
      <form>
        <Wrapper>
          <MediaQuery query="(min-width: 767px)">
            <BetItem light>
              <h4>Last yes</h4>
              <BetBox small>
                <Value>{odds === 0 ? '-' : latestNo}</Value>
              </BetBox>
              <BetBox>
                {Math.sign(deltaNo) > 0 ? (
                  <Value green flex>
                    <Svg icon={iconArrowUp} />
                    {deltaNo}
                  </Value>
                ) : (
                  <Value red flex>
                    {deltaNo === 0 ? '' : <Svg icon={iconArrowDown} />}
                    {deltaNo === 0 ? '' : deltaNo}
                  </Value>
                )}
              </BetBox>
            </BetItem>
          </MediaQuery>
          <BetItem>
            <h4 style={{ marginBottom: '1em' }}>Yes</h4>
            <BetBox white onClick={() => this.placeBetSummary('yes')}>
              <Title>Best offer</Title>
              <Value dark>
                {'₹'} {lowestAsk === 100 ? '-' : lowestAsk}
              </Value>
            </BetBox>
            <MediaQuery query="(max-width: 767px)">
              <BetItem light top>
                <h4>Last yes</h4>
                <BetBox small>
                  <Value>{odds === 0 ? '-' : latestNo}</Value>
                </BetBox>
                <BetBox>
                  {Math.sign(deltaNo) > 0 ? (
                    <Value green flex>
                      <Svg icon={iconArrowUp} />
                      {deltaNo}
                    </Value>
                  ) : (
                    <Value red flex>
                      {deltaNo === 0 ? '' : <Svg icon={iconArrowDown} />}
                      {deltaNo === 0 ? '' : deltaNo}
                    </Value>
                  )}
                </BetBox>
              </BetItem>
            </MediaQuery>
          </BetItem>

          <BetItem>
            <h3 style={{ marginBottom: '0.5em' }}>
              ₹ {`${odds === 0 ? '-' : odds}`}
            </h3>
            <PriceBox>
              <Field
                name="betInput"
                component={BetInput}
                type="text"
                normalize={validate}
                white
                placeholder="100"
                onChange={this.handleAmountChange}
                style={{ textAlign: 'center' }}
              />
            </PriceBox>
          </BetItem>

          <BetItem>
            <h4 style={{ marginBottom: '1em' }}>No</h4>
            <BetBox blue onClick={() => this.placeBetSummary('no')}>
              <Title>Best offer</Title>
              <Value>
                {'₹'} {highestBid === 0 ? '-' : 100 - highestBid}
              </Value>
            </BetBox>

            <MediaQuery query="(max-width: 767px)">
              <BetItem light top>
                <h4>Last No</h4>
                <BetBox small>
                  <Value>{odds === 0 ? '-' : latestYes}</Value>
                </BetBox>
                <BetBox>
                  {Math.sign(deltaYes) > 0 ? (
                    <Value green flex>
                      <Svg icon={iconArrowUp} />
                      {deltaYes}
                    </Value>
                  ) : (
                    <Value red flex>
                      {deltaYes === 0 ? '' : <Svg icon={iconArrowDown} />}
                      {deltaYes === 0 ? '' : deltaYes}
                    </Value>
                  )}
                </BetBox>
              </BetItem>
            </MediaQuery>
          </BetItem>
          <MediaQuery query="(min-width: 767px)">
            <BetItem light>
              <h4>Last No</h4>
              <BetBox small>
                <Value>{odds === 0 ? '-' : latestYes}</Value>
              </BetBox>
              <BetBox>
                {Math.sign(deltaYes) > 0 ? (
                  <Value green flex>
                    <Svg icon={iconArrowUp} />
                    {deltaYes}
                  </Value>
                ) : (
                  <Value red flex>
                    {deltaYes === 0 ? '' : <Svg icon={iconArrowDown} />}
                    {deltaYes === 0 ? '' : deltaYes}
                  </Value>
                )}
              </BetBox>
            </BetItem>
          </MediaQuery>
        </Wrapper>
      </form>
    )
  }
}

Footer = reduxForm({})(Footer)

function mapStateToToProps({ user }) {
  return { isAuthenticated: user.authenticated }
}

export default connect(mapStateToToProps, {
  openConfirmBetModal: openPlaceBetModal,
  openInfoModal
})(Footer)
