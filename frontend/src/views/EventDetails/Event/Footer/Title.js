import styled from 'styled-components'

const Title = styled.div`
  font-size: 0.75rem;
`

export default Title
