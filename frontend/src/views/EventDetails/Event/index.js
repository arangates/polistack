/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Image from './Image'
import Content from './Content'
import Footer from './Footer'
import Box from './Box'
import { Divider } from 'components'
import { getEventDetailsHref } from 'utils'

class Event extends Component {
  render() {
    const { data } = this.props

    return data ? (
      <Box>
        <Link to={getEventDetailsHref(data)}>
          <div>
            <Image statement={data.statement} image={data.image} />
          </div>
          <Content statement={data.statement} volume={data.volume} />
        </Link>
        <div>
          <Divider wide />
          <Footer
            form={data.id.toString()}
            id={data.id}
            statement={data.statement}
            odds={data.odds}
            highestBid={data.highestBid}
            lowestAsk={data.lowestAsk}
            latestYes={data.latest_yes}
            latestNo={data.latest_no}
            deltaYes={data.delta_yes}
            deltaNo={data.delta_no}
          />
        </div>
      </Box>
    ) : (
      ''
    )
  }
}

export default Event
