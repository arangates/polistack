import styled from 'styled-components'
import { LazyLoadImage } from 'react-lazy-load-image-component'

const Holder = styled(LazyLoadImage)`
  width: 100%;
  object-fit: cover;
  background-size: cover;
  background-position: center;
`

export default Holder
