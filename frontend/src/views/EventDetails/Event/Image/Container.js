import styled from 'styled-components'

const Container = styled.div`
  max-height: 12em;
  overflow: hidden;
`

export default Container
