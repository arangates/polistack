import styled from 'styled-components'

const Main = styled.div`
  padding-top: 2em;
  @media screen and (min-width: 768px) {
    display: flex;
    align-items: flex-start;
    flex-wrap: wrap;
    justify-content: space-between;
  }
`

export default Main
