import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Container } from 'components'
import Wrapper from './Wrapper'
import Heading from './Heading'
import { Section, Title, StyledLink } from './Section'

class HowItWorks extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  render() {
    const { host, protocol } = this.props
    const canonicalUrl = `${protocol}://${host}/how-it-works`
    return (
      <Wrapper>
        <Helmet>
          <title>Polistack | Predict politics | How it works</title>
          <link rel="canonical" href={canonicalUrl} />
          <meta
            name="description"
            content="Polistack - How it works - bet on politics, bet on elections, election betting odds. The portal of politics predictions"
          />
        </Helmet>
        <Container>
          <Heading>
            <h1>How it works?</h1>
          </Heading>
          <Section>
            <Title>
              1. Create your account at
              <StyledLink href="https://polistack.com/register">
                https://polistack.com/register
              </StyledLink>
            </Title>
            <Title>2. Deposit money to your Polistack account</Title>
            <p>Receive a signing bonus of up to ₹1000</p>
            <Title>
              3. Find the events you are interested in at
              <StyledLink href="https://polistack.com/">
                https://polistack.com/
              </StyledLink>
            </Title>
            <p>
              Let's assume the event you are interested in it is "Modi will be
              reelected in 2023".
            </p>
            <Title>4. Click "Yes" or "No"</Title>
            <p>
              This is depending on whether you think Modi will or will not be
              reelected in 2023. Let's assume that you clicked "Yes".
            </p>
            <Title>5. Type in the price and the volume you wish to trade</Title>
            <p>
              The price can range from ₹1 to ₹99. The maximum volume you can
              input depends on your account balance.<br /> <br /> Let's assume
              that you input a price of ₹30 and the volume of 10.
            </p>
            <Title>6. Click "Confirm"</Title>
            <p>
              The order for your trade will then be placed.If some other
              participant decides to take the opposite side of the order, your
              trade will be filled.
            </p>
            <Title>
              7. Fast forward to the end of the elections in 2023. The price of
              the event finished at either ₹100 (if Modi got reelected) or ₹0
              (if Modi did not get reelected).
            </Title>
            <p>
              If Modi got reelected you make a gross profit of ₹70 per unit
              volume, as the price of the event increased to ₹100 (and you
              bought it for ₹30). In total your gross profit is ₹700.
              <br /> <br /> If Modi Modi did not get reelected you make a loss
              ₹30 per unit volume, as the price of the event decreased to ₹0. In
              total total your loss is ₹300.
            </p>
            <Title>
              8. For more details visit our FAQ page:
              <StyledLink href="https://polistack.com/faq">
                https://polistack.com/faq
              </StyledLink>
            </Title>
          </Section>
        </Container>
      </Wrapper>
    )
  }
}

HowItWorks.propTypes = {
  host: PropTypes.string,
  protocol: PropTypes.string
}

const mapStateToProps = ({ site: { host, protocol } }) => ({ host, protocol })

export default connect(mapStateToProps)(HowItWorks)
