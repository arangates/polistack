import styled from 'styled-components'

const Section = styled.div`
  margin-top: 2em;
`

const Title = styled.h3`
  font-size: 1rem;
  font-weight: 500;
  margin-bottom: 0.5em;
  margin-top: 2em;
`

const StyledLink = styled.a`
  font-size: 1rem;
  margin-left: 0.5em;
  font-weight: 500;
  color: #44aef3;
`

export { Section, Title, StyledLink }
