/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import HowItWorks from '../index'

describe('Faq', () => {
  it('should render correctly', () => {
    const output = shallow(<HowItWorks />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
