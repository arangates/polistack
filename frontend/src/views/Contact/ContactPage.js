import styled from 'styled-components'

const ContactPage = styled.div`
  margin-top: 5em;
  padding: 1.875em 0 3em;
  height: 74.5vh;
`

const ContactHeader = styled.h1`
  color: #293244;
`

const ContactForm = styled.form`
  padding-top: 1.875em;
  max-width: 28.563em;
  width: 100%;
`

const Buttons = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`

export { ContactPage, ContactHeader, ContactForm, Buttons }
