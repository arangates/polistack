/* eslint-disable camelcase */
import { isEmail } from 'validator'

const validate = ({ email, first_name, last_name, message }) => {
  const errors = {}

  if (email && !isEmail(email)) {
    errors.email = 'Invalid email'
  }

  if (!email) {
    errors.email = 'Required'
  }

  if (!first_name || first_name === '') {
    errors.first_name = 'Required'
  }

  if (!last_name || last_name === '') {
    errors.last_name = 'Required'
  }

  if (!message || message === '') {
    errors.message = 'Required'
  }

  return errors
}

export default validate
