/* eslint-disable no-undef */
import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import Contact from '../index'

describe('Contact', () => {
  it('should render correctly', () => {
    const output = shallow(<Contact />)
    expect(shallowToJson(output)).toMatchSnapshot()
  })
})
