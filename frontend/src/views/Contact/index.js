/* eslint-disable react/prop-types,no-class-assign */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Container, Button, Input, TextArea, InputGroup } from 'components'
import { ContactPage, ContactHeader, ContactForm, Buttons } from './ContactPage'
import { postContactForm } from 'actions/contactAction'
import validate from './validators'

class Contact extends Component {
  sendData = async data => {
    await this.props.postContactForm(data)
  }
  render() {
    const { host, protocol, handleSubmit } = this.props
    const canonicalUrl = `${protocol}://${host}/contact`

    return (
      <Container>
        <Helmet>
          <title>Polistack | Predict politics | Contact</title>
          <link rel="canonical" href={canonicalUrl} />
          <meta
            name="description"
            content="Polistack - Contact us to send your suggestions on political predictions. We would love to publish it on our website."
          />
        </Helmet>
        <ContactPage>
          <ContactHeader>Contact</ContactHeader>
          <ContactForm onSubmit={handleSubmit(this.sendData)}>
            <InputGroup>
              <Field
                name="email"
                component={Input}
                type="text"
                placeholder="Email"
              />
            </InputGroup>
            <InputGroup>
              <Field
                name="first_name"
                component={Input}
                type="text"
                placeholder="First name"
              />
            </InputGroup>

            <InputGroup>
              <Field
                name="last_name"
                component={Input}
                type="text"
                placeholder="Last name"
              />
            </InputGroup>

            <InputGroup>
              <Field
                name="phone"
                component={Input}
                type="tel"
                placeholder="Phone number"
              />
            </InputGroup>

            <InputGroup>
              <Field
                name="message"
                component={TextArea}
                placeholder="Message"
                topMargin
                rows="8"
              />
            </InputGroup>

            <Buttons>
              <Button blue type="submit">
                Send
              </Button>
            </Buttons>
          </ContactForm>
        </ContactPage>
      </Container>
    )
  }
}

Contact.propTypes = {
  host: PropTypes.string,
  protocol: PropTypes.string
}

const mapStateToProps = ({ site: { host, protocol } }) => ({ host, protocol })

Contact = reduxForm({
  form: 'contact',
  validate
})(Contact)

export default connect(mapStateToProps, {
  postContactForm
})(Contact)
