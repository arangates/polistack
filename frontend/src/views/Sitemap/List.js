import styled from 'styled-components'

const List = styled.ul`
  padding-left: 10px;
  list-style: none;
`
export default List
