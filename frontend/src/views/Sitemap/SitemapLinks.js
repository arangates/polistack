import styled from 'styled-components'

const SitemapLinks = styled.div`
  padding-left: 8%;
  padding-top: 3%;
`

export default SitemapLinks
