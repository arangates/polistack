import styled from 'styled-components'

const Wrapper = styled.div`
  padding: 3em 0;
  margin-bottom: 30vh;
  p {
    color: #293244;
    font-size: 0.875rem;
    font-weight: 300;
  }
`

export default Wrapper
