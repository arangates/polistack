import { iconBullet } from 'images'
import styled from 'styled-components'

const ListItem = styled.li`
  position: relative;
  padding: 5px;

  &:before {
    padding: 8px;
    content: url(${iconBullet});
  }
`
export default ListItem
