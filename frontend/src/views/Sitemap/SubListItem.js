import { styled } from 'styled-components'
import { iconBulletWhite } from 'images'

const SubListItem = styled.li`
  position: relative;
  padding: 5px;
  padding-left: 36px;

  &:before {
    padding: 8px;
    content: url(${iconBulletWhite});
  }
`
export default SubListItem
