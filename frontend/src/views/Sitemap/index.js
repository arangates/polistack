/* eslint-disable react/prop-types */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Container } from 'components'
import { Link } from 'react-router-dom'
import Wrapper from './Wrapper'
import List from './List'
import ListItem from './ListItem'
import SitemapLinks from './SitemapLinks'
class Sitemap extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  render() {
    const { host, protocol } = this.props
    const canonicalUrl = `${protocol}://${host}/sitemap`
    return (
      <Wrapper>
        <Helmet>
          <title>Polistack - Predict politics - Sitemap</title>
          <link rel="canonical" href={canonicalUrl} />
        </Helmet>
        <Container>
          <h1>Sitemap</h1>
          <SitemapLinks>
            <Link to="/">Home</Link>
            <List>
              <ListItem>
                <Link to="/login">Log in</Link>
              </ListItem>

              <ListItem>
                <Link to="/register">Create Account</Link>
              </ListItem>

              <ListItem>
                <Link to="/faq">FAQ</Link>
              </ListItem>
              <ListItem>
                <Link to="/how-it-works">How it works</Link>
              </ListItem>

              <ListItem>
                <Link to="/news">News</Link>
              </ListItem>

              <ListItem>
                <Link to="/terms">Terms of service</Link>
              </ListItem>

              <ListItem>
                <Link to="/privacy">Privacy Policy</Link>
              </ListItem>

              <ListItem>
                <Link to="/contact">Contact</Link>
              </ListItem>
            </List>
          </SitemapLinks>
        </Container>
      </Wrapper>
    )
  }
}

Sitemap.propTypes = {
  host: PropTypes.string,
  protocol: PropTypes.string
}

const mapStateToProps = ({ site: { host, protocol } }) => ({ host, protocol })

export default connect(mapStateToProps)(Sitemap)
