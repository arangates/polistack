import App from './App'
import React from 'react'
import { renderToString } from 'react-dom/server'
import { Helmet } from 'react-helmet'
import Loadable from 'react-loadable'
import { getBundles } from 'react-loadable/webpack'
import { Provider } from 'react-redux'
import { matchPath, StaticRouter } from 'react-router-dom'
import { routerMiddleware } from 'react-router-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { ServerStyleSheet } from 'styled-components'
import url from 'url'
import express from 'express'
import routes from 'routes'
import rootReducer from './reducers'

const stats = require(process.env.RAREBETS_REACT_LOADABLE_JSON)
const assets = require(process.env.RAREBETS_ASSETS_MANIFEST)

const server = express()

const template = (
  styleTag = '',
  scriptTag = '',
  bundles = [],
  styledComponentsStyleTag = '',
  preloadedStateScriptTag = '',
  appMarkup = '',
  helmet
) => `
<!DOCTYPE html>
<html ${helmet.htmlAttributes.toString()}>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#102b3b">
    ${typeof helmet.meta !== 'undefined' && helmet.meta.toString()}
    <link rel="shortcut icon" href="/favicon.ico">
    ${
      typeof helmet.title === 'undefined'
        ? 'Polistack'
        : helmet.title.toString()
    }
    ${typeof helmet.link !== 'undefined' && helmet.link.toString()}
    ${styleTag}
    ${bundles
      .filter(bundle => bundle.file.endsWith('.css'))
      .map(
        bundle =>
          `<link rel="preload" href="${
            bundle.publicPath
          }" as="style" onload="this.onload=null;this.rel='stylesheet'">`
      )
      .join('\n')}
    ${bundles
      .filter(bundle => bundle.file.endsWith('.js'))
      .map(bundle => `<script defer src="${bundle.publicPath}"></script>`)
      .join('\n')}
    ${scriptTag}
    ${styledComponentsStyleTag}
    ${preloadedStateScriptTag}
  </head>
  <body>
    <div id="root">${appMarkup}</div>
  </body>
</html>`

server
  .disable('x-powered-by')
  .use(express.static(process.env.RAREBETS_PUBLIC_DIR))
  .get('/*', (req, res) => {
    // Grab an empty store
    const context = {}
    const staticRouter = new StaticRouter()
    staticRouter.props = { location: req.url, context }
    const { props: { history: staticHistory } } = staticRouter.render()
    const middleware = routerMiddleware(staticHistory)
    const store = createStore(
      rootReducer,
      {
        site: {
          host: req.hostname,
          protocol: process.env.NODE_ENV === 'production' ? 'https' : 'http',
          pathname: req.url
        }
      },
      applyMiddleware(thunk, middleware)
    )
    // Use the matching function used internally by react-router to identify the matched component
    // See https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/api/matchPath.md
    const apiCalls = []
    const queryString = url.parse(req.url).query || ''
    routes.some(route => {
      // use `matchPath` here
      const match = matchPath(req.path, route)
      if (
        match &&
        typeof route.component !== 'undefined' &&
        typeof route.component.loadData === 'function'
      ) {
        apiCalls.push(
          route.component.loadData(
            { search: queryString },
            match,
            store.dispatch.bind(store)
          )
        )
      }
      return match
    })
    Promise.all(apiCalls).then(() => {
      // This will be used to extract styled-components styles as CSS classes
      const styledComponentsCss = new ServerStyleSheet()
      let modules = []
      const appMarkup = renderToString(
        styledComponentsCss.collectStyles(
          <Loadable.Capture report={moduleName => modules.push(moduleName)}>
            <Provider store={store}>
              <App history={staticHistory} />
            </Provider>
          </Loadable.Capture>
        )
      )
      let bundles = getBundles(stats, modules)
      const helmet = Helmet.renderStatic()
      if (context.url) {
        res.redirect(context.url)
      } else {
        // Add style tag to the document head
        let styleTag = ''
        if (assets.client.css) {
          styleTag = `<link rel="preload" href="${
            assets.client.css
          }" as="style" onload="this.onload=null;this.rel='stylesheet'">`
        }
        // Handle styled-components CSS
        const styledComponentsStyleTags = styledComponentsCss.getStyleTags()
        // Add script tag to the document head
        const scriptTag = `<script defer ${
          process.env.NODE_ENV === 'production' ? 'crossorigin' : ''
        } src="${assets.client.js}"></script>`
        // Add the preloaded state to the template
        const preloadedState = store.getState()
        const preloadedStateScriptTag = `<script>
        window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(
          /</g,
          '\\u003c'
        )}
      </script>`
        const markup = template(
          styleTag,
          scriptTag,
          bundles,
          styledComponentsStyleTags,
          preloadedStateScriptTag,
          appMarkup,
          helmet
        )
        res.status(200).send(markup)
      }
    })
  })

export default server
