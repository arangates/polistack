import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import createHistory from 'history/createBrowserHistory'
import { routerMiddleware } from 'react-router-redux'
import Loadable from 'react-loadable'
import ReactGA from 'react-ga'

import api from 'services/api'

import rootReducer from './reducers'

import { unregister } from './registerServiceWorker'

import App from './App'

import 'constants/global.styles'
import { IS_CLIENT, GOOGLE_ANANLYTICS_ID } from './constants/globals'

const persistConfig = {
  key: 'user',
  storage,
  whitelist: ['user']
}

const history = createHistory()
const middleware = routerMiddleware(history)
const persistedReducer = persistReducer(persistConfig, rootReducer)
// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__ || {}
// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__
// Grab the state variables created by Redux Devtools Extension
const reduxDevtoolsState = window.__REDUX_DEVTOOLS_EXTENSION__
  ? window.__REDUX_DEVTOOLS_EXTENSION__()
  : {}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  persistedReducer,
  {
    ...reduxDevtoolsState,
    ...preloadedState,
    site: {
      host: window.location.host,
      protocol: process.env.NODE_ENV === 'production' ? 'https' : 'http'
    }
  },
  composeEnhancers(applyMiddleware(thunk, middleware))
)
const getProfileToken = () => {
  return store.getState().user.profile.token
}

const handleChange = () => {
  const token = getProfileToken()
  if (token !== undefined) {
    api.defaults.headers.common['Authorization'] = `JWT ${token}`
  }
}
if (process.env.NODE_ENV === 'production' && IS_CLIENT) {
  ReactGA.initialize(GOOGLE_ANANLYTICS_ID)

  // track the intial view when the site loads first time
  ReactGA.pageview(window.location.pathname)

  history.listen(location => {
    ReactGA.pageview(location.pathname)
  })
}

store.subscribe(handleChange)

const persistor = persistStore(store)

Loadable.preloadReady().then(() => {
  ReactDOM.hydrate(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App history={history} />
      </PersistGate>
    </Provider>,
    document.getElementById('root')
  )
})

unregister()
