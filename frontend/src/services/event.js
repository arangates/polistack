/* eslint-disable camelcase,space-before-function-paren */

import api from 'services/api'
import { BETS, ORDER_BOOK_ENTRIES } from 'services/endpoints'

class EventService {
  removeTrailingZeros = number => {
    return Math.round(number * 100) / 100
  }

  getEventDetails = async eventId => {
    const res = await api.get(`${BETS}${eventId}`)
    const item = res.data
    item.odds = this.removeTrailingZeros(item.odds)
    item.highestBid = this.removeTrailingZeros(item.highest_bid)
    item.lowestAsk = this.removeTrailingZeros(item.lowest_ask)
    item.orderBookId = item.order_book
    return item
  }

  getRelatedEvents = async eventId => {
    return api.get(`${BETS}${eventId}/similar/`)
  }

  getOrderBook = async bookId => {
    return api.get(`${ORDER_BOOK_ENTRIES}${bookId}`)
  }

  getGraphData = async (bookId, interval = 7) => {
    return api.get(`${ORDER_BOOK_ENTRIES}graph/${bookId}?interval=${interval}`)
  }
}

export default new EventService()
