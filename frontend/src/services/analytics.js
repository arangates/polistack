import ReactGA from 'react-ga'
import { GOOGLE_ANANLYTICS_ID } from '../constants/globals'

class AnalyticsService {
  sendSignupFormEvent = ({ firstName, lastName }) => {
    const username = `${firstName} ${lastName}`
    ReactGA.initialize(GOOGLE_ANANLYTICS_ID)
    ReactGA.event({
      category: 'Form',
      action: 'Sign up',
      label: username
    })
  }
}

export default new AnalyticsService()
