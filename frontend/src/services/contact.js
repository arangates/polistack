import api from 'services/api'
import { CONTACT } from 'services/endpoints'

class ContactService {
  postContactForm = async data => {
    const res = await api.post(CONTACT, data)
    return res
  }
}

export default new ContactService()
