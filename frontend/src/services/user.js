/* eslint-disable camelcase,space-before-function-paren */
import api from 'services/api'

import {
  LOGIN,
  REGISTER,
  PROFILE,
  ACTIVATE_USER,
  USER_TRADES,
  TRANSACTIONS
} from 'services/endpoints'
import {
  ACTIVE,
  PENDING,
  ENDED,
  ORDER_PENDING,
  ORDER_PROCESSED
} from 'constants/bet.statuses'

class UserService {
  buildRequest = (pageNum = 1, currStatus = null, searchData, getState) => {
    const formInstance = getState().form.searchMyBets

    let currSearch = null

    if (!searchData && (formInstance && formInstance.values)) {
      currSearch = formInstance.values.phrase
    } else if (searchData) {
      currSearch = searchData.phrase
    }

    const page = `?page=${pageNum}`
    const statement = !currSearch
      ? ''
      : `&order__bet__statement__icontains=${currSearch}`
    let betStatus = !currStatus ? '' : `&order__bet__status=${currStatus}`
    let orderStatus = ''
    if (currStatus === ACTIVE || currStatus === null) {
      orderStatus = `&status=${ORDER_PROCESSED}`
      betStatus = `&order__bet__status=${ACTIVE}`
    }
    if (currStatus === PENDING) {
      orderStatus = `&status=${ORDER_PENDING}`
      betStatus = `&order__bet__status=${ACTIVE}`
    }
    if (currStatus === ENDED) {
      orderStatus = `&status=${ORDER_PROCESSED}`
      betStatus = `&order__bet__status=${ENDED}`
    }

    return `${USER_TRADES}${page}${statement}${orderStatus}${betStatus}`
  }

  login = async loginData => {
    const res = await api.post(LOGIN, loginData)

    return {
      token: res.data.token
    }
  }

  getProfile = async _ => {
    const res = await api.get(PROFILE)
    return res.data
  }

  register = async registerData => {
    const {
      email,
      password,
      firstName: first_name,
      lastName: last_name
    } = registerData

    const res = await api.post(REGISTER, {
      email,
      password,
      first_name,
      last_name
    })

    return res
  }

  editProfile = async profileData => {
    const res = await api.put('/auth/me/', profileData)
    return res
  }

  forgotPassword = async email => {
    const res = await api.post('/auth/password/reset', email)
    return res.data
  }
  updatePassword = async passwordData => {
    const res = await api.post('/auth/password/reset/confirm', passwordData)
    return res.status
  }

  activateUser = async activationData => {
    const res = await api.post(ACTIVATE_USER, activationData)
    return res.status
  }

  removeTrailingZeros = number => {
    return Math.round(number * 100) / 100
  }

  fetchTransactions = async (page = 1) => {
    const res = await api.get(`${TRANSACTIONS}?page=${page}`)
    return res
  }

  fetchBets = async (page, status, searchData, getState) => {
    const res = await api.get(
      this.buildRequest(page, status, searchData, getState)
    )
    return res.data
  }

  fetchTrades = async (page, status, searchData, getState) => {
    const betOnBothSides = bet => {
      const betOnYes = bet.user_bet_actions.some(item => item.side === 'yes')
      const betOnNo = bet.user_bet_actions.some(item => item.side === 'no')

      return !!(betOnYes && betOnNo)
    }

    const filterBySide = (bet, side) => {
      return bet.user_bet_actions.filter(item => item.side === side)
    }

    const res = await api.get(
      this.buildRequest(page, status, searchData, getState)
    )

    const sortedResults = []

    res.data.results.map(bet => {
      if (betOnBothSides(bet)) {
        sortedResults.push({
          ...bet,
          listId: `${bet.id}-yes`,
          user_bet_actions: filterBySide(bet, 'yes')
        })
        sortedResults.push({
          ...bet,
          listId: `${bet.id}-no`,
          user_bet_actions: filterBySide(bet, 'no')
        })
      } else {
        sortedResults.push({ ...bet, listId: `${bet.id}` })
      }
    })

    const newRes = { ...res, data: { ...res.data, results: sortedResults } }

    return newRes.data
  }

  requestDeposit = async depositData => {
    const res = await api.post(`${TRANSACTIONS}request_deposit/`, depositData)
    return res.data
  }

  makePayment = async paymentData => {
    await this.requestDeposit({
      payment_option: paymentData.newPaymentOption,
      amount: paymentData.newAmountValue
    })
  }
}

export default new UserService()
