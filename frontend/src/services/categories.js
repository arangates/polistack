import api from 'services/api'
import { CATEGORIES } from 'services/endpoints'

class CategoryService {
  fetchCategories = async _ => {
    const res = await api.get(CATEGORIES)

    return res.data.results
      .filter(category => category.name !== 'Other')
      .sort((a, b) => {
        if (a.name < b.name) return -1
        if (a.name > b.name) return 1
        return 0
      })
  }
}

export default new CategoryService()
