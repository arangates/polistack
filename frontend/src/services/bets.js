/* eslint-disable no-unused-vars,space-before-function-paren */
import isString from 'lodash.isstring'
import { getDeserializedCategoryName } from 'utils'
import api from 'services/api'

import {
  BETS,
  BET_ACTIONS,
  ORDER_ACTIONS,
  USER_TRADES
} from 'services/endpoints'
import { OPEN_MODAL } from 'actions/types'
import { INFO } from 'constants/modal.types'
class BetService {
  buildRequest = filters => {
    const categoryNameFilter =
      filters.categoryName !== null
        ? `&categories__name=${getDeserializedCategoryName(
            filters.categoryName
          )}`
        : ''
    const pageNumberFilter = `?page=${filters.page}`
    const searchPhraseFilter =
      isString(filters.q) && filters.q.trim().length > 0
        ? `&statement__icontains=${filters.q.trim()}`
        : ''
    const sortOrder =
      filters.sortOrder !== null ? `&sortOrder=${filters.sortOrder}` : ''
    return `${BETS}${pageNumberFilter}${categoryNameFilter}${searchPhraseFilter}${sortOrder}`
  }

  removeTrailingZeros = number => {
    return Math.round(number * 100) / 100
  }

  fetchBets = async filters => {
    const res = await api.get(this.buildRequest(filters))
    const data = res.data
    return {
      ...data,
      results: res.data.results.map(item => {
        item.odds = this.removeTrailingZeros(item.odds)
        item.highestBid = this.removeTrailingZeros(item.highest_bid)
        item.lowestAsk = this.removeTrailingZeros(item.lowest_ask)
        item.orderBook = item.order_book
        return item
      })
    }
  }

  createBet = async betData => {
    return api.post(BETS, betData)
  }

  askToResolveBet = async bet => {
    return api.get(`${BETS}${bet.id}/please_resolve/`)
  }

  isLanding = getState => getState().router.location.pathname === '/'

  placeBet = async (betData, getState) => {
    const userId = getState().user.profile.id

    const reqObject = {
      user: userId,
      bet: betData.betId,
      amount: betData.amount,
      side: betData.side
    }

    const res = await api.post(BET_ACTIONS, reqObject)
    return res.data
  }

  patchBet = async betData => {
    const reqObject = {
      shares: betData.amount
    }

    const res = await api.patch(`${USER_TRADES}${betData.betId}/`, reqObject)
    return res.data
  }

  insufficientYesBet = yesReqObject => {
    return api.post(ORDER_ACTIONS, yesReqObject)
  }

  insufficientNoBet = noReqObject => {
    return api.post(ORDER_ACTIONS, noReqObject)
  }

  createInsufficientYesOrNoBet = async (newBetData, userBalance, dispatch) => {
    let betResult = null
    if (newBetData.side === 'yes') {
      const yesReqObject = {
        bet: newBetData.betId,
        price: newBetData.price,
        shares: Math.floor(userBalance / newBetData.price),
        side: 'yes'
      }
      if (yesReqObject.shares) {
        betResult = await this.insufficientYesBet(yesReqObject)
      } else {
        dispatch({
          type: OPEN_MODAL,
          modal: INFO,
          data: {
            header: 'No Balance',
            text: 'Please top up your account balance',
            changeLocation: {
              link: '/profile/account',
              text: 'Go to Account'
            },
            cancel: true
          }
        })
      }
    } else {
      const noReqObject = {
        bet: newBetData.betId,
        price: 100 - newBetData.price,
        shares: Math.floor(userBalance / newBetData.price),
        side: 'no'
      }
      if (noReqObject.shares) {
        betResult = await this.insufficientNoBet(noReqObject)
      } else {
        dispatch({
          type: OPEN_MODAL,
          modal: INFO,
          data: {
            header: 'No Balance',
            text: 'Please top up your account balance',
            changeLocation: {
              link: '/profile/account',
              text: 'Go to Account'
            },
            cancel: true
          }
        })
      }
    }
    if (betResult.status === 201) {
      dispatch({
        type: OPEN_MODAL,
        modal: INFO,
        data: {
          header: 'Insufficient funds available',
          text:
            'You account balance allowed to place only part of the trade. Please deposit funds to your account to submit an order with larger volume',
          changeLocation: {
            link: '/profile/account',
            text: 'Go to Account'
          },
          cancel: true
        }
      })
    } else {
      dispatch({
        type: OPEN_MODAL,
        modal: INFO,
        data: {
          header: 'No Balance',
          text: betResult.non_field_errors[0]
        },
        cancel: true
      })
    }
  }

  confirmBet = async (newBetData, userBalance, dispatch) => {
    const requiredBalance = newBetData.price * newBetData.shares
    if (requiredBalance > userBalance) {
      this.createInsufficientYesOrNoBet(newBetData, userBalance, dispatch)
    } else {
      if (newBetData.side === 'no') {
        newBetData.price = 100 - newBetData.price
      }
      const reqObject = {
        bet: newBetData.betId,
        price: newBetData.price,
        shares: newBetData.shares,
        side: newBetData.side
      }
      const res = await api.post(ORDER_ACTIONS, reqObject)
      return res
    }
  }

  mapRequests = ({ id }) => {
    return api.get(`${BET_ACTIONS}${id}/back_out_amount/`)
  }

  getBackOutAmount = async bet => {
    const userActions = bet.user_bet_actions
    const requests = userActions.map(this.mapRequests)
    const result = await Promise.all(requests)
    return result
      .map(item => item.data.back_out_amount)
      .reduce((prev, curr) => prev + curr, 0)
  }

  updateBetAmount = async (bet, transactionType) => {
    await this.patchBet({
      betId: bet.id,
      amount: bet.newValue,
      side: bet.side
    })
  }

  updateBetStatus = async (betId, status) => {
    const request = await api.patch(`${BETS}/${betId}/`, { status })
  }
}

export default new BetService()
