/* eslint-disable no-undef */
import axios from 'axios'
import { IS_CLIENT } from 'constants/globals'

let baseURL = 'http://localhost:8000'

if (process.env.NODE_ENV === 'production' && IS_CLIENT) {
  baseURL = `https://api.${window &&
    window.location &&
    window.location.hostname}`
} else if (process.env.NODE_ENV === 'production') {
  baseURL = process.env.API_HOST || baseURL
}

const instance = axios.create({
  baseURL,
  headers: {
    'Content-Type': 'application/json'
  }
})

export default instance
