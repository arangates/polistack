/* eslint-disable no-undef */
const NotificationService = class {
  socket = {}

  constructor(token) {
    this.connect(token)

    this.socket.onmessage = event => {
      const data = this.parseNotifications(event.data)

      switch (data.action) {
        case 'pending':
          this.onPendingEvent(data)
          break
        case 'Bet':
          this.onBetEvent(data)
          break
      }
    }
  }

  connect = token => {
    this.socket = new WebSocket(
      `ws://localhost:8000/notifications/?jwt=${token}`
    )
  }

  parseNotifications = notifications => JSON.parse(notifications)

  markAsSeen = async notifications => {
    const filtered = notifications
      .filter(item => !item.seen)
      .map(item => item.id)

    if (filtered.length !== 0) {
      this.socket.send(
        JSON.stringify({
          action: 'see',
          data: filtered
        })
      )
    }
  }

  sortByDate = notifications =>
    notifications.sort((a, b) => {
      return new Date(b.created_at) - new Date(a.created_at)
    })

  onPendingEvent = event => {}

  onBetEvent = event => {}

  disconnect = () => {
    this.socket.close()
  }
}

export default NotificationService
