0) Choose an image from for example unsplash.com and prepare the image with https://squoosh.app/: target measurement should be exactly 1143x660 and file type: .jpg
======

I) Adding news article 
====== 

1. create a filename which is required in the URL (for eg: to get url as https://twojretail.pl/aktualnosci/moda-na-pop-upy , add filename as moda-na-pop-upy.md).

     - i) create file inside frontend/src/views/Achievements/posts/en 

2. paste the image to `/frontend/src/images/achievements/news_1.jpg` and rename to news*\${next_number}

3.  Add metadata such as title,date,image . 

    for eg:
    ***
    ``` id: 12
    title: Warszawa, Kraków i Gdańsk - kulinarne must visit
    date: 2019-08-22T03:24:00
    image: ../static/media/achievement_12.webp
    ```
    ***

    -  id -> not required , since we sort by date
    -  title -> Title of the article from teams
    -  date -> date from teams
    -  image -> refer point 2

4. copy the text from article from teams to file. (Refer previous news articles or https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet for syntax).
   Do this step for both EN and PL version of article

II) Import and export the image file
====== 

Import newly added article image in /home/mateusz/Documents/rarebets/frontend/src/constants/images.js. for eg.`import firstNews from '../images/achievements/news_1.jpg' and add to export.


