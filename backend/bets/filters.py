from django_filters import CharFilter
from django_filters import rest_framework as filters

import bets.constants as bet_constants
from bets.models import Bet


class BetFilter(filters.FilterSet):
    result = CharFilter(name='result', method='filter_result')

    def filter_result(self, queryset, name, value):
        # THINK, why name is passed
        filter_kwargs = {
            bet_constants.WON: {'transactions__amount__gt': 0},
            bet_constants.LOST: {'transactions__amount__lt': 0},
        }[value]
        return queryset.filter(**filter_kwargs, users=self.request.user).distinct()

    class Meta:
        model = Bet
        fields = {
            'created_by__email': ['icontains'],
            'statement': ['icontains'],
            'categories__name': ['icontains', 'exact'],
            'status': ['exact', 'in'],
            'ends': ['lt', 'gt', 'exact'],
            # 'users__email': ['icontains'],
            # 'users__id': ['in'],
            'created_by': ['exact'],
        }
