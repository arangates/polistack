from random import randint

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
import factory.fuzzy

from bets import constants as bets_constants
from bets.factories_data import ICONS
from bets.models import Bet, Category, Transaction
from users.factories import UserFactory
from utils.factories import CreatedAtMixin

User = get_user_model()


class CategoryFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('word', ext_word_list=('Sport', 'People', 'Cryptocurrency', 'World news', 'Other'))
    icon = factory.Faker('word', ext_word_list=ICONS)

    class Meta:
        model = Category
        django_get_or_create = ('name',)


class BetFactory(CreatedAtMixin, factory.django.DjangoModelFactory):
    created_by = factory.SubFactory(UserFactory)
    image = factory.django.ImageField(color='blue')
    statement = factory.Faker('sentence', nb_words=randint(4, 30))
    admin_notes = factory.Faker('paragraph')
    ends = factory.Faker('date')
    status = factory.fuzzy.FuzzyChoice(bets_constants.STATUS.ALL)
    factory.django.ImageField(color='blue', width=500, height=500)

    class Meta:
        model = Bet
        django_get_or_create = ('statement',)

    @factory.post_generation
    def users(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for user in extracted:
                self.users.add(user)

    @factory.post_generation
    def categories(self, create, extracted, **kwargs):
        # http://factoryboy.readthedocs.io/en/latest/recipes.html#simple-many-to-many-relationship
        if not create:
            return
        if extracted:
            for category in extracted:
                self.categories.add(category)
        else:
            # at least one
            self.categories.add(CategoryFactory())


class GroupFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('name')

    class Meta:
        model = Group


class TransactionFactory(CreatedAtMixin, factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    amount = factory.LazyAttribute(lambda o: o.orig - o.fee)
    fee = factory.fuzzy.FuzzyDecimal(0, 20)
    bet = factory.SubFactory(BetFactory)
    orig = factory.fuzzy.FuzzyDecimal(-1000, 1000)

    class Meta:
        model = Transaction
    # TODO create coveninet method topup so that we don't have to remember
    # TransactionFactory(user=myUser1, amount=100000, bet=None)
