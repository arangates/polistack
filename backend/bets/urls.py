from rest_framework import routers

from bets.views import BetViewSet, CategoryViewSet, TransactionViewSet

router = routers.SimpleRouter()
router.register('bets', BetViewSet, base_name='bets')
router.register('transactions', TransactionViewSet, base_name='transactions')
router.register('categories', CategoryViewSet, base_name='categories')
urlpatterns = router.urls
