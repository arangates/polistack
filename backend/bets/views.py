from django.db.models import Sum
from django.db.models.functions import Coalesce
from rest_framework import status, viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from bets.constants import SIMILAR_BETS_COUNT, STATUS
from bets.filters import BetFilter
from bets.models import Bet, Category, Transaction
from bets.permissions import IsOwnerOrReadOnly
from bets.serializers import BetDetailSerializer, BetSerializer, CategorySerializer, TransactionSerializer
from bets.utils import format_docstring


@format_docstring(STATUS=STATUS)
class BetViewSet(viewsets.ModelViewSet):
    """
    Anonymous users can only see {STATUS.VISIBLE_TO_EVERYONE} Bets.
    Authenticated user can see {STATUS.VISIBLE_TO_AUTHENTICATED}.

    /with_bet_actions provides {STATUS.VISIBLE_TO_AUTHENTICATED} to authenticated users
    """
    queryset = Bet.objects.select_related(
        'order_book'
    ).prefetch_related(
        'order_book__entries'
    ).annotate(
        volume=Coalesce(Sum('order_book__entries__shares'), 0)
    ).order_by('-volume').all()
    filter_class = BetFilter
    serializer_class = BetSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)

    def get_serializer_class(self):
        if self.action in ('retrieve', 'similar',):
            return BetDetailSerializer
        return BetSerializer

    def get_queryset(self):
        # TODO: clarify logic in the docstring
        # Not authenticated users can see only Bets with proper status.
        sortOrder = 'sortOrder' in self.request.GET
        if self.request.method == 'GET':
            if sortOrder:
                return self.queryset.order_by(self.request.GET['sortOrder'])
            return self.queryset.filter(status__in=STATUS.VISIBLE_TO_EVERYONE)
        # Authenticated user can update (PATCH) Bets with different statuses
        return self.queryset.filter(status__in=STATUS.VISIBLE_TO_AUTHENTICATED)

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    @detail_route(methods=['get'], permission_classes=[IsAuthenticated])
    def please_resolve(self, request, pk=None):
        bet = self.get_object()
        bet.resolve_query = bet.resolve_query + 1
        bet.save()
        # TODO: Michał, what is this?
        # if bet.bet_actions.filter(user=request.user).exists():
        #     bet.bet_actions.filter(user=request.user).update(resolve_query=True)
        #     return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_200_OK, data={'changed_to': bet.resolve_query})

    @detail_route(methods=['get'], permission_classes=[])
    def similar(self, request, pk=None):
        queryset = Bet.objects.select_related(
            'order_book'
        ).prefetch_related(
            'order_book__entries'
        ).annotate(
            volume=Coalesce(Sum('order_book__entries__shares'), 0)
        ).filter(
            status=STATUS.ACTIVE
        ).exclude(
            id=pk
        ).order_by(
            '-volume'
        )[:SIMILAR_BETS_COUNT]
        serializer_class = self.get_serializer_class()
        serializer_data = [serializer_class(bet, context={'request': request}).data for bet in queryset]
        return Response(status=status.HTTP_200_OK, data=serializer_data)


class TransactionViewSet(viewsets.ModelViewSet):
    serializer_class = TransactionSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = Transaction.objects.select_related('bet').all()

        if self.request.user.is_staff:
            return queryset
        return queryset.filter(account__user=self.request.user)

    @list_route(methods=['post'], permission_classes=[IsAuthenticated])
    def request_deposit(self, request):
        user = request.user
        payment_option = request.data['payment_option']
        amount = request.data['amount']
        user.request_deposit(amount, payment_option)
        return Response(status=status.HTTP_204_NO_CONTENT)


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
