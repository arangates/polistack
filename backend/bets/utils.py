import operator


def combine_dicts(a, b, op=operator.add):
    """
    combine two dicts (adding values for keys that appear in both)

    https://stackoverflow.com/questions/11011756/is-there-any-pythonic-way-to-com
        bine-two-dicts-adding-values-for-keys-that-appe/11011846#comment81459046_11012181
    """
    # https://stackoverflow.com/questions/11011756/is-there-any-pythonic-way-to-com
    #
    return {**a, **b, **{k: op(a[k], b[k]) for k in a.keys() & b}}


def format_docstring(*args, **kwargs):
    # https://stackoverflow.com/a/10308363
    # Bejesus, that is strange
    def inner(obj):
        obj.__doc__ = obj.__doc__.format(*args, **kwargs)
        return obj
    return inner
