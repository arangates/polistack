illegal_status_transition = 'Changing status from {old} to {new} not allowed.'
missing_image = 'Add photo when changing from {old}.'
