# Generated by Django 2.0.4 on 2019-07-02 13:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_useraccount'),
        ('bets', '0002_auto_20190619_0831'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='account',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='transactions', to='users.UserAccount'),
        ),
        migrations.AddField(
            model_name='transaction',
            name='amount_after',
            field=models.DecimalField(decimal_places=2, max_digits=9, null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='amount_before',
            field=models.DecimalField(decimal_places=2, max_digits=9, null=True),
        ),
    ]
