from collections import namedtuple
from decimal import Decimal
from typing import DefaultDict, List

import bets.models  # noqa
import users.models  # noqa

# TODO: clean ths, use models, don't imitate only for the sake of brevity
BA = A = namedtuple('BetAction', field_names=['user', 'amount'])

BetActions = List['bets.models.BetAction']
GroupedBetActions = DefaultDict['user.smodels.User', Decimal]
