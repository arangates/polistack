from decimal import Decimal
import json

import colorhash
from django.conf import settings
from django.contrib import admin, messages
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator
from django.db.models import Q
from django.forms import Field, ModelForm, Widget
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django_object_actions import DjangoObjectActions

from bets.constants import STATUS
from bets.models import Bet, Category, Transaction
from bets.validation import illegal_status_transition, missing_image
from orderbooks.constants import ORDER_BOOK_ENTRY_STATUS, ORDER_SIDE
from orderbooks.models import OrderBook
from orderbooks.serializers import OrderSerializer
from users.serializers import UserSerializer
from utils import RBJSONEncoder
from utils.admin_helpers import created_at_formatted


class BetDebugWidget(Widget):
    VIEWPORT = Decimal('800')
    template_name = 'bet_debug.html'

    def __init__(self, bet, attrs=None):
        self.bet = bet
        super().__init__(attrs)

    @staticmethod
    def contrasting_text_color(hex_str):
        # http://stackoverflow.com/a/37603471/1472229
        # http://codepen.io/WebSeed/full/pvgqEq/
        # there's a copy of this in frontend
        if hex_str.startswith('#'):
            hex_str = hex_str[1:]
        r, g, b = hex_str[:2], hex_str[2:4], hex_str[4:]
        if 1 - (int(r, 16) * 0.299 + int(g, 16) * 0.587 + int(b, 16) * 0.114) / 255 < 0.5:
            return 'black'
        else:
            return 'white'

    @classmethod
    def get_order_box(cls, orders, scale=Decimal('0.1')):
        boxes = []
        for order in orders:
            text = f'{order.owner.email}<br>{settings.CURRENCY_SIGN}{order.price}<br>{order.shares}'
            height = max(order.price, order.shares) * scale
            bg = colorhash.ColorHash(order.owner).hex
            color = cls.contrasting_text_color(bg)
            # TODO: not efficient
            data = json.dumps(OrderSerializer(instance=order).data, indent=4)
            # mark invisible boxes, maybe we can show them using some CSS tricks
            maybe_class = 'class=too_small ' if height < 5 * scale else ''
            # we display stacks, so older orders go on top of previous ones
            boxes.insert(
                0,
                f'<box data-serialized=\'{data}\' {maybe_class} '
                f'style="height:{height}px;color:{color};background-color:{bg}">'
                f'{text}'
                f'</box>'
            )
        return mark_safe(''.join(boxes))

    @classmethod
    def get_gain_box(cls, gains, scale):
        boxes = []
        for user, (sum_played, gain, total) in gains:
            text = f'{user.email}<br>{settings.CURRENCY_SIGN}{gain:.2f}'
            height = abs(gain * scale)
            transaction = gain * (1 - user.fee) if gain > 0 else gain
            text_align = 'left' if gain > 0 else 'right'
            bg = colorhash.ColorHash(user).hex
            color = cls.contrasting_text_color(bg)
            # TODO: not efficient
            fee = gain * user.fee if gain > 0 else 0
            data = json.dumps(UserSerializer(instance=user).data, indent=8, cls=RBJSONEncoder)
            boxes.insert(
                0,
                f'<box data-serialized=\''
                f'{{'
                f'\n'
                f'    sum_played: ${sum_played:.2f},\n'
                f'    gain: "${gain:.2f}",\n'
                f'    transaction: "${transaction:.2f}",\n'
                f'    user_fee: "{user.fee}",\n'
                f'    fee: "{fee:.2f}",\n'
                f'    total: "${total:.2f}",\n'
                f'    user: {data}\n'
                f'}}\''
                f'style="height:{height}px;color:{color};background-color:{bg};text-align:{text_align}">'
                f'{text}'
                f'</box>'
            )
        return mark_safe(''.join(boxes))


class BetAdminModelForm(ModelForm):
    bet_debug = Field(required=False, disabled=True, initial='disabled')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs and kwargs['instance']:
            self.fields['bet_debug'].widget = BetDebugWidget(bet=kwargs['instance'])

    def clean_status(self):
        old = self.instance.status
        new = self.cleaned_data['status']
        if old != new and new not in STATUS.ADMIN_TRANSITION_MAP.get(old, []):
            raise ValidationError(illegal_status_transition.format(old=old, new=new))
        return new

    def clean(self):
        if (
            'status' in self.cleaned_data
            and self.cleaned_data['status'] == STATUS.ENDED
            and not self.cleaned_data['win']
        ):
            raise ValidationError('Trying to resolve a bet without deciding the winner')
        if (
            'status' in self.cleaned_data
            and self.instance.status == STATUS.PENDING
            and self.cleaned_data['status'] in STATUS.ADMIN_TRANSITION_MAP[STATUS.PENDING]
            and 'image' in self.cleaned_data
            and not self.cleaned_data['image']
        ):
            raise ValidationError(missing_image.format(old=self.instance.status))


@admin.register(Bet)
class BetAdmin(DjangoObjectActions, admin.ModelAdmin):
    form = BetAdminModelForm
    list_display = (
        'created_at_formatted',
        'id',
        'statement',
        'created_by',
        'image',
        'ends',
        'status',
    )
    list_filter = ('status', )
    save_on_top = True
    save_as_continue = True
    search_fields = ('statement', )
    readonly_fields = (
        'get_transactions',
        'get_paired_order_book_entries',
        'get_order_book_entries',
        'get_orders',
        'win',
        'created_by',
    )
    fieldsets = (
        (
            'Basic data', {
                'fields': (
                    'image',
                    'statement',
                    'description',
                    'admin_notes',
                    'ends',
                    'status',
                    'categories',
                )
            }
        ),
        (
            'Non editable', {
                'fields': (
                    'get_transactions',
                    'get_paired_order_book_entries',
                    'get_order_book_entries',
                    'get_orders',
                    'win',
                    'created_by',
                )
            }
        ),

        (
            'DEBUG', {
                'fields': (
                    'bet_debug',
                )
            }
        ),
    )

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        super().save_model(request, obj, form, change)
        if not change:
            OrderBook.objects.create(bet=obj)

    created_at_formatted = created_at_formatted

    def get_queryset(self, request):
        self.get_params = request.GET
        return super().get_queryset(request)

    def resolve_yes(self, request, obj):
        if 'apply' not in request.POST:
            return render(
                request,
                'bets/resolve_confirmation.html',
                context={'obj': obj, 'resolution': 'Resolve Yes'}
            )
        if not obj.can_be_resolved(STATUS.ENDED, obj.status):
            messages.error(request, 'Cannot be resolved.')
            return
        obj.win = ORDER_SIDE.BID
        obj.status = STATUS.ENDED
        obj.save(resolved_by=request.user)
        messages.info(request, 'Resolved to YES')

    def resolve_no(self, request, obj):
        if 'apply' not in request.POST:
            return render(
                request,
                'bets/resolve_confirmation.html',
                context={'obj': obj, 'resolution': 'Resolve No'}
            )
        if not obj.can_be_resolved(STATUS.ENDED, obj.status):
            messages.error(request, 'Cannot be resolved.')
            return
        obj.win = ORDER_SIDE.ASK
        obj.status = STATUS.ENDED
        obj.save(resolved_by=request.user)
        messages.warning(request, 'Resolved to NO')

    def order_book(self, request, obj):
        return redirect('/orderbooks/books/admin/{}/'.format(obj.order_book.id))

    change_actions = ('resolve_yes', 'resolve_no', 'order_book',)

    def get_change_actions(self, request, object_id, form_url):
        actions = super().get_change_actions(request, object_id, form_url)
        actions = list(actions)

        obj = self.model.objects.get(pk=object_id)
        if not obj.can_be_resolved(STATUS.ENDED, obj.status):
            actions.remove('resolve_yes')
            actions.remove('resolve_no')

        return actions

    def get_transactions(self, bet):
        queryset = bet.transactions.select_related(
            'bet',
            'user',
            'account'
        ).prefetch_related(
            'account__user'
        ).all().order_by('-created_at')
        paginator = Paginator(queryset, 20)
        page = self.get_params.get('transaction_page', 1)
        return render_to_string('bets/transactions_list.html', {
            'transactions': paginator.page(page),
        })

    get_transactions.short_description = 'Transactions'

    def get_paired_order_book_entries(self, bet):
        page_param = 'paired_entry_page'
        queryset = bet.order_book.entries.select_related(
            'order',
            'traded_with'
        ).prefetch_related(
            'order__owner'
        ).filter(
            status=ORDER_BOOK_ENTRY_STATUS.PROCESSED,
            is_aggressor=True
        ).order_by('-created_at')
        paginator = Paginator(queryset, 20)
        page = self.get_params.get(page_param, 1)
        return render_to_string('bets/orderbookentries_list.html', {
            'entries': paginator.page(page),
            'page_param': page_param,
        })

    get_paired_order_book_entries.short_description = 'Paired Order Book Entries'

    def get_order_book_entries(self, bet):
        page_param = 'entry_page'
        queryset = bet.order_book.entries.select_related(
            'order',
            'traded_with'
        ).prefetch_related(
            'order__owner'
        ).filter(
            Q(is_aggressor=True) | Q(status=ORDER_BOOK_ENTRY_STATUS.PENDING)
        ).order_by('-created_at')
        paginator = Paginator(queryset, 20)
        page = self.get_params.get(page_param, 1)
        return render_to_string('bets/orderbookentries_list.html', {
            'entries': paginator.page(page),
            'page_param': page_param,
        })

    get_order_book_entries.short_description = 'Order Book Entries'

    def get_orders(self, bet):
        page_param = 'order_page'
        queryset = bet.orders.select_related('owner').order_by('-created_at')
        paginator = Paginator(queryset, 20)
        page = self.get_params.get(page_param, 1)
        return render_to_string('bets/orders_list.html', {
            'orders': paginator.page(page),
            'page_param': page_param,
        })

    get_orders.short_description = 'Raw Orders'


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_icon')
    fields = ('name', 'get_icon', 'icon')
    readonly_fields = ('get_icon',)

    def get_icon(self, category):
        return mark_safe(category.icon)

    get_icon.short_description = 'image'


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = (
        'created_at_formatted',
        'bet',
        'amount',
        'fee',
        'orig',
        'user',
    )

    list_per_page = 20

    created_at_formatted = created_at_formatted

    def get_queryset(self, request):
        # copy bets.views.TransactionViewSet#get_queryset
        return (
            self.model._default_manager
            .select_related('bet')
            .select_related('user')
        )
