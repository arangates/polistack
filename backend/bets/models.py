from collections import defaultdict
import itertools

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models, transaction
from django.utils.translation import ugettext_lazy as _

from bets.constants import ONE_MILLION_DOLLAR, STATUS
from notifications.websockets import BetNotifier
from orderbooks.constants import ORDER_BOOK_ENTRY_STATUS, ORDER_SIDE
from utils.models import OrderedTimestampModel

User = get_user_model()


class Category(models.Model):
    name = models.CharField(max_length=120)
    icon = models.TextField()  # vector graphics (svg)

    class Meta:
        verbose_name_plural = 'categories'

    def __str__(self):
        return f'Category: {self.name}'


class Bet(OrderedTimestampModel):
    """
    User initially provides statement, yes, no, amount (that he enters with)
    Categories and image are controlled by the admin
    """
    STATUS_CHOICES = (
        (STATUS.PENDING, _('pending')),
        (STATUS.ACTIVE, _('active')),
        (STATUS.ENDED, _('ended')),
    )
    SIDE_CHOICES = ((ORDER_SIDE.BID, 'bid'), (ORDER_SIDE.ASK, 'ask'))

    image = models.ImageField(blank=True, null=True)
    statement = models.CharField(max_length=320, unique=True)
    description = models.TextField(blank=True, null=True)
    admin_notes = models.TextField(blank=True, null=True)
    ends = models.DateField(blank=True, null=True)
    status = models.CharField(choices=STATUS_CHOICES, default=STATUS.PENDING, max_length=9)
    win = models.CharField(choices=SIDE_CHOICES, max_length=3, null=True, blank=True)

    categories = models.ManyToManyField(Category, related_name='bets')
    # users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='bets', through='orderbooks.Order')
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='created_bets', on_delete=models.PROTECT)
    resolve_query = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.statement}'

    def group_orders_by_side(self):
        # actions in a form consumable by solve_bet
        orders = {'bid': tuple(), 'ask': tuple()}
        orders.update({
            side: tuple(orders)
            for side, orders in
            itertools.groupby(
                self.orders.order_by('side', 'created_at'),
                lambda order: order.side
            )
        })
        return orders

    def can_be_resolved(self, current_status, previous_status):
        return (
            # not new
            self.pk
            and previous_status == STATUS.ACTIVE
            and current_status == STATUS.ENDED
        )

    @transaction.atomic
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None, resolved_by=None):
        old_bet = Bet.objects.get(pk=self.pk) if self.pk else None
        super().save(force_insert, force_update, using, update_fields)
        if old_bet and self.can_be_resolved(self.status, old_bet.status):
            self.resolve(resolved_by=resolved_by)
        if old_bet:
            bet_notifier = BetNotifier(old_bet, self)
            bet_notifier.notify()

    def resolve(self, resolved_by=None):

        minuend = 100 if self.win == ORDER_SIDE.BID else 0

        entries = self.order_book.entries.filter(
            status=ORDER_BOOK_ENTRY_STATUS.PROCESSED
        )
        user_side_value_map = defaultdict(lambda: {ORDER_SIDE.BID: 0, ORDER_SIDE.ASK: 0})
        user_id_map = {}

        for entry in entries:
            user = entry.order.owner
            user_side_value_map[user.id][entry.order.side] += entry.shares * (minuend - entry.price)
            user_id_map[user.id] = user

        for user_id, side_value_map in user_side_value_map.items():
            user = user_id_map[user_id]
            bid_value = side_value_map[ORDER_SIDE.BID]
            ask_value = side_value_map[ORDER_SIDE.ASK]
            gross_value = bid_value - ask_value

            user.account.update(
                amount=gross_value,
                is_fee_applicable=gross_value > 0,
                bet=self,
                transaction_by=resolved_by
            )


class Transaction(OrderedTimestampModel):
    # orig - fee = amount
    orig = models.DecimalField(**ONE_MILLION_DOLLAR)
    amount = models.DecimalField(**ONE_MILLION_DOLLAR)
    fee = models.DecimalField(**ONE_MILLION_DOLLAR)
    amount_before = models.DecimalField(**ONE_MILLION_DOLLAR, null=True)
    amount_after = models.DecimalField(**ONE_MILLION_DOLLAR, null=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='transactions')
    # null means a real money transaction, not connected to any bet
    bet = models.ForeignKey(Bet, null=True, blank=True, on_delete=models.PROTECT, related_name='transactions')

    account = models.ForeignKey(
        'users.UserAccount',
        null=True,
        related_name='transactions',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.amount} {self.created_at}'
