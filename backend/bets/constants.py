ONE_MILLION_DOLLAR = {'decimal_places': 2, 'max_digits': 9}
ODDS = {'decimal_places': 2, 'max_digits': 5}

WON = 'won'
LOST = 'lost'

SIMILAR_BETS_COUNT = 3


class SIDE:
    YES = 'yes'
    NO = 'no'
    ALL = YES, NO

    @classmethod
    def get_opposite(cls, side: str) -> str:
        if side == cls.YES:
            return cls.NO
        else:
            return cls.YES


class STATUS:
    PENDING = 'PENDING'
    # by admin
    ACTIVE = 'ACTIVE'
    ENDED = 'ENDED'
    ALL = {PENDING, ACTIVE, ENDED}
    # statuses that locks balance
    LOCKING = {PENDING, ACTIVE}
    VISIBLE_TO_EVERYONE = {ACTIVE}
    VISIBLE_TO_AUTHENTICATED = ALL

    # CURRENT STATUS: STATUS WE WANT CHANGE TO
    ADMIN_TRANSITION_MAP = {
        # admin decides to accept/publish (ACTIVE)
        PENDING: {ACTIVE},
        ACTIVE: {PENDING},
    }
