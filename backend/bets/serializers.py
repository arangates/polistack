import datetime
import urllib
import xml.etree.cElementTree as et

from django.conf import settings
import pytz
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from bets.constants import ONE_MILLION_DOLLAR
from bets.models import Bet, Category, Transaction
from orderbooks.constants import ORDER_BOOK_ENTRY_STATUS, ORDER_SIDE
from orderbooks.models import OrderBook


class ImageField(serializers.ImageField):
    def to_representation(self, *args, **kwargs):
        representation = super().to_representation(*args, **kwargs)
        parsed_representation = urllib.parse.urlparse(representation)
        updated_parsed_representation = urllib.parse.ParseResult(
            scheme=parsed_representation.scheme,
            netloc=settings.DOMAIN_NAME,
            path=parsed_representation.path,
            params=parsed_representation.params,
            query=parsed_representation.query,
            fragment=parsed_representation.fragment
        )
        updated_representation = urllib.parse.urlunparse(updated_parsed_representation)
        return updated_representation


class BetSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(write_only=True, **ONE_MILLION_DOLLAR)
    volume = serializers.SerializerMethodField()
    odds = serializers.SerializerMethodField()
    highest_bid = serializers.SerializerMethodField()
    lowest_ask = serializers.SerializerMethodField()
    image = ImageField()

    class Meta:
        model = Bet
        fields = (
            'id',
            'odds',
            'statement',
            'ends',
            'created_by',
            'image',
            'amount',
            'status',
            'volume',
            'highest_bid',
            'lowest_ask',
            'order_book',
        )
        read_only_fields = ('created_by',)

    def __init__(self, *args, **kwargs):
        # http://www.django-rest-framework.org/api-guide/serializers/#dynamically-modifying-fields
        fields = kwargs.pop('fields', None)
        super().__init__(*args, **kwargs)
        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            # This means that fields cannot superset self.fields
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)

    def get_odds(self, bet):
        return bet.order_book.latest_traded_price

    def get_volume(self, bet):
        return bet.order_book.volume

    def get_highest_bid(self, bet):
        return bet.order_book.highest_bid

    def get_lowest_ask(self, bet):
        return bet.order_book.lowest_ask

    def create(self, validated_data: dict):
        instance = super().create(validated_data)
        OrderBook.objects.create(bet=instance)
        return instance


class BetDetailSerializer(BetSerializer):
    latest_yes = serializers.SerializerMethodField()
    latest_no = serializers.SerializerMethodField()
    delta_yes = serializers.SerializerMethodField()
    delta_no = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        time = datetime.datetime.now(
            pytz.timezone('Asia/Kolkata')
        ).replace(
            hour=0, minute=0, second=0, microsecond=0
        )
        latest_trade = self.instance.order_book.latest_trade
        previous_day_trade = self.instance.order_book.entries.filter(
            created_at__lt=time,
            status=ORDER_BOOK_ENTRY_STATUS.PROCESSED
        ).order_by(
            'id'
        ).last()
        self._latest_yes = self.get_yes_price(latest_trade)
        self._latest_no = self.get_no_price(latest_trade)
        previous_day_yes = self.get_yes_price(previous_day_trade)
        previous_day_no = self.get_no_price(previous_day_trade)
        self._delta_yes = self._latest_yes - previous_day_yes
        self._delta_no = self._latest_no - previous_day_no

    def get_yes_price(self, trade):
        if trade is not None:
            if trade.order.side == ORDER_SIDE.BID:
                return trade.price
            else:
                return 100 - trade.price
        else:
            return 0

    def get_no_price(self, trade):
        if trade is not None:
            if trade.order.side == ORDER_SIDE.ASK:
                return trade.price
            else:
                return 100 - trade.price
        else:
            return 100

    def get_latest_yes(self, obj):
        return self._latest_yes

    def get_latest_no(self, obj):
        return self._latest_no

    def get_delta_yes(self, obj):
        return self._delta_yes

    def get_delta_no(self, obj):
        return self._delta_no

    class Meta:
        model = Bet
        fields = (
            'id',
            'odds',
            'statement',
            'ends',
            'created_by',
            'image',
            'amount',
            'status',
            'volume',
            'highest_bid',
            'lowest_ask',
            'order_book',
            'description',
            'latest_yes',
            'latest_no',
            'delta_yes',
            'delta_no',
        )
        read_only_fields = ('created_by', 'latest_yes', 'latest_no', 'delta_yes', 'delta_no')


class TransactionSerializer(serializers.ModelSerializer):
    bet_statement = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = (
            'id',
            'orig',
            'fee',
            'amount',
            'bet_statement',
            'type',
        )

    def get_type(self, transaction):
        if transaction.bet is None:
            if transaction.amount < 0:
                transaction_type = 'Withdraw'
            else:
                transaction_type = 'Deposit'
        else:
            if transaction.amount < 0:
                transaction_type = 'Loss'
            else:
                transaction_type = 'Profit'

        return transaction_type

    def get_bet_statement(self, transaction):
        if transaction.bet is None:
            return ""
        else:
            return transaction.bet.statement


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'name',
            'icon',
        )

    def validate_icon(self, value):
        """
        Validates if icon is valid svg graphic.
        https://gist.github.com/ambivalentno/9bc42b9a417677d96a21
        """
        try:
            svg_tag = et.fromstring(value)[0].tag
        except et.ParseError as e:
            raise ValidationError('This is not valid svg image.')
        if not svg_tag == '{http://www.w3.org/2000/svg}path':
            raise ValidationError('This is not valid svg image.')
        return value
