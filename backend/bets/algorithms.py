# from collections import defaultdict
# from copy import deepcopy
# from decimal import Decimal
# from functools import lru_cache
# import itertools

# from bets.types import BetActions, GroupedBetActions
# from orderbooks.constants import ORDER_SIDE
# from utils import log


# def find_gains(bids, asks, ratio: Decimal, winning_side):
#     # or losses
#     bid_matched = matched(bids, sum(order.amount for order in asks), ratio)
#     ask_matched = matched(asks, sum(order.amount for order in bids), reciprocal(ratio))
#     if ORDER_SIDE.BID == winning_side:
#         bid_multiplier = reciprocal(ratio)
#         ask_multiplier = -1
#     else:
#         bid_multiplier = -1
#         ask_multiplier = ratio

#     bid_gains = {
#         user: (sum_played, sum_played * bid_multiplier, sum_played * bid_multiplier + sum_played)
#         for user, sum_played
#         in sum_actions_by_user(bid_matched).items()
#     }
#     ask_gains = {
#         user: (sum_played, sum_played * ask_multiplier, sum_played * ask_multiplier + sum_played)
#         for user, sum_played
#         in sum_actions_by_user(ask_matched).items()
#     }

#     return list(itertools.chain(bid_gains.items(), ask_gains.items()))


# def sum_actions_by_user(orders):
#     c = defaultdict(Decimal)
#     for order in orders:
#         c[order.owner] += order.amount
#     return c


# def logic_error(*args, **kwargs):
#     raise RuntimeError('save on matched BetAction attempted, logic error. ')


# def matched(actions: BetActions, opposite_actions_sum: int, ratio: Decimal) -> BetActions:
#     """
#     Given a list of time sorted actions (oldest first), decide how many of them match the opposite
#     side. Produce a "split" action as a remainder.
#     """
#     ret = []
#     bag = 0
#     # when yes entered with 10, no with 5, on 2:1 ratio, the threshold is 5 * 2 as it applies to yes
#     threshold = opposite_actions_sum * ratio
#     for action in actions:
#         # >= on fully matched sides would create a remainder action which
#         # - doesn't matter, these actions won't be processed further, saved or whatever
#         # - is confusing and not needed
#         if bag + action.amount > threshold:
#             log.debug(
#                 'creating remainder because %r > %r, so create %r',
#                 bag + action.amount,
#                 threshold,
#                 threshold - bag,
#             )
#             remaining_bet_action = deepcopy(action)
#             remaining_bet_action.save = logic_error
#             remaining_bet_action.amount = threshold - bag
#             ret.append(remaining_bet_action)
#             break
#         bag += action.amount
#         ret.append(action)
#     return ret


# @lru_cache()
# def reciprocal(n: Decimal) -> Decimal:
#     # >>> (reciprocal(3/4) == 4/3) == True
#     #
#     # inverse of a fraction / odwrotność ułamka
#     # given a float, find what numbers were used to create a float and divide them reversed
#     #
#     #
#     #  /ɹɪˈsɪpɹək(ə)l/ ;)
#     #
#     # https://en.wikipedia.org/wiki/Multiplicative_inverse
#     # https://stackoverflow.com/questions/40180963/how-to-take-reciprocal-of-a-float-in-python
#     return Decimal('1.0') / n


# def find_distances(left, rght):
#     # NOT USED, can't remember why I needed that
#     #
#     # This problem conceptually is dividing a line into segments using two divisions
#     # In Polish: "stworzenie jednego wspólnego podziału odcinka z danych dwóch podziałów."
#     # find the division points (accumulate), remove duplicates, order
#     #
#     # also check pairwise https://docs.python.org/3/library/itertools.html#itertools-recipes
#     # but seems it's two times slower
#     # division_points = [1, 2, 3, 4, 5, 6]
#     # timeit [a - b for a, b in zip(division_points, [5] + division_points)]
#     # 1.24 µs ± 5.01 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
#     #
#     # timeit [a - b for a, b in pairwise(division_points)]
#     # 3.03 µs ± 15.9 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)
#     division_points = sorted(set(sorted(itertools.accumulate(left)) + sorted(itertools.accumulate(rght))))
#     # find distances between division points
#     return [a - b for a, b in zip(division_points, [0] + division_points)]
