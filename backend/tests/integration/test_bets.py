from decimal import Decimal

from django.core.exceptions import ValidationError
import pytest
from rest_framework import status
from rest_framework.reverse import reverse

from bets.constants import STATUS
from bets.factories import BetFactory
from bets.factories_data import ICONS
from bets.models import Bet
from orderbooks.constants import ORDER_SIDE
from orderbooks.factories import OrderFactory
from tests.integration.data import sample_bet
from users.factories import UserFactory


@pytest.mark.django_db
def test_user_participation_on_bet_creation(api_client, users):
    user, auth_headers = users.get_user()

    create_bet = api_client.post(
        # THINK why is this called list
        reverse('bets-list'),
        data=sample_bet,
        **auth_headers
    )
    assert create_bet.status_code == status.HTTP_201_CREATED

    assert Bet.objects.filter(users=user).exists()


@pytest.mark.django_db
def test_svg_image_validation(api_client, users):
    user, auth_headers = users.get_user(is_staff=True)

    # Good one
    response = api_client.post(
        reverse('categories-list'),
        data={'name': 'Some random name', 'icon': ICONS[0]},
        **auth_headers
    )
    assert response.status_code == status.HTTP_201_CREATED

    # Bad one
    response = api_client.post(
        reverse('categories-list'),
        data={'name': 'Some random name', 'icon': 'Lubie placki'},
        **auth_headers
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST


# @pytest.mark.django_db
# class TestPotFilling:

#     def get_back_out_amount_for_side(
#             self,
#             bet_actions: Dict[str, List[BA]],
#             yes: int,
#             no: int,
#             side: str
#     ) -> float:
#         """
#         Returns total amount that side can back out.
#         Negative amount means that side can't back out anything.
#         """
#         yes_bets = [bet_action.amount for bet_action in bet_actions[SIDE.YES]]
#         no_bets = [bet_action.amount for bet_action in bet_actions[SIDE.NO]]
#         return (
#            sum(yes_bets) - sum(no_bets) * yes / no if side == SIDE.YES else sum(no_bets) - sum(yes_bets) * no / yes
#         )
#     def get_pull_amount_for_bet_action(
#             self,
#             bet_actions: Dict[str, List[BA]],
#             yes: int,
#             no: int,
#             side: str,
#             bet_action_index: int,
#     ) -> float:
#         """
#         Returns amount that can be pulled out for a specific bet_action.
#         Negative amount means that side can't back out anything.
#         """
#         from_right_sum = sum(bet_action.amount for bet_action in bet_actions[side][bet_action_index + 1:])
#         can_back_out = self.get_back_out_amount_for_side(bet_actions, yes, no, side) - from_right_sum
#         return can_back_out

#     def test_back_out_amount_for_side(self):
#         bet_actions = {
#             SIDE.YES: [BA(0, 2), BA(1, 6), BA(2, 5), BA(3, 5)],
#             SIDE.NO: [BA(4, 2), BA(5, 2)]
#         }
#         yes = 1
#         no = 4

#         assert self.get_back_out_amount_for_side(bet_actions, yes, no, SIDE.YES) == 17
#         assert self.get_back_out_amount_for_side(bet_actions, yes, no, SIDE.NO) == -68

#     def test_back_out_for_bet_action_on_lists(self):
#         bet_actions = {
#             SIDE.YES: [BA(0, 1), BA(1, 2), BA(2, 4), BA(3, 3)],
#             SIDE.NO: [BA(4, 2), BA(5, 2), BA(6, 1)]
#         }
#         yes = 3
#         no = 4

#         assert self.get_pull_amount_for_bet_action(bet_actions, yes, no, SIDE.YES, 2) == 3.25
#         assert self.get_pull_amount_for_bet_action(bet_actions, yes, no, SIDE.YES, 1) <= 0
#         assert self.get_pull_amount_for_bet_action(bet_actions, yes, no, SIDE.YES, 3) == 6.25

#     def test_back_out_amount_for_bet_action(self, users, api_client):
#         user, auth_headers = users.get_user()
#         bet = BetFactory(yes=5, no=4)

#         yes_bet_actions = [
#             BetActionFactory(
#                 bet=bet,
#                 user=user,
#                 amount=amount,
#                 side=bets_constants.SIDE.YES,
#                 created_at=timezone.now() + timedelta(seconds=delay)
#             )
#             for delay, amount in enumerate([2, 3, 6, 6, 1, 7])
#         ]
#         for amount in [4, 1, 1, 2]:
#             BetActionFactory(bet=bet, user=user, amount=amount, side=bets_constants.SIDE.NO)

#         for expected_result, bet_action in zip([0, 0, 1, 6, 1, 7], yes_bet_actions):
#             response = api_client.get(
#                 reverse('bet-actions-back-out-amount', kwargs={'pk': bet_action.id}),
#                 **auth_headers
#             )
#             assert expected_result == response.data['back_out_amount']

#     def test_back_out_for_bet_action(self, users, api_client):
#         user, auth_headers = users.get_user()
#         bet = BetFactory(yes=4, no=3)

#         yes_bet_actions = [
#             BetActionFactory(
#                 bet=bet,
#                 user=user,
#                 amount=amount,
#                 side=bets_constants.SIDE.YES,
#                 created_at=timezone.now() + timedelta(seconds=delay)
#             )
#             for delay, amount in enumerate((5, 2, 6, 6, 1, 7))
#         ]
#         paired_partially = yes_bet_actions[-4]

#         for amount in [4, 1, 1, 2]:
#             BetActionFactory(bet=bet, user=user, amount=amount, side=bets_constants.SIDE.NO)

#         # Check if user cannot extend their bet
#         response = api_client.patch(
#             reverse('bet-actions-back-out', kwargs={'pk': paired_partially.id}),
#             data={'amount': 10000000},
#             **auth_headers
#         )
#         assert response.status_code == status.HTTP_400_BAD_REQUEST

#         # Check if user can back_out to given amount
#         response = api_client.patch(
#             reverse('bet-actions-back-out', kwargs={'pk': paired_partially.id}),
#             data={'amount': 5},
#             **auth_headers
#         )
#         assert response.status_code == status.HTTP_200_OK

#         response = api_client.patch(
#             reverse('bet-actions-back-out', kwargs={'pk': paired_partially.id}),
#             data={'amount': 1},
#             **auth_headers
#         )
#         assert response.status_code == status.HTTP_400_BAD_REQUEST

#     def test_you_cant_bet_on_non_active_bet(self, users, api_client):
#         user, auth_headers = users.get_user()
#         assert user.get_available_balance() == Decimal('1000')

#         bet = BetFactory(status=STATUS.ENDED)
#         bet_action_data = {
#             'side': bets_constants.SIDE.YES,
#             'bet': bet.id,
#             'amount': 500
#         }
#         # paranoid, check initial sate
#         assert BetAction.objects.count() == 0
#         assert user.get_available_balance() == Decimal('1000')
#         betting = api_client.post(
#             reverse('bet-actions-list'),
#             data=bet_action_data,
#             **auth_headers
#         )
#         # no charge, no bet action
#         assert BetAction.objects.count() == 0
#         assert user.get_available_balance() == Decimal('1000')
#         assert betting.status_code != status.HTTP_201_CREATED

#     def test_bet_action_validation(self, users, api_client):
#         user, auth_headers = users.get_user()
#         bet = BetFactory(status=STATUS.ACTIVE)
#         bet_action_data = {
#             'side': bets_constants.SIDE.YES,
#             'bet': bet.id,
#             'amount': 1000,
#         }
#         TransactionFactory(user=user, amount=2000, orig=2000, bet=None, fee=0)
#         response = api_client.post(
#             reverse('bet-actions-list'),
#             data=bet_action_data,
#             **auth_headers
#         )
#         assert response.status_code == status.HTTP_201_CREATED

#         # What if user have no enough bucks?
#         bet_action_data['amount'] = 2001
#         response = api_client.post(
#             reverse('bet-actions-list'),
#             data=bet_action_data,
#             **auth_headers
#         )
#         assert response.status_code == status.HTTP_400_BAD_REQUEST

#     def test_bet_action_back_out_amount(self, users):
#         user, auth_headers = users.get_user()
#         bet = BetFactory(yes=1, no=1)
#         yes_bet_actions = [
#             BetActionFactory(user=user, bet=bet, amount=amount, side=SIDE.YES)
#             for amount in [10, 20, 40, 30]
#         ]
#         no_bet_actions = [BetActionFactory(user=user, bet=bet, amount=amount, side=SIDE.NO) for amount in [10, 30]]

#         assert [ba.get_back_out_amount() for ba in yes_bet_actions] == [
#             Decimal('0'), Decimal('0'), Decimal('30'), Decimal('30')]
#         assert [ba.get_back_out_amount() for ba in no_bet_actions] == [Decimal('0')] * 2


@pytest.mark.django_db
def test_number_of_resolve_queries(api_client, users):
    users = [users.get_user() for _ in range(5)]
    bet = BetFactory(status=STATUS.ACTIVE)
    users_wants_to_resolve = 3

    for _user, auth_headers in users:
        for _ in range(users_wants_to_resolve):
            OrderFactory(owner=_user, bet=bet, price=10, side=ORDER_SIDE.BID)

    for user, auth_headers in users[:users_wants_to_resolve]:
        response = api_client.get(
            reverse('bets-please-resolve', kwargs={'pk': bet.id}),
            **auth_headers
        )
        assert response.status_code == status.HTTP_200_OK
    bet.refresh_from_db()
    assert users_wants_to_resolve == bet.resolve_query == 3


def test_user_cant_bet_on_insufficent_funds(db):
    """
    Protection on database level
    """
    bet = BetFactory()
    user = UserFactory()
    with pytest.raises(ValidationError):
        # THINK with precision 4 Decimal('0.01') fails
        OrderFactory(bet=bet, owner=user, price=(user.get_available_balance() + Decimal('0.02')), shares=2)
