from decimal import Decimal as D

from bets.algorithms import find_distances, find_gains, matched, reciprocal
from bets.constants import SIDE
from bets.types import A


def test_find_distances():
    assert find_distances([D(3), D(7)], [D(5), D(4), D(1)]) == [D(3), D(2), D(4), D(1)]
    assert find_distances([D(3), D(7)], [D(3), D(6), D(1)]) == [D(3), D(6), D(1)]
    assert find_distances([D(10)], [D(5), D(5)]) == [D(5), D(5)]
    assert find_distances([D(1), D(2), D(1)], [D(2), D(2)]) == [D(1), D(1), D(1), D(1)]


def test_reciprocal():
    assert reciprocal(D(3) / D(4)) == D(4) / D(3)
    assert reciprocal(D(1) / D(3)) == D(3) / D(1)
    assert reciprocal(D(663) / D(415)) == D(415) / D(663)


def test_matched():
    # nothing aggregates
    yes = A('A', D(2)), A('B', D(1)), A('A', D(3)), A('C', D(2)), A('A', D(2))
    noo = A('D', D(4)), A('E', D(4)), A('X', D(1)), A('D', D(1))
    yes_sum = sum(ba.amount for ba in yes)
    noo_sum = sum(ba.amount for ba in yes)
    assert matched(yes, noo_sum, reciprocal(D(1) / D(2))) == yes == matched(yes, noo_sum, D(2) / D(1))
    assert matched(noo, yes_sum, D(1) / D(2)) == (A('D', D(4)), A('E', D(1.0))) == \
        matched(noo, yes_sum, reciprocal(D(2) / D(1)))

    # well, silly but necessary
    assert matched(yes, noo_sum, reciprocal(D(1) / D(1))) == yes == matched(yes, noo_sum, D(1) / D(1))
    assert matched(noo, yes_sum, D(1) / D(1)) == noo == matched(noo, yes_sum, reciprocal(D(1) / D(1)))
    # TODO: add more cases


def test_combine_dicts():
    pass


def test_participation():
    pass


def test_find_gains():
    """
    Two users add money on both sides of the bet in a complex manner.
    - the left side of the pot is distributed to right users in right amounts
    - the remainders are returned to users balances when the bet is finished
    - the balances are locked for the duration of the bet
    - the balances are deducted and increased after the bet is finished
    """
    # three players participate YES, A several times
    yes = A('A', D(2.0)), A('B', D(1.0)), A('A', D(3.0)), A('C', D(2.0)), A('A', D(2.0))
    # yes_aggregated = A('A', D(7.0)), A('B', D(1.0)), A('C', D(2.0))
    # two players bet NO
    no = A('D', D(4.0)), A('E', D(5.0)), A('D', D(1.0))
    # no_aggregated = ('D', D(4.0)), ('E', D(5.0)), ('D', D(1.0))

    # let's play
    expected = [
        # who participated
        # {'A', 'B', 'C', 'D', 'E'},
        # gains or losses, without initial investment
        ('A', D(7.0)), ('B', D(1.0)), ('C', D(2.0)), ('D', D(-5.0)), ('E', D(-5.0)),
        # transactions, what will be added/subtracted from user account
        # {'A': D(14.0), 'B': D(2.0), 'C': D(4.0), 'D': D(0), 'E': D(0)},
        # participation history
        # {'A': (D(2.0), D(3.0), D(2.0)), 'B': (D(1.0),), 'C': (D(2.0),), 'D': (D(4.0), D(1.0),), 'E': (D(5.0),)},
        # TODO: later, I'm interested in that but it seems hard
        # who pays whom, detailed, in time order
        # (('D', 'A', D(2)), ('D', 'B', D(1)), ('D', 'A', D(2)),
        #       ('E', 'A', D(2)), ('E', 'C', D(2)), ('E', 'A', D(1)), ('D', 'A', D(1))),
        # who pays whom, aggregated, no time order
        # (('D', 'A', D(4)), ('D', 'B', D(1)), ('E', 'A', D(3)), ('E', 'C', D(2))),
    ]
    assert find_gains(yes, no, D(1) / D(1), SIDE.YES) == expected

    # Now, the other side wins
    expected = [
        # {'A', 'B', 'C', 'D', 'E'},
        # only sign changes
        ('A', D(-7.0)), ('B', D(-1.0)), ('C', D(-2.0)), ('D', D(5.0)), ('E', D(5.0)),
        # zeroes go to the other side
        # {'A': D(0), 'B': D(0), 'C': D(0), 'D': D(10.0), 'E': D(10.0)},
        # stays the same, always
        # {'A': (D(2.0), D(3.0), D(2.0)), 'B': (D(1.0),), 'C': (D(2.0),), 'D': (D(4.0), D(1.0),), 'E': (D(5.0),)},
    ]
    assert find_gains(yes, no, D(1) / D(1), SIDE.NO) == expected

    # let's do the same on a different, simple multiplier
    expected = [
        # {'A', 'B', 'C', 'D', 'E'},
        ('A', D(3.5)), ('B', D(0.5)), ('C', D(1.0)), ('D', D(-4.0)), ('E', D(-1.0)),
        # {'A': D(10.5), 'B': D(1.5), 'C': D(3), 'D': D(0), 'E': D(0)},
        # {'A': (D(2.0), D(3.0), D(2.0)), 'B': (D(1.0),), 'C': (D(2.0),), 'D': (D(4.0), D(1.0),), 'E': (D(5.0),)},
    ]
    assert find_gains(yes, no, D(1) / D(2), SIDE.YES) == expected

    # the other side wins
    expected = [
        # {'A', 'B', 'C', 'D', 'E'},
        ('A', D(-7.0)), ('B', D(-1.0)), ('C', D(-2.0)), ('D', D(8.0)), ('E', D(2.0)),
        # {'A': D(0), 'B': D(0), 'C': D(0), 'D': D(12.0), 'E': D(3.0)},
        # {'A': (D(2.0), D(3.0), D(2.0)), 'B': (D(1.0),), 'C': (D(2.0),), 'D': (D(4.0), D(1.0),), 'E': (D(5.0),)},
    ]
    assert find_gains(yes, no, D(1) / D(2), SIDE.NO) == expected
