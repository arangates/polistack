"""
Validation of models switching from status x to y.
"""

from django.db import IntegrityError
from django.urls import reverse
import pytest
from rest_framework import status

from bets.admin import BetAdminModelForm
from bets.constants import STATUS
from bets.factories import BetFactory
from bets.models import Bet
from bets.serializers import BetSerializer
from bets.validation import illegal_status_transition, missing_image
from tests.integration.data import sample_bet


def test_178_transition_not_allowed(db, api_client, users):
    _, auth_headers = users.get_user()

    create_bet = api_client.post(
        reverse('bets-list'),
        data=sample_bet,
        **auth_headers
    )
    assert create_bet.status_code == status.HTTP_201_CREATED
    assert create_bet.data['status'] == STATUS.PENDING

    new_bet_id = create_bet.data['id']
    # illegal for both users and admins
    wrong_status = {'status': STATUS.ENDED}
    transition_not_allowed = illegal_status_transition.format(old=STATUS.PENDING, new=wrong_status['status'])
    update_bet = api_client.patch(
        reverse('bets-detail', kwargs={'pk': new_bet_id}),
        data=wrong_status,
        **auth_headers
    )
    assert update_bet.status_code == status.HTTP_400_BAD_REQUEST
    assert transition_not_allowed in update_bet.data['status']
    # same on serializer, maybe paranoid
    bet = Bet.objects.get(pk=new_bet_id)
    serializer = BetSerializer(instance=bet, data=wrong_status, partial=True)
    assert not serializer.is_valid()
    assert transition_not_allowed in serializer.errors['status']

    # in admin, we need to extend ModelForm with fields that ModelAdmin populates
    # note that ADMIN_TRANSITION_MAP is used
    class TestBetAdminModelForm(BetAdminModelForm):
        class Meta:
            model = Bet
            fields = ('status',)
    admin_form = TestBetAdminModelForm(instance=bet, data={'status': STATUS.ENDED})
    assert not admin_form.is_valid()
    assert transition_not_allowed in admin_form.errors['status']


def test_183_activate_bet_only_if_other_fields_are_filled(db):
    """
    * Statement
    * Odds
    * Category
    * Photo
    """
    # most are guarded by the database
    #
    for kwarg in 'statement', :
        with pytest.raises(IntegrityError):
            BetFactory(status=STATUS.PENDING, **{kwarg: None})

    # categories are made required by the default value of Field.blank = False

    bet = BetFactory(status=STATUS.PENDING, image=None)

    class TestBetAdminModelForm(BetAdminModelForm):
        class Meta:
            model = Bet
            fields = 'status', 'image',
    admin_form = TestBetAdminModelForm(instance=bet, data={'status': STATUS.ACTIVE})
    assert not admin_form.is_valid()
    assert missing_image.format(old=STATUS.PENDING) in admin_form.errors['__all__']
