from django.shortcuts import resolve_url
from django.utils.translation import ugettext
from freezegun import freeze_time

from users.factories import UserFactory


@freeze_time("12:00")
def test_token_traffic(client, db):
    """
    Follow user create and refresh token flow.
    Expect token to change after expiring
    """
    user = UserFactory(password='pass')
    token = client.post(resolve_url('jwt-create'), data={'email': user.email, 'password': 'pass'}).data
    non_expired_token = client.post(resolve_url('jwt-refresh'), data=token).data
    assert token == non_expired_token
    with freeze_time('12:20'):
        expired_token = client.post(resolve_url('jwt-refresh'), data=token).data
        assert token != expired_token


def test_ignore_HTTP_ACCEPT_LANGUAGE(client, db, settings):
    """
    For the moment, the app is english only so the errors are too.
    """
    # Desired settings
    assert 'django.middleware.locale.LocaleMiddleware' not in settings.MIDDLEWARE
    assert [('en', ugettext('English'),)] == settings.LANGUAGES
    user = UserFactory(password='pass')
    assert client.post(
        resolve_url('jwt-create'),
        data={'email': user.email, 'password': 'wrong'},
        HTTP_ACCEPT_LANGUAGE='pl'
    ).data == {'non_field_errors': ['Unable to log in with provided credentials.']}


def test_login_attempts(client, db):
    """
    Test if proper login info was created and located correctly.
    """
    user = UserFactory(password='pass')
    client.post(
        resolve_url('jwt-create'), data={'email': user.email, 'password': 'pass'},
        REMOTE_ADDR='89.65.75.76'
    )
    last_login_attempt = user.login_attempts.last()
    assert last_login_attempt.ip == '89.65.75.76'
    assert last_login_attempt.country == 'Poland'
    assert last_login_attempt.city == 'Warsaw'
    assert last_login_attempt.region == 'MZ'
