# from decimal import Decimal

# import pytest

# from bets.algorithms import logic_error, matched, reciprocal, sum_actions_by_user
# from bets.constants import SIDE
# from bets.factories import BetActionFactory, BetFactory
# from bets.models import Bet, BetAction
# from users.factories import UserFactory


# def assert_equal_amounts(yes, no):
#     assert [ba.amount.quantize(Decimal('.01')) for ba in yes] == [ba.amount.quantize(Decimal('.01')) for ba in no]


# def test_matched(db):
#     user1, user2 = UserFactory.create_batch(2)
#     batch1 = BetActionFactory.build_batch(3, user=user1, amount=Decimal('10'))
#     batch2 = BetActionFactory.build_batch(3, user=user2, amount=Decimal('7'))

#     # on 2:1, expect to match 15, last '7' is cut to 1
#     assert_equal_amounts(matched(batch2, 30, Decimal('0.5')), [
#         # 15 together
#         batch2[0],
#         batch2[1],
#         BetAction(amount=Decimal('1.0')),
#     ])
#     # on 1:1, expect to match 21, on both sides
#     assert_equal_amounts(matched(batch2, 30, Decimal('1')), batch2)
#     assert_equal_amounts(matched(batch1, 21, Decimal('1')), [
#         # 15 together
#         batch1[0],
#         batch1[1],
#         BetAction(amount=Decimal('1.0')),
#     ])


# def test_matched_prevents_save_on_remaining_action(db):
#     original_batch = BetActionFactory.create_batch(2, amount=Decimal('10'))
#     matched_batch = matched(original_batch, Decimal('15'), Decimal('1'))
#     assert_equal_amounts(matched_batch, [
#         # 15 together
#         original_batch[0],
#         BetAction(amount=Decimal('5.0')),
#     ])
#     # in match I disable the save method as a hack
#     assert matched_batch[0].save is not logic_error
#     assert matched_batch[1].save is logic_error
#     # that's a remainder
#     with pytest.raises(RuntimeError):
#         matched_batch[1].save()
#     # saves normally
#     matched_batch[0].save()

#     # now, let's get to the question of whether matched should > or >=
#     # there's no functional difference, but in case of >= we will create the remainder
#     # which gives us nothing
#     matched_batch = matched(original_batch, Decimal('20'), Decimal('1'))
#     assert_equal_amounts(matched_batch, [
#         # 20 together
#         original_batch[0],
#         BetAction(amount=Decimal('10.0')),
#     ])
#     matched_batch[1].save()
#     # now, let's match one cent less
#     matched_batch = matched(original_batch, Decimal('19.99'), Decimal('1'))
#     assert_equal_amounts(matched_batch, [
#         # 20 together
#         original_batch[0],
#         BetAction(amount=Decimal('9.99')),
#     ])
#     with pytest.raises(RuntimeError):
#         matched_batch[1].save()


# def test_sum_actions_by_user(db):
#     user1, user2 = UserFactory.create_batch(2)
#     batch1 = BetActionFactory.build_batch(3, user=user1, amount=10)
#     batch2 = BetActionFactory.build_batch(3, user=user2, amount=7)
#     assert sum_actions_by_user(batch1 + batch2) == {
#         user1: 30,
#         user2: 21
#     }


# @pytest.mark.django_db
# def test_RB_235():
#     """
#     a 1:1 bet with two opposing equal bet actions of 11 caused algo error:

#         Exception at /admin/bets/bet/28/change/
#             algo error
#     """
#     self_yes = self_no = Decimal('1')
#     yes = no = Decimal('11')
#     if yes * self_no / self_yes > no:
#         return (no * self_yes / self_no + no).quantize(Decimal('.01'))
#     if no * self_yes / self_no > yes:
#         return (yes * self_no / self_yes + yes).quantize(Decimal('.01'))
#     # with my rpevious erroneous understanding, passing the two aobve conditions
#     # was an undefined behaviour, wrong, this just means equal
#     # let's create a proper test
#     b = BetFactory(yes=Decimal('1'), no=Decimal('1'))
#     ba1 = BetActionFactory(amount=Decimal('11'), side=SIDE.YES, bet=b)
#     BetActionFactory(amount=Decimal('11'), side=SIDE.NO, user=ba1.user, bet=b)
#     assert b.whole_bet_paired_amount() == Decimal(0)

#     # does the same happen when one side is multiplied by a factor
#     Bet.objects.all().delete()
#     BetAction.objects.all().delete()
#     factor = Decimal('1.79').quantize(Decimal('.01'))
#     b = BetFactory(yes=Decimal('1') * (factor), no=Decimal('1') * reciprocal(factor))
#     ba1 = BetActionFactory(amount=Decimal('11') * factor, side=SIDE.YES, bet=b)
#     BetActionFactory(amount=Decimal('11') * reciprocal(factor), side=SIDE.NO, user=ba1.user, bet=b)
#     assert b.whole_bet_paired_amount() == Decimal('0')

#     # weirdly, Decimal('1.79865') will cause:
#     # 6.138 != 6.11, therefore assert Decimal('25.91') == Decimal('0')
