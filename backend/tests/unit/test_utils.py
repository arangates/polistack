from bets.utils import combine_dicts


def test_combine_dicts():
    assert combine_dicts({1: 2, 3: 4}, {1: 5, 6: 7}) == {
        1: 7,
        3: 4,
        6: 7,
    }
