from typing import Dict

from django.contrib.auth import get_user_model
import pytest
from rest_framework.test import APIClient

from users.factories import UserFactory

User = get_user_model()


class UserFixture:
    def get_user(self, **user_kwargs) -> (User, Dict[str, str]):
        """
        Creates user and returns him along with authorization headers.
        """
        user = UserFactory(**user_kwargs)
        auth_headers = {'HTTP_AUTHORIZATION': f'JWT {user.get_jwt_token()}'}
        return user, auth_headers


@pytest.fixture()
def users():
    return UserFixture()


@pytest.fixture()
def api_client():
    return APIClient()
