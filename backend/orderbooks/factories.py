import factory.fuzzy

from bets.factories import BetFactory
from orderbooks.constants import ORDER_BOOK_ENTRY_STATUS, ORDER_SIDE
from users.factories import UserFactory


class OrderFactory(factory.django.DjangoModelFactory):
    bet = factory.SubFactory(BetFactory)
    side = factory.fuzzy.FuzzyChoice(ORDER_SIDE.ALL)
    shares = factory.fuzzy.FuzzyInteger(1, 500)
    price = factory.fuzzy.FuzzyInteger(1, 99)
    owner = factory.SubFactory(UserFactory)

    class Meta:
        model = 'orderbooks.Order'


class OrderBookFactory(factory.django.DjangoModelFactory):
    bet = factory.SubFactory(BetFactory)

    class Meta:
        model = 'orderbooks.OrderBook'


class OrderBookEntryFactory(factory.django.DjangoModelFactory):
    order = factory.SubFactory(OrderFactory)
    order_book = factory.SubFactory(OrderBookFactory)
    status = factory.fuzzy.FuzzyChoice(ORDER_BOOK_ENTRY_STATUS.ALL)

    class Meta:
        model = 'orderbooks.OrderBookEntry'
