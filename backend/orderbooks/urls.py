from rest_framework import routers

from orderbooks.views import (
    AdminOrderBookViewSet,
    OrderBookEntryViewSet,
    OrderBookGraphViewSet,
    OrderBookViewSet,
    OrderViewSet,
)

router = routers.SimpleRouter()
router.register('orders', OrderViewSet, base_name='orders')
router.register('books', OrderBookViewSet, base_name='orderbooks')
router.register('books/graph', OrderBookGraphViewSet, base_name='orderbooks_graph')
router.register('books/admin', AdminOrderBookViewSet, base_name='orderbooks_admin')
router.register('entries', OrderBookEntryViewSet, base_name='entries')
urlpatterns = router.urls
