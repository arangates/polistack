from django.test import TestCase

from orderbooks.constants import ORDER_BOOK_ENTRY_STATUS, ORDER_SIDE
from orderbooks.factories import OrderBookEntryFactory, OrderBookFactory, OrderFactory


class OrderBookTestCase(TestCase):

    bids = {
        40: [5, 5, 5],
        55: [10, 40],
    }

    asks = {
        67: [30, 15, 45],
        68: [20, 10, 20],
        70: [10],
        98: [98, 1],
    }

    def get_order_book(self):
        #                  || 98: 99 (98, 1)
        #                  ||
        #                  ||
        #                  || 70: 10 (10)
        #                  || 68: 50 (20, 10, 20)
        #                  || 67: 90 (30, 15, 45)
        #                  ||
        # 55: 50 (10, 40)  ||
        # 40: 15 (5, 5, 5) ||

        # bids: 55, 60, 68
        # asks: 97, 60, 40

        ob = OrderBookFactory()

        # Add Bids
        for price, shares_list in self.bids.items():
            for shares in shares_list:
                self.create_order_book_entry(
                    ob,
                    order=OrderFactory(side=ORDER_SIDE.BID),
                    shares=shares,
                    price=price
                )

        # Add asks
        for price, shares_list in self.asks.items():
            for shares in shares_list:
                self.create_order_book_entry(
                    ob,
                    order=OrderFactory(side=ORDER_SIDE.ASK),
                    shares=shares,
                    price=price
                )

        return ob

    def create_order_book_entry(self, order_book, order, shares=1, price=10, status=ORDER_BOOK_ENTRY_STATUS.PENDING):
        entry = OrderBookEntryFactory(
            order=order,
            order_book=order_book,
            shares=shares,
            price=price,
            status=status
        )
        return entry

    def test_adding_bid_order_to_empty_orderbook(self):
        ob = OrderBookFactory()
        self.assertEqual(ob.highest_bid, 0)
        entry = self.create_order_book_entry(ob, OrderFactory(side=ORDER_SIDE.BID))
        self.assertEqual(ob.highest_bid, entry.price)
        self.assertEqual(ob.lowest_ask, 100)

    def test_adding_ask_order_to_empty_orderbook(self):
        ob = OrderBookFactory()
        self.assertEqual(ob.lowest_ask, 100)
        entry = self.create_order_book_entry(ob, OrderFactory(side=ORDER_SIDE.ASK))
        self.assertEqual(ob.lowest_ask, entry.price)
        self.assertEqual(ob.highest_bid, 0)

    def test_order_book_metrics(self):
        ob = self.get_order_book()
        self.assertEqual(ob.highest_bid, 55)
        self.assertEqual(ob.lowest_ask, 67)
        self.assertEqual(ob.volume, 314)

    def test_get_bids_and_asks_for_price(self):
        ob = self.get_order_book()
        empty_slot_ask_price = 99
        empty_slot_bid_price = 10
        lowest_ask_price = 40
        highest_bid_price = 98
        intermediate_ask_price = 45
        intermediate_bid_price = 68

        self.assertFalse(ob.get_bids_for_price(empty_slot_ask_price).exists())
        self.assertFalse(ob.get_asks_for_price(empty_slot_bid_price).exists())

        bids_for_lowest_ask = list(ob.get_bids_for_price(lowest_ask_price))
        self.assertEqual(len(bids_for_lowest_ask), 5)
        # bids are sorted in descending order of price (highest bid first)
        lowest_bid_encountered = ob.highest_bid
        for bid in bids_for_lowest_ask:
            self.assertTrue(lowest_bid_encountered >= bid.price)
            lowest_bid_encountered = bid.price

        asks_for_highest_bid = list(ob.get_asks_for_price(highest_bid_price))
        self.assertEqual(len(asks_for_highest_bid), 9)
        # asks are sorted in ascending order of price (lowest ask first)
        highest_ask_encountered = ob.lowest_ask
        for ask in asks_for_highest_bid:
            self.assertTrue(highest_ask_encountered <= ask.price)
            highest_ask_encountered = ask.price

        self.assertEqual(ob.get_bids_for_price(intermediate_ask_price).count(), 2)
        self.assertEqual(ob.get_asks_for_price(intermediate_bid_price).count(), 6)

    def test_processing_bids(self):
        ob = self.get_order_book()
        original_highest_bid = ob.highest_bid

        # lower than the lowest ask and lower than the highest bid
        order = OrderFactory(price=45, side=ORDER_SIDE.BID)
        entries = ob.process_order(order)
        self.assertEqual(len(entries), 1)
        entry = entries[0]
        self.assertEqual(entry.price, order.price)
        self.assertEqual(entry.shares, order.shares)
        self.assertEqual(ob.highest_bid, original_highest_bid)

        # lower than the lowest ask but higher than the highest bid
        order = OrderFactory(price=55, side=ORDER_SIDE.BID)
        entries = ob.process_order(order)
        self.assertEqual(len(entries), 1)
        entry = entries[0]
        self.assertEqual(entry.price, order.price)
        self.assertEqual(entry.shares, order.shares)
        self.assertEqual(ob.highest_bid, entry.price)

        # higher than the lowest ask
        # Trade:
        #  - 90 shares from 67 (3 trades)
        #  - 50 shares from 68 (3 trades)
        #  - a new bid for 10 shares at 68
        order = OrderFactory(price=68, shares=150, side=ORDER_SIDE.BID)
        asks = list(ob.get_asks_for_price(68))
        entries = ob.process_order(order)
        self.assertEqual(len(entries), 7)
        shares_left = order.shares
        for i, ask in enumerate(asks):
            ask.refresh_from_db()
            created_entry = entries[i]
            self.assertEqual(ORDER_BOOK_ENTRY_STATUS.PROCESSED, ask.status)
            self.assertEqual(ORDER_BOOK_ENTRY_STATUS.PROCESSED, created_entry.status)
            self.assertEqual(ask.traded_with, created_entry)
            self.assertEqual(ask.price, created_entry.price)
            self.assertEqual(ask.shares, created_entry.shares)
            shares_left -= created_entry.shares
        pending_new_entry = entries[i + 1]
        self.assertEqual(ORDER_BOOK_ENTRY_STATUS.PENDING, pending_new_entry.status)
        self.assertIsNone(pending_new_entry.traded_with)
        self.assertEqual(pending_new_entry.price, order.price)
        self.assertEqual(pending_new_entry.shares, shares_left)
        self.assertEqual(ob.highest_bid, pending_new_entry.price)

    def test_processing_asks(self):
        ob = self.get_order_book()
        original_lowest_ask = ob.lowest_ask

        # higher than the highest bid and higher than the lowest ask
        order = OrderFactory(price=97, side=ORDER_SIDE.ASK)
        entries = ob.process_order(order)
        self.assertEqual(len(entries), 1)
        entry = entries[0]
        self.assertEqual(entry.price, order.price)
        self.assertEqual(entry.shares, order.shares)
        self.assertEqual(ob.lowest_ask, original_lowest_ask)

        # higher than the highest bid but lower than the lowest ask
        order = OrderFactory(price=60, side=ORDER_SIDE.ASK)
        entries = ob.process_order(order)
        self.assertEqual(len(entries), 1)
        entry = entries[0]
        self.assertEqual(entry.price, order.price)
        self.assertEqual(entry.shares, order.shares)
        self.assertEqual(ob.lowest_ask, entry.price)

        # lower than the highest bid
        # Trade:
        #  - 50 shares from 55 (2 trades)
        #  - 15 shares from 40 (3 trades)
        #  - a new ask for 85 shares at 40
        order = OrderFactory(price=40, shares=150, side=ORDER_SIDE.ASK)
        bids = list(ob.get_bids_for_price(order.price))
        entries = ob.process_order(order)
        self.assertEqual(len(entries), 6)
        shares_left = order.shares
        for i, bid in enumerate(bids):
            bid.refresh_from_db()
            created_entry = entries[i]
            self.assertEqual(ORDER_BOOK_ENTRY_STATUS.PROCESSED, bid.status)
            self.assertEqual(ORDER_BOOK_ENTRY_STATUS.PROCESSED, created_entry.status)
            self.assertEqual(bid.traded_with, created_entry)
            self.assertEqual(bid.price, created_entry.price)
            self.assertEqual(bid.shares, created_entry.shares)
            shares_left -= created_entry.shares
        pending_new_entry = entries[i + 1]
        self.assertEqual(ORDER_BOOK_ENTRY_STATUS.PENDING, pending_new_entry.status)
        self.assertIsNone(pending_new_entry.traded_with)
        self.assertEqual(pending_new_entry.price, order.price)
        self.assertEqual(pending_new_entry.shares, shares_left)
        self.assertEqual(ob.lowest_ask, pending_new_entry.price)

    def test_other_side_order_splitting(self):
        ob = OrderBookFactory()
        bid_order = OrderFactory(price=60, shares=20, side=ORDER_SIDE.BID)
        ask_order = OrderFactory(price=60, shares=7, side=ORDER_SIDE.ASK)
        ob.process_order(bid_order)
        created_entries = ob.process_order(ask_order)

        # 7 shares of the bid entry will be paired with the ask entry.
        # A new pending entry will be created for the bid with 13 shares.
        self.assertEqual(len(created_entries), 2)

        bid_order.refresh_from_db()
        bid_entries = bid_order.entries.all()
        self.assertEqual(len(bid_entries), 2)
        for entry in bid_entries:
            if entry.status == ORDER_BOOK_ENTRY_STATUS.PENDING:
                self.assertEqual(entry.shares, 13)
            else:
                self.assertEqual(entry.shares, 7)

        for entry in created_entries:
            if entry.status == ORDER_BOOK_ENTRY_STATUS.PENDING:
                self.assertEqual(entry.order_id, bid_order.id)
                self.assertEqual(entry.shares, 13)
            else:
                self.assertEqual(entry.order_id, ask_order.id)
                self.assertEqual(entry.shares, 7)
