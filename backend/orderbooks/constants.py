class ORDER_SIDE:
    BID = 'bid'
    ASK = 'ask'
    ALL = BID, ASK


class ORDER_BOOK_ENTRY_STATUS:
    PENDING = 'pending'
    PROCESSED = 'processed'
    ALL = PENDING, PROCESSED
