from django_filters import rest_framework as filters

from orderbooks.models import Order, OrderBookEntry


class OrderFilter(filters.FilterSet):
    class Meta:
        model = Order
        fields = {
            'price': ['lt', 'gt', 'exact'],
            'shares': ['lt', 'gt', 'exact'],
            'created_at': ['lt', 'gt', 'exact'],
            'bet': ['exact'],
            'owner': ['exact'],
            'bet__statement': ['icontains'],
            'owner__email': ['icontains'],
        }


class OrderBookEntryFilter(filters.FilterSet):
    class Meta:
        model = OrderBookEntry
        fields = {
            'status': ['exact'],
            'order__bet__statement': ['icontains'],
            'order__bet__status': ['exact'],
        }
