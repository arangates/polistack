from collections import defaultdict
import datetime

from django.db.models import Max, Sum
from django.db.models.functions import ExtractDay, ExtractHour
from django.utils import timezone
import pytz
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from bets.constants import STATUS
from orderbooks.constants import ORDER_BOOK_ENTRY_STATUS, ORDER_SIDE
from orderbooks.models import Order, OrderBook, OrderBookEntry
from users.serializers import UserSerializer
from utils.serializer_fields import ChoicesField


class TradedOrderBookEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderBookEntry
        fields = (
            'id',
            'price',
            'shares'
        )


class NestedOrderBookEntrySerializer(serializers.ModelSerializer):
    owner = serializers.IntegerField(source='order.owner_id')
    traded_with = TradedOrderBookEntrySerializer(read_only=True)

    class Meta:
        model = OrderBookEntry
        fields = (
            'id',
            'price',
            'shares',
            'status',
            'traded_with',
            'order',
            'created_at',
            'owner',
        )


class OrderSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True)
    entries = NestedOrderBookEntrySerializer(many=True, read_only=True)
    side = ChoicesField(choices={'yes': ORDER_SIDE.BID, 'no': ORDER_SIDE.ASK})

    class Meta:
        model = Order
        fields = (
            'id',
            'side',
            'bet',
            'price',
            'shares',
            'created_at',
            'owner',
            'entries'
        )
        read_only_fields = ('id', 'created_at', 'owner')

    def validate(self, data):
        if 'bet' in data and data['bet'].status != STATUS.ACTIVE:
            raise ValidationError(f"Can only bet on {STATUS.ACTIVE} bets.")

        order_owner = self.context['request'].user
        order_price = (100 - data['price']) if data['side'] == ORDER_SIDE.ASK else data['price']
        order_amount = order_price * data['shares']
        available_amount = order_owner.account.available_amount
        if order_amount > available_amount:
            raise ValidationError(
                f"Not enough balance to place the order. You need {order_amount - available_amount} more."
            )
        return data

    def create(self, validated_data):
        order = Order.objects.create(**validated_data)
        order.bet.order_book.process_order(order)
        return order


class OrderBookSerializer(serializers.ModelSerializer):
    entries = serializers.SerializerMethodField()

    def get_entries(self, order_book):
        entries = order_book.entries.filter(
            status=ORDER_BOOK_ENTRY_STATUS.PENDING
        ).values(
            'price', 'order__side'
        ).annotate(
            volume=Sum('shares')
        ).order_by('price')

        result = [{'price': i, 'yes_volume': 0, 'no_volume': 0} for i in range(100)]
        for entry in entries:
            if entry['order__side'] == ORDER_SIDE.BID:
                result[entry['price']]['yes_volume'] = entry['volume']
            else:
                result[entry['price']]['no_volume'] = entry['volume']
        return result[1:]

    class Meta:
        model = OrderBook
        fields = ('entries',)


class AdminOrderBookSerializer(serializers.ModelSerializer):
    entries = serializers.SerializerMethodField()

    def get_entries(self, order_book):
        entries = order_book.entries.all().order_by('created_at')
        prices = set()
        price_entries_dict = defaultdict(lambda: {ORDER_SIDE.BID: [], ORDER_SIDE.ASK: []})
        for entry in entries:
            price_entries_dict[entry.price][entry.order.side].append(
                dict(OrderBookEntrySerializer(entry).data)
            )
            price_entries_dict[entry.price]['price'] = entry.price
            prices.add(entry.price)
        sorted_prices = sorted(prices)

        result = []
        for price in sorted_prices:
            result.append(price_entries_dict[price])
        return result

    class Meta:
        model = OrderBook
        fields = ('entries', 'highest_bid', 'lowest_ask', 'volume')


class OrderBookGraphSerializer(serializers.ModelSerializer):

    INTERVAL_MAP = {
        1: ExtractHour,
        7: ExtractDay,
        30: ExtractDay,
        90: ExtractDay,
    }

    points = serializers.SerializerMethodField()

    @timezone.override('Asia/Kolkata')
    def get_points(self, order_book):
        time = datetime.datetime.now(
            pytz.timezone('Asia/Kolkata')
        ).replace(
            hour=0, minute=0, second=0, microsecond=0
        ).astimezone(pytz.utc)
        interval = self.context['interval']
        extract_func = self.INTERVAL_MAP[interval]

        entry_ids = order_book.entries.filter(
            status=ORDER_BOOK_ENTRY_STATUS.PROCESSED,
            order__side=ORDER_SIDE.BID,
            created_at__gte=time - datetime.timedelta(days=interval - 1)
        ).annotate(
            x=extract_func('created_at')
        ).values(
            'x'
        ).annotate(
            y=Max('id')
        ).values_list(
            'y', flat=True
        ).order_by('x')

        entries = order_book.entries.filter(id__in=entry_ids)

        return [dict(OrderBookEntryPointSerializer(entry).data) for entry in entries]

    class Meta:
        model = OrderBook
        fields = ('points',)


class OrderBookGraphQueryParamsSerializer(serializers.Serializer):
    interval = serializers.IntegerField()

    def validate_interval(self, val):
        if val not in OrderBookGraphSerializer.INTERVAL_MAP.keys():
            raise ValidationError("Invalid interval selected.")
        return val


class OrderBookEntrySerializer(serializers.ModelSerializer):
    bet_statement = serializers.CharField(source='order.bet.statement')
    side = ChoicesField(choices={'yes': ORDER_SIDE.BID, 'no': ORDER_SIDE.ASK}, source='order.side')
    winning_side = ChoicesField(
        choices={'yes': ORDER_SIDE.BID, 'no': ORDER_SIDE.ASK},
        source='order.bet.win'
    )

    class Meta:
        model = OrderBookEntry
        fields = (
            'id',
            'bet_statement',
            'side',
            'price',
            'shares',
            'status',
            'winning_side',
        )

    def validate(self, data):
        if self.instance.status != ORDER_BOOK_ENTRY_STATUS.PENDING:
            raise ValidationError('You cannot update a non-pending order.')
        if self.instance.order.bet.status != STATUS.ACTIVE:
            raise ValidationError('You cannot update an order in a non-active bet.')
        shares = data.pop('shares', None)
        if not shares:
            raise ValidationError('You cannot set the number of shares to 0. Delete the order instead.')
        if data:
            raise ValidationError('You can only update shares.')

        return {'shares': shares}


class OrderBookEntryPointSerializer(serializers.ModelSerializer):
    x = serializers.DateTimeField(source='created_at')
    y = serializers.IntegerField(source='price')

    class Meta:
        model = OrderBookEntry
        fields = ('x', 'y',)
