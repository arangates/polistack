from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from orderbooks.constants import ORDER_BOOK_ENTRY_STATUS, ORDER_SIDE
from utils.models import OrderedTimestampModel


class Order(OrderedTimestampModel):
    SIDE_CHOICES = ((ORDER_SIDE.BID, 'yes'), (ORDER_SIDE.ASK, 'no'))

    bet = models.ForeignKey('bets.Bet', on_delete=models.PROTECT, related_name='orders', null=True)
    side = models.CharField(choices=SIDE_CHOICES, max_length=16)
    shares = models.IntegerField(validators=[MinValueValidator(1)])
    price = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(99)])
    owner = models.ForeignKey('users.User', on_delete=models.PROTECT, related_name='orders')

    def __str__(self):
        return f'{self.shares} * {self.price} {self.side} {self.created_at}'

    def get_back_out_amount(self):
        shares_left = self.entries.filter(
            status=ORDER_BOOK_ENTRY_STATUS.PENDING, price=self.price
        ).aggregate(models.Sum('shares'))
        return self.price * shares_left


class OrderBookEntry(OrderedTimestampModel):
    STATUS_CHOICES = (
        (ORDER_BOOK_ENTRY_STATUS.PENDING, 'pending'),
        (ORDER_BOOK_ENTRY_STATUS.PROCESSED, 'processed'),
    )

    order = models.ForeignKey('orderbooks.Order', related_name='entries', on_delete=models.PROTECT)
    order_book = models.ForeignKey('orderbooks.OrderBook', related_name='entries', on_delete=models.PROTECT)
    status = models.CharField(choices=STATUS_CHOICES, max_length=16)
    shares = models.IntegerField()  # the sum of all order.entries.shares should be equal to order.shares
    price = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(99)])  # Must be <= order.price
    traded_with = models.OneToOneField('orderbooks.OrderBookEntry', null=True, on_delete=models.PROTECT)
    is_aggressor = models.BooleanField(default=False)

    def __str__(self):
        return "{} / {} / {} / {} / {}".format(
            self.order_id, self.order.side, self.status, self.shares, self.price
        )

    __repr__ = __str__


class OrderBook(models.Model):
    bet = models.OneToOneField('bets.Bet', on_delete=models.PROTECT, related_name='order_book')

    @property
    def highest_bid(self):
        entry = self.entries.filter(
            status=ORDER_BOOK_ENTRY_STATUS.PENDING
        ).filter(
            order__side=ORDER_SIDE.BID
        ).order_by(
            '-price'
        ).first()

        if entry:
            return entry.price
        else:
            return 0

    @property
    def lowest_ask(self):
        entry = self.entries.filter(
            status=ORDER_BOOK_ENTRY_STATUS.PENDING
        ).filter(
            order__side=ORDER_SIDE.ASK
        ).order_by(
            'price'
        ).first()

        if entry:
            return entry.price
        else:
            return 100

    @property
    def volume(self):
        return self.entries.aggregate(models.Sum('shares'))['shares__sum'] or 0

    def get_asks_for_price(self, price):
        return self.entries.filter(
            status=ORDER_BOOK_ENTRY_STATUS.PENDING
        ).filter(
            order__side=ORDER_SIDE.ASK
        ).filter(
            price__lte=price
        ).order_by(
            'price', 'created_at'
        )

    def get_bids_for_price(self, price):
        return self.entries.filter(
            status=ORDER_BOOK_ENTRY_STATUS.PENDING
        ).filter(
            order__side=ORDER_SIDE.BID
        ).filter(
            price__gte=price
        ).order_by(
            '-price', 'created_at'
        )

    def process_order(self, order: Order):
        if order.side == ORDER_SIDE.BID:
            entries = self.get_asks_for_price(order.price)
        else:
            entries = self.get_bids_for_price(order.price)
        shares_left = order.shares
        created_entries = []
        for entry in entries:
            if shares_left > 0:
                shares_processed = min(shares_left, entry.shares)
                new_entry = OrderBookEntry.objects.create(
                    order=order,
                    order_book=self,
                    status=ORDER_BOOK_ENTRY_STATUS.PROCESSED,
                    shares=shares_processed,
                    traded_with=entry,
                    price=entry.price,
                    is_aggressor=True
                )
                created_entries.append(new_entry)
                entry.traded_with = new_entry
                entry.status = ORDER_BOOK_ENTRY_STATUS.PROCESSED
                if shares_processed < entry.shares:
                    other_side_new_entry = OrderBookEntry.objects.create(
                        order=entry.order,
                        order_book=self,
                        status=ORDER_BOOK_ENTRY_STATUS.PENDING,
                        shares=entry.shares - shares_processed,
                        price=entry.price,
                    )
                    other_side_new_entry.created_at = entry.created_at
                    other_side_new_entry.save()
                    created_entries.append(other_side_new_entry)
                    entry.shares = shares_processed
                entry.save()

                shares_left -= shares_processed
            else:
                break
        if shares_left > 0:
            new_entry = OrderBookEntry.objects.create(
                order=order,
                order_book=self,
                status=ORDER_BOOK_ENTRY_STATUS.PENDING,
                shares=shares_left,
                price=order.price
            )
            created_entries.append(new_entry)
        return created_entries

    @property
    def latest_trade(self):
        return self.entries.filter(
            status=ORDER_BOOK_ENTRY_STATUS.PROCESSED
        ).order_by('-created_at').first()

    @property
    def latest_traded_price(self):
        entry = self.latest_trade
        if entry:
            return entry.price
        else:
            return None
