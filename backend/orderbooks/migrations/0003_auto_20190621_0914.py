# Generated by Django 2.0.4 on 2019-06-21 09:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orderbooks', '0002_order_owner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='side',
            field=models.CharField(choices=[('bid', 'yes'), ('ask', 'no')], max_length=16),
        ),
    ]
