from rest_framework.decorators import detail_route
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from orderbooks.filters import OrderBookEntryFilter, OrderFilter
from orderbooks.models import Order, OrderBook, OrderBookEntry
from orderbooks.permissions import IsOwnerOrReadOnly
from orderbooks.serializers import (
    AdminOrderBookSerializer,
    OrderBookEntrySerializer,
    OrderBookGraphQueryParamsSerializer,
    OrderBookGraphSerializer,
    OrderBookSerializer,
    OrderSerializer,
)
from utils.permissions import IsAdminUser
from utils.views import CreateListRetrieveViewSet, ListUpdateViewSet


class OrderViewSet(CreateListRetrieveViewSet):
    """
    According to business logic we can not delete, modify Orders with one exception:
    We can back out from bet. This requires validation and updating only one field so I
    decided to create detail_route to handle this instead of overriding perform_update and
    partial_update.
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_class = OrderFilter
    permission_classes = (IsAuthenticatedOrReadOnly,)
    user_attr = 'owner'

    def get_serializer_context(self):
        return {'request': self.request}

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    @detail_route(methods=['get'], permission_classes=[IsAuthenticated])
    def back_out_amount(self, request, pk=None):
        return Response({'back_out_amount': self.get_object().get_back_out_amount()})

    @detail_route(methods=['patch'], permission_classes=[IsAuthenticated, IsOwnerOrReadOnly])
    def back_out(self, request, pk=None):
        order = self.get_object()
        serializer = self.get_serializer(order, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data)


class AdminOrderBookViewSet(RetrieveModelMixin, GenericViewSet):
    permission_classes = (IsAdminUser,)
    serializer_class = AdminOrderBookSerializer
    queryset = OrderBook.objects.all()


class OrderBookViewSet(RetrieveModelMixin, GenericViewSet):
    permission_classes = []
    serializer_class = OrderBookSerializer
    queryset = OrderBook.objects.all()


class OrderBookGraphViewSet(RetrieveModelMixin, GenericViewSet):
    permission_classes = []
    serializer_class = OrderBookGraphSerializer
    queryset = OrderBook.objects.all()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        query_params_serializer = OrderBookGraphQueryParamsSerializer(data=self.request.query_params)
        if query_params_serializer.is_valid(raise_exception=True):
            context.update(query_params_serializer.data)
        return context


class OrderBookEntryViewSet(ListUpdateViewSet):
    filter_class = OrderBookEntryFilter
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderBookEntrySerializer

    def get_queryset(self):
        return OrderBookEntry.objects.filter(order__owner=self.request.user).order_by('-created_at')
