from collections import defaultdict
from decimal import Decimal
from itertools import zip_longest
import math
from typing import List

from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.validators import validate_ipv4_address
from django.db import models
from django.db.models import F, Sum
from django.db.models.functions import Coalesce
from django.utils.translation import ugettext_lazy as _
from rest_framework_jwt.settings import api_settings as jwt_api_settings

from bets import constants as bets_constants
from orderbooks.constants import ORDER_SIDE
from orderbooks.models import OrderBookEntry
from utils.models import OrderedTimestampModel

jwt_payload_handler = jwt_api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = jwt_api_settings.JWT_ENCODE_HANDLER


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            **extra_fields
        )

        user.set_password(password)
        user.save(using=self._db)

        UserAccount.objects.create(user=user)
        return user

    def create_superuser(self, email, password, **extra_fields):
        user = self.create_user(
            email,
            password=password,
            is_staff=True,
            # are both necessary?
            is_superuser=True,
            **extra_fields
        )
        user.save(using=self._db)
        return user


def percent(value):
    if not (Decimal('1') >= value >= Decimal('0')):
        raise ValidationError(
            _('%(value)s is not a percent (Decimal("1") >= value >= Decimal("0"))'),
            params={'value': value},
        )


class User(AbstractBaseUser, PermissionsMixin):
    """
    User model with admin-compliant permissions.

    Email and password are required.
    """
    email = models.EmailField(
        _('email address'),
        unique=True,
        error_messages={
            'unique': _("A user with that email address already exists."),
        },
    )
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=150)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    created_at = models.DateTimeField(_('date joined'), auto_now_add=True)
    image = models.ImageField(blank=True, null=True)
    channel_name = models.CharField(max_length=50, blank=True, null=True)
    fee = models.DecimalField(decimal_places=2, max_digits=3, default=settings.DEFAULT_FEE, validators=[percent])

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = UserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        ordering = ['created_at', 'id']

    def __str__(self):
        return f'{self.email}'

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_jwt_token(self) -> str:
        """Enforce authentication and returns valid jwt token"""
        return jwt_encode_handler(jwt_payload_handler(self))

    def get_full_name(self) -> str:
        full_name = '{} {}'.format(self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self) -> str:
        return self.first_name

    def get_group_names(self) -> List[str]:
        return [group.name for group in self.groups.all()]

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def balance_total(self) -> Decimal:
        """Available + Locked"""
        return self.transactions.aggregate(Sum('amount'))['amount__sum'] or Decimal('0')

    def get_available_balance(self) -> Decimal:
        """Money user has on account and can actually do something with."""

        # https://docs.djangoproject.com/en/2.0/ref/models/database-functions/#coalesce
        # balance = Coalesce(Sum('transactions__amount'), 0)
        # locked_balance = Coalesce(
        #     Sum('bet_actions__amount', filter=(Q(bet_actions__bet__status__in=bets_constants.STATUS.LOCKING))),
        #     0
        # )
        # return (User.objects
        #         .filter(id=self.id)
        #         .aggregate(available_balance=(balance - locked_balance)))['available_balance']

        # Temporary solution that generates 2 queries. Investigate why solution above is not working.
        return self.balance_total() - self.get_locked_balance()

    def get_locked_balance(self) -> Decimal:
        """Locked balance ( locked in bets or pending withdrawal )"""
        # TODO Take into account pending withdrawals when it will be implemented.

        return self.orders.filter(
            bet__status__in=bets_constants.STATUS.LOCKING
        ).aggregate(
            locked_balance=Coalesce(Sum('price', field='price*shares'), Decimal('0'))
        )['locked_balance'] or Decimal('0')

    def request_deposit(self, amount, payment_option):
        subject = "New Deposit"
        message = "There is a new deposit request.\nEmail: {}\nAmount: {}\nPayment Option: {}".format(
            self.email, amount, payment_option
        )
        support_email = 'support@polistack.com'
        send_mail(subject, message, support_email, [support_email])


class LoginAttempt(OrderedTimestampModel):  # Some better name?
    ip = models.CharField(max_length=16, validators=[validate_ipv4_address])
    country = models.CharField(max_length=76, null=True, blank=True)
    city = models.CharField(max_length=85, null=True, blank=True)
    region = models.CharField(max_length=5, null=True, blank=True)
    successful = models.BooleanField()

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='login_attempts')


class UserAccount(OrderedTimestampModel):
    user = models.OneToOneField('users.User', related_name='account', on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)

    def update(self, amount, is_fee_applicable=False, transaction_by=None, bet=None):
        if is_fee_applicable:
            fee = self.user.fee
        else:
            fee = 0
        net_amount = amount - math.ceil(amount * fee)
        prev_account_balance = self.amount
        self.amount = max(self.amount + net_amount, 0)
        self.save()
        self.transactions.create(
            orig=amount,
            amount=net_amount,
            user=transaction_by or self.user,
            fee=fee,
            amount_before=prev_account_balance,
            amount_after=self.amount,
            bet=bet
        )

    @property
    def available_amount(self):
        return self.amount - self.frozen_amount

    @property
    def frozen_amount(self):
        active_orders = self.user.orders.filter(bet__status=bets_constants.STATUS.ACTIVE)
        entries = OrderBookEntry.objects.filter(order__in=active_orders).order_by('price')
        bet_trades_map = defaultdict(lambda: {
            ORDER_SIDE.BID: [],
            ORDER_SIDE.ASK: [],
        })

        for entry in entries:
            order = entry.order
            bet_trades_map[order.bet_id][order.side].extend([entry.price] * entry.shares)

        total_frozen_amount = 0
        for side_trades_map in bet_trades_map.values():
            bids = side_trades_map[ORDER_SIDE.BID][::-1]
            asks = map(lambda x: 100 - x, side_trades_map[ORDER_SIDE.ASK])

            frozen_amount = sum([max(bid, ask) for bid, ask in zip_longest(bids, asks, fillvalue=0)])
            total_frozen_amount += frozen_amount

        return total_frozen_amount

    @property
    def fees_paid(self):
        return self.transactions.annotate(
            fees_paid=(F('orig') - F('amount'))
        ).aggregate(
            Sum('fees_paid')
        )['fees_paid__sum'] or Decimal('0')
