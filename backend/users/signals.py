from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import User, UserAccount


@receiver(post_save, sender=User)
def user_saved(sender, instance, created, **kwargs):
    if created:
        UserAccount.objects.get_or_create(user=instance)
