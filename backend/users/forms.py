from django import forms
from django.contrib.admin.helpers import ActionForm


class AccountActionForm(ActionForm):
    amount = forms.IntegerField(
        required=True,
        help_text='How much to withdraw / deposit?',
    )
