from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from django.template.loader import render_to_string

from bets.constants import STATUS
from users.forms import AccountActionForm
from users.models import UserAccount
from utils.admin_helpers import created_at_formatted

admin.site.unregister(Group)

User = get_user_model()


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    list_filter = ('is_active', 'is_superuser')
    list_display = (
        'created_at_formatted',
        'email',
        'last_name',
        'id',
        'is_active',
        'balance_total',
        'balance_available',
        'balance_locked',
        'fees_paid',
        'fee',
        'country',
    )
    search_fields = ('email',)
    ordering = ('-created_at',)
    fieldsets = (
        (
            'Basic data', {
                'fields': (
                    'email',
                    'password',
                    'last_login',
                    'first_name',
                    'last_name',
                    'created_at',
                    'fee',
                    'is_active',
                    'is_superuser',
                    'is_staff',
                )
            }
        ),
        (
            'Balances', {
                'fields': (
                    'balance_total',
                    'balance_available',
                    'balance_locked',
                )
            }
        ),
        (
            'Additional info',
            {
                'fields': (
                    'get_transactions',
                    'get_active_bets',
                    'get_past_bets',
                ),
            }
        ),
        (
            'Login attempts',
            {
                'fields': (
                    'get_login_attempts',
                ),
            }
        ),
    )
    readonly_fields = (
        'created_at',
        'get_transactions',
        'get_active_bets',
        'get_past_bets',
        'get_login_attempts',
        'balance_total',
        'balance_available',
        'balance_locked',
        'last_login',
        'country',
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    save_on_top = True

    created_at_formatted = created_at_formatted

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if not change:
            UserAccount.objects.create(user=obj)

    def country(self, user):
        last_login_attempt = user.login_attempts.last()
        return last_login_attempt.country if last_login_attempt else None

    def balance_total(self, user):
        return user.account.amount

    def balance_available(self, user):
        return user.account.available_amount

    def balance_locked(self, user):
        return user.account.frozen_amount

    def fees_paid(self, user):
        return user.account.fees_paid

    def get_transactions(self, user):
        queryset = (
            user
            .account
            .transactions
            # THINK selects whole model, is there a way to select only statement?
            .select_related('bet')
            .all()
        )
        return render_to_string('users/transactions_list.html', {
            'transactions': queryset,
        })

    get_transactions.short_description = 'Transactions'

    def get_active_bets(self, user):
        q = (
            user
            .bet_actions
            .filter(bet__status=STATUS.ACTIVE)
            .select_related('bet')
        )
        # TODO: query killer in bet_action.get_back_out_amount()
        for bet_action in q:
            bet_action.paired_amount = bet_action.amount - bet_action.get_back_out_amount()
        return render_to_string('users/bet_actions_list.html', {
            'bet_actions': q
        })

    get_active_bets.short_description = 'Active bets'

    def get_past_bets(self, user):
        q = (
            user
            .bet_actions
            .exclude(bet__status__in=[STATUS.ACTIVE, STATUS.ENDED])
            .select_related('bet')
        )
        # TODO: query killer in bet_action.get_back_out_amount()
        for bet_action in q:
            bet_action.paired_amount = bet_action.amount - bet_action.get_back_out_amount()
        return render_to_string('users/bet_actions_list.html', {
            'bet_actions': q
        })

    get_past_bets.short_description = 'Other bets'

    def get_login_attempts(self, user):
        return render_to_string('users/login_attempt_list.html', {
            'login_attempts': user.login_attempts.exclude(successful=False)
        })

    get_login_attempts.short_description = 'Login info'

    def update_account(modeladmin, request, queryset):
        amount = request.POST['amount']
        amount = int(amount)
        for u in queryset:
            u.account.update(amount=amount, transaction_by=request.user)

    update_account.short_description = 'Deposit / Withdraw selected accounts'

    action_form = AccountActionForm

    actions = [update_account]
