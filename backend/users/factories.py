import random

from django.contrib.auth import get_user_model
import factory.fuzzy
from faker.providers import BaseProvider

from users.models import LoginAttempt, UserAccount
from utils import log

User = get_user_model()


class IPProvider(BaseProvider):
    def ipv4(self):
        return '.'.join(str(random.randint(1, 254)) for _ in range(4))


factory.Faker.add_provider(IPProvider)


class UserAccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserAccount


class UserFactory(factory.django.DjangoModelFactory):
    email = factory.Sequence(lambda n: 'user{}@example.pl'.format(n))
    password = factory.PostGenerationMethodCall('set_password', 'pass')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    fee = factory.fuzzy.FuzzyDecimal(0.03, 0.08)
    # account = factory.RelatedFactory(UserAccountFactory)

    class Meta:
        model = User
        django_get_or_create = ('email',)

    @factory.post_generation
    def transactions(self, create, extracted, **kwargs):
        from bets.factories import TransactionFactory
        # http://factoryboy.readthedocs.io/en/latest/recipes.html#simple-many-to-many-relationship
        if not create:
            return
        if extracted:
            for category in extracted:
                self.transactions.add(category)
        else:
            topup = 1000
            TransactionFactory(
                user=self,
                amount=topup,
                orig=topup,
                bet=None,
                fee=0,
            )
            log.debug('%r got %r', self.email, topup)
        UserAccountFactory(user=self)

    @factory.post_generation
    def login_attempts(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for login_attempt in extracted:
                self.login_attempts.add(login_attempt)
        else:
            LoginAttemptFactory(user=self)


class SuperUserFactory(UserFactory):
    is_superuser = True
    is_staff = True
    is_active = True


class LoginAttemptFactory(factory.django.DjangoModelFactory):
    ip = factory.Faker('ipv4')
    country = factory.Faker('country')
    city = factory.Faker('city')
    region = factory.Faker('state_abbr')
    successful = random.getrandbits(1)

    user = UserFactory

    class Meta:
        model = LoginAttempt
