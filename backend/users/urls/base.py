from django.conf.urls import url

from users.views import UserList

urlpatterns = [
    url(r'^$', UserList.as_view()),
]
