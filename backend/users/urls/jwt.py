from django.conf.urls import url
from rest_framework_jwt import views

from users.views import ObtainJSONWebTokenLoginAttempt

urlpatterns = [
    url(r'^jwt/create/', ObtainJSONWebTokenLoginAttempt.as_view(), name='jwt-create'),
    url(r'^jwt/refresh/', views.refresh_jwt_token, name='jwt-refresh'),
    url(r'^jwt/verify/', views.verify_jwt_token, name='jwt-verify'),

]
