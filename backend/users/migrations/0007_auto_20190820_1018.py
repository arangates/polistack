# Generated by Django 2.0.4 on 2019-08-20 10:18

from django.db import migrations

def create_user_accounts(apps, schema_editor):
    User = apps.get_model('users', 'User')
    UserAccount = apps.get_model('users', 'UserAccount')

    users = User.objects.filter(account__isnull=True)
    for user in users:
        UserAccount.objects.create(user=user)


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20190724_1207'),
    ]

    operations = [
        migrations.RunPython(create_user_accounts),
    ]
