from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    balance = serializers.IntegerField(source='account.available_amount', read_only=True)
    amount = serializers.IntegerField(source='account.amount', read_only=True)

    class Meta:
        model = User
        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
            'balance',
            'amount',
        )
