"""
Note to self: The existence of this file was caused by:

    backend_1   | django.core.exceptions.ImproperlyConfigured: The app module <module 'users' (namespace)> has multiple
                | filesystem locations (['./users', '/code/users']); you must configure this app with an AppConfig
                | subclass with a 'path' class attribute.

discovered with https://stackoverflow.com/a/28222093/1472229

Apparently runserver doesn't mind.

"""
default_app_config = 'users.apps.UsersConfig'
