from django.contrib.auth import get_user_model
from geoip2.errors import AddressNotFoundError
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.views import ObtainJSONWebToken

from users.apps import geoip2
from users.models import LoginAttempt
from users.serializers import UserSerializer
from utils import log
from utils.helpers import get_client_ip
from utils.permissions import IsAdminUser

jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER

User = get_user_model()


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)


class ObtainJSONWebTokenLoginAttempt(ObtainJSONWebToken):  # Some better name?

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        user = User.objects.filter(email=request.data['email']).first()
        if user:
            ip = get_client_ip(request)
            try:
                info = geoip2.city(ip)
            except AddressNotFoundError:
                log.debug('IP: %r was not found', ip)
                info = {
                    'country_name': None,
                    'city': None,
                    'region': None,
                }
            LoginAttempt.objects.create(
                user=user,
                ip=ip,
                country=info['country_name'],
                city=info['city'],
                region=info['region'],
                successful=response.status_code == status.HTTP_200_OK
            )
        return response
