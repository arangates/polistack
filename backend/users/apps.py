from django.apps import AppConfig

geoip2 = None


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = 'Users'

    def ready(self):
        from django.contrib.gis.geoip2 import GeoIP2
        global geoip2
        geoip2 = GeoIP2()  # Initialize object only once cause it's expensive
