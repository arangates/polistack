#!/bin/bash
# the following file should be manually created in the /tmp/ 
# docker exec rarebets_staging_db_1 pg_dump -U postgres rb > /tmp/rb_dump.sql
staging_dump_file=/tmp/rb_dump.sql
staging_contabo_node=5.189.141.218
server_host=0.0.0.0
server_port=8000
db_name=rarebets
# check if Django is running and kill if it is
is_server_up () {
 	echo "checking if Postgres is busy..."
 	PID8000='lsof -Pi :8000 -sTCP:LISTEN -t'
 	if eval $PID8000 >/dev/null ; then
        echo "running $($PID8000)"
        echo "killing Django graciously"
        pkill -f runserver
        is_server_up
	else
    	echo "not running"
	fi
}

start_server () {
	./manage.py runserver $server_host:$server_port
}

# Postgres - update Local DB
update_db () {
	is_server_up
    echo "plough and populate local DB with staging dump..."
    dropdb $db_name && createdb $db_name && psql -U $USER $db_name < $staging_dump_file > /dev/null
	# psql -U $USER $db_name -a -f reset_local_db.sql
	echo "starting back"
	start_server
}

echo "Check if local has dump..."

# check if there is staging dump file present in local already
if [ -e "$staging_dump_file" ]; then
    echo "Found local staging dump..."
    update_db
else
    echo "File does not exist in local"
    # check if SCP command exists
    if [ -x "$(command -v scp)" ]; then
      echo "SCP exist..."
      echo 'starting transfer'
        if nc -z $staging_contabo_node 22 2>/dev/null; then
            echo "✓ SSH to $staging_contabo_node successful!"
            # replace with your username
                scp arek@5.189.141.218:$staging_dump_file /tmp/
                OUT=$?
                if [ $OUT = 0 ]; then
                echo 'File Transfer successful'
                    if [ -e "$staging_dump_file" ]; then
                        echo "successfully fetched staging dump..."
                        update_db
                    else
                        echo "Error in SCP transfer...try again!"
                    fi
                else
                echo 'Transfer failed'
                fi
        else
            echo "✗ SSH to $staging_contabo_node failed."
        fi

    else
        echo "Install SCP to proceed!
        On Ubuntu/Debian: sudo apt-get install openssh-client
        On CentOS/RHEL and Fedora: yum install openssh-clients
        "
    fi
fi
