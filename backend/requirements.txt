celery==4.2.1
colorhash==1.0.2
dj-database-url==0.5.0
django-celery-email==2.0.0
django-cors-headers==2.2.0
django-environ==0.4.4
django-filter==1.1.0
django-object-actions==1.0.0
django-username-email==2.2.0
Django==2.0.4
djangorestframework-jwt==1.11.0
djangorestframework==3.7.7
djoser==1.4.1
logging-tree==1.7
Pillow==5.0.0
prettyrepr==2018.03.08.3
psycopg2-binary==2.7.4
tqdm==4.19.6
uWSGI==2.0.17
raven==6.6.0
factory-boy==2.10.0
geoip2==2.8.0
channels==2.1.0
channels-redis==2.1.0

# CI
flake8==3.5.0
isort==4.3.4
binaryornot==0.4.4
