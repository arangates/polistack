import logging
from decimal import Decimal

from django.conf import settings
from logging_tree.format import build_description
from rest_framework.utils import encoders
log = logging.getLogger('global')


class dotdict(dict):
    """
    # https://stackoverflow.com/a/13520518/1472229

    a dictionary that supports dot notation
    as well as dictionary access notation
    usage: d = DotDict() or d = DotDict({'val1':'first'})
    set attributes: d.val2 = 'second' or d['val2'] = 'second'
    get attributes: d.val2 or d['val2']
    """
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __init__(self, dct):
        for key, value in dct.items():
            if hasattr(value, 'keys'):
                value = dotdict(value)
            self[key] = value


if settings.LOGGING_DEBUG:
    log.debug('logger tree:\n%s', build_description()[:-1])


class RBJSONEncoder(encoders.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj.quantize(Decimal('1.000')))
        return super().default(obj)
