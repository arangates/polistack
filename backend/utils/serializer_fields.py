from rest_framework import serializers


class ChoicesField(serializers.Field):
    """Custom ChoiceField serializer field."""

    def __init__(self, choices, **kwargs):
        self._choices = dict(choices)
        self._reverse_choices = {val: key for key, val in self._choices.items()}
        super(ChoicesField, self).__init__(**kwargs)

    def to_representation(self, obj):
        """Used while retrieving value for the field."""
        return self._reverse_choices[obj]

    def to_internal_value(self, data):
        """Used while storing value for the field."""
        try:
            return self._choices[data]
        except KeyError:
            raise serializers.ValidationError("Acceptable values are {0}.".format(list(self._choices.keys())))
