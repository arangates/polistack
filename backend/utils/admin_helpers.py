def created_at_formatted(self, obj):
    return obj.created_at.strftime("%y.%m.%d  %H:%M:%S")


created_at_formatted.short_description = 'time'
