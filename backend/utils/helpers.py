def get_client_ip(request):
    """
    Returns client ip.

    Warning: Client can fabricate their ip address setting X_FORWARDED_FOR header.
    See: https://stackoverflow.com/questions/4581789/how-do-i-get-user-ip-address-in-django
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
