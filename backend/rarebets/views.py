from django.core.mail import send_mail
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .serializers import ContactDataSerializer


@api_view(http_method_names=['POST'])
@permission_classes((permissions.AllowAny,))
def contact_view(request):
    serializer = ContactDataSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    support_email = 'support@polistack.com'
    subject = "New contact request from Landing Page"
    message = (
        "Details:\n"
        "Email: {email}\n"
        "First Name: {first_name}\n"
        "Last Name: {last_name}\n"
        "Phone: {phone}\n"
        "Message: {message}".format(**serializer.data)
    )
    send_mail(subject, message, support_email, [support_email])
    return Response(status=status.HTTP_204_NO_CONTENT)
