from django.core.management.commands.makemessages import Command as MakeMessagesCommand


class Command(MakeMessagesCommand):

    def handle(self, *args, **options):
        # our defaults
        # -a --no-wrap --no-obsolete --no-location
        options['all'] = True
        options['no_wrap'] = True
        options['no_obsolete'] = True
        options['no_location'] = True
        super().handle(*args, **options)
