import logging
import random
import sys
from typing import List

from django.contrib.auth import get_user_model
from django.core.management import BaseCommand
import factory
import tabulate
from tqdm import tqdm

from bets.constants import SIDE, STATUS
from bets.factories import BetFactory, CategoryFactory, TransactionFactory
from orderbooks.factories import OrderBookFactory, OrderFactory
from users.factories import UserFactory

User = get_user_model()

# will be configured, not affected by settings
log = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        super().add_arguments(parser)

        parser.add_argument(
            '--categories',
            type=int,
            default=10
        )
        parser.add_argument(
            '--bets-per-category',
            type=int,
            default=10
        )
        parser.add_argument(
            '--admins',
            type=int,
            default=1
        )
        parser.add_argument(
            '--regular-users',
            type=int,
            default=6
        )
        parser.add_argument(
            '--transactions-per-user',
            type=int,
            default=10
        )
        parser.add_argument(
            '--order-per-bet',
            type=int,
            default=10
        )
        parser.add_argument(
            '-g',
            '--log-level',
            choices=logging._nameToLevel.keys(),
            default=logging._levelToName[logging.INFO]
        )

    def override_logging(self, level: str):
        # this script is executed remotely and in isolation from the app
        # let's make sure that the logging is always on
        # https://docs.python.org/3/howto/logging.html#configuring-logging
        log.setLevel(logging._nameToLevel[level])
        log.propagate = False
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s %(levelname)8.8s - %(message)s', datefmt='%H:%M:%S')
        ch.setFormatter(formatter)
        log.addHandler(ch)

    @staticmethod
    def create_users(name: str, quantity: int, **kwargs) -> List[User]:
        """
        Creates N users with given name and custom password.
        Eg.
        call with params (name='placki' quantity=3) will return:
            User(name=placki0@example.com, password=placki0pass, username=placki0)
            User(name=placki1@example.com, password=placki1pass, username=placki1)
            User(name=placki2@example.com, password=placki2pass, username=placki2)
        """
        users = [
            UserFactory(
                email=f'{name}{x}@example.com',
                password=f'{name}{x}pass',
                first_name=f'{name}{x}_first_name',
                last_name=f'{name}{x}_last_name',
                fee=random.uniform(0.03, 0.08),
                **kwargs
            ) for x in range(quantity)
        ]
        return users

    def handle(self, *args, **options):
        self.override_logging(options['log_level'])

        self.create_users('adm', options['admins'], is_staff=True, is_superuser=True)
        regular_users = self.create_users('reg', options['regular_users'])

        for category in tqdm(CategoryFactory.create_batch(options['categories']), 'categories'):
            # Create N bets to every category
            for _ in tqdm(range(options['bets_per_category']), 'bets'):
                bet = BetFactory(
                    categories=[category],
                    created_by=random.choice(regular_users),
                    ends=[
                        factory.Faker('future_date', end_date="+30d"),
                        factory.Faker('past_date', start_date="-30d"),
                    ][random.getrandbits(1)],
                    status=random.choice([STATUS.ACTIVE, STATUS.PENDING]),
                    resolve_query=random.randrange(0, 100) > 75
                )
                OrderBookFactory(bet=bet)

                for _ in range(options['order_per_bet']):
                    user = random.choice(regular_users)
                    amount = random.randint(1, 10)
                    if 2 * amount >= user.get_available_balance():
                        # I'm broke? Then top me up
                        topup = random.randint(2 * amount, 2 * amount + 100)
                        TransactionFactory(
                            user=user,
                            amount=topup,
                            orig=topup,
                            bet=None,
                            fee=0,
                        )
                    OrderFactory(
                        owner=user,
                        bet=bet,
                        price=amount,
                        shares=1
                    )
                # solve
                if bet.status == STATUS.ACTIVE and random.randrange(0, 100) > 10:
                    bet.win = random.choice(SIDE.ALL)
                    bet.status = STATUS.ENDED
                    bet.save()
                    bet.transactions.update(created_at=bet.created_at)

        sys.stdout.write(tabulate.tabulate(
            [
                (
                    u.email,
                    u.email.split('@')[0] + 'pass',
                    '%s %s' % (u.first_name, u.last_name),
                )
                for u in
                User.objects.order_by('last_name')
            ],
            (
                'email',
                'password',
                'name',
            ),
            tablefmt='fancy_grid'
        ) + '\n')
