import logging
import random

from django.contrib.auth import get_user_model
from django.core.management import BaseCommand

from bets.constants import SIDE, STATUS
from bets.factories import BetFactory, CategoryFactory, TransactionFactory
from bets.factories_data import ICONS
from orderbooks.constants import ORDER_SIDE
from orderbooks.factories import OrderBookFactory, OrderFactory
from users.factories import UserFactory

User = get_user_model()

# will be configured, not affected by settings
log = logging.getLogger()


class Command(BaseCommand):
    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument(
            '-g',
            '--log-level',
            choices=logging._nameToLevel.keys(),
            default=logging._levelToName[logging.INFO]
        )

    def override_logging(self, level: str):
        # this script is executed remotely and in isolation from the app
        # let's make sure that the logging is always on
        # https://docs.python.org/3/howto/logging.html#configuring-logging
        log.setLevel(logging._nameToLevel[level])
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s %(levelname)8.8s - %(message)s', datefmt='%H:%M:%S')
        ch.setFormatter(formatter)
        log.addHandler(ch)

    def handle(self, *args, **options):
        self.override_logging(options['log_level'])
        # for x in range(3):
        #     myUser = UserFactory(email=f'fish{x}@example.com', password=f'fish{x}pass', first_name=f'Fish{x}', last_name=f'Fish{x}')
        #     transaction = TransactionFactory(user=myUser, amount=100000, bet=None)

        myUser1 = UserFactory(email='wolfram@example.com', password='wolframpass', first_name='Wolfram', last_name='Example')
        transaction = TransactionFactory(user=myUser1, amount=100000, bet=None)
        myUser = UserFactory(email='sven@example.com', password='svenpass', first_name='Sven', last_name='Example')
        transaction = TransactionFactory(user=myUser, amount=100000, bet=None)

        categories = [
            CategoryFactory(name='Trump', icon=random.choice(ICONS)), # noqa
            CategoryFactory(name='Sport', icon=random.choice(ICONS)), # noqa
            CategoryFactory(name='World news', icon=random.choice(ICONS)), # noqa
            CategoryFactory(name='Cryptocurrencies', icon=random.choice(ICONS)), # noqa
            CategoryFactory(name='People', icon=random.choice(ICONS)), # noqa
            CategoryFactory(name='Financial Markets', icon=random.choice(ICONS)), # noqa
            CategoryFactory(name='Other', icon=random.choice(ICONS)), # noqa
        ]

        bets = [
            [myUser, 'trump.jpg', 3, 2, 'Trump will be reelected in 2020', STATUS.ACTIVE],
            [myUser, 'trumptwitterban.png', 1, 6, 'Twitter account of Trump will be banned at least once by the end of 2018', STATUS.ACTIVE],
            [myUser, 'trumpclinton.jpg', 1, 2, 'Trump will tweet the words Crooked Hillary until end of 2018', STATUS.ACTIVE],
            [myUser, 'melaniatrump.jpg', 1, 2, 'Melania Trump will give birth to a child by 2020', STATUS.ACTIVE],
            [myUser, 'ukgovtbantrump.jpg', 1, 10, 'UK governement will ban Trump from visiting before the end of his first term', STATUS.ACTIVE],
            [myUser, 'eminemtrump.jpg', 1, 50, 'Eminem will publicly endorse Trump in the 2020 elections', STATUS.ACTIVE],
            [myUser, 'lebronharden.jpg', 1, 2, 'LeBron James will have more points per game than James Harden at season end', STATUS.ACTIVE],
            [myUser, 'gatesnba.jpg', 1, 50, 'Bill Gates will become the sole owner of a NBA team in 2018', STATUS.ACTIVE],
            [myUser, 'cristianoronaldo.jpg', 1, 5, 'Cristiano Ronaldo will score 20 goals or more in the Champions League in 2018', STATUS.ACTIVE],
            [myUser, 'abramovichchelsea.jpg', 1, 8, 'Abramovich will sell all his shares in Chelsea by end of 2018', STATUS.ACTIVE],
            [myUser, 'northkoreabomb.jpg', 1, 8, 'North Korea will publicly withdraw from the nuclear weapon program by end of 2018', STATUS.ACTIVE],
            [myUser, 'zuckerbergarrested.jpg', 1, 6, 'Zuckerberg will get arrested by end of 2019', STATUS.ACTIVE],
            [myUser, 'uberbanned.jpg', 1, 5, 'Uber will be banned in the State of New York by 2020', STATUS.ACTIVE],
            [myUser, 'xistepdown.jpg', 1, 10, 'Xi Jinping will step down by 2020', STATUS.ACTIVE],
            [myUser, 'chinaexpelsrussia.jpg', 1, 15, 'China will expel at least one Russian diplomat in 2018', STATUS.ACTIVE],
            [myUser, 'dutertetrump.jpg', 1, 5, 'Rodrigo Duterte will insult Trump publicly in 2018', STATUS.ACTIVE],
            [myUser, 'bitcoinrise.png', 1, 8, 'Bitcoin will rise above 15000 by end of April', STATUS.ACTIVE],
            [myUser, 'ethereumfall.png', 1, 4, 'Ethereum will fall below 300 by end of May', STATUS.ACTIVE],
            [myUser, 'binancebankrupt.png', 1, 10, 'Binance will file for bankrupcy protection by the end of 2018', STATUS.ACTIVE],
            [myUser, 'obamasdivorce.jpg', 1, 20, 'Michelle and Barack Obama will get a divorce by 2020', STATUS.ACTIVE],
            [myUser, 'pewdiepiesubs.jpg', 1, 5, 'Pewdiepie will have at least 70 mln subscriptions by end of 2018', STATUS.ACTIVE],
            [myUser, 'trumptarriffs.jpeg', 1, 8, 'Trump will impose a tariff on Saudi Arabia for at least one product or service', STATUS.ACTIVE],
            [myUser, 'facebooksharefall.jpg', 1, 3, 'Facebook share price will fall below 100 USD by end of 2018', STATUS.ACTIVE],
            [myUser, 'amazon.jpg', 1, 4, 'Amazon share price will fall below 1200 USD by end of 2018', STATUS.ACTIVE],
            [myUser, 'reddit.png', 1, 5, 'this will be one of the words of the most popular reddit link at day end on April 20th', STATUS.ACTIVE],
            [myUser, 'reddit.png', 1, 50, 'this will be the first word of the most popular reddit link at day end on April 20th', STATUS.ACTIVE],
            [myUser, 'youtubemostpopular.jpg', 1, 20, 'The most popular youtube video by 2018 will have more than 8 billion views', STATUS.ACTIVE],
        ]
        for bet in bets:
            new_bet = BetFactory(
                created_by=bet[0],
                image=bet[1],
                statement=bet[4],
                status=bet[5],
                categories=[random.choice(categories)],
            )
            OrderBookFactory(bet=new_bet)
            OrderFactory(
                bet=new_bet,
                owner=bet[0],
                side=ORDER_SIDE.ASK,
                price=random.randint(1, 99),
                shares=random.randint(1, 100),
            )

        for u in User.objects.all():
            log.info('%r %s', u.email.split('@')[0] + 'pass', u.email)
