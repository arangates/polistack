from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Used to create a superuser.'

    def add_arguments(self, parser):
        parser.add_argument('email')
        parser.add_argument('password')

    def handle(self, *args, **options):
        get_user_model().objects.create_superuser(
            **{'email': options['email'], 'password': options['password']})
