from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.core.management.base import BaseCommand

from utils import dotdict, log

User = get_user_model()


class Command(BaseCommand):
    help = """
    rb.manage send_emails bartek michał cyk=wiór
    """

    def add_arguments(self, parser):
        parser.add_argument('from_email')
        parser.add_argument('recipient_list', help='comma separated')
        parser.add_argument('-s', '--subject', default='Subject Line.')
        parser.add_argument('-m', '--message', default="Here's the message")
        # parser.add_argument('context', nargs='*')

    def handle(self, *args, **options):
        options = dotdict(options)
        log.debug(options)
        log.debug(settings.CELERY_BROKER_URL)
        log.debug(settings.EMAIL_CONFIG)

        send_mail(
            options.subject,
            options.message,
            options.from_email,
            options.recipient_list.split(','),
            fail_silently=False,
        )
