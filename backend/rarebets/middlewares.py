from django.conf import settings
from django.contrib import auth
from django.utils.deprecation import MiddlewareMixin


class AutoLoginAdmin(MiddlewareMixin):
    """
    https://gist.github.com/hgdeoro/3336947

    Makes my life easier in development
    """
    if not settings.DEBUG:
        raise RuntimeError("Don't!")

    def process_request(self, request):
        user = auth.authenticate(username='admin', password='admin')
        if user:
            request.user = user
            auth.login(request, user)
