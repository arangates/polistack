import os


def env(_):
    return {
        # set in deploy, so only present on staging
        'commit': os.getenv('CI_COMMIT_TAG', '-')[:8]
    }
