from rest_framework import serializers


class ContactDataSerializer(serializers.Serializer):
    email = serializers.EmailField()
    phone = serializers.CharField(required=False, default="Not Provided")
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    message = serializers.CharField()
