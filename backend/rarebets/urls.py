import sys

from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.http import HttpResponse
from django.urls import include, path
from djoser.views import UserView

from .views import contact_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', lambda request: HttpResponse('Yes, master?\n')),
    url(r'^auth/', include('djoser.urls')),
    url(r'^auth/', include('users.urls.jwt')),
    url(r'^contact/', contact_view),
    url(r'^auth/users/update/$', UserView.as_view()),  # Add here cause it is not provided via djoser by default
    url(r'^', include('bets.urls')),
    url(r'^orderbooks/', include('orderbooks.urls')),
    url(r'^users/', include('users.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += [url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))]
    try:
        import debug_toolbar
        urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls))]
    except ImportError:
        sys.stdout.write('no debug toolbar')
