from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf.urls import url

from notifications.consumers import NotificationConsumer
from notifications.middlewares import WebsocketJWTAuthMiddleware

application = ProtocolTypeRouter({
    "websocket": WebsocketJWTAuthMiddleware(
        URLRouter([
            url("^notifications/$", NotificationConsumer),
        ])
    ),
})
