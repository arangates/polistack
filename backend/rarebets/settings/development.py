from django.db import models
from prettyrepr import django_repr_color_indent

from .base import *

INSTALLED_APPS += [
    'django_extensions',
    'debug_toolbar',
]
MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]
ALLOWED_HOSTS = (
    'backend.rarebets.dev', 'localhost'
)
DEBUG = TEMPLATE_DEBUG = True
AUTH_PASSWORD_VALIDATORS = []

CORS_ORIGIN_WHITELIST = (
    'localhost:3000',
)

models.Model.__repr__ = django_repr_color_indent
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'color': {
            '()': 'colorlog.ColoredFormatter',
            'format': "%(log_color)s%(levelname)-8s%(reset)s %(green)s%(message)s"
        },
        'color-verbose': {
            '()': 'colorlog.ColoredFormatter',
            'format': "%(log_color)s%(levelname)-8s%(reset)s %(filename)s:%(lineno)d#%(funcName)s %(message)s"
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
        'color': {
            'class': 'logging.StreamHandler',
            'formatter': 'color'
        },
        'color-verbose': {
            'class': 'logging.StreamHandler',
            'formatter': 'color-verbose'
        },
    },
    'loggers': {
        'django.db': {
            'handlers': ['color'],
            # 'level': 'DEBUG',
            'level': 'INFO',
        },
        'global': {
            'handlers': ['color-verbose'],
            'level': 'INFO',
        },
        'celery': {
            'handlers': ['color-verbose'],
            'level': 'DEBUG',
        },
        'kombu': {
            'handlers': ['color-verbose'],
            'level': 'DEBUG',
        },

    },
}
LOGGING_DEBUG = False
SHELL_PLUS_POST_IMPORTS = (
    # examples:
    # ('module.submodule1', ('class1', 'function2')),
    # ('module.submodule2', 'function3'),
    # ('module.submodule3', '*'),
    ('django.utils.translation', ('activate', 'ugettext')),
    ('django.core.mail', 'send_mail'),
    ('django.shortcuts', '*'),
    ('django.db.models', '*'),
    ('faker', '*'),
    ('bets.factories', '*'),
    ('bets.serializers', '*'),
    ('bets.views', '*'),
    ('django.db.models.functions.base', '*'),
)

# JWT
JWT_AUTH.update({
    'JWT_VERIFY_EXPIRATION': False,
})


# To test actual delivery:
# - set vars:
#   in dc
#   EMAIL_URL: "smtp+tls://rarebets@officehawk.com:dU97MNRwQdpzORn7Q8m2@mail.officehawk.com:587"
#   locally
#   CELERY_BROKER_URL=amqp://guest:guest@0.0.0.0:15672/
# - docker-compose up celery
# - ./manage.py test_emails szatan@mg.brak.me bartek.rychlicki@gmail.com
# - uncomment EMAIL_BACKEND
# both api and celery containers must be able to find it (have CELERY_BROKER_URL set, but different)
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'


# maybe not needed
CELERY_TASK_ALWAYS_EAGER = CELERY_ALWAYS_EAGER = True
DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    # 'debug_toolbar.panels.timer.TimerPanel',
    # 'debug_toolbar.panels.templates.TemplatesPanel',
    # 'debug_toolbar.panels.settings.SettingsPanel',
    # 'debug_toolbar.panels.headers.HeadersPanel',
    # 'debug_toolbar.panels.request.RequestPanel',
    # 'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    # 'debug_toolbar.panels.profiling.ProfilingPanel',
]
DEBUG_TOOLBAR_CONFIG = {
    'SHOW_COLLAPSED': True,
    # The SQL panel highlights queries that took more that this amount of time, in milliseconds, to execute.
    'SQL_WARNING_THRESHOLD': 500
}

INTERNAL_IPS = ['127.0.0.1']
DOMAIN = 'localhost:3000'
SITE_NAME = 'Polistack Local'
