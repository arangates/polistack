import datetime
import decimal
from decimal import Decimal
import os

from django.utils.translation import ugettext_lazy as _
import environ

# affect globally
# I got scared about precision of Decimals initialized from floats, read the
# documentation, discussed with colleagues and to disallow float source, look:
#
# In [3]: Decimal(1.1)
# Out[3]: Decimal('1.100000000000000088817841970012523233890533447265625')
#
# In [4]: Decimal(1.1) == Decimal('1.1')
# Out[4]: False
decimal.getcontext().traps[decimal.FloatOperation] = True
decimal.getcontext().rounding = decimal.ROUND_HALF_UP


this = environ.Path(__file__)
repo = this - 4
backend = this - 3
rarebets = this - 2
env = environ.Env(DEBUG=(bool, False), )
environ.Env.read_env(repo('.env'))
SITE_ROOT = rarebets()

SECRET_KEY = env('SECRET_KEY', default='insecure')

TEMPLATE_DEBUG = DEBUG = env('DEBUG')

DATABASES = {
    'default': env.db(default='postgresql:///rarebets'),
    'extra': env.db('SQLITE_URL', default='sqlite:///db.sqlite3')
}
DATABASES['default']['ATOMIC_REQUESTS'] = True

MEDIA_ROOT = rarebets('media_root')
MEDIA_URL = '/media/'
STATIC_ROOT = rarebets('static_root')
STATIC_URL = '/static/'

DOMAIN_NAME = env('VIRTUAL_HOST', default='localhost:8000')

# CACHES = {
#     'default': env.cache(),
#     'redis': env.cache('REDIS_URL')
# }

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'django_object_actions',
    'django.contrib.admin',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # third party
    'rest_framework',
    'corsheaders',
    'django_filters',
    'djoser',
    'djcelery_email',
    'channels',
    # our
    'rarebets',  # for generic management commands
    'bets',
    'users',
    'notifications',
    'orderbooks'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'rarebets.urls'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [rarebets('templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # 'django.template.context_processors.debug',
                # 'django.template.context_processors.request',
                # ?: (admin.E402) 'django.contrib.auth.context_processors.auth'
                # must be in TEMPLATES in order to use the admin application.
                'django.contrib.auth.context_processors.auth',
                # used in bet admin
                'django.contrib.messages.context_processors.messages',
                # show deploy commit on contabo
                'rarebets.context_processors.env',
            ],
        },
    },
]

WSGI_APPLICATION = 'rarebets.wsgi.application'

AUTH_USER_MODEL = 'users.User'
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
LOCALE_PATHS = ['locale/']
LANGUAGES = [('en', _('English'))]

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.DjangoModelPermissions',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # front-end
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',

        'rest_framework.authentication.SessionAuthentication',
        # why ?
        'rest_framework.authentication.BasicAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 20,
}

DJOSER = {
    'PASSWORD_RESET_CONFIRM_URL': 'password/reset/confirm/{uid}/{token}',
    'ACTIVATION_URL': 'activate/{uid}/{token}',
    'SEND_ACTIVATION_EMAIL': True,
    'SERIALIZERS': {
        'user': 'users.serializers.UserSerializer',
    },
    'PASSWORD_RESET_CONFIRM_RETYPE': True,
}

# http://getblimp.github.io/django-rest-framework-jwt/#additional-settings
JWT_AUTH = {
    'JWT_ALLOW_REFRESH': True,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=15),
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=30),
}

CURRENCY_SIGN = '$'

# CHANNELS
BROADCAST_CHANNEL_NAME = 'broadcast'

# change to env(
redis_host = os.environ.get('REDIS_HOST', 'localhost')

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            'hosts': [(redis_host, 6379)],
        },
    },
}
ASGI_APPLICATION = 'rarebets.routing.application'

# Does not send anything, but serializes tasks to be used in Celery container
EMAIL_CONFIG = env.email_url(
    'EMAIL_URL', default='smtp://user@:password@localhost:25', backend='djcelery_email.backends.CeleryEmailBackend')

CELERY_BROKER_URL = env('CELERY_BROKER_URL', default='amqp://guest:guest@localhost:5672/')
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

vars().update(EMAIL_CONFIG)

# LOGGING
LOGGING_DEBUG = False

DEFAULT_FEE = Decimal("0.1")

# GEOIP2
GEOIP_PATH = rarebets('geoip2')  # Path to geoip2 database
