from .base import *  # noqa

ALLOWED_HOSTS = (
    'api.polistack.com',
)

CORS_ORIGIN_WHITELIST = (
    'polistack.com',
)

RAVEN_CONFIG = {
    'dsn': 'https://06c5338e6ac946d68c252432fe48b71f:0261f1c73ff747f296a5ef505dbd86b7@sentry.softheart.io/6',
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': RAVEN_CONFIG['dsn'],
        },
    },
    'loggers': {
        '': {
            'handlers': ['console', 'sentry'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}


STATIC_ROOT = '/var/static'
MEDIA_ROOT = '/var/media/pictures'
MEDIA_URL = '/media/pictures/'

DOMAIN = 'polistack.com'
SITE_NAME = 'Polistack'

EMAIL_HOST = 'smtp.eu.mailgun.org'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'postmaster@mail.polistack.com'
EMAIL_HOST_PASSWORD = os.environ['DEFAULT_EMAIL_PASSWORD']
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'noreply@polistack.com'
