import os
from .base import *  # noqa

DEBUG = True

ALLOWED_HOSTS = (
    'rbapi',
    'api.polistack.softheart.io',
)
CORS_ORIGIN_WHITELIST = (
    'polistack.softheart.io',
)

RAVEN_CONFIG = {
    'dsn': 'https://348dd54600a94325b9f19a2d9e9a2a40:649eb4695c5641aa98b3a799264104d0@sentry.softheart.io/5',
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': RAVEN_CONFIG['dsn'],
        },
    },
    'loggers': {
        '': {
            'handlers': ['console', 'sentry'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}


STATIC_ROOT = '/var/static'
MEDIA_ROOT = '/var/media/pictures'

DOMAIN = 'polistack.softheart.io'
SITE_NAME = 'Polistack'

EMAIL_HOST = 'smtp.eu.mailgun.org'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'postmaster@mail.polistack.com'
EMAIL_HOST_PASSWORD = os.environ['DEFAULT_EMAIL_PASSWORD']
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'noreply@polistack.com'
