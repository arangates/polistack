from celery import Celery
from django.conf import settings
import environ

from utils import log

this = environ.Path(__file__)
repo = this - 3
env = environ.Env(DEBUG=(bool, False),)

app = Celery(broker=env('CELERY_BROKER_URL'))
log.debug('CELERY_BROKER_URL', env('CELERY_BROKER_URL'))
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(settings.INSTALLED_APPS)


if __name__ == '__main__':
    app.start()
