#!/usr/bin/env python
import os
import sys
from time import time

start = time()

if __name__ == "__main__":
    # .bashrc: 'export DJANGO_USER_SETTINGS=bartek'
    user_settings = os.getenv('DJANGO_USER_SETTINGS')
    if user_settings:
        user_settings = 'rarebets.settings.' + user_settings
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', user_settings or 'rarebets.settings.development')
    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)

end = time()
sys.stdout.write('Δ %.2fs\n' % (end - start))
