"""
These are not django settings MIDDLEWARE classes,
used in rarebets.routing only.
"""
import re
from typing import Tuple

from django.contrib.auth import get_user_model
from rest_framework_jwt.authentication import BaseJSONWebTokenAuthentication

User = get_user_model()


class WebsocketJSONTokenAuthentication(BaseJSONWebTokenAuthentication):
    # TODO Move token from query params
    QUERY_PARAM_TOKEN_REGEX = b'^jwt=[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$'

    def authenticate(self, query_params: bytes) -> Tuple[User, str]:
        """
        Signature of this function changed. Now authenticate takes query parameters instead of request object.
        It is okay to pass query parameters to super() cause we overrode get_jwt_value
        which is the only function that uses request object in Ancestors
        """
        return super().authenticate(query_params)

    def get_jwt_value(self, query_params: bytes) -> bytes:
        jwt_token = re.search(self.QUERY_PARAM_TOKEN_REGEX, query_params)
        if jwt_token:
            return jwt_token.group().split(b'=')[1]


class WebsocketJWTAuthMiddleware:
    """
    Build in Channels AuthMiddlewares supports only default django authentication.
    """

    def __init__(self, inner):
        # Store the ASGI application we were passed
        self.inner = inner

    def __call__(self, scope):
        """
        If Authentication is successful then add to scope appropriate user.
        """
        # TODO Handle 500
        authenticator = WebsocketJSONTokenAuthentication()
        user, jwt_value = authenticator.authenticate(scope['query_string'])

        scope['user'] = user
        return self.inner(scope)
