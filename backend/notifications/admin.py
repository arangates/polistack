from django.contrib import admin

from notifications.models import Notification


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = (
        'created_at_formatted',
        'name',
        'details',
        'seen',
        'user',
    )

    def created_at_formatted(self, transaction):
        return transaction.created_at.strftime("%y.%m.%d  %H:%M:%S")

    created_at_formatted.short_description = 'time'
