from django.apps import AppConfig

from utils import log


class NotificationsConfig(AppConfig):
    name = 'notifications'

    def ready(self):
        import notifications.signals  # noqa
        log.debug('%r loaded', notifications.signals)
