# TODO Some better module name?
# TODO make readme and docstrings
import json

from channels.generic.websocket import WebsocketConsumer
from django.contrib.auth import get_user_model
from django.db.models import QuerySet
from rest_framework.serializers import Serializer
from rest_framework.settings import api_settings

from bets.constants import STATUS
from notifications.models import Notification

User = get_user_model()


class DataStructureError(ValueError):
    pass


class ActionDrivenWebsocketConsumer(WebsocketConsumer):
    """
    This class extends default WebsockerConsumer adding following functionality:
    1. Restricts incoming and outgoing data structure: { 'action': 'some string', 'data': [list of objects (dicts)] }
    2. Calls custom action that is passed in 'action' which is defacto a name of method defined in class eg.
    - Incoming data:{ 'action': 'foo' , [ some data ] } will call method 'foo' on self with 'data' passed as an argument
    """

    def _deserialize(self, text_data: str) -> tuple:
        """
        Deserializes incoming data.
        """
        request_data = self.decode_json(text_data)

        action = request_data.get('action', None)
        if not action:
            raise DataStructureError()

        data = request_data.get('data', None)

        return action, data

    def receive(self, text_data=None, bytes_data=None, **kwargs):
        """
        Receive data -> deserialize -> call pre function -> call action -> call post function
        """
        if text_data:
            try:
                action, data = self._deserialize(text_data)
            except DataStructureError:
                self.send_error({'detail': 'Bad data structure.'})
                return

            # Added this for user convenience
            self.pre_action(data, **kwargs)

            handler = getattr(self, action, None)
            if handler:
                handler(data)
            else:
                self.send_error({'detail': f'No {action} action found.'})

            # Added this for user convenience
            self.post_action(data, **kwargs)
        else:
            raise ValueError("No text section for incoming WebSocket frame!")

    def post_action(self, content: dict, **kwargs):
        """
        Called with decoded JSON content after calling action.
        """
        pass

    def pre_action(self, data: dict, **kwargs):
        """
        Called with decoded JSON content before calling action.
        """
        pass

    def send_error(self, data: dict, **kwargs):
        self.send_data('Error', data, **kwargs)

    def send_data(self, action: str, data: dict, close=False):
        """
        Encode the given content as JSON and send it to the client.
        """
        super().send(
            text_data=self.encode_json({'action': action, 'data': data}),
            close=close,
        )

    def send_model_changed_notification(self, data: dict, close=False):
        """
        Encode the given content as JSON and send it to the client.
        """
        super().send(
            text_data=self.encode_json(data.get('text')),
            close=close,
        )

    @classmethod
    def decode_json(cls, text_data: str) -> dict:
        return json.loads(text_data)

    @classmethod
    def encode_json(cls, content: dict) -> str:
        return json.dumps(content)


class SerializingWebsocketConsumerMixin:
    """
    This mixin provides some standard rest framework functions for user convenience.
    GenericAPIView: get_queryset, get_serializer, get_serializer_class, get_serializer_context, filter_queryset
    """
    queryset = None
    serializer_class = None
    filter_backends = api_settings.DEFAULT_FILTER_BACKENDS

    def get_queryset(self) -> QuerySet:
        assert self.queryset is not None, (
            "'%s' should either include a `queryset` attribute, "
            "or override the `get_queryset()` method."
            % self.__class__.__name__
        )

        queryset = self.queryset
        if isinstance(queryset, QuerySet):
            # Ensure queryset is re-evaluated on each request.
            queryset = queryset.all()
        return queryset

    def get_serializer(self, *args, **kwargs) -> Serializer:
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def get_serializer_class(self):
        assert self.serializer_class is not None, (
            "'%s' should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method."
            % self.__class__.__name__
        )

        return self.serializer_class

    def get_serializer_context(self) -> dict:
        return {
            'scope': self.scope,
            'consumer': self
        }

    def filter_queryset(self, queryset) -> QuerySet:
        for backend in list(self.filter_backends):
            queryset = backend().filter_queryset(self.scope, queryset, self)
        return queryset


class BetNotifier:
    BET_ACCEPTED = 'Bet accepted', 'Your bet has been accepted by the administrator'
    BET_REPHRAZED = 'Bet rephrased by moderator', 'Your bet has been rephrased by the administrator'
    BET_REJECTED = 'Bet rejected', 'Your bet has been rejected by the administrator'
    BET_RESOLVED = 'Bet resolved', 'Your bet has been resolved'

    def __init__(self, old_bet, current_bet):
        self.old_bet = old_bet
        self.current_bet = current_bet

    def get_notification_data(self) -> tuple:
        if self.old_bet.status == STATUS.PENDING and self.current_bet.status == STATUS.ACTIVE:
            return self.BET_ACCEPTED
        elif self.old_bet.status == STATUS.ACTIVE and self.current_bet.status == STATUS.ENDED:
            return self.BET_RESOLVED
        else:
            return None, None

    def notify(self):
        name, details = self.get_notification_data()
        if name and details:
            return  # TODO Fix
            for user in self.current_bet.users.distinct():
                Notification.objects.create(content_object=self.current_bet, name=name, details=details, user=user)
