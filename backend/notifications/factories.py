from django.contrib.contenttypes.models import ContentType
import factory

from bets.factories import BetFactory
from notifications.models import Notification
from users.factories import UserFactory
from utils.factories import CreatedAtMixin


class NotificationItemFactory(CreatedAtMixin, factory.DjangoModelFactory):
    object_id = factory.SelfAttribute('content_object.id')
    content_type = factory.LazyAttribute(
        lambda o: ContentType.objects.get_for_model(o.content_object))

    class Meta:
        exclude = ['content_object']
        abstract = True


class NotificationBetFactory(NotificationItemFactory):
    content_object = factory.SubFactory(BetFactory)
    name = factory.Faker('name')
    details = factory.Faker('bs')
    created_at = factory.Faker('date_time')
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Notification


class UserWithBetNotificationFactory(UserFactory):
    notifications = factory.RelatedFactory(NotificationBetFactory, 'user')
