from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from utils.models import OrderedTimestampModel

User = get_user_model()


# TODO Make periodically cleanup
class Notification(OrderedTimestampModel):
    name = models.CharField(max_length=30)
    details = models.TextField(blank=True, null=True)
    seen = models.DateTimeField(blank=True, null=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='notifications')

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
