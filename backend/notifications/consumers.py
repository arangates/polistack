from asgiref.sync import async_to_sync
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils import timezone

from notifications.models import Notification
from notifications.serializers import NotificationSerializer
from notifications.websockets import ActionDrivenWebsocketConsumer, SerializingWebsocketConsumerMixin

User = get_user_model()


# TODO HANDLE MULTIPLE DEVICES
# TODO prefix all routes with /ws/ see documentation for more info
# TODO Add origin validator
class NotificationConsumer(ActionDrivenWebsocketConsumer, SerializingWebsocketConsumerMixin):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer

    def connect(self):
        """
        Handle incoming user connection.
        Retrieves all not seen notification by user and sends it to front.
        """
        self.accept()
        pending_notifications = self.get_queryset().filter(user=self.scope['user'])
        serializer = self.get_serializer(list(pending_notifications), many=True)
        self.send_data('pending', serializer.data)

        self.scope['user'].channel_name = self.channel_name
        self.scope['user'].save()

        # Add user to broadcast. This lets us send notifications to all connected users
        async_to_sync(self.channel_layer.group_add)(settings.BROADCAST_CHANNEL_NAME, self.channel_name)

    def disconnect(self, code):
        """
        Makes some cleanup. Removes disconnected user from groups.
        """
        self.scope['user'].channel_name = None
        self.scope['user'].save()
        async_to_sync(self.channel_layer.group_discard)(settings.BROADCAST_CHANNEL_NAME, self.channel_name)

    def see(self, data: dict = None):
        """
        When frontend sends to us appropriate signal we mark appropriate Notifications as seen.
        """
        self.scope['user'].notifications.update(seen=timezone.now())

# curl --include --no-buffer \
#   --header "GET ws://localhost:8000/notifications/ HTTP/1.1" \
#   --header "Host: localhost:8000" \
#   --header "Connection: Upgrade" \
#   --header "Pragma: no-cache" \
#   --header "Cache-Control: no-cache" \
#   --header "Upgrade: websocket" \
#   --header "Origin: chrome-extension://pfdhoblngboilpfeibdedpjgfnlcodoo" \
#   --header "Sec-WebSocket-Version: 13" \
#   --header "User-Agent: koza" \
#   --header "Accept-Encoding: gzip, deflate, br" \
#   --header "Accept-Language: en-US,en;q=0.9,pl;q=0.8" \
#   --header "Cookie: csrftoken=U0e3RlkW4PXzW3DzWJPzJ19FyMgiGjLMIskCMY3s5MAZdZTmS5yTSkYtWuIJ78mX; tabstyle=html-tab" \
#   --header "Sec-WebSocket-Key: bVdkQCMNK+z/i2Nbpa4eIQ==" \
#   --header "Sec-WebSocket-Extensions: permessage-deflate; client_max_window_bits"
