from rest_framework import serializers

from bets.models import Bet
from bets.serializers import BetSerializer
from notifications.models import Notification


class NotificationObjectRelatedField(serializers.RelatedField):
    """
    A custom field to use for the `notification_object` generic relationship.
    http://www.django-rest-framework.org/api-guide/relations/#generic-relationships
    """

    def to_representation(self, value):
        """
        Serialize bet instances using a bet serializer.
        """
        if isinstance(value, Bet):
            serializer = BetSerializer(value)
        else:
            raise Exception('Unexpected type of tagged object')

        return serializer.data


class NotificationSerializer(serializers.ModelSerializer):
    content_object = NotificationObjectRelatedField(read_only=True)

    class Meta:
        model = Notification
        fields = (
            'id',
            'seen',
            'name',
            'details',
            'created_at',
            'content_object',
        )
