from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models.signals import post_save
from django.dispatch import receiver

from notifications.models import Notification
from notifications.serializers import NotificationSerializer

channel_layer = get_channel_layer()


@receiver(post_save, sender=Notification)
def post_save_notification_handler(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        # If user has channel name (is currently connected) then send notification
        if instance.user.channel_name:
            watched_model_class_name = instance.content_object.__class__.__name__
            async_to_sync(channel_layer.send)(
                instance.user.channel_name,
                {
                    'type': 'send.model.changed.notification',
                    'text': {
                        'action': watched_model_class_name,
                        'data': [NotificationSerializer(instance=instance).data]
                    }
                }
            )
