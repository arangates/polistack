from setuptools import find_packages, setup

# this file is here only to make it possible to `pip install -e backend`
# so that PYTHONPATH includes the modules and pytest can be run from
# anywhere
# don't change it ever, it's not used in production

setup(
    name='rarebets',
    version='0',
    packages=find_packages(),
)
