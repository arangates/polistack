#!/usr/bin/env python3
"""
python3.6

This script has lax approach to variable scope, relying on closures, to pass
global configuration (glob) around. Don't complain.

Changelog:
2018-05-23: end of alpha stage, cleaned and made to shine, a bit
- add colours class
- add very very very nice markers colours
- clean scope a bit
- add step deltas
- unicode icons
"""
from argparse import ArgumentParser
from collections import namedtuple
import configparser
import functools
import logging
import os
from pprint import pformat
import re
import subprocess
import sys
from time import time
from typing import Match, Optional

from binaryornot.check import is_binary
from flake8.main.git import find_modified_files
from flake8.main.git import hook as flake8_hook
from isort.hooks import git_hook as isort_hook

CMD_NOT_FOUND_MSG = 'No such file or directory'
CMD_NOT_FOUND_RC = -100


Result = namedtuple('Result', ['out', 'rc'])


class C:
    # https://misc.flogisoft.com/bash/tip_colors_and_formatting
    R = '\033[0m'
    red = '\033[31m'
    green = '\033[32m'
    yellow = '\033[33m'
    blue = '\033[34m'
    magenta = '\033[35m'
    cyan = '\033[36m'
    light_gray = '\033[37m'
    dark_gray = '\033[90m'
    light_red = '\033[91m'
    light_green = '\033[92m'
    light_yellow = '\033[93m'
    light_blue = '\033[94m'
    light_magenta = '\033[95m'
    light_cyan = '\033[96m'
    RED = '\033[41m'
    GREEN = '\033[42m'
    YELLOW = '\033[43m'
    BLUE = '\033[44m'
    MAGENTA = '\033[45m'
    CYAN = '\033[46m'
    LIGHT_RED = '\033[101m'
    LIGHT_GREEN = '\033[102m'
    LIGHT_YELLOW = '\033[103m'
    LIGHT_BLUE = '\033[104m'
    LIGHT_MAGENTA = '\033[105m'
    LIGHT_CYAN = '\033[106m'


def step(func):
    # collect step functions, run conditionally
    name = func.__name__
    step.collect.append(name)

    def inner(*args_, **kwargs):
        # either of lists defined and contains or both empty
        if (
            (glob.skip and name not in glob.skip)
            or (glob.only and name in glob.only)
            or (not glob.skip and not glob.only)
        ):
            title(name)
            start = time()
            _fails, _warnings = func(*args_, **kwargs)
            done = time() - start
            fails[name] = _fails
            warnings[name] = _warnings
            title(f'{done:.2f}s {name} fails:{_fails} warnings:{_warnings}', 'Δ')
        else:
            debug(name, 'skipped')
    return inner


step.collect = []


def set_logging():
    if glob.debug:
        # flake has nice logs, useful for getting setup.cfg right, isort doesn't have logs
        log = logging.getLogger('flake8.options.config')
        log.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter(f'{C.yellow}%(name)s %(message)s{C.R}', datefmt='%H:%M:%S')
        ch.setFormatter(formatter)
        log.addHandler(ch)


def colour(colour_label: str, msg: str):
    if msg:
        sys.stdout.write(f'{colour_label}{msg}{C.R}\n')


def debug(*msgs, c=C.yellow):
    # takes strings or callables to save time if not DEBUG
    if glob.debug:
        colour(c, ' '.join(str(x()) if callable(x) else str(x) for x in msgs))


def git_available():
    return os.path.exists('.git')


@functools.lru_cache()
def run(cmd: str, fine_if_command_not_found=False, do_print=False, **kwargs) -> Result:
    # pass shell=True to use bash globs, pipes and other builtins
    # TODO: fine_if_command_not_found logic is getting out of hand
    try:
        finished = subprocess.run(
            cmd if kwargs.get('shell', False) else cmd.split(),
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, **kwargs
        )
        out, rc = finished.stdout.decode('utf-8'), finished.returncode
    except Exception as e:
        # won't raise if shell=True
        out, rc = str(e), CMD_NOT_FOUND_RC
        if fine_if_command_not_found and CMD_NOT_FOUND_MSG in str(e):
            rc = 0
    if CMD_NOT_FOUND_MSG in str(out):
        rc = CMD_NOT_FOUND_RC
        if fine_if_command_not_found:
            rc = 0

    debug('run', cmd, kwargs, out.strip(), rc, c='blue')
    if do_print:
        sys.stdout.write(out)
    return Result(out, rc)


def generic_debug_info():
    debug('in:', __file__, c='magenta')
    debug('repo exists:', lambda: git_available())
    debug('cwd:', lambda: os.getcwd())
    debug('last commit:', lambda: run('git rev-parse --short=8 HEAD~1').out)
    debug('branch_name:', lambda: repr(run('git rev-parse --abbrev-ref HEAD').out.rstrip()))


def title(msg: str, char='\n✊'):
    # http://www.fileformat.info/info/unicode/block/block_elements/list.htm
    # columns = int(os.popen('stty size', 'r').read().split()[1])
    # sys.stdout.write(f'\n{char * columns}\r\t {msg} \n')
    sys.stdout.write(f'{char} {msg} \n')


@step
def frontend_lint():
    y = run('yarn lint', cwd='frontend', fine_if_command_not_found=True)
    sys.stdout.write(y.out)
    return y.rc, 0


@step
def backend_isort():
    if git_available():
        return isort_hook(strict=True), 0
    else:
        i = run('isort -c -rc backend *.py', shell=True)
        sys.stdout.write(i.out)
        return len(i.out.rstrip().split('\n')) if i.rc else i.rc


@step
def backend_flake8():
    if git_available():
        return flake8_hook(strict=True), 0
    else:
        f = run('flake8 backend *.py', shell=True)
        sys.stdout.write(f.out)
        return len(f.out.rstrip().split('\n')) if f.rc else f.rc, 0


@step
def run_backend_tests():
    tests = run('pytest backend --create-db')
    if tests.rc != 0:
        sys.stdout.write(tests.out)
    return tests.rc, 0


@step
def yamllint():
    lint = run(
        'find -name \*yml -not -path "./frontend/*" | xargs yamllint',
        fine_if_command_not_found=True,
        shell=True,
    )
    if CMD_NOT_FOUND_MSG in lint.out:
        # TODO: add yamllint to containers running the hook (none as of 2018-05-08)
        colour(C.yellow, 'yamllint not found')
        return 0, 1
    ret = 0
    out = lint.out.rstrip()
    if out:
        sys.stdout.write(lint.out)
        ret = len([line for line in out.split('\n') if ':' in line])
    return ret, 0


def validate_branch(branch_name_) -> Optional[Match]:
    return re.match(valid_branch_name.format(jira_project_symbol=glob.jira_project_symbol), branch_name_)


prod = [line.rstrip() for line in open('backend/requirements.txt').readlines() if not line.startswith('#')]
dev = [line.rstrip() for line in open('backend/requirements_dev.txt').readlines() if not line.startswith('#')]


@step
def make_sure_dev_requirements_contain_prod_ones():
    debug('prod', pformat(prod))
    debug('dev', pformat(dev))
    debug('not in prod', set(dev) - set(prod))
    debug('not in dev', pformat(set(prod) - set(dev)))
    prod_requirements_in_dev = int(not set(dev).issuperset(set(prod)))
    if prod_requirements_in_dev:
        sys.stdout.write('not in prod: %s' % pformat(set(prod) - set(dev)))
    return prod_requirements_in_dev, 0


INVALID_PATTERNS = re.compile(
    '\n([^#].*'
    '('
    '<<<<<<< '  # noqa
    '|======= '  # noqa
    '|=======\n'
    '|>>>>>>> '  # noqa
    '|[\n ]print\('
    '|dupa'  # noqa
    '|chuj'  # noqa
    '|kurwa'  # noqa
    '|jebać'  # noqa
    '|shit'  # noqa
    '|#, fuzzy'  # noqa
    '|\bDecimal\([0-9.]+\)'  # noqa
    '|console.log'  # noqa
    ').*'
    ')'
)


@step
def detect_invalid_patterns(modified_files):
    debug('modified_files', modified_files)
    count = 0
    for filename in modified_files:
        if is_binary(filename):
            continue
        with open(filename, 'r', encoding='utf8') as input_file:
            read = input_file.read()
            for m in INVALID_PATTERNS.finditer(read):
                line, pattern = m.groups()
                if '# noqa' in line:
                    continue
                count += 1
                line_no = read[:m.start()].count('\n') + 2
                sys.stdout.write('%s:%s :\n' % (filename, line_no))
                sys.stdout.write(f'{C.LIGHT_YELLOW}{pattern}{C.R}'.join(line.split(pattern)).replace('\n', '') + '\n')
    return count, 0


@step
def prod_requirements_are_sorted():
    return int(not prod == sorted(prod, key=lambda s: s.lower())), 0


@step
def dev_requirements_are_sorted():
    return int(not dev == sorted(dev, key=lambda s: s.lower())), 0


@step
def dev_requirements_are_pinned():
    dev_not_pinned = [l for l in dev if not l.startswith('#') and '==' not in l and l]
    debug('dev_not_pinned', dev_not_pinned)
    if dev_not_pinned:
        sys.stdout.write('prod_not_pinned %r\n' % dev_not_pinned)
    return len(dev_not_pinned), 0


@step
def prod_requirements_are_pinned():
    prod_not_pinned = [l for l in prod if not l.startswith('#') and '==' not in l and l]
    debug('prod_not_pinned', prod_not_pinned)
    if prod_not_pinned:
        sys.stdout.write('prod_not_pinned %r\n' % prod_not_pinned)
    return len(prod_not_pinned), 0


@step
def find_markers():
    MARKERS_RE = r"^(?P<file>.+):\s*(?P<rest>.*?(?P<marker>TODO|HACK|EXPLAIN|REMOVE|THINK|@[A-Z][a-z]+: ):?.*)$"
    colour_map = {
        'TODO': C.yellow, 'HACK': C.red, 'EXPLAIN': C.cyan,
        'REMOVE': C.blue, 'THINK': C.magenta
    }

    def _colour_match(m):
        g = m.groupdict()
        debug(g, __file__)
        if g['file'].startswith(os.path.basename(__file__)):
            # TODO: produces empty lines
            return '.'
        if g['marker'] in colour_map:
            return f'{colour_map[g["marker"]]}{g["marker"]:10.10}{C.R} {g["file"]} {C.dark_gray}{g["rest"]}{C.R}'
        elif g['marker'].startswith('@'):
            return f'{C.light_green}{g["marker"]:10.10}{C.R} {g["file"]} {C.dark_gray}{g["rest"]}{C.R}'
        return f'{C.CYAN}{g["marker"]:10.10}{C.R} {g["file"]} {C.dark_gray}{g["rest"]}{C.R}'

    markers = run(
        f'''
        egrep --binary-files=without-match --recursive --line-number \
        --exclude-dir=node_modules --exclude-dir=build --exclude-dir=.git \
        --exclude-dir=.idea --exclude-dir=dist --exclude=\*pyc \
        "TODO|HACK|EXPLAIN|REMOVE|THINK|@[A-Z][a-z]+: "
        ''',
        shell=True,
    )
    sys.stdout.write(re.sub(MARKERS_RE, _colour_match, markers.out, flags=re.MULTILINE))
    return 0, len(markers.out.strip().split('\n'))


def hook():
    generic_debug_info()
    debug('all_files', glob.all_files)
    if git_available() and not glob.all_files:
        title('branch name analysis')
        branch_name = run('git rev-parse --abbrev-ref HEAD').out.rstrip()
        valid_branch = validate_branch(branch_name)
        if not valid_branch:
            colour(C.RED, f'Branch {branch_name} invalid.')
            sys.stdout.write(f'Follow {valid_branch_name}, eg:\n')
            sys.stdout.write(f'- backend/{glob.jira_project_symbol}-47/basic_e2e_tests\n')
            sys.stdout.write(f'- frontend/{glob.jira_project_symbol}-41/forms_made_proper\n')
            # TODO temporarily disabled so rebase can work, will solve
            warnings['branch'] = 1
        else:
            branch_info = valid_branch.groupdict()
            colour(C.green, f'Branch {branch_info["branch_name"]!r} valid: {branch_info}.')

            if branch_info['front_or_back'] == 'frontend':
                frontend_lint()
            elif branch_info['front_or_back'] == 'backend':
                backend_flake8()
                backend_isort()
                run_backend_tests()
            else:
                frontend_lint()
                backend_flake8()
                backend_isort()
    else:
        # on master, in CI
        frontend_lint()
        backend_flake8()
        backend_isort()

    yamllint()
    make_sure_dev_requirements_contain_prod_ones()
    files = (
        set(run('git ls-files').out.strip().split('\n')) if glob.all_files
        else set(find_modified_files(True))
    ) - glob.excluded_files
    detect_invalid_patterns(files)
    prod_requirements_are_pinned()
    # find patterns and highlight separately
    # https://stackoverflow.com/a/981831/1472229
    find_markers()

    debug('fails', fails)
    debug('warnings', warnings)
    any_fails = {k: v for k, v in fails.items() if v}
    colour(C.light_red if any_fails else C.green, 'fails: %s' % pformat(any_fails or None))
    any_warnings = {k: v for k, v in warnings.items() if v}
    colour(C.light_yellow if any_warnings else C.green, 'warnings: %s' % pformat(any_warnings or None))
    return 0


def parse_args():
    cfg_parser = configparser.ConfigParser(strict=False)
    with open('setup.cfg') as setup_cfg:
        cfg_parser.read_file(setup_cfg)
        jira_project_symbol = cfg_parser.get('pre-commit.hook.py', 'jira_project_symbol')
        excluded_files = set(
            excluded_file.strip()
            for excluded_file in
            re.split(r'[,\n]', cfg_parser.get('pre-commit.hook.py', 'excluded_files', fallback=''))
        )

    parser = ArgumentParser()
    parser.add_argument(
        '-a', '--all-files',
        help='Run on all files rather on git "Changes to be committed", the default.',
        action='store_true'
    )
    parser.add_argument(
        '-d', '--debug',
        action='store_true'
    )
    parser.add_argument(
        '-s', '--skip',
        help='Functions to skip',
        default=[],
        choices=step.collect
    )
    parser.add_argument(
        '-o', '--only',
        help='Only run these functions',
        default=[],
        choices=step.collect
    )
    args = parser.parse_args()
    args.jira_project_symbol = jira_project_symbol
    args.excluded_files = excluded_files
    assert not all((args.only, args.skip)), '--skip and --only are mutually exclusive.'
    return args


if __name__ == '__main__':
    valid_branch_name = (
        '(?P<branch_name>'
        '(?P<front_or_back>frontend|backend)'
        '/'
        '{jira_project_symbol}-(?P<task_no>\d+)'
        '/'
        '(?P<description>.*)'
        # during rebase branch name is called HEAD
        '|master'
        '|HEAD'
        '|wip_.*'
        ')'
    )
    fails = {}
    warnings = {}
    glob = parse_args()
    debug('argparse', glob)
    set_logging()
    sys.exit(hook())
